import threading # Multithreading
import trace
import sys

import bge # Bibliothèque Blender Game Engine (UPBGE)

###############################################################################
# twin_threading.py
# @title: Gestion des tâches (threads)
# @project: Blender-EduTech
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2020-2023 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

scene = bge.logic.getCurrentScene()
debug_thread = scene.objects['System']['debug_thread']

###############################################################################
# Méthode kill pour les tâches (threads)
###############################################################################

class thread_with_trace(threading.Thread):
    def __init__(self, *args, **keywords):
        threading.Thread.__init__(self, *args, **keywords)
        self.killed = False
     
    def start(self):
        self.__run_backup = self.run
        self.run = self.__run
        threading.Thread.start(self)

    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup
    
    def globaltrace(self, frame, event, arg):
        if event == 'call':
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, event, arg):
        if self.killed:
            if event == 'line':
                raise SystemExit()
        return self.localtrace

    def kill(self):
        self.killed = True

###############################################################################
# Start et stop des tâches (threads)
###############################################################################

def start(threads, type_txt, fct):
    threads.append(thread_with_trace(target = fct))
    threads[len(threads)-1].start()
    if (debug_thread):
        print ("Thread",type_txt, "#", len(threads)-1, "open.")

# Stop des threads
def stop(threads, type_txt):
    i=0
    zombie_flag=False
    for t in threads:
        if not t.is_alive():
            if (debug_thread):
                print ("Thread",type_txt, "#",i,"closed.")
        else:
            if debug_thread:
                print ("Thread",type_txt, "#",i,"still open ...")
            t.kill()
            t.join()
            if not t.is_alive():
                if (debug_thread):
                    print ("Thread",type_txt, "#",i,"killed.")
            else:
                if debug_thread:
                    print ("Thread",type_txt, "#",i,"zombie...")
                zombie_flag=True
        i +=1
    if zombie_flag==False:
        if debug_thread:
            print ("All threads",type_txt, "are closed.")
        if scene.objects['System']['thread_cmd']:
            scene.objects['System']['thread_cmd']=False
        return True
    else:
        if debug_thread:
            print ("There are zombies threads",type_txt, ".")
        return False
