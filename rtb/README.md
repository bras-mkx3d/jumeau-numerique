# Robotics Toolbox for Python (RTB)

Espace d'essais pour l'utilisation de Robotics Toolbox for Python

- [Dépôt](https://github.com/petercorke/robotics-toolbox-python)
- [Documentation](https://petercorke.github.io/robotics-toolbox-python/index.html)
- [Exemples](https://github.com/petercorke/robotics-toolbox-python/tree/master/roboticstoolbox/examples)

## Découverte et essais :

### Génération de trajet (MGI) : test avec le robot Puma

![capture d'écran](img/puma.png)

- Les trajets générés par le calcul analytique (ikine_a(T)) sont continus et donc exploitable pour la commande.
- Les trajets générés par le calcul Levemberg-Marquadt (ikine_LM(T)) ne sont pas continus et donc pas exploitables directement pour la commande.

### Défintion du bras MKX3D : Blender vs RTB (MGD)

![capture d'écran](img/mkx3d.png)

- L'assemblage sous Blender comporte des approximations d'orientation au niveau des axes des différents pivots, cela produit des erreurs de position et
d'orientation très importants (0,5 mm et 0,5 degré). La quantification de ces écarts va nous permettre de corriger finement le modèle 3D.

- Par ailleurs, actuelement, la manipulation du modèle 3D (Pan, Orbit, Zoom) génère aussi des erreurs de position et d'orientation, cela viens de la manière
utilisée pour changer la vue. C'est un point à fixer.

- Pour la commande du robot, c'est le solveur de RTB qui produira les coordonnées articulaires et opérationnelles.

### Evaluation des propositions de trajectoire (limites géométriques et cinématiques)

![capture d'écran](img/mkx3d-traj.png)

- La génération de trajectoire détermine aussi sa faisabilité : 
  - Est-ce que la position est atteignable ? Si oui avec quelle orientation ? 
  - Est-ce que le trajet proposé comporte des discontinuitées sur les coordonnées articulaires ? 
  - Est-ce que les limites de vitesse sont respectées ? 

## Cinématique directe et inverse :

- [API Robot Arms](https://petercorke.github.io/robotics-toolbox-python/arm.html)
- [Wiki Kinematics](https://github.com/petercorke/robotics-toolbox-python/wiki/Kinematics)

## Bibliothèques Python :

- [Spatial Math Toolbox for Python](https://github.com/bdaiinstitute/spatialmath-python) : Fonctions mathématiques de manipulation spatiale
- [Block diagram simulator for Python](https://github.com/petercorke/bdsim) : Editeur et solveur de schéma bloc
- [pycapacity](https://auctus-team.gitlabpages.inria.fr/people/antunskuric/pycapacity/index.html#) : Espace utilisateur pour robot
- [Swift](https://github.com/jhavl/swift) : Visualisation 3D intégrée dans Robotics Toolbox for Python

## Documentation :

 - [A Tutorial on Manipulator Differential Kinematics](https://github.com/jhavl/dkt) : Tutoriel pour la cinématique inverse - Jesse Haviland et Peter Corke, 2023, Queensland University of Technology (QUT)
 - [A simple and systematic approach to assigning Denavit-Hartenberg parameters](https://petercorke.com/robotics/a-simple-and-systematic-approach-to-assigning-denavit-hartenberg-parameters/) : Modélisation géométrique - Peter Corke, 2007
 - [A Novel Expeditious Optimisation Algorithm for Reactive Motion Control of Manipulators](https://jhavl.github.io/neo/)
 - [Juste Un Petit Cours](https://www.youtube.com/@justeunpetitcours2108/featured) : Chaîne vidéo sur la Robotique
 
