import roboticstoolbox as rtb
from spatialmath import SE3
import spatialmath.base as sm
import numpy as np
from roboticstoolbox.backends.PyPlot import PyPlot
import matplotlib.pyplot as plt
import time

env = PyPlot()

##
# Définition du robot Denavit-Hartenberg modifié
##
    
robot = rtb.DHRobot(
    [
        rtb.RevoluteMDH(alpha=0.0,      a=0.0,     offset=-np.pi/2, d=0.22075, qlim=[  -np.pi,     np.pi]),   # Segment 1
        rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=np.pi/2,  d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 2
        rtb.RevoluteMDH(alpha=0.0,      a=0.22152, offset=np.pi/2,  d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 3
        rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=0.0,      d=0.228,   qlim=[  -np.pi,     np.pi]),   # Segment 4
        rtb.RevoluteMDH(alpha=-np.pi/2, a=0.0,     offset=0.0,      d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 5
        rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=np.pi,    d=0.203,   qlim=[-2*np.pi,   2*np.pi]),   # Segment 6
    ], name="MKX3D")

# print(robot)
env.launch()
env.add(robot)
env.step()

##
# Trajectoires
##

# Classe trajectoire
class traj:
    def __init__(self, p1, p2, q1, q_result, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, error_cont, error_speed):
        self.p1 = p1
        self.p2 = p2
        self.q1 = q1
        self.q_result = q_result
        self.t = t
        self.speed = speed
        self.dist = dist
        self.nb_pt = nb_pt
        self.resol = resol
        self.qd_max = qd_max
        self.error_args = error_args
        self.error_limit = error_limit
        self.error_cont = error_cont
        self.error_cont = error_cont

# Modèle géométrique direct
def rtb_fkine(q, unit="rad"):
    if unit=="deg":
        T = robot.fkine(np.array(q)*(np.pi/180))
    else:
        T = robot.fkine(np.array(q))
    p_list=[]
    position=T.t
    for i in range (len(position)):
        p_list.append(round(position[i], 4))
    orientation=sm.tr2rpy(np.array(T), unit='deg')
    for i in range (len(orientation)):
        p_list.append(round(orientation[i], 4))
    return p_list, T

# Définir la position courante du robot avec q en deg 
def rtb_setq(q):
    robot.q = np.array(q)*(np.pi/180)

# Affichage formaté
def rtb_print_q(text_begin, q, text_end):
    print (text_begin+" : ["+'{:>7.2f}'.format(q[0])+", "+
           '{:>7.2f}'.format(q[1])+", "+
           '{:>7.2f}'.format(q[2])+", "+
           '{:>7.2f}'.format(q[3])+", "+
           '{:>7.2f}'.format(q[4])+", "+
           '{:>7.2f}'.format(q[5])+"]"+text_end)

def rtb_print_p(text_begin, p, text_end):
    print (text_begin+" : ["+'{:>6.4f}'.format(p[0])+", "+
           '{:>6.4f}'.format(p[1])+", "+
           '{:>6.4f}'.format(p[2])+", "+
           '{:>7.2f}'.format(p[3])+", "+
           '{:>7.2f}'.format(p[4])+", "+
           '{:>7.2f}'.format(p[5])+"]"+text_end)

# Affichage des courbes cinématiques (position/angle, vitesse, accélération)
def rtb_kplot(trj):

    # Axe du temps
    dt = trj.t/trj.nb_pt
    t = np.arange(0, len(trj.q_result)*trj.t/trj.nb_pt, trj.t/trj.nb_pt)
    fig, axs = plt.subplots(3, 1, sharex=True)

    # Données angles
    q_plot = [[],[],[],[],[],[]]
    for i in range (len(trj.q_result)):
        for j in range (6):
            q_plot[j].append(trj.q_result[i][j]*(180/np.pi))

    # Données vitesse angulaire
    qd_plot = [[],[],[],[],[],[]]
    for i in range (6):
        for j in range (len(q_plot[i])):
            if j != 0 :
                qd_plot[i].append((q_plot[i][j]-q_plot[i][j-1])/dt)
            else:
                qd_plot[i].append(0)

    # Données accélération angulaire
    qdd_plot = [[],[],[],[],[],[]]
    for i in range (6):
        for j in range (len(qd_plot[i])):
            if j != 0 :
                qdd_plot[i].append((qd_plot[i][j]-qd_plot[i][j-1])/dt)
            else:
                qdd_plot[i].append(0)


    # Coordonnées angulaires
    # rtb.RevoluteMDH(alpha=0.0,      a=0.0,     offset=-np.pi/2, d=0.22075, qlim=[  -np.pi,     np.pi]),   # Segment 1
    # rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=np.pi/2,  d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 2
    # rtb.RevoluteMDH(alpha=np.pi/2,  a=0.22152, offset=np.pi/2,  d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 3
    # rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=0.0,      d=0.228,   qlim=[  -np.pi,     np.pi]),   # Segment 4
    # rtb.RevoluteMDH(alpha=-np.pi/2, a=0.0,     offset=0.0,      d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 5
    # rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=np.pi,    d=0.203,   qlim=[-2*np.pi,   2*np.pi]),   # Segment 6

    axs[0].plot(t, q_plot[0], label="q1", linewidth=1.25, color='tab:red')
    axs[0].axhline(y=182, linewidth=1, linestyle='dotted', color='tab:red') # 180
    axs[0].axhline(y=-182, linewidth=1, linestyle='dotted', color='tab:red') # -180

    axs[0].plot(t, q_plot[1], label="q2", linewidth=1.25, color='tab:blue')
    axs[0].axhline(y=89, linewidth=1, linestyle='dotted', color='tab:blue') # 90
    axs[0].axhline(y=-89, linewidth=1, linestyle='dotted', color='tab:blue') # - 90

    axs[0].plot(t, q_plot[2], label="q3", linewidth=1.25, color='tab:purple')
    axs[0].axhline(y=91, linewidth=1, linestyle='dotted', color='tab:purple') # 90
    axs[0].axhline(y=-91, linewidth=1, linestyle='dotted', color='tab:purple') # 90

    axs[0].plot(t, q_plot[3], label="q4", linewidth=1.25, color='tab:grey')
    axs[0].axhline(y=180, linewidth=1, linestyle='dotted', color='tab:grey') # 180
    axs[0].axhline(y=-180, linewidth=1, linestyle='dotted', color='tab:grey') # 180

    axs[0].plot(t, q_plot[4], label="q5", linewidth=1.25, color='tab:pink')
    axs[0].axhline(y=88, linewidth=1, linestyle='dotted', color='tab:pink') # 90
    axs[0].axhline(y=-88, linewidth=1, linestyle='dotted', color='tab:pink') # 90
    
    axs[0].plot(t, q_plot[5], label="q6", linewidth=1.25, color='tab:olive')
    axs[0].axhline(y=360, linewidth=1, linestyle='dotted', color='tab:olive')
    axs[0].axhline(y=-360, linewidth=1, linestyle='dotted', color='tab:olive')

    axs[0].set_xlim(0, trj.t)
    axs[0].set_xlabel("t (s)")
    axs[0].set_ylabel("q1 à q6 (deg)")
    axs[0].grid(True)
    axs[0].legend()

    # Vitesses angulaires
    axs[1].plot(t, qd_plot[0], label="q'1", linewidth=1.25, color='tab:red')
    axs[1].plot(t, qd_plot[1], label="q'2", linewidth=1.25, color='tab:blue')
    axs[1].plot(t, qd_plot[2], label="q'3", linewidth=1.25, color='tab:purple')
    axs[1].plot(t, qd_plot[3], label="q'4", linewidth=1.25, color='tab:grey')
    axs[1].plot(t, qd_plot[4], label="q'5", linewidth=1.25, color='tab:pink')
    axs[1].plot(t, qd_plot[5], label="q'6", linewidth=1.25, color='tab:olive')

    axs[1].axhline(y=qd_max*(180/np.pi), linewidth=1.5, linestyle='dotted', color='tab:orange', label="q'max",)
    axs[1].axhline(y=-qd_max*(180/np.pi), linewidth=1.5, linestyle='dotted', color='tab:orange')

    axs[1].set_xlim(0, trj.t)
    axs[1].set_xlabel("t (s)")
    axs[1].set_ylabel("q'1 à q'6 (deg/s)")
    axs[1].grid(True)
    axs[1].legend()

    # Accélérations angulaires
    axs[2].plot(t, qdd_plot[0], label="q''1", linewidth=1.25, color='tab:red')
    axs[2].plot(t, qdd_plot[1], label="q''2", linewidth=1.25, color='tab:blue')
    axs[2].plot(t, qdd_plot[2], label="q''3", linewidth=1.25, color='tab:purple')
    axs[2].plot(t, qdd_plot[3], label="q''4", linewidth=1.25, color='tab:grey')
    axs[2].plot(t, qdd_plot[4], label="q''5", linewidth=1.25, color='tab:pink')
    axs[2].plot(t, qdd_plot[5], label="q''6", linewidth=1.25, color='tab:olive')
    axs[2].set_xlim(0, trj.t)
    axs[2].set_xlabel("t (s)")
    axs[2].set_ylabel("q''1 à q''6 (deg/s2)")
    axs[2].grid(True)
    axs[2].legend()

    plt.show()

# Affichage du robot (position fixe ou animation)
def rtb_plot(q, unit="rad", dt=0.001):
    if isinstance(q[0], np.ndarray) == False:
        if unit=="deg":
            robot.q=np.array(q)*(np.pi/180)
        else:
            robot.q=q
        env.step()
    else:
        for i_q in q:
            # time.sleep(dt)
            if unit=="deg":
                robot.q=np.array(i_q)*(np.pi/180)
            else:
                robot.q=i_q
            env.step()

# Modèle géométrique indirect avec le contrôle sur la continuité de la trajectoire
def rtb_ikine(T, q_prev, q_step_max, i_pt, dt):
    cont_iterations=0
    cont_iterations_lim=50
    # cont_iterations_lim=500
    cont_step_max=np.pi*dt # Pas max entre deux calculs (pi rad/s) -> assure la continuité
    cont_success=False
     
    while cont_success==False:
        
        # Pas de solution continue
        cont_iterations +=1
        if cont_iterations > cont_iterations_lim:
            break

        # Modèle géométrique indirect avec le solveur Levemberg-Marquadt
        sol = robot.ikine_LM(T)
        
        # Pas de solution de cinématique inverse
        if sol.success==False:
            break
        
        # Controle de la continuité
        cont_success=True
        for i in range(6):
            if abs(sol.q[i] - q_prev[i]) >= cont_step_max:
                print ("\033[0;45m"+
                       'pt : {:>2}'.format(i_pt)+" ; "+
                       'iteration : {:>2}'.format(cont_iterations)+" ; "+
                       'moteur : '+str(i)+" ; "+
                       'q step : {:.2f}'.format(abs(sol.q[i]-q_prev[i]))+" ; "+
                       'q_step_max cont : {:.4f}'.format(cont_step_max)+" ; "+
                       'q_step_max speed : {:.4f}'.format(q_step_max)+"\033[0m")
                # print ("\033[0;45m pt :",i_pt,"; iteration :",cont_iterations,"; moteur :",i,"; q step :",format(abs(sol.q[i]-q_prev[i]),'.2f'),"; q_step_max cont :",format(cont_step_max,'.2f'),"; q_step_max speed :",format(q_step_max,'.2f'), "\033[0m")
                cont_success=False
                break

        # FIXME : Vitesse moteur trop grande
        # cont_success=True
        # for i in range(6):
        #     if abs(sol.q[i] - q_prev[i]) > q_step_max:
        #         print ("\033[0;45m", i_pt, cont_iterations, i, abs(sol.q[i] - q_prev[i]), q_step_max, "\033[0m")
        #         cont_success=False
        #         break

    return (sol, cont_success, cont_iterations)


##
# Trajectoire entre p1 et p2 rigide
##

def rtb_traj(p1, p2, q1, t=None, speed=None, nb_pt=None, resol=None, qd_max=None, **kwargs):

    error_args = False # Erreur sur les arguments
    error_limit = False # Erreur sur les limites des joints
    error_cont = False # Erreur sur la continuité de la trajectoire
    error_speed = False # Erreur sur le dépassement de vitesse
    q_result_list = []

    # Contrôle des arguments
    if speed == None and t == None:
        print ("Trajet erreur : manque la durée du trajet 't' (s) ou la vitesse 'speed' (m/s)")
        error_args = True
    if speed != None and t != None:
        print ("Trajet erreur : spécifier soit la durée du trajet 't' (s) soit la vitesse 'speed' (m/s)")
        error_args = True
    if nb_pt == None and resol == None:
        print ("Trajet erreur : manque le nombre de points 'nb_pt' (trajectoire polynomiale) ou la résolution 'resol' (pt/m)")
        error_args = True
    if nb_pt != None and resol != None:
        print ("Trajet erreur : spécifier soit le nombre de points 'nb_pt' (trajectoire polynomiale) soit la résolution 'resol' (pt/m)")
        error_args = True
    if qd_max == None:
        print ("Trajet erreur : spécifier la vitesse max des moteurs : 'qd_max' (rad/s)")
        error_args = True
    if error_args:
        q_result = np.array(q_result_list)
        return [error_args, error_limit, error_cont, error_speed], q_result

    # Vitesse et résolution
    dist = np.sqrt((p2[0]-p1[0])**2+(p2[1]-p1[1])**2+(p2[2]-p1[2])**2) # Distance
    angle = np.sqrt((p2[3]-p1[3])**2+(p2[4]-p1[4])**2+(p2[5]-p1[5])**2) # Angle à parcourir (transposition angle -> distance)
    if t==None:
        t = dist/speed
    if speed==None:
        speed = dist/t
    if nb_pt==None:
        nb_pt = round(dist*resol)
    if resol==None:
        resol = nb_pt/dist
    dt=t/nb_pt
    q_step_max = (qd_max*t)/nb_pt # Pas maxi des moteurs en rad/point
    
    # Go !
    print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
           " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
           "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
    env.ax.scatter(p1[0], p1[1], p1[2], c='green') # Point de départ
    env.ax.scatter(p2[0], p2[1], p2[2], c='orange') # Point d'arrivée (point cible)
    q_prev = np.array(q1)*(np.pi/180)
    for i_pt in range (nb_pt):
        
        T = SE3(p1[0]+(((p2[0]-p1[0])/nb_pt)*(i_pt+1)),
                p1[1]+(((p2[1]-p1[1])/nb_pt)*(i_pt+1)),
                p1[2]+(((p2[2]-p1[2])/nb_pt)*(i_pt+1)))*SE3.RPY(p1[3]+(((p2[3]-p1[3])/nb_pt)*(i_pt+1)),
                                                                p1[4]+(((p2[4]-p1[4])/nb_pt)*(i_pt+1)),
                                                                p1[5]+(((p2[5]-p1[5])/nb_pt)*(i_pt+1)), unit="deg")
        
        sol, cont_success, cont_iterations = rtb_ikine(T, q_prev, q_step_max, i_pt, dt)

        # Limites des joints (portée et angulaire) -> Trajectoire pas aboutie
        if sol.success==False:
            error_limit = True
            print ("\033[0;41mTrajet erreur : point", i_pt,": pas de solution géométrique", "\033[0m")
            p_list, T = rtb_fkine(q_prev)

            print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
                   " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
                   "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
            rtb_print_p("\033[0;41mPosition de départ p1 ", p1, "\033[0m")
            rtb_print_p("\033[0;41mPosition cible     p2 ", p2, "\033[0m")
            rtb_print_p("\033[0;41mPosition d'arrivée p  ", p_list, "\033[0m\n")
            rtb_print_q("\033[0;41mPosition de départ q1 ", q1, "\033[0m")
            rtb_print_q("\033[0;41mPosition d'arrivée q  ", q_prev*(180/np.pi), "\033[0m")

            if dist != 0: # Progression en position en %
                dist_stop = np.sqrt((p2[0]-p_list[0])**2+(p2[1]-p_list[1])**2+(p2[2]-p_list[2])**2)
                print ("\033[0;41mProgression distance :", format(((dist-dist_stop)/dist)*100, '.2f'), "% \033[0m")
            if angle != 0: # Progression angulaire en %
                angle_stop = np.sqrt((p2[3]-p_list[3])**2+(p2[4]-p_list[4])**2+(p2[5]-p_list[5])**2)
                print ("\033[0;41mProgression angulaire :", format(((angle-angle_stop)/angle)*100, '.2f'), "% \033[0m")
            q_result = np.array(q_result_list)
            return traj(p1, p2, q1, q_result, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, error_cont, error_speed)

        # Continuité de la trajectoire -> Trajectoire pas aboutie
        if cont_success== False:
            error_cont = True
            print ("\033[0;45mTrajet erreur : point", i_pt, ": pas de solution continue", "\033[0m")
            p_list, T = rtb_fkine(q_prev)

            print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
                   " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
                   "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
            rtb_print_p("\033[0;45mPosition de départ p1 ", p1, "\033[0m")
            rtb_print_p("\033[0;45mPosition cible     p2 ", p2, "\033[0m")
            rtb_print_p("\033[0;45mPosition d'arrivée p  ", p_list, "\033[0m\n")
            rtb_print_q("\033[0;45mPosition de départ q1 ", q1, "\033[0m")
            rtb_print_q("\033[0;45mPosition d'arrivée q  ", q_prev*(180/np.pi), "\033[0m")
            
            if dist != 0: # Progression en position en %
                dist_stop = np.sqrt((p2[0]-p_list[0])**2+(p2[1]-p_list[1])**2+(p2[2]-p_list[2])**2)
                print ("\033[0;45mProgression distance :", format(((dist-dist_stop)/dist)*100, '.2f'), "% \033[0m")
            if angle != 0: # Progression angulaire en %
                angle_stop = np.sqrt((p2[3]-p_list[3])**2+(p2[4]-p_list[4])**2+(p2[5]-p_list[5])**2)
                print ("\033[0;45mProgression angulaire :", format(((angle-angle_stop)/angle)*100, '.2f'), "% \033[0m")
            q_result = np.array(q_result_list)
            return traj(p1, p2, q1, q_result, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, error_cont, error_speed)

        # Vitesse moteur trop grande -> Trajectoire pas aboutie
        # FIXME 
        # if cont_success== False:
        #     error_speed = True
        #     print ("\033[0;45m", i_pt, "Vitesse moteur trop importante -> iterations=", cont_iterations, "\033[0m")
        #     q_result = np.array(q_result_list)
        #     return [error_args, error_limit, error_cont, error_speed], q_result

        # Ok
        # print (i_pt, "Trajet : solution continue -> iterations=", cont_iterations)
        # print (i_pt, sol)
        q_result_list.append(sol.q)
        q_prev=sol.q

    # Résultats
    print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
           " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
           "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
    q_result = np.array(q_result_list)
    p_list, T = rtb_fkine(q_result[len(q_result)-1])
    print (sol)
    print (q_result[len(q_result)-1])
    rtb_print_p("\033[0;42mPosition de départ p1 ", p1, "\033[0m")
    rtb_print_p("\033[0;42mPosition cible     p2 ", p2, "\033[0m")
    rtb_print_p("\033[0;42mPosition d'arrivée p  ", p_list, "\033[0m\n")
    rtb_print_q("\033[0;42mPosition de départ q1 ", q1, "\033[0m")
    rtb_print_q("\033[0;42mPosition d'arrivée q  ", q_result[len(q_result)-1]*(180/np.pi), "\033[0m")
    return traj(p1, p2, q1, q_result, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, error_cont, error_speed)

##
# Trajectoire entre p1 et p2 souple
# Variation des coordonnées articuliares
##

def rtb_traj_soft(p1, p2, q1, t=None, speed=None, nb_pt=None, resol=None, qd_max=None, **kwargs):

    error_args = False # Erreur sur les arguments
    error_limit = False # Erreur sur les limites des joints
    error_speed = False # Erreur sur le dépassement de vitesse
    q_result_list = []

    # Contrôle des arguments
    if speed == None and t == None:
        print ("Trajet erreur : manque la durée du trajet 't' (s) ou la vitesse 'speed' (m/s)")
        error_args = True
    if speed != None and t != None:
        print ("Trajet erreur : spécifier soit la durée du trajet 't' (s) soit la vitesse 'speed' (m/s)")
        error_args = True
    if nb_pt == None and resol == None:
        print ("Trajet erreur : manque le nombre de points 'nb_pt' (trajectoire polynomiale) ou la résolution 'resol' (pt/m)")
        error_args = True
    if nb_pt != None and resol != None:
        print ("Trajet erreur : spécifier soit le nombre de points 'nb_pt' (trajectoire polynomiale) soit la résolution 'resol' (pt/m)")
        error_args = True
    if qd_max == None:
        print ("Trajet erreur : spécifier la vitesse max des moteurs : 'qd_max' (rad/s)")
        error_args = True
    if error_args:
        q_result = np.array(q_result_list)
        return [error_args, error_limit, error_speed], q_result

    # Vitesse et résolution
    dist = np.sqrt((p2[0]-p1[0])**2+(p2[1]-p1[1])**2+(p2[2]-p1[2])**2) # Distance
    angle = np.sqrt((p2[3]-p1[3])**2+(p2[4]-p1[4])**2+(p2[5]-p1[5])**2) # Angle à parcourir (transposition angle -> distance)
    if t==None:
        t = dist/speed
    if speed==None:
        speed = dist/t
    if nb_pt==None:
        nb_pt = round(dist*resol)
    if resol==None:
        resol = nb_pt/dist
    dt=t/nb_pt
    q_step_max = (qd_max*t)/nb_pt # Pas maxi des moteurs en rad/point

    # Go !
    print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
           " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
           "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
    env.ax.scatter(p1[0], p1[1], p1[2], c='green') # Point de départ
    env.ax.scatter(p2[0], p2[1], p2[2], c='orange') # Point d'arrivée (point cible)
    T1 = SE3(p1[0],p1[1],p1[2])*SE3.RPY(p1[3],p1[4],p1[5], unit="deg")
    T2 = SE3(p2[0],p2[1],p2[2])*SE3.RPY(p2[3],p2[4],p2[5], unit="deg")
    
    sol1=robot.ikine_LM(T1)
    q1= sol1.q
    sol2=robot.ikine_LM(T2)
    q2 = sol2.q
    tg=rtb.jtraj(q1, q2, nb_pt)

    # Départ impossible
    if sol1.success==False:
        error_limit = True
        print ("\033[0;41mTrajet erreur : départ impossible", "\033[0m")
        return traj(p1, p2, q1, tg.q, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, False, error_speed)

    # Controle de la vitesse angulaire des moteur (vitesse maxi)
    # FIXME 
    # for 
    #     cont_success=True
    #     for i in range(6):
    #         if abs(sol.q[i] - q_prev[i]) > q_step_max:
    #             print ("\033[0;45m", i_pt, cont_iterations, i, abs(sol.q[i] - q_prev[i]), q_step_max, "\033[0m")
    #             cont_success=False
    #             break

    # Limites des joints (portée et angulaire) -> Trajectoire pas aboutie
    if sol2.success==False:
        error_limit = True
        print ("\033[0;41mTrajet erreur : pas de solution géométrique", "\033[0m")
        p_list, T = rtb_fkine(tg.q[len(tg.q)-1])

        print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
               " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
               "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
        rtb_print_p("\033[0;41mPosition de départ p1 ", p1, "\033[0m")
        rtb_print_p("\033[0;41mPosition cible     p2 ", p2, "\033[0m")
        rtb_print_p("\033[0;41mPosition d'arrivée p  ", p_list, "\033[0m\n")
        rtb_print_q("\033[0;41mPosition de départ q1 ", q1, "\033[0m")
        rtb_print_q("\033[0;41mPosition d'arrivée q  ", tg.q[len(tg.q)-1]*(180/np.pi), "\033[0m")
       
        if dist != 0: # Progression en position en %
            dist_stop = np.sqrt((p2[0]-p_list[0])**2+(p2[1]-p_list[1])**2+(p2[2]-p_list[2])**2)
            print ("\033[0;41mProgression distance :", format(((dist-dist_stop)/dist)*100, '.2f'), "% \033[0m")
        if angle != 0: # Progression angulaire en %
            angle_stop = np.sqrt((p2[3]-p_list[3])**2+(p2[4]-p_list[4])**2+(p2[5]-p_list[5])**2)
            print ("\033[0;41mProgression angulaire :", format(((angle-angle_stop)/angle)*100, '.2f'), "% \033[0m")
        return traj(p1, p2, q1, tg.q, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, False, error_speed)

    # Résultats
    print ("Trajet : d :",format(dist,'.4f'),"m ; t :",format(t,'.3f'),"s ; v :",format(speed,'.2f'),
           " m/s ; resolution :",format(resol,'.0f'), "pt/m ; points :",nb_pt,
           "; dt :",format(dt,'.3f'),"s ; pas moteur max :",format(q_step_max,'.4f'),"rad/point\n")
    p_list, T = rtb_fkine(tg.q[len(tg.q)-1])
    rtb_print_p("\033[0;42mPosition de départ p1 ", p1, "\033[0m")
    rtb_print_p("\033[0;42mPosition cible     p2 ", p2, "\033[0m")
    rtb_print_p("\033[0;42mPosition d'arrivée p  ", p_list, "\033[0m\n")
    rtb_print_q("\033[0;42mPosition de départ q1 ", q1, "\033[0m")
    rtb_print_q("\033[0;42mPosition d'arrivée q  ", tg.q[len(tg.q)-1]*(180/np.pi), "\033[0m")
    return traj(p1, p2, q1, tg.q, t, speed, dist, nb_pt, resol, qd_max, error_args, error_limit, False, error_speed)


##
# Trajectoire entre p1 et p2
##

# t=20 # Durée du trajet en s
# speed=0.25 # Vitesse 1m/4s
speed=0.1 # Vitesse 1m/10s
resol=1000 # Resolution 1000pt/m
nb_pt = 10 # Nombre de points pour la trajectoire polynomiale
qd_max = 1.57 # Vitesse maxi des moteurs 1,57 rad/s

q1 = [10, 70, 20, 30, 40, 20] # Position de test comme point de départ P1
p1 = [-0.3185, 0.4395, 0.4679, -58.6753, 21.685, 86.3985]

# p2 = [ 0,      0.4395, 0.4679, -58.6753, 21.685, 86.3985]
# p2 = [-0.3, 0.4395, 0.4679, -58.6753, 21.685, 86.3985]
# p2 = [-0.3185, 0.1, 0.1, -58.6753, 21.685, 86.3985]
p2 = [-0.3185+0.5, 0.4395, 0.4679, -58.6753, 21.685, 86.3985]
# p2 = [-1.3185, 0.1, 0.1, -58.6753, -21.685, -86.3985]

T2 = SE3(p2[0],p2[1],p2[2])*SE3.RPY(p2[3],p2[4],p2[5], unit="deg")
sol2=robot.ikine_LM(T2)
q2=np.array(sol2.q)*(180/np.pi)
print ("p2", p2)
print (sol2)
print ("q2", q2)




# Trajectoire entre p1 et p2 avec orientation contrainte
# print ("Trajectoire entre p1 et p2 avec orientation contrainte")
# trj = rtb_traj(p1, p2, q1, speed=speed, nb_pt = 10, qd_max=qd_max)
#rtb_plot(trj.q_result)
# rtb_kplot(trj)

# Trajectoire entre p1 et p2 avec orientation non contrainte (souple)
# print ("Trajectoire entre p1 et p2 avec orientation non contrainte (souple)")
# trj = rtb_traj_soft(p1, p2, q1, speed=speed, nb_pt = 10, qd_max=qd_max)
# trj = rtb_traj_soft(p1, p2, q1, t=10, resol=resol, qd_max=qd_max)
# rtb_plot(trj.q_result)
# rtb_kplot(trj)
