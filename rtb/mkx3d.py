import roboticstoolbox as rtb
from spatialmath import SE3
import spatialmath.base as sm
import numpy as np

##
# Définition du robot Denavit-Hartenberg modifié
##

robot = rtb.DHRobot(
    [
        rtb.RevoluteMDH(alpha=0.0,      a=0.0,     offset=-np.pi/2, d=0.22075, qlim=[  -np.pi,     np.pi]),   # Segment 1
        rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=np.pi/2,  d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 2
        rtb.RevoluteMDH(alpha=0.0,      a=0.22152, offset=np.pi/2,  d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 3
        rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=0.0,      d=0.228,   qlim=[  -np.pi,     np.pi]),   # Segment 4
        rtb.RevoluteMDH(alpha=-np.pi/2, a=0.0,     offset=0.0,      d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 5
        rtb.RevoluteMDH(alpha=np.pi/2,  a=0.0,     offset=np.pi,    d=0.203,   qlim=[-2*np.pi,   2*np.pi]),   # Segment 6
    ], name="MKX3D")

print(robot)

##
# Test RTB (Bobotics ToolBox) vs Blender
##

# Poses

q0=np.array([0,0,0,0,0,0]) # Tout à 0
qt = np.array([10, 70, 20, 30, 40, 20]) # Position de test

# Visualisation

q = qt
print ("Coordonnées articulaires (q1 à q6) :", q)

robot.q = q*(np.pi/180)
robot.plot(robot.q) # Visualisation

# Modèle de Géométrie Direct (MGD)

T = robot.fkine(robot.q) # Pose
print ("Pose :\n")
print (T)
print ("Position (Xp,Xy,Xz) :", T.t)
print ("Orientation RPY (RXp, RYp, RZp) :", sm.tr2rpy(np.array(T), unit='deg'))



# Définition du robot avec des repères intermédiaires -> marche pas ?
# robot = rtb.DHRobot(
#     [
#         # Segment 1
#         rtb.RevoluteMDH(alpha=0.0,        a=0.0,                     d=0.068015),                  # Rm    -> R1

#         # Segment 2
#         rtb.RevoluteMDH(alpha=0.0,        a=0.0,      offset=np.pi/2,  d=0.152735, qlim=[0.0, 0.0]), # R1   -> R2'
#         rtb.RevoluteMDH(alpha=-np.pi/2,   a=0.0,                       d=0.0),                       # R2'  -> R2

#         # Segment 3
#         rtb.RevoluteMDH(alpha=np.pi/2,    a=0.22152,                   d=0.0),                       # R2   -> R3
        
#         # Segment 4
#         rtb.RevoluteMDH(alpha=0,          a=0.1329,   offset=-np.pi/2, d=0.0,      qlim=[0.0, 0.0]), # R3   -> R4'
#         rtb.RevoluteMDH(alpha=-np.pi/2,   a=0,        offset=np.pi/2,  d=0.0,      qlim=[0.0, 0.0]), # R4'  -> R4''
#         rtb.RevoluteMDH(alpha=0,          a=0,                         d=0.0),                       # R4'' -> R4

#         # Segment 5
#         rtb.RevoluteMDH(alpha=0,          a=0.0949,   offset=-np.pi/2, d=0.0,      qlim=[0.0, 0.0]), # R4   -> R5'
#         rtb.RevoluteMDH(alpha=np.pi/2,    a=0,        offset=np.pi/2,  d=0.0),                       # R5'  -> R5

#         # Segment 6
#         rtb.RevoluteMDH(alpha=0,          a=0.07491,  offset=-np.pi/2, d=0.0,      qlim=[0.0, 0.0]), # R5   -> R6'
#         rtb.RevoluteMDH(alpha=-np.pi/2,   a=0,                         d=0.0),                       # R6'  -> R6
#         rtb.RevoluteMDH(alpha=0,          a=0.128009,                  d=0.0,      qlim=[0.0, 0.0])  # R6   -> Rp

#     ], name="MKX3D")


# Définition du robot basée sur le code de P. DALMEIDA & P. de POUMAYRAC
# J'ai de petit changements par rapport au modèle Blender

# Géométrie
# r1=0.22075
# d3=0.22152
# r4=0.228
# r6=0.203

# Positions limites
# Xmin=-.5                ; Xmax=.5;
# Ymin=-.5                ; Ymax=.5;
# Zmin=0                  ; Zmax=.5;
# Rxmin=-360              ; Rxmax=360;
# Rymin=-360              ; Rymax=360;
# Rzmin=-360              ; Rzmax=360;

# robot = rtb.DHRobot(
#     [
#         rtb.RevoluteMDH(alpha=0.0,     a=0.0,     offset=-0.0,    d=0.22075, qlim=[  -np.pi,     np.pi]),   # Segment 1
#         rtb.RevoluteMDH(alpha=np.pi/2, a=0.0,     offset=np.pi/2, d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 2
#         rtb.RevoluteMDH(alpha=0.0,     a=0.22152, offset=np.pi/2, d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 3
#         rtb.RevoluteMDH(alpha=np.pi/2, a=0.0,     offset=0.0,     d=0.228,   qlim=[  -np.pi,     np.pi]),   # Segment 4
#         rtb.RevoluteMDH(alpha=np.pi/2, a=0.0,     offset=np.pi,   d=0.0,     qlim=[  -np.pi/2,   np.pi/2]), # Segment 5
#         rtb.RevoluteMDH(alpha=np.pi/2, a=0.0,     offset=0.0,     d=0.203,   qlim=[-2*np.pi,   2*np.pi]),   # Segment 6
#     ], name="MKX3D")
