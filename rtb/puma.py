import roboticstoolbox as rtb
from spatialmath import SE3
import numpy as np

# Définition du robot
robot = rtb.models.DH.Puma560()
#robot = rtb.models.DH.Panda()
print(robot)
robot.q = robot.qz

# Evaluation de la cinématique inverse
# Comparaison entre trajectoire de base et celle recalculée
q_initial_list=[]
q_a_result_list=[]
q_lm_result_list=[]
for i in range (100):
    q_i=[0,i*(np.pi/180),0,0,0,0]
    q_initial_list.append(q_i)
    T= robot.fkine(q_i)
    sol_a = robot.ikine_a(T) # Analytique
    sol_lm = robot.ikine_LM(T) # Levemberg-Marquadt
    q_a_result_list.append(sol_a.q)
    q_lm_result_list.append(sol_lm.q)

q_initial = np.array(q_initial_list)
q_a_result = np.array(q_a_result_list)
q_lm_result = np.array(q_lm_result_list)
# robot.plot(q_initial, backend='pyplot', dt=0.050)
# robot.plot(q_a_result, backend='pyplot', dt=0.050)
# robot.plot(q_lm_result, backend='pyplot', dt=0.050)

# Trajectoire entre q1 et q2
q1=[0,1*(np.pi/180),0,0,0,0]
q2=[0,100*(np.pi/180),0,0,0,0]
T1 = robot.fkine(q1)
T2 = robot.fkine(q2)
t=100
traj = robot.jtraj(T1, T2, t)
# print (traj.q)
# traj.plot()
# robot.plot(traj.q, backend='pyplot', dt=0.050)

# Trajectoire entre q1 et q2 découpée
q_traj_result_list=[]
for i in range (100):
    q1=[0,i*(np.pi/180),0,0,0,0]
    q2=[0,(i+1)*(np.pi/180),0,0,0,0]
    T1 = robot.fkine(q1)
    T2 = robot.fkine(q2)
    t=1
    traj = robot.jtraj(T1, T2, t)
    q_traj_result_list.append(traj.q)
q_traj_result = np.array(q_traj_result_list)
# robot.plot(q_traj_result, backend='pyplot', dt=0.050)


# Trajectoire entre T1 et T2
q_a_result_list=[]
q_lm_result_list=[]
T = SE3(0.7, 0.2, 1) * SE3.RPY(0, 0, 0, unit="deg") 
robot.q = robot.ikine_a(T).q
robot.plot(robot.q)
for i in range (100):
    T = SE3(0.7-(0.01*i), 0.2, 1) * SE3.RPY(0, 0, 0, unit="deg") 
    sol_a = robot.ikine_a(T) # Analytique
    sol_lm = robot.ikine_LM(T) # Levemberg-Marquadt
    if sol_a.success:
        q_a_result_list.append(sol_a.q)
    else:
        print (sol_a)
    if sol_lm.success:
        q_lm_result_list.append(sol_lm.q)
    else:
        print (sol_lm)
q_a_result = np.array(q_a_result_list)
q_lm_result = np.array(q_lm_result_list)
robot.plot(q_a_result, backend='pyplot', dt=0.050)
robot.plot(q_lm_result, backend='pyplot', dt=0.050)



##
# Poses simples
##

# T0 = robot.fkine(robot.qr) # q à 0
# print ("T0\n", T0)

# # Pose avec un angle 45° en x et en y
# T1 = SE3(0.7, 0.2, 1) * SE3.RPY(45, 45, 0, unit="deg") 
# print ("T1\n", T1)

# # Pose avec 45° sur q1, q2, q3 et q4
# T2 = robot.fkine([45, 45, 45, 45, 0, 0])
# print ("T2\n", T2)

# # Pose avec deux vecteur rotations // à Y et // à Z
# # T3 = SE3(0.7, 0.2, 1) * SE3.OA([0, 1, 0], [0, 0, 1])

# # Cinématique inverse (recherche des q)

# sol = robot.ikine_LM(T1)
# print (sol)
# robot.plot(sol.q)


