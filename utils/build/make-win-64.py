#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

###############################################################################
# make-win-64.py
# @title: Packaging script (called by pixi) for Windows (win-64)
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2025 Philippe Roy
# @license: GNU GPL
###############################################################################

# Executable
exe_file     = 'bras_mkx3d'

# Sources
blend_file   = 'bras_mkx3d-7.blend'
directories  = ['asset', 'wx', 'object', 'exemples', 'img']
files        = ['bras.py', 'bras_lib.py',
                'twin.py', 'twin_traj.py', 'twin_daq.py',
                'twin_ik_blender.py', 'twin_ik_rtb.py', 'twin_plot.py', 'twin_plot_wx.py', 'twin_plot_qt.py',
                'twin_pylint.py', 'twin_serial.py', 'twin_threading.py', 'twin_time.py',
                'bras_mkx3d.sh',
                'README.md', 'LICENSE', 'AUTHORS.md']
config_files = [['twin_config-bak.xml', 'twin_config.xml']]
script_files = [['bras_cmd-bak.py'    , 'bras_cmd.py'],
                ['bras_startcmd.py'   , 'bras_startcmd.py']]

# Command with logging
def command(commandline):
    print(commandline + "... ")
    os.system(r'%s' % commandline)

if __name__ == "__main__":

    # Copy UPBGE
    command(r"copy C:\Users\phroy\Desktop\blender-edutech\build\upbge-0.36.1-windows-x86_64.7z")
    command(r"7z.exe x -bso0 upbge-0.36.1-windows-x86_64.7z")
    
    # Install by pip (pixi repo failed)
    command(f"copy {os.path.join('utils','build','get-pip.py')} {os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','bin')}")
    command(f"{os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','bin','python.exe')} \
    {os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','bin','get-pip.py --no-warn-script-location')}")
    command(f"{os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','Scripts','pip.exe')} install scipy==1.11.1 --no-warn-script-location")
    command(f"{os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','Scripts','pip.exe')} install roboticstoolbox-python==1.1.0 --no-warn-script-location")
    command(f"{os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','Scripts','pip.exe')} install wxpython --no-warn-script-location")

    # Remove rtb_data pip package (reducing space)
    command(f"rmdir /S /Q {os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','lib','site-packages','rtb_data-1.0.1.dist-info')}")
    command(f"rmdir /S /Q {os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','lib','site-packages','rtbdata')}")

    # Export as game runtime
    command("mkdir build")
    print ("write 'export_game_runtime.py' script ...")
    with open("export_game_runtime.py", "w") as f:
        f.write("import bpy\n")
        f.write(f"bpy.ops.wm.open_mainfile(filepath='{blend_file}')\n")
        txt ="bpy.ops.wm.save_as_runtime(filepath=r'"+f"{os.path.join(os.getcwd(),'build',exe_file)}')\n" 
        f.write(r'%s' % txt)
    command(f"{os.path.join('upbge-0.36.1-windows-x86_64','blender.exe')} --background --python export_game_runtime.py")
    command("del export_game_runtime.py")

    # Copy Python executable
    command(f"xcopy /I {os.path.join('upbge-0.36.1-windows-x86_64','3.6','python','bin')} {os.path.join('build','3.6','python','bin')}")

    # Remove Cycles module and Blender addons (reducing space)
    command(f"del {os.path.join('build','3.6','config','userpref.blend')}")
    command(f"rmdir /S /Q {os.path.join('build','3.6','scripts','addons','cycles')}")
    command(f"copy {os.path.join('utils','build','addon_utils.py')} {os.path.join('build','3.6','scripts','modules')}")

    # Copy asset and code
    for dir in directories:
        command(f"xcopy /S /E /I {dir} {os.path.join('build',dir)} ")
    for file in files:
        command(f"copy {file} build")
    for file in config_files:
        command(f"copy {file[0]} {os.path.join('build',file[1])}")
    for file in script_files:
        command(f"copy {file[0]} {os.path.join('build',file[1])}")

    # Copy lib
    command(f"xcopy /I /S /E /Y {os.path.join(os.getcwd(),'.pixi','envs','default','Lib','site-packages')} \
    {os.path.join(os.getcwd(),'build','3.6','python','lib','site-packages')} ")

    # Copy static lib

    # Zip
    command(f"move build {sys.argv[1]}-{sys.argv[2]}-windows64")
    command(f"7z.exe a -bso0 -t7z -mx=9 {sys.argv[1]}-{sys.argv[2]}-windows64.7z {sys.argv[1]}-{sys.argv[2]}-windows64")

    # Purge
    command(f"del  {os.path.join(os.getcwd(),'upbge-0.36.1-windows-x86_64.7z')}")
    command(f"rmdir /S /Q {os.path.join(os.getcwd(),'upbge-0.36.1-windows-x86_64')}")
    command(f"rmdir /S /Q {sys.argv[1]}-{sys.argv[2]}-windows64")
    
