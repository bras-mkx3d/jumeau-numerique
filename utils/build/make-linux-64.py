#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

###############################################################################
# make-linux-64.py
# @title: Packaging script (called by pixi) for GNU/Linux (linux-64)
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2025 Philippe Roy
# @license: GNU GPL
###############################################################################

# Executable
exe_file     = 'bras_mkx3d'

# Sources
blend_file   = 'bras_mkx3d-7.blend'
directories  = ['asset', 'gtk', 'object', 'exemples', 'img']
files        = ['bras.py', 'bras_lib.py',
                'twin.py', 'twin_traj.py', 'twin_daq.py',
                'twin_ik_blender.py', 'twin_ik_rtb.py', 'twin_plot.py', 'twin_plot_wx.py', 'twin_plot_qt.py',
                'twin_pylint.py', 'twin_serial.py', 'twin_threading.py', 'twin_time.py',
                'bras_mkx3d.sh',
                'README.md', 'LICENSE', 'AUTHORS.md']
config_files = [['twin_config-bak.xml', 'twin_config.xml']]
script_files = [['bras_cmd-bak.py'    , 'bras_cmd.py'],
                ['bras_startcmd.py'   , 'bras_startcmd.py']]

# Command with logging
def command(commandline):
    print(commandline + "... ")
    os.system(r'%s' % commandline)

if __name__ == "__main__":

    # Copy UPBGE
    command("cp /home/phroy/Bureau/seriousgames/blender-edutech/build/upbge-0.36.1-linux-x86_64.tar.xz ./")
    command("tar -xf upbge-0.36.1-linux-x86_64.tar.xz")

    # Install by pip (pixi repo failed)
    command(f"cp {os.path.join('utils','build','get-pip.py')} {os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','bin')}")
    command(f"{os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','bin','python3.10')} \
    {os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','bin','get-pip.py --no-warn-script-location')}")
    command(f"{os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','bin','pip')} install scipy==1.11.1 --no-warn-script-location")
    command(f"{os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','bin','pip')} install roboticstoolbox-python==1.1.0 --no-warn-script-location")

    # Remove rtb_data pip package (reducing space)
    command(f"rm -rf {os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','lib','python3.10','site-packages','rtb_data-1.0.1.dist-info')}")
    command(f"rm -rf {os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','lib','python3.10','site-packages','rtbdata')}")

    # Export as game runtime
    command("mkdir build")
    print ("write 'export_game_runtime.py' script...")
    with open("export_game_runtime.py", "w") as f:
        f.write("import bpy\n")
        f.write(f"bpy.ops.wm.open_mainfile(filepath='{blend_file}')\n")
        f.write(f"bpy.ops.wm.save_as_runtime(filepath='{os.path.join(os.getcwd(),'build',exe_file)}')\n")
    command(f"{os.path.join('upbge-0.36.1-linux-x86_64','blender')} --background --python export_game_runtime.py")
    command("rm export_game_runtime.py")

    # Copy Python binary
    command(f"cp -r {os.path.join('upbge-0.36.1-linux-x86_64','3.6','python','bin')} {os.path.join('build','3.6','python')}")

    # Remove Cycles module and Blender addons (reducing space)
    command(f"rm {os.path.join('build','3.6','config','userpref.blend')}")
    command(f"rm -rf {os.path.join('build','3.6','scripts','addons','cycles')}")
    command(f"cp {os.path.join('utils','build','addon_utils.py')} {os.path.join('build','3.6','scripts','modules')}")

    # Copy asset and Python code
    for dir in directories:
        command(f"cp -r {dir} build")
    for file in files:
        command(f"cp {file} build")
    for file in config_files:
        command(f"cp {file[0]} {os.path.join('build',file[1])}")
    for file in script_files:
        command(f"cp {file[0]} {os.path.join('build',file[1])}")

    # Copy lib (repo ok)
    command(f"cp -r {os.path.join(os.getcwd(),'.pixi','envs','default','lib','python3.1','site-packages')} \
    {os.path.join(os.getcwd(),'build','3.6','python','lib','python3.10')} ")

    # Copy static lib (repo failed)
    command(f"unzip -q {os.path.join(os.getcwd(),'utils', 'build', 'PyGObject-3.38.0.zip')}")
    command(f"cp -r {os.path.join(os.getcwd(),'PyGObject-3.38.0','site-packages')} \
    {os.path.join(os.getcwd(),'build','3.6','python','lib','python3.10')} ")

    # Zip
    command(f"mv build {sys.argv[1]}-{sys.argv[2]}-linux64")
    command(f"7z a -t7z -mx=9 {sys.argv[1]}-{sys.argv[2]}-linux64.7z {sys.argv[1]}-{sys.argv[2]}-linux64")

    # Purge
    command("rm upbge-0.36.1-linux-x86_64.tar.xz")
    command("rm -rf upbge-0.36.1-linux-x86_64")
    command("rm -rf PyGObject-3.38.0")
    command(f"rm -rf {sys.argv[1]}-{sys.argv[2]}-linux64")
    
