import importlib
import time

import bge # Bibliothèque Blender Game Engine (UPBGE)
import serial # Liaison série
from serial.tools.list_ports import comports # Détection du port automatique

###############################################################################
# twin_serial.py
# @title: Gestion de la liaison série
# @project: Blender-EduTech
# @project specifical: Bras MKX3D (suppression de pyFirmata)
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2022-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# FIXME : Suppression de pyFirmata -> refaire la communication sur port serie

# UPBGE scene
scene = bge.logic.getCurrentScene()

# Colors
color_io            = (0.198, 0.109, 0.8, 1)   # Violet
color_io_activate   = (0.051, 0.270, 0.279,1)  # Turquoise
color_io_state_high = (0.799, 0.031, 0.038, 1) # Rouge : état haut
color_io_state_low  = (0.283, 0.283, 0.283, 1) # Gris : état bas

# Récupérer le brochage du jumeau réel
system=importlib.import_module(scene.objects['System']['system']) # Système
pin_config = system.get_public_vars()

###############################################################################
# Liaison série
###############################################################################

##
# Recherche automatique du port
##

def autoget_port():
    # USB Vendor ID,  USB Product ID
    board_dict={'microbit' :[3368, 516],
        'uno' :[9025, 67],
        'mega' :[9025, 66]}
    for com in comports(): # Arduino Uno
        if com.vid == board_dict["uno"][0] and com.pid == board_dict["uno"][1]:
            return [com.device,"Arduino Uno"]
    for com in comports(): # Arduino Mega
        if com.vid == board_dict["mega"][0] and com.pid == board_dict["mega"][1]:
            return [com.device,"Arduino Mega"]
    return [None,""]

##
# Création de l'objet carte (protocole Firmata)
##

def board_init(port):
    try:
        pass
        # return pyfirmata.Arduino(port)
    except:
       return None

##
# Création de l'objet serial (communication série)
##

def serial_init(port,speed):
    try:
        return serial.Serial(port,speed)
    except:
       return None

##
# Affiche la liste des cartes (communication série)
##

def devices():
    for com in comports():
        print ("Name : "+str(com.name)+"\n"
               +" Device : "+str(com.device)+"\n"
               +" Hardware ID : "+str(com.hwid)+"\n"
               +" USB Vendor ID : "+str(com.vid)+"\n"
               +" USB Product ID : "+str(com.pid)+"\n"
               +" USB device location : "+str(com.location)+"\n"
               +" USB manufacturer : "+str(com.manufacturer)+"\n"
               +" USB product : "+str(com.product)+"\n"
               +" Interface-specific : "+str(com.interface))

##
# Activation de la communication avec la carte de communication (Arduino, Micro:bit)
# Vitesse  : 115200 -> 7 fps, 38400 -> 6 fps,  9600 -> 2 fps
# pyserial : baudrate=115200
# pyfirmata : baudrate=57600
##

def serial_open():

    # UI : étape 1
    scene.objects['Twins-text']['Text'] = "Connection en cours ..."

    # Mise en place de la carte
    speed = 57600
    # speed = 115200
    # speed = 9600
    [device, board_name] =autoget_port() # Recherche automatique du port
    if device is None:
        scene.objects['System']['twins'] = False
        scene.objects['Twins-text']['Text'] = "Aucune connection disponible : jumeau réel débranché."
        print ("Jumelage : aucune connection disponible, jumeau réel débranché.")
        return None, False
    board = board_init(device)
    if board is None:
        scene.objects['System']['twins'] = False
        scene.objects['Twins-text']['Text'] = "Aucune connection disponible : port "+device+" pas prêt."
        print ("Jumelage : aucune connection disponible : port "+device+" pas prêt.")
        return None, False
    scene.objects['System']['twins'] = True
    # board_it = pyfirmata.util.Iterator(board) # Itérateur pour les entrées
    board_it.start()

    # UI : étape 2
    if board =="":
        print ("Jumelage : "+device+" - "+str(speed)+" baud.")
        scene.objects['Twins-text']['Text'] = "Connection ouverte : "+device+" - "+str(speed)+" baud."
    else:
        print ("Jumelage : "+board_name+" sur "+device+" à "+str(speed)+" baud.")
        scene.objects['Twins-text']['Text'] = "Connection ouverte : "+board_name+" sur "+device+" à "+str(speed)+" baud."
    time.sleep(0.1)
    return board, True

##
# Fermeture de la communication série
##

def serial_close(board):
    if scene.objects['System']['twins']:
        scene.objects['Twins-text']['Text'] = "Connection fermée."
        board.exit() # Fermer proprement la communication avec la carte
        time.sleep(0.1)
        scene.objects['System']['twins'] = False
        print ("Jumelage : connection fermée.")

# Configuration manuelle du port
# FIXME : plus tard
def config(port, speed):
    pass
    # global board
    # global twins_serial
    # if scene.objects['System']['twins']:
    #     serial_msg1 = "CF\n"
    #     twins_serial.write(serial_msg1.encode())
    #     sleep(1)
    #     serial_msg2 = str(speed)+"\n"
    #     twins_serial.write(serial_msg2.encode())
    #     sleep(1)
    #     serial_msg3 = str(temps_avancer)+"\n"
    #     twins_serial.write(serial_msg3.encode())
    #     sleep(1)
    #     serial_msg4 = str(temps_tourner)+"\n"
    #     twins_serial.write(serial_msg4.encode())
    #     sleep(1)
    #     serial_msg5 = "FC\n"
    #     twins_serial.write(serial_msg5.encode())

###############################################################################
# Jumeau
###############################################################################

##
# Créer une broche
##

def twin_get_pin(board, name, brochage):

    # Recherche de l'adresse directement puis sans le "_r"
    pin_ok = False
    for pin in brochage :
        if pin == name:
            pin_ok = True
            break
    if pin_ok ==False:
        for pin in brochage :
            if pin == name[:-2]: # sans le "_r"
                pin_ok = True
                break
    if pin_ok ==False:
        return None
    
    # Brochage incomplet
    if len (brochage[pin])!=3:
        print ("Jumelage : adressage incomplet de l'objet 3D :", pin)
        return None

    # Panneau IO
    if len(pin_config[name])>3:
        scene.objects["IO_var-"+str(pin_config[pin][3][0])+"-r_activ"]['pluged']=True
        scene.objects["IO_var-"+str(pin_config[pin][3][0])+"-r_activ"]['pin']=brochage[pin][0].upper()+str(brochage[pin][1])
        scene.objects["IO_var-"+str(pin_config[pin][3][0])+"-r_value"]['pin']=brochage[pin][0].upper()+str(brochage[pin][1])

    # Retourne la broche
    print ("Jumelage : "+ pin + " sur la broche "+brochage[pin][0].upper()+str(brochage[pin][1]))
    return board.get_pin(brochage[pin][0]+':'+str(brochage[pin][1])+':'+brochage[pin][2]) # Retourne la broche

##
# Activer le jumelage
##

def twin_open(brochage=None):
    
    # Carte
    board, board_ok =serial_open()
    if board_ok == False:
        return None
    scene.objects['System']['board']=board

    # Brochage
    if brochage is not None:
        for pin in pin_config :
            if len (pin_config[pin][1])>0:
                scene.objects[pin_config[pin][0][0]][pin_config[pin][1][0]]=twin_get_pin(board, pin, brochage)
        return board
    return None

##
# Désactiver le jumelage
##

def twin_stop():
    serial_close(scene.objects['System']['board'])

    # Brochage -> Panneau IO
    for i in range (1,19,1):
        name= "IO_var-"+str(i)
        if scene.objects[name]['visible']:
            scene.objects[name+"-r_activ"]['pluged']=False
            scene.objects[name+"-r_activ"].color= color_io_state_low

###############################################################################
# Message
###############################################################################

##
# Envoi d'un message vers la communication série
# FIXME : plus tard
##

# def  serie_msg(text):
#     global twins_serial
#     text2= text+"\n"
#     scene.objects['Twins-text']['Text'] = "Communication : envoi message : "+text
#     twins_serial.write(text2.encode())

##
# Mise en écoute de jumeau numérique (figeage de la scène)
# FIXME : plus tard
##

# def  twins_listen(cont):
#     global twins_serial
#     if scene.objects['System']['twins']:
#         if scene.objects['System']['twins_readline'] != "":
#             scene.objects['Twins-text']['Text'] = "Écoute de la connection figeage de la scène.... Message reçu : "+scene.objects['System']['twins_readline']
#         else:
#             scene.objects['Twins-text']['Text'] = "Écoute de la connection  figeage de la scène..."
#         if cont.sensors['Property'].positive:
#             if scene.objects['System']['twins_listen'] :
#                 serial_msg = twins_serial.readline()
#                 if serial_msg is not None:
#                     scene.objects['System']['twins_readline'] = str(serial_msg)
#                     # scene.objects['Twins-text']['Text'] = "Message reçu : "+str(serial_msg)
#                     scene.objects['System']['twins_listen'] = False

##
# Réception d'un message de la communication série
# FIXME : plus tard
##

# def  serie_rcpt():
#     # scene.objects['Twins-text']['Text'] = "Écoute de la \nconnection\n figeage de \n la scène"
#     scene.objects['System']['twins_readline'] = ""
#     scene.objects['System']['twins_listen'] = True
#     while  scene.objects['System']['twins_readline'] == "":
#         if scene.objects['System']['twins_readline'] != "":
#             break
#     # scene.objects['Twins-text']['Text'] = "Connection\nouverte :\n"+scene.objects['System']['twins_port']+"\n"+str(scene.objects['System']['twins_speed'])+" baud"
#     return scene.objects['System']['twins_readline']
