import sys

import wx # wxPython

###############################################################################
# config.py
# @title: Configurateur en wxPython
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

###############################################################################
# Fenêtre
###############################################################################

##
# Quitter
##

# Choisir une configuration
def ok_click(event, frame, size_combo, qual_combo, size_list):
    size = size_list[size_combo.GetSelection()].split('x')
    print(size[0]+","+size[1]+","+str(qual_combo.GetSelection())+",0") # stdout = "1280,720,1,0" ; le thème est toujours à 0 car pas de thème sous wxPython
    frame.Close()

# Fermer la fenêtre ou annuler
def end(event, frame):
    frame.Close()

##
# Implantation
##

def layout(twin_config):

    # Fenêtre
    frame = wx.Frame(None, -1, "Configuration du jumeau numérique du portail coulissant")
    frame.SetSize((500, 187))
    win_box = wx.BoxSizer(wx.VERTICAL)
    frame.SetSizer(win_box)
    win_box.Add((-1, 5))

    # Configuration courante de la taille de l'écran
    screen_width_mode  = [640, 960, 1280, 1920]
    screen_height_mode = [360, 540,  720, 1080]
    if config[0] in screen_width_mode and config[1] in screen_height_mode:
        pass
    else:
        for i in range(len(screen_width_mode)):
            if config[0] < screen_width_mode[i]:
                screen_width_mode.insert(i, config[0])
                screen_height_mode.insert(i, config[1])
                break
    size_list = []
    for i in range(len(screen_width_mode)):
        size_list.append(str(screen_width_mode[i])+"x"+str(screen_height_mode[i]))

    # Taille de l'écran
    size_hbox = wx.BoxSizer(wx.HORIZONTAL)
    win_box.Add(size_hbox, 1, wx.EXPAND, border=10)
    size_label = wx.StaticText(frame, label="Taille de l'écran", style=wx.ALIGN_LEFT)
    size_combo = wx.Choice(frame, -1, (-1, -1), choices=size_list)
    size_hbox.Add(size_label, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALL, border=5)
    size_hbox.Add(size_combo, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, border=5)
    size_combo.SetSelection(screen_width_mode.index(config[0]))

    # Qualité du rendu
    qual_list = ["Inconvenant", "Basse", "Moyenne", "Haute", "Épique"]
    qual_hbox = wx.BoxSizer(wx.HORIZONTAL)
    win_box.Add(qual_hbox, 1, wx.EXPAND, border=10)
    qual_label = wx.StaticText(frame, label="Qualité (redémarrage nécessaire)", style=wx.ALIGN_LEFT)
    qual_combo = wx.Choice(frame, -1, (-1, -1), choices=qual_list)
    qual_hbox.Add(qual_label, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALL, border=5)
    qual_hbox.Add(qual_combo, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, border=5)
    qual_combo.SetSelection(int(config[2]))

    # Boutons
    win_box.Add((-1, 5))
    line = wx.StaticLine(frame, wx.ID_ANY)
    win_box.Add(line, 0, wx.EXPAND | wx.ALL, 5)
    btn_hbox = wx.BoxSizer(wx.HORIZONTAL)
    win_box.Add(btn_hbox, 1, flag=wx.EXPAND, border=10)
    cancel_button = wx.Button(frame, label='Annuler', size=(-1, 30))
    frame.Bind(wx.EVT_BUTTON, lambda event: end(event, frame), cancel_button)
    ok_button = wx.Button(frame, label='Sélectionner la configuration', size=(-1, 30))
    frame.Bind(wx.EVT_BUTTON, lambda event: ok_click(event, frame, size_combo, qual_combo, size_list), ok_button)
    btn_hbox.Add(cancel_button, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, border=5)
    btn_hbox.Add((0, 0), 1, wx.ALL | wx.EXPAND, 1)
    btn_hbox.Add(ok_button, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, border=5)

    # Résultat
    frame.Layout()
    return frame


# Boucle principale
if __name__ == "__main__":

    # Implantation de la fenêtre
    config = [int(i) for i in sys.argv[1].split(',')] # stdin = 1280,720,1,1
    app   = wx.App()
    frame = layout(config)

    # Go !
    frame.Show()
    app.MainLoop()
