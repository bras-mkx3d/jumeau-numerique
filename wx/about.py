import os, sys
import textwrap

import wx, wx.adv # wxPython

###############################################################################
# about.py
# @title: Fenêtre A propos en wxPython
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##


def dialog():

    # Fenêtre
    info = wx.adv.AboutDialogInfo()
    info.SetName("Bras MKX3D")
    info.SetVersion("1.2")
    info.SetIcon(wx.Icon(os.path.join(os.getcwd(),"asset","icons","app","bras_mkx3d-128x148.png"), wx.BITMAP_TYPE_PNG))

    # Website
    info.SetWebSite("https://forge.apps.education.fr/bras-mkx3d/jumeau-numerique" , "Dépôt Git - La Forge")

    # Copyright
    info.SetCopyright("© 2024-2025 Philippe Roy\n    Licence GNU GPL 3.0")

    # Licence
    licence1 = """Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier sous les termes de la licence publique générale GNU telle qu'elle est publiée par la Free Software Foundation ; soit la version 3 de la licence, ou (comme vous voulez) toute version ultérieure."""
    licence2 = """Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; même sans la garantie de COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la licence publique générale GNU pour plus de détails."""
    licence3 = """Vous devriez avoir reçu une copie de la licence publique générale GNU avec ce programme. Si ce n'est pas le cas, voir http://www.gnu.org/licenses/ ."""
    licence11 = "\n".join(textwrap.wrap(licence1, 80))
    licence12 = "\n".join(textwrap.wrap(licence2, 80))
    licence13 = "\n".join(textwrap.wrap(licence3, 80))
    licence = licence11+"\n\n"+licence12+"\n\n"+licence13
    info.SetLicence(licence)

    # Crédits
    dev_list=["Philippe Roy (https://forge.apps.education.fr/phroy)"]
    dev_list.append("\nModèle CAO du Bras (https://forge.apps.education.fr/bras-mkx3d/modele-3d/) - CC BY-NC-SA 4.0")
    # dev_list.append("")
    
    credits_description = {
    'Blender'                     : ["\n\nBlender","Plateforme de modélisation et d\'animation 3D","https://blender.org", "GNU GPL"],
    'UPBGE'                       : ["UPBGE","Moteur de jeu 3D","https://upbge.org", "GNU GPL"],
    'Python'                      : ["Python","Langage de programmation","https://python.org", "PSFL"],
    'Pylint'                      : ["Pylint","Bibliothèque de vérification d\'un code Python","https://pylint.pycqa.org", "GNU GPL"],
    'pySerial'                    : ["pySerial","Bibliothèque de communication série","https://pyserial.readthedocs.io", "BSD-3-Clause"],
    'Robotics Toolbox for Python' : ["Robotics Toolbox for Python","Solveur de cinématique inverse","https://github.com/petercorke/robotics-toolbox-python", "MIT Licence"],
    'Matplotlib'                  : ["Matplotlib","Bibliothèque de visualisation graphique de données","https://matplotlib.org", "BSD"],
    'wxPython'                    : ["wxPython","API Python de GTK","https://pygobject.gnome.org/", "GNU LGPL 2.1"],
    'Game-icons.net'              : ["Game-icons.net","Icônes","https://game-icons.net/","CC BY 3.0"],
    'Kenney'                      : ["Kenney","Icônes","https://www.kenney.nl/","CC0 1.0"],
    'EspressoDolce'               : ["Espresso Dolce","Police de caractères","https://www.dafont.com/fr/espresso-dolce.font", "OFL"],
    'Sphinx'                      : ["Sphinx","Générateur de documentation","https://www.sphinx-doc.org/", "BSD"],
    'Pixi'                        : ["Pixi","Gestionnaire de paquets","https://prefix.dev/", "BSD"]}

    credits_description_list=list(credits_description)
    for i in range(len(credits_description_list)):
        dev_list.append(f"\n{credits_description[credits_description_list[i]][0]} "+
                          f"({credits_description[credits_description_list[i]][2]}) : "+
                          f"{credits_description[credits_description_list[i]][1]} - "+
                          f"{credits_description[credits_description_list[i]][3]}")
    info.SetDevelopers(dev_list)
    wx.adv.AboutBox(info)

# Boucle principale
if __name__ == '__main__':
    app   = wx.App()
    frame = wx.Frame(None, -1, "À propos - Bras MKX3D")
    dialog()
    frame.Hide()
    app.MainLoop()
