import os, sys

import wx # wxPython

###############################################################################
# file.py
# @title: Sélecteur de fichier en wxPython
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Sélection du script Python
def dialog_cmd(frame):
    dialog = wx.FileDialog(frame, "Sélection du fichier de commande", "", "", 
                           "Script Python|*.py", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
    return dialog

# Sélection de la trajectoire
def dialog_traj(frame):
    dialog = wx.FileDialog(frame, "Sélection du fichier trajectoire", "", "", 
                           "Tableau CSV|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
    return dialog

# Boucle principale
if __name__ == "__main__":

    # Fenêtre
    app    = wx.App()
    frame  = wx.Frame(None, -1, "Sélection du fichier de commande - Bras MKX3D")
    if sys.argv[2]=='cmd':
        dialog=dialog_cmd(frame)
    if sys.argv[2]=='traj':
        dialog=dialog_traj(frame)

    # Affichage
    if dialog.ShowModal() != wx.ID_CANCEL:
        print(dialog.GetPath())
    dialog.Destroy()
