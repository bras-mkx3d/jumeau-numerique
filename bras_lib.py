import os
import math, mathutils
import csv as csvlib  # Parser de fichier CSV
import inspect        # Examen de la pile d'exécution

import bge, bpy  # Blender Game Engine (UPBGE)

import twin_threading # Multithreading (multitâches)
import twin_traj      # Gestion des trajectoires
import bras           # Gestion du bras robotisé

from twin_ik_rtb     import rtb_fkine, rtb_ikine, rtb_get_robot                # Solveur IK Robotics Toolbox for Python
from twin_ik_blender import ikb_fkine, ikb_ikine, ikb_fkine_slv, ikb_ikine_slv # Solveur IK de Blender

from twin_serial import twin_open, serial_close # Liaison série
from twin_daq    import get, set, csv, csv_generate, csv_plot_generate, plot, plot_generate # Acquisition des données
from twin_time   import sleep, tempo, get_t, set_t, reset_t # Gestion du temps

###############################################################################
# bras_lib.py
# @title: Bibliothèque utilisateur du bras MKX3D
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024-2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Maths
PRECISION_DIST  = 5 # Nombre de décimal pour les distances en m
PRECISION_ANGLE = 2 # Nombre de décimal pour les angles en degré

# Armature
TEMPO_RENDER    = 0.1 # Temporisation en s pour pouvoir bien lire l'armature (calcul IK par rendu 3D)

# UPBGE scene
scene = bge.logic.getCurrentScene()
motors=['Moteur S0', 'Moteur S1', 'Moteur S2', 'Moteur S3', 'Moteur S4', 'Moteur S5']

# Colors
color_io            = (0.198, 0.109, 0.800, 1) # Violet
color_io_activate   = (0.051, 0.270, 0.279, 1) # Turquoise
color_io_state_high = (0.799, 0.031, 0.038, 1) # Rouge : état haut
color_io_state_low  = (0.283, 0.283, 0.283, 1) # Gris : état bas

# Générales
color_white          = (0.8  , 0.8  , 0.8  , 1) # Blanc
color_blue           = (0.007, 0.111, 0.638, 1) # Couleur bleu (actionneur type moteur)
color_blue_dark      = (0.004, 0.054, 0.296, 1) # Couleur bleu noir
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_grey           = (0.285, 0.285, 0.285, 1) # Couleur gris
color_turquoise      = (0.051, 0.270, 0.279, 1) # Couleur turquoise
color_turquoise_dark = (0.030, 0.148, 0.152, 1) # Couleur turquoise foncée
color_yellow         = (0.800, 0.619, 0.021, 1) # Jaune
color_led_yellow     = (0.799, 0.617, 0.021, 1) # Couleur Led jaune (led)
color_red            = (0.694, 0.098, 0.098, 1) # Rouge
color_aru            = (0.693, 0.097, 0.097, 1) # Couleur de l'arrêt d'urgence
color_green          = (0.083, 0.223, 0.028, 1) # Vert
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_purple         = (0.799, 0.005, 0.314, 1) # Violet foncé
color_purple_light   = (0.687, 0.125, 0.578, 1) # Violet clair

# UPBGE constants
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED  = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE       = bge.logic.KX_INPUT_ACTIVE
# JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

# Threads de commandes
threads_cmd = []

# Threads de visualisation de données
threads_plot = []

###############################################################################
# Print formaté (debug)
###############################################################################

def print_q(text_begin, q, text_end):
    print(text_begin+" : ["+'{:>6.2f}'.format(q[0])+", "+
          '{:>6.2f}'.format(q[1])+", "+
          '{:>6.2f}'.format(q[2])+", "+
          '{:>6.2f}'.format(q[3])+", "+
          '{:>6.2f}'.format(q[4])+", "+
          '{:>6.2f}'.format(q[5])+"]"+text_end)

def print_p(text_begin, p, text_end):
    print(text_begin+" : ["+'{:>5.4f}'.format(p[0])+", "+
          '{:>5.4f}'.format(p[1])+", "+
          '{:>5.4f}'.format(p[2])+", "+
          '{:>6.2f}'.format(p[3])+", "+
          '{:>6.2f}'.format(p[4])+", "+
          '{:>6.2f}'.format(p[5])+"]"+text_end)

###############################################################################
# Actionneurs
###############################################################################

##
# Coordonnées articulaires
##

# Déplacer le robot en allant aux coordonnées articulaires
def move_q(q, ot=None, draw=False, marker=None, color=color_yellow, thickness=1, tickskip=3, wait=True, cont=True, force=False):
    """
    **Déplacer le robot en allant aux coordonnées articulaires.**

    :param q:         coordonnées articulaires en degré [q1,q2,q3,q4,q5,q6]
    :type  q:         liste de flottant
    :param ot:        valeur d'ouverture de la pince en pourcentage (de 0 pour fermée à 100 pour complètement ouverte). 'None' pour garder la valeur courante.
    :type  ot:        entier, optionel
    :param draw:      affiche un trajectoire entre deux points, uniquement en mode bloquant
    :type  draw:      booléen, optionel
    :param marker:    type de marque : 'point', 'axis' (repère), 'pose' (repère pour la sauvegarde d'une trajectoire) ou None pour ignorer
    :type  marker:    chaîne de caractères ou None, optionel
    :param color:     couleur des points de la trajectoire, jaune par défaut
    :type  color:     RGBA tuple (Rouge Vert Bleu Alpha), optionel
    :param thickness: épaisseur de la trajectoire, taille des points (facteur d'échelle)
    :type  thickness: flottant, optionel
    :param tickskip:  fréquence d'apparition des points de la trajectoire, tous les x ticks (60 fps)
    :type  tickskip:  entier, optionel
    :param wait:      mode bloquant, attente de la fin du trajet
    :type  wait:      booléen, optionel
    :param cont:      mode trajectoire continue (tous les mouvements arrivent en même temps), uniquement en mode bloquant
    :type  cont:      booléen, optionel
    :param force:     permet de dépasser les limites angulaires des moteurs
    :type  force:     booléen, optionel

    :return: mouvement réalisé sans erreur
    :rtype:  booléen 

    :exemple: success = move_q([10,70,20,30,40,20])
    """
    
    # Contrôle des arguments
    if q is None:
        print("Mouvement : erreur sur move_q(...) : liste des coordonnées articulaires indéfinie")
        return False
    if len(q)<6:
        print("Mouvement : erreur sur move_q(...) : il manque des coordonnées articulaires -> de q1 à q6")
        return False
    if len(q)>6:
        print("Mouvement : erreur sur move_q(...) : il y a trop de coordonnées articulaires -> de q1 à q6")
        return False

    # Point de départ en jaune
    q1=[]
    for i_txt in motors:
        q1.append(scene.objects[i_txt]['q'])
    p1  = rtb_fkine(q1, unit="deg") # RTB est plus fiable
    if draw and marker is not None:
        if marker=='point':
            draw_point(p1, color_yellow)
        if marker=='axis' or marker=='pose':
            draw_axis(p1, color_yellow)

    # Mouvement
    bras.move_q(q, ot, wait=wait, cont=cont, draw=draw, color=color, thickness=thickness, tickskip=tickskip, force=force)

    # Point d'arrivé
    if draw:
        p2 = rtb_fkine(q, unit="deg") # RTB est plus fiable
        if marker=='point':
            draw_point(p2, color_green)
        if marker=='axis':
            draw_axis(p2, color_green)
        if marker=='pose':
            add_pose(None, q2, ot, p2, color_green, prop='', mode='move_q')

    return True

# Récupérer les coordonnées articulaires à partir des coordonnées cartésiennes
def get_q(*p2, draw=False, marker=None, solver='blender', ctrl=None, iterations=None, orientation=True, error_max=0.001, verbose=False):
    """
    **Calcul les coordonnées articulaires à partir des coordonnées cartésiennes.**

    :param p2:          coordonnées cartésiennes en m et en degré [x,y,z,rx,ry,rz] de la position cible
    :type  p2:          liste de flottant, optionel
    :param draw:        affiche le point cible et le point résulat
    :type  draw:        booléen, optionel
    :param marker:      type de marque : 'point', 'axis' (repère), 'pose' (repère pour la sauvegarde d'une trajectoire) ou None pour ignorer
    :type  marker:      chaîne de caractères ou None, optionel
    :param solver:      choix du solveur de cinématique inverse (IK) : 'blender' ou 'rtb'
    :type  solver:      chaîne de caractères, optionel
    :param ctrl:        type de controle du solveur : 'delta', 'near', None pour pas de contrôle (par défaut)
    :type  ctrl:        chaîne de caractères, optionel
    :param iterations:  nombre d'itérations pour le calcul
    :type  iterations:  entier, optionel
    :param orientation: contrainte aussi sur l'orientation
    :type  orientation: booléen, optionel
    :param error_max:   marge d'erreur sur la validation du solveur en m
    :type  error_max:   flottant, optionel
    :param verbose:     affiche les informations du solveur de cinématique inverse
    :type  verbose:     booléen, optionel

    :return: solution géométrique trouvée, coordonnées articulaires de la pose, coordonnées cartésiennes de la pose trouvée
    :rtype:  booléen, liste de flottant, liste de flottant

    :exemple: success, q, p_find = get_q([0.0277, -0.4114, 0.6295, -2.47,-23.18, 32.15])

    .. hint:: Si la pose p2 est ignorée, c'est la position courante (p1) qui est prise en compte.
    .. hint:: Si le point n'a pas de solution géométrique la liste retournée sera le point considéré comme la solution la plus proche.

    .. topic:: Choix du solveur de cinématique inverse (IK) :
    
        - **blender** : solveur IK Blender (iTaSC IK) -> calcul plus rapide (par défaut).
        - **rtb** : solveur Robotic ToolBox for Python (RTB) -> calcul plus précis.

    .. topic:: **Solveur IK Blender** : paramétrage du solveur iTaSC IK de Blender

        - **ctrl=None :** premier calcul considéré comme suffisant (par défaut).
        - **ctrl='delta' :** minimise l'écart entre la position trouvée et la position théorique.
        - **ctrl='near' :** minimise le déplacement articulaire par rapport aux coordonnées courantes.
        - **iterations :** nombre d'itérations pour le calcul (entier, 2 par défaut).
        - **orientation :** contrainte aussi sur l'orientation (binaire, par défaut).
        - **error_max :** marge d'erreur sur la validation du solveur (réel, 0.001 m par défaut)

    .. topic:: **Solveur RTB** : paramétrage du solveur Robotic ToolBox for Python :

        - **ctrl=None :** premier calcul considéré comme suffisant (par défaut).
        - **ctrl='delta' :** minimise l'écart entre la position trouvée et la position théorique.
        - **ctrl='near' :** minimise le déplacement articulaire par rapport aux coordonnées courantes.
        - **iterations :** nombre d'itérations pour le calcul (entier, 20 par défaut).
        
    .. hint:: Avec le solveur RTB : 

        - il y a toujours la contrainte de l'orientation,
        - la marge d'erreur est à 0.
   """

    # Position courante (p1, q1), position cible (q2)
    q1=[]
    for i_txt in motors:
        q1.append(round(scene.objects[i_txt]['q'], 2))
    p1 = rtb_fkine(q1, unit="deg") # RTB est plus fiable
    if len(p2) == 0:
        return True, q1, p1
    if solver=='blender':
        ikb_fkine(q1, unit="deg") # Positionner l'armature sur p1
    p2=p2[0]

    # Contrôle des arguments
    head_txt="Solveur IK Blender : get_q(...) : " if solver=='blender' else "Solveur RTB : get_q(...) : "
    if solver=='rtb': orientation=True
    if len(p2)<6:
        print(head_txt+"erreur sur le point "+str(p2)+" : il manque des coordonnées opérationnelles -> [x,y,z,rx,ry,rz]")
        return False, None, None
    if len(p2)>6:
        print(head_txt+"erreur sur le point "+str(p2)+" : il y a trop de coordonnées opérationnelles -> [x,y,z,rx,ry,rz]")
        return False, None, None
    if iterations is None:
        if solver=='blender':
            iterations=2
        if solver=='rtb':
            iterations=20
    if verbose:
        print("\033[0;45m"+head_txt+"contrôle="+str(ctrl)+" ; orientation="+str(orientation)+" ; error_max="+str(error_max))

    # Go !
    q, error_limit, error_ctrl, i, error = ikine_ctrl(p2, q1, None, 0, 1, None, solver, ctrl, iterations, orientation, None, error_max, verbose)
    p = rtb_fkine(q, unit="deg") # RTB est plus fiable

    # Pas de solution géométrique
    if error_limit:

        # Affichage de la cible
        if draw and marker is not None:
            if marker=='point':
                draw_point(p2, color_red)
            if marker=='axis' or marker=='pose':
                draw_axis(p2, color_red)

        # Affichage du point/repère d'arrivée
        if draw and marker is not None:
            if marker=='point':
                draw_point(p, color_purple)
            if marker=='axis' or marker=='pose':
                draw_axis(p, color_purple)

        # Message solveur
        scene.objects['Solver-text']['Text']=head_txt+"erreur sur le point "+str(p2)+" : pas de solution géométrique."
        if verbose:
            distance = round(math.sqrt((p2[0]-p[0])**2+(p2[1]-p[1])**2+(p2[2]-p[2])**2), PRECISION_DIST)
            print("\033[0;41m"+head_txt+"contrôle="+str(ctrl)+" ; orientation="+str(orientation)+" ; error_max="+str(error_max))
            print("\033[0;41m"+head_txt+"erreur sur le point "+str(p2)+" : pas de solution géométrique"+"\033[0m")
            print_p("\033[0;41m"+head_txt+"position de départ p1 ",   p1, "\033[0m")
            print_p("\033[0;41m"+head_txt+"position cible     p2 ",   p2, "\033[0m")
            print_p("\033[0;41m"+head_txt+"position trouvée   p  ",   p,  "\033[0m")
            print_q("\033[0;41m"+head_txt+"position de départ q1 ",   q1, "\033[0m")
            print_q("\033[0;41m"+head_txt+"position trouvée   q  ",   q,  "\033[0m")
            print("\033[0;41m"+head_txt+  "distance entre p et p2 : "+str(distance)+"\033[0m")
            print("\033[0;41m"+head_txt+  "erreur entre p et p2   : "+str(round(error,PRECISION_DIST))+"\033[0m")
        return False, q, p

    # Résultat
    if draw and marker is not None:
        if marker=='point':
            draw_point(p2, color_green)
        if marker=='axis':
            draw_axis(p2, color_green)
        if marker=='pose':
            add_pose(None, q, None, p, color_green, prop='', mode='get_q') # FIXME : prop plus tards
    if solver=='blender':
        scene.objects['Armature ikb'].setVisible(False, True)
        scene.objects['Rc ikb'].setVisible(False, True)

    # Message solveur
    if verbose:
        distance = round(math.sqrt((p2[0]-p[0])**2+(p2[1]-p[1])**2+(p2[2]-p[2])**2), PRECISION_DIST)
        scene.objects['Solver-text']['Text']=head_txt+"point "+str(p2)+" : solution géométrique trouvée."
        print("\033[0;42m"+head_txt+"contrôle="+str(ctrl)+" ; orientation="+str(orientation)+" ; error_max="+str(error_max))
        print("\033[0;42m"+head_txt+"point "+str(p2)+" : solution géométrique trouvée"+"\033[0m")
        print_p("\033[0;42m"+head_txt+"position de départ p1 ",   p1, "\033[0m")
        print_p("\033[0;42m"+head_txt+"position cible     p2 ",   p2, "\033[0m")
        print_p("\033[0;42m"+head_txt+"position trouvée   p  ",   p,  "\033[0m")
        print_q("\033[0;42m"+head_txt+"position de départ q1 ",   q1, "\033[0m")
        print_q("\033[0;42m"+head_txt+"position trouvée   q  ",   q,  "\033[0m")
        print("\033[0;42m"+head_txt+  "distance entre p et p2 : "+str(distance)+"\033[0m")
        print("\033[0;42m"+head_txt+  "erreur entre p et p2   : "+str(round(error,PRECISION_DIST))+"\033[0m")
    return True, q, p

##
# Coordonnées opérationnelles (coordonnées cartésiennes)
##

# Modèle géométrique indirecte (MGI) avec le contrôle du solveur (solution géométrique, continuité, optimisation déplacement, ...)
def ikine_ctrl(p2, q_prev, q_step_max, i_pt, nb_pt, dt, solver, ctrl, iterations, orientation, step, error_max, verbose):
    head_txt = "Solveur IK Blender : " if solver == 'blender' else "Solveur RTB : "
    i = 1 # Itérations courante
    error = 0 # Distance entre le point cible et le point trouvé
    if solver=='rtb': orientation=True
    head_txt += str(inspect.stack()[1][3]) + "(...) : " # Fonction appelante

    # Coordonnées articulaires calculées q
    if solver=='blender':
        q, success, error = ikb_ikine_slv(p2, orientation=orientation, error_max=error_max)
    if solver=='rtb':
        q, success, error = rtb_ikine(p2)

    # Pas de solution géométrique
    if success == False:
        return (q, True, False, 1, error)

    # Première solution suffisante (ctrl = None, pas de contrôle)
    if ctrl is None:
        scene.objects['Solver-text']['Text']=head_txt+"première solution suffisante (pas de contrôle) : point "+str(i_pt+1)+"/"+str(nb_pt)
        if verbose :
            print("\033[0;45m"+head_txt+"solveur sans contrôle : "+
                  'point {:>2}'.format(i_pt+1)+'/{:>2}'.format(nb_pt) +"\033[0m")
        return (q, False, False, 1, error)

    ##
    # Minimisation de l'écart entre la position théorique (p2) et la position trouvée (p)
    ##

    if ctrl=='delta':
        q_best = q
        i_best = 0
        if solver=='blender':
            p = ikb_fkine_slv(q, unit="deg")
        if solver=='rtb':
            p = rtb_fkine(q, unit="deg")
        lin_min = math.sqrt((p[0]-p2[0])**2+(p[1]-p2[1])**2+(p[2]-p2[2])**2)
        rot_min = math.sqrt((p[3]-p2[3])**2+(p[4]-p2[4])**2+(p[5]-p2[5])**2)
        for i in range (iterations):

            # MGI
            scene.objects['Solver-text']['Text']=head_txt+"recherche des coordonnées minimisant l\"écart positionnel : point "+str(i_pt+1)+"/"+str(nb_pt)+" - itération "+str(i+1)+"/"+str(iterations)
            if solver=='blender':
                q, success, error = ikb_ikine_slv(p2, orientation=orientation, error_max=error_max) # FIXME : chercher ne plus faire le ikb_fkine_slv trop proche du ikb_ikine -> sinon plantage
                p = ikb_fkine_slv(q, unit="deg") # FIXME : chercher ne plus faire le ikb_fkine_slv trop proche du ikb_ikine -> sinon plantage
            if solver=='rtb':
                q, success, error = rtb_ikine(p2)
                p = rtb_fkine(q, unit="deg")

            # Contrôle
            lin_delta = math.sqrt((p[0]-p2[0])**2+(p[1]-p2[1])**2+(p[2]-p2[2])**2)
            rot_delta = math.sqrt((p[3]-p2[3])**2+(p[4]-p2[4])**2+(p[5]-p2[5])**2)
            if verbose :
                print("\033[0;45m"+head_txt+"recherche de l'écart mini : "+
                      'point {:>2}'         .format(i_pt+1)    +'/{:>2}'.format(nb_pt) +" ; "+
                      'iteration {:>2}'     .format(i+1)       +'/{:>2}'.format(iterations) +" ; "+
                      'delta lin : {:>7.6f}'.format(lin_delta) +" ; "+
                      'min lin : {:>7.6f}'  .format(lin_min)   +" ; "+
                      'delta ang : {:>5.4f}'.format(rot_delta) +" ; "+
                      'min ang : {:>5.4f}'  .format(rot_min)   +" ; "+
                      'erreur : {:>6.5f}'   .format(error)     +"\033[0m")
            if lin_delta < lin_min:
                q_best = q
                i_best = i
                lin_min = lin_delta
            elif round(lin_delta, 4) == round(lin_min, 4):
                if rot_delta < rot_min:
                    q_best = q
                    rot_min = rot_delta

        # Affichage de la solution
        if verbose :
            print("\033[0;42m"+head_txt+"recherche de l'écart mini : "+
                  'point {:>2}'       .format(i_pt+1)   +'/{:>2}'.format(nb_pt) +" ; "+
                  'iteration {:>2}'   .format(i_best+1) +'/{:>2}'.format(iterations) +" ; "+
                  'min lin : {:>7.6f}'.format(lin_min)  +" ; "+
                  'min ang : {:>5.4f}'.format(rot_min)  +" ; "+
                  'erreur : {:>6.5f}' .format(error)    +" ; "+
                  'q : '              +str(q_best)      +"\033[0m")
        return (q_best, False, False, i, error)

    ##
    # Minimisation du mouvement : recherche des coordonnées articulaires (q) les plus proches des coordonnées actuelles (q1)
    ##
    
    if ctrl == "near":
        q_best = q
        i_best = 0
        rot_min = abs(q[0]-q_prev[0])+abs(q[1]-q_prev[1])+abs(q[2]-q_prev[2])+abs(q[3]-q_prev[3])+abs(q[4]-q_prev[4])+abs(q[5]-q_prev[5])
        for i in range (iterations):

            # MGI
            scene.objects['Solver-text']['Text']=head_txt+"recherche des coordonnées minimisant le déplacement articulaire : point "+str(i_pt+1)+"/"+str(nb_pt)+" - itération "+str(i+1)+"/"+str(iterations)
            if solver=='blender':
                q, success, error = ikb_ikine_slv(p2, orientation=orientation, error_max=error_max)
            if solver=='rtb':
                q, success, error = rtb_ikine(p2)

            # Contrôle
            rot = abs(q[0]-q_prev[0])+abs(q[1]-q_prev[1])+abs(q[2]-q_prev[2])+abs(q[3]-q_prev[3])+abs(q[4]-q_prev[4])+abs(q[5]-q_prev[5])
            if verbose :
                print("\033[0;45m"+head_txt+"recherche du plus proche : "+
                      'point {:>2}'            .format(i_pt+1)  +'/{:>2}'.format(nb_pt) +" ; "+
                      'iteration {:>2}'        .format(i+1)     +'/{:>2}'.format(iterations) +" ; "+
                      'rotation : {:>6.2f}'    .format(rot)     +" ; "+
                      'rotation min : {:>6.2f}'.format(rot_min) +" ; "+
                      'erreur : {:>6.5f}'      .format(error)   +"\033[0m")
            if rot < rot_min:
                q_best = q
                i_best = i
                rot_min = rot

        # Affichage de la solution
        if verbose :
            print("\033[0;42m"+head_txt+"recherche du plus proche : "+
                  'point {:>2}'            .format(i_pt+1)   +'/{:>2}'.format(nb_pt) +" ; "+
                  'iteration {:>2}'        .format(i_best+1) +'/{:>2}'.format(iterations) +" ; "+
                  'rotation min : {:>6.2f}'.format(rot_min)  +" ; "+
                  'erreur : {:>6.5f}'      .format(error)    +" ; "+
                  'q : '+ str(q_best) +"\033[0m")
        return (q_best, False, False, i, error)


    ##
    # Anti-flip : supprime les discontinuitées avec la validation par seuil -> calcul rapide
    ##
    
    if ctrl=='step':
        i = 0
        ctrl_step_max = step*dt
        error_ctrl = True
        while error_ctrl:

            # Limite atteinte -> Pas de solution continue
            if i > iterations:
                error_ctrl=True
                break

            # MGI
            scene.objects['Solver-text']['Text']=head_txt+"move_p(...) : génération du trajet avec la validation par seuil : point "+str(i_pt+1)+"/"+str(nb_pt)+" - itération "+str(i+1)+"/"+str(iterations)
            if solver=='blender':
                q, success, error = ikb_ikine_slv(p2, orientation=orientation, error_max=error_max)
            if solver=='rtb':
                q, success, error = rtb_ikine(p2)

            # Contrôle de la continuité
            error_ctrl=False
            q_step=[]
            for i_mot in range(6):
                ctrl_step = abs(q[i_mot] - q_prev[i_mot])
                if ctrl_step >= ctrl_step_max:
                    if verbose:
                        print("\033[0;45m"+head_txt+"validation par seuil : "+
                              'point {:>2}'                .format(i_pt+1)        +'/{:>2}'.format(nb_pt) +" ; "+
                              'iteration {:>2}'            .format(i+1)           +'/{:>2}'.format(iterations) +" ; "+
                              'moteur '                    +str(i_mot)                +" ; "+
                              'q step : {:>6.2f}'          .format(ctrl_step)     +" ; "+
                              'q_step_max ctrl : {:>6.2f}' .format(ctrl_step_max) +" ; "+
                              'erreur : {:>6.5f}'          .format(error)         +" ; "+
                              'q_step_max speed : {:>6.2f}'.format(q_step_max)    +"\033[0m")
                    error_ctrl=True
                    i +=1
                    break
                q_step.append(round(ctrl_step,2))

        # Affichage de la solution
        if verbose :
            print("\033[0;42m"+head_txt+"validation par seuil : "+
                  'point {:>2}'      .format(i_pt+1) +'/{:>2}'.format(nb_pt) +" ; "+
                  'iteration {:>2}'  .format(i+1)    +'/{:>2}'.format(iterations) +" ; "+
                  'q step : '        +str(q_step)    +" ; "+
                  'erreur : {:>6.5f}'.format(error)  +" ; "+
                  'q : '             +str(q)         +"\033[0m")
        return (q, False, error_ctrl, i, error)


# Déplacer le robot en allant aux coordonnées cartésiennes avec une trajectoire rectiligne
# FIXME : ikb_ikine avec moteurs bloqués
def move_p(p2, ot=None, resol=1000, nb_pt=None, draw=False, marker=None, color=None, thickness=1, tickskip=3, solver='blender', ctrl=None, iterations=None, orientation=False, error_max=0.001, step=180, wait=True, cont=True, verbose=False, force=False):
    """
    **Déplacer le robot en allant aux coordonnées cartésiennes avec une trajectoire rectiligne.**

    :param p2:          coordonnées cartésiennes en m et en degré [x,y,z,rx,ry,rz] du point cible
    :type  p2:          liste de flottant
    :param ot:          valeur d'ouverture de la pince en pourcentage (de 0 pour fermée à 100 pour complètement ouverte). 'None' pour garder la valeur courante.
    :type  ot:          entier, optionel
    :param resol:       résolution de la trajectoire polynomiale en nombre de points par mètre, par défaut 1000 pt/m, à ne pas définir si nb_pt l'est)
    :type  resol:       entier, optionel
    :param nb_pt:       nombre de points de la trajectoire polynomiale, à ne pas définir si resol l'est
    :type  nb_pt:       entier, optionel
    :param draw:        affiche la trajectoire, uniquement en mode bloquant
    :type  draw:        booléen, optionel
    :param marker:      type de marque : 'point', 'axis' (repère), 'pose' (repère pour la sauvegarde d'une trajectoire) ou None pour ignorer
    :type  marker:      chaîne de caractères ou None, optionel
    :param thickness:   épaisseur de la trajectoire, taille des points (facteur d'échelle)
    :type  thickness:   flottant, optionel
    :param tickskip:    fréquence d'apparition des points de la trajectoire, tous les x ticks (60 fps)
    :type  tickskip:    entier, optionel
    :param solver:      choix du solveur de cinématique inverse (IK) : 'blender' ou 'rtb'
    :type  solver:      chaîne de caractères, optionel
    :param ctrl:        type de controle du solveur : 'delta', 'near', None pour pas de contrôle (par défaut)
    :type  ctrl:        chaîne de caractères, optionel
    :param iterations:  nombre d'itérations pour le calcul
    :type  iterations:  entier, optionel
    :param orientation: contrainte aussi sur l'orientation
    :type  orientation: booléen, optionel
    :param error_max:   marge d'erreur sur la validation du solveur en m
    :type  error_max:   flottant, optionel
    :param step:        valeur du seuil en degrée pour la recherche des écarts de coordonnées ne dépassant ce seuil
    :type  step:         entier, optionel
    :param wait:        mode bloquant, attente de la fin du trajet
    :type  wait:        booléen, optionel
    :param cont:        mode trajectoire continue (tous les mouvements arrivent en même temps), uniquement en mode bloquant
    :type  cont:        booléen, optionel
    :param verbose:     affiche les informations du solveur de cinématique inverse
    :type  verbose:     booléen, optionel
    :param force:       permet de dépasser les limites angulaires des moteurs
    :type  force:       booléen, optionel

    :return:  mouvement réalisé sans erreur, tableau des coordonnées articulaires de chaque point en degrée
    :rtype:   booléen, liste de liste de flottant

    :exemple: success, qtraj = move_p([0.0277, -0.4114, 0.6295, -2.47,-23.18, 32.15], nb_pt=10, draw=True, marker='pose', solver='rtb', ctrl='near', verbose=True)

    .. topic:: Choix du solveur de cinématique inverse (IK) :
    
        - **blender** : solveur IK Blender (iTaSC IK) -> calcul plus rapide (par défaut).
        - **rtb** : solveur Robotic ToolBox for Python (RTB) -> calcul plus précis.

    .. topic:: **Solveur IK Blender** : paramétrage du solveur iTaSC IK de Blender

        - **ctrl=None :** premier calcul considéré comme suffisant (par défaut).
        - **ctrl='delta' :** minimise l'écart entre la position trouvée et la position théorique.
        - **ctrl='near' :** minimise le déplacement articulaire par rapport aux coordonnées courantes.
        - **iterations :** nombre d'itérations pour le calcul (entier, 2 par défaut).
        - **orientation :** contrainte aussi sur l'orientation (binaire, par défaut).
        - **error_max :** marge d'erreur sur la validation du solveur (réel, 0.001 m par défaut)

    .. topic:: **Solveur RTB** : paramétrage du solveur Robotic ToolBox for Python :

        - **ctrl=None :** premier calcul considéré comme suffisant (par défaut).
        - **ctrl='delta' :** minimise l'écart entre la position trouvée et la position théorique.
        - **ctrl='near' :** minimise le déplacement articulaire par rapport aux coordonnées courantes.
        - **ctrl='step' :** valide une solution si l'écart entre le début et la solution ne dépasse pas un seuil.
        - **iterations :** nombre d'itérations pour le calcul (entier, 20 par défaut pour 'delta' et 'near', 50 par défaut pour 'step').
        
    .. hint:: Avec le solveur RTB : 

        - il y a toujours la contrainte de l'orientation,
        - la marge d'erreur est à 0.
    """

    ##
    # Coordonnées opérationnelles
    ##

    # Contrôle des arguments - Position cible (p2)
    head_txt="Solveur IK Blender : move_p(...) : " if solver=='blender' else "Solveur RTB : move_p(...) : "
    if solver=='rtb': orientation=True
    
    scene.objects['Solver-text']['Text']="Solveur prêt."
    if p2 is None:
        print(head_txt+"erreur sur le point "+str(p2)+" : liste des coordonnées opérationnelles indéfinie")
        return False, None
    if len(p2)<6:
        print(head_txt+"erreur sur le point "+str(p2)+" : il manque des coordonnées opérationnelles -> [x,y,z,rx,ry,rz]")
        return False, None
    if len(p2)>6:
        print(head_txt+"erreur sur le point "+str(p2)+" : il y a trop de coordonnées opérationnelles -> [x,y,z,rx,ry,rz]")
        return False, None
    
    # Position courante (p1, q1)
    q1=[]
    for i_txt in motors:
        q1.append(scene.objects[i_txt]['q'])
    p1 = rtb_fkine(q1, unit="deg") # RTB est plus fiable
    if solver=='blender':
        ikb_fkine(q1, unit="deg") # Positionner l'armature sur p1
    dist = math.sqrt((p2[0]-p1[0])**2+(p2[1]-p1[1])**2+(p2[2]-p1[2])**2) # Distance
    if dist==0:
        print(head_txt+"erreur sur le point "+str(p2)+" : le point cible est confondu avec le point courant")
        return False, None
    ot1 = scene.objects['Moteur Pince']['value']
    if ot is None:
        ot=ot1

    ##
    # Calcul
    ##

    # Nombre d'itérations par défaut
    if iterations is None:
        if solver=='blender':
            iterations=2
        if solver=='rtb':
            if ctrl=='delta':
                iterations=20
            if ctrl=='near':
                iterations=20
            if ctrl=='step':
                iterations=50

    # Vitesse et résolution
    # speed=0.25 # Vitesse 1m/4s
    t = None
    speed=0.1 # Vitesse 1m/10s
    qd_max = 90 # Vitesse maxi des moteurs 1,57 rad/s ou 90 deg/s
    dist  = math.sqrt((p2[0]-p1[0])**2+(p2[1]-p1[1])**2+(p2[2]-p1[2])**2) # Distance
    angle = math.sqrt((p2[3]-p1[3])**2+(p2[4]-p1[4])**2+(p2[5]-p1[5])**2) # Angle à parcourir (transposition angle -> distance)
    if t is None:
        t = dist/speed
    if speed is None:
        speed = dist/t
    if nb_pt==None:
        nb_pt = round(dist*resol)
    if resol==None:
        resol = nb_pt/dist
    dt=t/nb_pt
    q_step_max = (qd_max*t)/nb_pt # Pas maxi des moteurs en rad/point
    if verbose:
        print("\033[0;45m"+head_txt+"d : "+format(dist,'.4f')+" m ; t : "+format(t,'.3f')+" s ; v : "+format(speed,'.2f')+" m/s ; "+
              "contrôle="+str(ctrl)+" ; orientation="+str(orientation)+" ; error_max="+str(error_max)+" ; wait="+str(wait)+" ; cont="+str(cont)+" ; force="+str(force))

    # Go !
    q_prev  , p_prev   = q1, p1
    q_result, p_result = [],[]
    for i_pt in range (nb_pt):
        p = [p1[0]+(((p2[0]-p1[0])/nb_pt)*(i_pt+1)),
             p1[1]+(((p2[1]-p1[1])/nb_pt)*(i_pt+1)),
             p1[2]+(((p2[2]-p1[2])/nb_pt)*(i_pt+1)),
             p1[3]+(((p2[3]-p1[3])/nb_pt)*(i_pt+1)),
             p1[4]+(((p2[4]-p1[4])/nb_pt)*(i_pt+1)),
             p1[5]+(((p2[5]-p1[5])/nb_pt)*(i_pt+1))]
        q, error_limit, error_ctrl, i, error = ikine_ctrl(p, q_prev, q_step_max, i_pt, nb_pt, dt, solver, ctrl, iterations, orientation, step, error_max, verbose)
        if error_limit or error_ctrl:
            break
        q_result.append(q)
        p_result_i = rtb_fkine(q, unit="deg") # RTB est plus fiable
        p_result.append(p_result_i)
        q_prev = q.copy()
        p_prev = p_result_i.copy()

    ##
    # Affichage
    ##

    # Message serveur
    if error_limit:
        scene.objects['Solver-text']['Text']=head_txt+"pas de solution géométrique pour le trajet au point "+str(i_pt+1)+"/"+str(nb_pt)
    if error_ctrl:
        scene.objects['Solver-text']['Text']=head_txt+"pas de solution continue pour le trajet au point "+str(i_pt+1)+"/"+str(nb_pt)
    if verbose:
        if error_limit or error_ctrl:
            distance = round(math.sqrt((p2[0]-p_prev[0])**2+(p2[1]-p_prev[1])**2+(p2[2]-p_prev[2])**2), PRECISION_DIST)
            print("\033[0;41m"+head_txt+"erreur sur le point "+str(i_pt+1)+" : pas de solution géométrique"+"\033[0m")
            print("\033[0;41m"+head_txt+"d : "+format(dist,'.4f')+" m ; t : "+format(t,'.3f')+" s ; v : "+format(speed,'.2f')+" m/s ; "+
                  "contrôle="+str(ctrl)+" ; orientation="+str(orientation)+" ; error_max="+str(error_max)+" ; wait="+str(wait)+" ; cont="+str(cont)+" ; force="+str(force))
            print_p("\033[0;41m"+head_txt+"position de départ p1  ", p1, "\033[0m")
            print_p("\033[0;41m"+head_txt+"position cible     p2  ", p2, "\033[0m")
            print_p("\033[0;41m"+head_txt+"position d'arrivée p   ", p_prev, "\033[0m")
            print_q("\033[0;41m"+head_txt+"position de départ q1  ", q1, "\033[0m")
            print_q("\033[0;41m"+head_txt+"position d'arrivée q   ", q_prev, "\033[0m")
            print("\033[0;41m"+head_txt+  "distance entre p et p2  : "+str(distance)+"\033[0m")
            print("\033[0;41m"+head_txt+  "erreur entre p et p2    : "+str(round(error,5))+"\033[0m")
            if dist != 0: # Progression en position en %
                dist_stop = math.sqrt((p2[0]-p_prev[0])**2+(p2[1]-p_prev[1])**2+(p2[2]-p_prev[2])**2)
                print("\033[0;41m"+head_txt+"progression distance    :", format(((dist-dist_stop)/dist)*100, '.2f'), "%\033[0m")
            if angle != 0: # Progression angulaire en %
                angle_stop = math.sqrt((p2[3]-p_prev[3])**2+(p2[4]-p_prev[4])**2+(p2[5]-p_prev[5])**2)
                print("\033[0;41m"+head_txt+"progression angulaire   :", format(((angle-angle_stop)/angle)*100, '.2f'), "%\033[0m")
        else:
            distance = round(math.sqrt((p2[0]-p_prev[0])**2+(p2[1]-p_prev[1])**2+(p2[2]-p_prev[2])**2), PRECISION_DIST)
            print("\033[0;42m"+head_txt+"d : "+format(dist,'.4f')+" m ; t : "+format(t,'.3f')+" s ; v : "+format(speed,'.2f')+" m/s ; "+
                  "contrôle="+str(ctrl)+" ; orientation="+str(orientation)+" ; error_max="+str(error_max)+" ; wait="+str(wait)+" ; cont="+str(cont)+" ; force="+str(force))
            print_p("\033[0;42m"+head_txt+"position de départ p1  ", p1, "\033[0m")
            print_p("\033[0;42m"+head_txt+"position cible     p2  ", p2, "\033[0m")
            print_p("\033[0;42m"+head_txt+"position d'arrivée p   ", p_prev, "\033[0m")
            print_q("\033[0;42m"+head_txt+"position de départ q1  ", q1, "\033[0m")
            print_q("\033[0;42m"+head_txt+"position d'arrivée q   ", q_prev, "\033[0m")
            print("\033[0;42m"+head_txt+  "distance entre p et p2  :", str(distance)+"\033[0m")
            print("\033[0;42m"+head_txt+  "erreur entre p et p2    :", str(round(error,5))+"\033[0m")

    # Point de départ en jaune
    if draw and marker is not None:
        if marker=='point':
            draw_point(p1, color_yellow)
        if marker=='axis' or marker=='pose':
            draw_axis(p1, color_yellow)
  
    # Pas de points
    if len(q_result)==0:
        return False, q_result

    # Points intermédiaires et d'arrivé
    for i in range(len(q_result)):
        bras.move_q(q_result[i], ot, wait=wait, cont=cont, draw=draw, color=color, thickness=thickness, tickskip=tickskip, force=force)
        p_i=[p_result[i][0],p_result[i][1],p_result[i][2],p_result[i][3],p_result[i][4],p_result[i][5]]
        if draw and i < len(q_result)-1:
            if marker=='point':
                draw_point(p_i, color_green)
            if marker=='axis':
                draw_axis(p_i, color_green)
            if marker=='pose':
                add_pose(None, q_result[i], ot, p_i, color_green, prop='', mode='move_p') # FIXME : prop plus tards
        if draw and i == len(q_result)-1: # Point d'arrivée
            if error_limit or error_ctrl :
                if marker=='point':
                    draw_point(p_i, color_purple)
                if marker=='axis':
                    draw_axis(p_i, color_purple)
                if marker=='pose':
                    add_pose(None, q_result[i], ot, p_i, color_purple, prop='', mode='move_p') # FIXME : prop plus tards

    # Point cible
    if error_limit or error_ctrl:
        if draw and marker is not None:
            if marker=='point':
                draw_point(p2, color_red)
            if marker=='axis' or marker=='pose':
                draw_axis(p2, color_red)
        else:
            draw_point(p2, color_red) # Point cible non atteint quand même affiché
    else:
        if draw and marker is not None:
            if marker=='point':
                draw_point(p2, color_green)
            if marker=='axis':
                draw_axis(p2, color_green)
            if marker=='pose':
                add_pose(None, q_result[i], ot, p2, color_green, prop='', mode='move_p') # FIXME : prop plus tards

    # Résultats
    sleep(TEMPO_RENDER) # TEMPO : crash ici ... c'est déjà arrivé
    scene.objects['Armature ikb'].setVisible(False, True)
    scene.objects['Rc ikb'].setVisible(False, True)
    if error_limit or error_ctrl:
        return False, q_result
    else:
        return True, q_result

# Récupérer les coordonnées cartésiennes à partir des coordonnées articulaires
def get_p(*q, solver='blender'):
    """
    **Calcul les coordonnées cartésiennes à partir des coordonnées articulaires.**

    :param q:      coordonnées articulaires en degré [q1,q2,q3,q4,q5,q6]
    :type  q:      liste de flottant
    :param solver: choix du solveur de cinématique inverse (IK) : 'blender' ou 'rtb'
    :type  solver: chaîne de caractères, optionel

    :return: coordonnées cartésiennes de la pose trouvée
    :rtype:  liste de flottant

    :exemple: p = get_p([10,70,20,30,40,20])

    .. hint:: Si les coordonnées articulaires (q) sont ignorées, c'est la position courante qui est prise en compte.

    .. topic:: Choix du solveur de cinématique inverse (IK) :
    
        - **blender** : solveur IK Blender (iTaSC IK) -> calcul plus rapide (par défaut).
        - **rtb** : solveur Robotic ToolBox for Python (RTB) -> calcul plus précis.
    """

    # Contrôle des arguments et position courante
    if len(q)==0:
        q=[]
        for i_txt in motors:
            q.append(scene.objects[i_txt]['q'])
    else:
        q=q[0]    

    # Résultat
    if solver=='blender':
        p_list = ikb_fkine_slv(q, unit="deg")
    if solver=='rtb':
        p_list = rtb_fkine(q, unit="deg")
    p_list2=[round(p_list[0],PRECISION_DIST) , round(p_list[1],PRECISION_DIST) , round(p_list[2],PRECISION_DIST),
             round(p_list[3],PRECISION_ANGLE), round(p_list[4],PRECISION_ANGLE), round(p_list[5],PRECISION_ANGLE)]
    return p_list2

##
# Coordonnées opérationnelles en utilisant le moteur géométrique de Blender
##

# Récupérer les coordonnées opérationnelles de la position actuelle
# Le modèle Blender comporte des approximations sur le positionnement au niveau des pivots
# Ce qui génère des erreurs, cumulées elles ne peuvent pas être négligées.

def get_p_blender():
    """
    **Retourne la liste des coordonnées cartésiennes courantes calculées avec le moteur géométrique de Blender.**

    :return: coordonnées cartésiennes de la pose trouvée
    :rtype:  liste de flottant

    :exemple: p = get_p_blender()

    .. caution:: Le modèle Blender comporte des approximations sur le positionnement au niveau des pivots. Ce qui génère des erreurs, cumulées elles ne peuvent pas être négligées.
    """

    obj=scene.objects['Rp']
    position = obj.worldPosition @ scene.objects['Rm'].worldTransform # Position de Rp dans le repère Rm
    x=round(position.x, 4)
    y=round(position.y, 4)
    z=round(position.z, 4)
    rx=round(math.degrees(obj.worldOrientation.to_euler().x) - math.degrees(scene.objects['Rm'].worldOrientation.to_euler().x), 2)
    ry=round(math.degrees(obj.worldOrientation.to_euler().y) - math.degrees(scene.objects['Rm'].worldOrientation.to_euler().y), 2)
    rz=round(math.degrees(obj.worldOrientation.to_euler().z) - math.degrees(scene.objects['Rm'].worldOrientation.to_euler().z), 2)
    return [x,y,z,rx,ry,rz]

##
# Commandes moteur bas niveau
# - numéro de l'axe de 1 à 6
# - order : 1 pour sens horaire, -1 sens anti-horaire et 0 pour stop
##

def mot(motor_i, order):
    """
    **Mettre en rotation un moteur.**

    :param motor_i: numéro du moteur (de 1 à 6)
    :type  motor_i: entier
    :param order:   ordre : 1 pour sens horaire, -1 pour sens anti-horaire et 0 pour stop
    :type  order:   entier

    :exemple: mot(1,1)
    """

    obj=scene.objects[motors[motor_i-1]]
    obj['q_forcing']=True
    if order == 1:
        obj['cw']=True
        obj['acw']=False
    if order == -1:
        obj['acw']=True
        obj['cw']=False
    if order == 0:
        obj['acw']=False
        obj['cw']=False


def set_speed(motor_i, speed_setting=None, speed_max=None):
    """
    **Définir les réglages de vitesse d'un/des moteurs.**

    :param motor_i:       numéro du moteur: de 1 à 6 et 0 pour l'ensemble des moteurs
    :type  motor_i:       entier
    :param speed_setting: réglage de la vitesse en rad/s, None pour ignorer
    :type  speed_setting: flottant, optionel
    :param speed_max:     réglage de la vitesse maximum en rad/s, utilisé quand la vitesse est variable, None pour ignorer
    :type  speed_max:     flottant, optionel
  
    :exemple: set_speed(0, 3, 3)
    """

    if motor_i==0:
        for i in range (len(motors)):
            if speed_setting is not None:
                scene.objects[motors[i]]['speed_setting'] = speed_setting
            if speed_max is not None:
                scene.objects[motors[i]]['speed_max'] = speed_max
    else:
        if speed_setting is not None:
            scene.objects[motors[motor_i-1]]['speed_setting'] = speed_setting
        if speed_max is not None:
            scene.objects[motors[motor_i-1]]['speed_max'] = speed_max

def reset_speed(motor_i):
    """
    **Réinitialiser les vitesses d'un/des moteurs.**

    :param motor_i: numéro du moteur (de 1 à 6) ou 0 pour l'ensemble des moteurs
    :type  motor_i: entier

    :exemple: reset_speed(0)

    .. hint:: La vitesse et la vitesse max sont définit à 100 pas/s. Avec un pas angulaire est de 1,8 deg, les vitesses sont réinitilisées à 1,57 rad /s (1/2 tr/s).
    """
    
    if motor_i==0:
        for i in range (len(motors)):
            scene.objects[motors[i]]['speed_setting'] = 1.57
            scene.objects[motors[i]]['speed_max'] = 1.57
    else:
        scene.objects[motors[motor_i-1]]['speed_setting'] = 1.57
        scene.objects[motors[motor_i-1]]['speed_max'] = 1.57
    
def get_speed(motor_i):
    """**Retourne les vitesses d'un/des moteurs.**

    :param motor_i: numéro du moteur (de 1 à 6) ou 0 pour l'ensemble des moteurs
    :type  motor_i: entier

    :return: vitesse courante en rad/s, réglage de la vitesse en rad/s, réglage de la vitesse maximum en rad/s, utilisé quand la vitesse est variable
    :rtype:  flottant, flottant, flottant

    :exemple: speed, speed_setting, speed_max = get_speed(2)
    """

    data = ['speed','speed_setting','speed_max']
    speed_list = []
    if motor_i == 0:
        speed_list_motor =[]
        for j in range (len(motors)):
            speed_list_motor = []
            for i_txt in data:
                speed_list_motor.append(scene.objects[motors[j]][i_txt])
            speed_list.append(speed_list_motor)
    else:
        for i_txt in data:
            speed_list.append(scene.objects[motors[motor_i-1]][i_txt])
    return speed_list

##
# Pince
##

def set_ot(value):
    """
    **Définir l'ouverture la pince.**

    :param value: valeur d'ouverture en pourcentage (de 0 pour fermée à 100 pour complètement ouverte)
    :type  value: entier

    :exemple: set_ot(60)
    """
    
    obj = scene.objects['Moteur Pince']
    if value != obj['value']:
        obj['value_target'] = value
        obj['activated'] = True

def get_ot():
    """
    **Retourne la valeur courante d'ouverture de la pince.**

    :return: valeur d'ouverture en pourcentage(de 0 pour fermée à 100 pour complètement ouverte)
    :rtype:  entier

    :exemple: value = get_ot()
    """
    return scene.objects['Moteur Pince']['value']


###############################################################################
# Trajectoire 
###############################################################################

##
# Pose
##

# Ajouter une pose
def add_pose(label, q, ot, p=None, color=color_white, prop='', mode='unspecified'):
    """
    **Ajouter une pose à la trajectoire courante.**

    :param label: texte de l'étiquette, None pour le numéro chrono, dans ce cas elle ne sera pas affichée
    :type  label: chaîne de caractères ou None
    :param q:     coordonnées articulaires [q1,q2,q3,q4,q5,q6] en degré (réel)
    :type  q:     liste de flottant
    :param ot:    valeur d'ouverture de la pince en pourcentage (de 0 pour fermée à 100 pour complètement ouverte)
    :type  ot:    entier
    :param p:     coordonnées cartésiennes [x,y,z,rx,ry,rz] en m et en degré, None pour son calcul à partir de q
    :type  p:     liste de flottant, optionel
    :param color: couleur du point, blanc par défaut
    :type  color: RGBA tuple (Rouge Vert Bleu Alpha), optionel
    :param prop:  propriétés de la trajectoire
    :type  prop:  chaîne de caractères, optionel
    :param mode:  mode de saisie du point
    :type  mode:  chaîne de caractères, optionel

    :exemple: add_pose('1',[-10,-12.0,0,0,0,0],33,,'thickness=0.5,tickskip=0','manual_q')

    .. hint:: Si la pose p (coordonnées cartésiennes) est None, elle sera calculée à partir des coordonnées articulaires avec le solveur RTB. 
        Les coordonnées cartésiennes ne servent qu'à l'affichage, ce sont uniquement les coordonnées articulaires qui pilotent les moteurs.

    .. topic:: Propriétés de la trajectoire :
    
        - **color :** couleur des points de la trajectoire (RGBA tuple (Rouge Vert Bleu Alpha), color_yellow si None (par défaut)).
        - **thickness :** épaisseur de la trajectoire, taille des points (flottant, facteur d'échelle, 1 par défaut).
        - **tickskip :** fréquence d'apparition des points de la trajectoire, tous les x ticks (60 fps) (entier, 3 par défaut).

    .. topic:: Mode de saisie du point : 

        - **'get_q' :** saisie lors du calcul du point par la fonction 'get_q(...)'
        - **'manual_q' :** saisie manuelle par la commande directe des moteurs
        - **'manual_rc' :** saisie manuelle par point cible
        - **'move_p' :** saisie lors de la génération de trajectoire par la fonction 'move_p(...)'
        - **'unspecified' :** mode de saisie inconnu (par défaut)
    """
    if p is None:
        p = rtb_fkine(q, unit="deg") # RTB est plus fiable
        tempo(0.01)
    scene.objects['Commands']['draw_rep']   = True
    scene.objects['Commands']['draw_p']     = p
    scene.objects['Commands']['draw_color'] = color
    if label is None:
        scene.objects['Commands']['draw_text'] = str(scene.objects['System']['move_draw_rep'])
    else:
        scene.objects['Commands']['draw_text'] = label
    data=[]
    data.append(q)
    data.append(ot)
    data.append(prop)
    data.append(mode)
    scene.objects['Commands']['draw_data'] = data
    scene.objects['Commands']['draw']      = True # Passage du thread du cycle pour aller au thread de blender
    while scene.objects['Commands']['draw']:
        # print("add_pose")
        # sleep(0.01)
        tempo(0.01)

# Ajouter plusieurs poses
def add_traj(traj):
    """
    **Ajouter une trajectoire pré-calculée à la trajectoire courante.**

    :param traj: liste des points : [['label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'],[...],...].
    :type  traj: liste

    :Exemple:    
    .. code:: python
            :number-lines:

            traj=[]
            traj.append(['1', [-141.56, 29.1,  84.13, 0.32, 67.14, -141.69], 33, [0.1971,  -0.24665, 0.12143, -179.25,  0.13, -0.13], '', ''])
            traj.append(['2', [-141.41, 38.07, 89.09, 0.37, 53.22, -141.63], 33, [0.19845, -0.24694, 0.05458, -179.25,  0.13, -0.13], '', ''])
            traj.append(['3', [-141.41, 38.07, 88.36, 0.37, 53.22, -141.63], 17, [0.20111, -0.25034, 0.05688, -179.82, -0.32, -0.13], '', ''])
            add_traj(traj)
    """
    scene.objects['Commands']['draw_traj'] = True
    scene.objects['Commands']['draw_data'] = traj
    scene.objects['Commands']['draw']      = True # Passage du thread du cycle pour aller au thread de blender
    while scene.objects['Commands']['draw']:
        # print("add_traj")
        sleep(0.01)

##
# Mouvement
##

# Déplace le robot avec la trajectoire pré-calculée
def move_traj(draw=False, wait=True, cont=True):
    """
    **Déplacer le robot avec la trajectoire courante.**

    :param draw:   affiche un trajectoire entre deux points, uniquement en mode bloquant
    :type  draw:   booléen, optionel
    :param wait:   mode bloquant, attente de la fin du trajet
    :type  wait:   booléen, optionel
    :param cont:   mode trajectoire continue (tous les mouvements arrivent en même temps), uniquement en mode bloquant
    :type  cont:   booléen, optionel

    :Exemple: move_traj(draw=True)

    .. hint:: La trajectoire courante correspond à l'ensemble des poses présentes à l'écran.
    .. caution:: L'ordre des poses est l'ordre de saisie des poses.
    """
    obj = scene.objects['System']
    for i in range (obj['move_draw_rep']):
        obj_rep = scene.objects['rep'+str(i)]
        eval("move_q(obj_rep['q'], obj_rep['ot'], draw=draw, wait=wait, cont=cont, "+str(obj_rep['prop'])+")")
    # for i in range (len(traj)):
    #     if len(traj[i][4])>0:
    #         eval("move_q(traj[i][1], ot=traj[i][2], draw=draw, "+traj[i][4]+", wait=wait, cont=cont)")
    #     else:
    #         move_q(traj[i][1], ot=traj[i][2], draw=draw, wait=wait, cont=cont)

##
# Objet
##

def object_clear(*name):
    """
    **Cacher le ou les objets.**
    
    :param name: nom de l'objet, ignoré pour cacher l'ensemble des objets
    :type  name: chaîne de caractères, optionel

    :exemple: object_clear('pavé')
    """
    
    if len(name)==0:
        twin_traj.object_clear()
    else:
        twin_traj.object_clear(name[0])

# Ajouter un objet
def add_object(*name, p=None):
    """
    **Ajouter un objet.**

    :param name: nom de l'objet, ignoré pour afficher tous les objets
    :type  name: chaîne de caractères, optionel
    :param p:    emplacement de l'objet [x,y,z,rx,ry,rz] en m et en degré, None pour réinitialiser à l'emplacement d'origine
    :type  p:    liste de flottant, optionel

    :exemple: add_object('pavé')

    .. note:: La liste des objets disponibles est accessible avec la fonction list_object().
    """
    
    if len(name)==0:
        twin_traj.object_show()
    else:
        twin_traj.object_show(name[0])
        if p is not None:
            set_object(name[0], p)

# Retourne la liste des objets
def list_object():
    """
    **Retourne la liste des objets de la scène.**
    
    :return: noms d'objet pouvant être affichés sur la scène
    :rtype:  liste de chaîne de caractères

    :exemple: print(list_object())

    .. hint:: Le répertoire 'object' contient tous les objets qui seront importés automatiquement dans la scène. Les objets sont au format Blender (fichier *.blend).
    """
    return scene.objects['System']['traj_objects']

# Positionne l'objet
def set_object(name, p=None):
    """
    **Positionne l'objet.**

    :param name: nom de l'objet
    :type  name: chaîne de caractères
    :param p:    emplacement de l'objet [x,y,z,rx,ry,rz] en m et en degré, None pour réinitialiser à l'emplacement d'origine
    :type  p:    liste de flottant, optionel

    :exemple: set_object('pavé', [1.25, -0.25, 0.05, 0, 0, 0])
    """

    obj = scene.objects[name]
    if obj.visible:
        if p is None:
            obj.worldTransform = obj['init']
        else:
            vec_loc = mathutils.Vector((p[0],p[1],p[2]))
            vec_rot = mathutils.Euler((math.radians(p[3]), math.radians(p[4]), math.radians(p[5])))
            vec_sca = mathutils.Vector((1,1,1))
            obj.worldTransform = mathutils.Matrix.LocRotScale(vec_loc, vec_rot, vec_sca)

# Retourne la liste des objets
def get_object(name):
    """
    **Retourne l'état de l'objet.**

    :param name: nom de l'objet
    :type  name: chaîne de caractères

    :return: état saisie de l'objet (True pour saisie, False pour libre), emplacement actuel de l'objet [x,y,z,rx,ry,rz] en m et en degré, emplacement actuel de l'objet sous forme de matrice de transformation
    :rtype:  bolléen, liste de flottant, matrice 4x4 de flottant

    :exemple: taken, p, mat = get_object('pavé')
    """
    
    obj = scene.objects[name]
    if obj.visible:
        p=[]
        p.append(obj.worldPosition.x)
        p.append(obj.worldPosition.y)
        p.append(obj.worldPosition.z)
        p.append(obj.worldOrientation.to_euler().x)
        p.append(obj.worldOrientation.to_euler().y)
        p.append(obj.worldOrientation.to_euler().z)
        return obj['taken'], p, obj.worldTransform
    else:
        return None, None, None

###############################################################################
# Affichage
###############################################################################

# Définition du robot Denavit-Hartenberg modifié
def get_robot():
    """
    **Retourne la définition du robot avec la convention Denavit-Hartenberg modifié.**

    :return: tableau décrivant les différents segments).
    :rtype:  chaîne de caractères

    :exemple: print(get_robot())

    .. hint:: C'est le modèle du robot utilisé par le solveur Robotics Toolbox for Python (RTB). 

    .. note:: Le solveur iTaSC IK de Blender (solveur interne) lui est basé sur le positionnement des segments dans la scène 3D. Or le modèle Blender comporte des
       approximations sur le positionnement au niveau des pivots de l'armature. Ce qui génère des erreurs, cumulées elles ne peuvent pas être négligées.

       Quand le choix du solveur n'est pas proposé, le solveur utilisé est le solveur Robotics Toolbox for Python (à la place donc du solveur iTaSC IK de Blender).

       Le solveur iTaSC IK de Blender est principalement utilisé lors de l'ancrage entre le bras et le point cible positionnée à la manette (mode_rc). 
    """

    return rtb_get_robot()

# Effacer les points, repères et étiquettes
def draw_clear(pt=True, rep=True, lbl=True):
    """
    **Effacer les points, repères et étiquettes.**

    :param pt:  efface les points
    :type  pt:  booléen, optionel
    :param rep: efface les repères
    :type  rep: booléen, optionel
    :param lbl: efface les étiquettes
    :type  lbl: booléen, optionel

    :exemple: draw_clear()
    """
    
    scene.objects['Commands']['clear_pt']  = pt
    scene.objects['Commands']['clear_rep'] = rep
    scene.objects['Commands']['clear_lbl'] = lbl
    scene.objects['Commands']['draw']=True # Passage du thread du cycle pour aller au thread de blender
    while scene.objects['Commands']['draw']:
        # print("draw_clear")
        sleep(0.01)


# Afficher un point
def draw_point(p, color=color_yellow, label=None):
    """
    **Afficher un point.**
    
    **draw_point([x,y,z,rx,ry,rz], color=color_yellow, label=None)**

    :param p:     coordonnées cartériennes du point en m et en degré
    :type  p:     liste de flottant
    :param color: couleur du point, jaune par défaut
    :type  color: RGBA tuple (Rouge Vert Bleu Alpha), optionel
    :param label: texte de l'étiquette, None pour ignorer
    :type  label: chaîne de caractères, optionel

    :exemple: draw_point([0.0277, -0.4114, 0.6295, -2.47,-23.18, 32.15], label='1')

    .. caution:: Un point n'est pas une pose, le point ajouté n'est donc pas intégré à la trajectoire courante. Pour cela, il faut utiliser la fonction add_pose(...)
    """
    
    scene.objects['Commands']['draw_pt']    = True
    scene.objects['Commands']['draw_p']     = p
    scene.objects['Commands']['draw_color'] = color
    scene.objects['Commands']['draw_text']  = label
    scene.objects['Commands']['draw']       = True # Passage du thread du cycle pour aller au thread de blender
    while scene.objects['Commands']['draw']:
        print("draw_point")
        sleep(0.01)

# Afficher un repère
def draw_axis(p, color=color_white, label=None):
    """
    **Afficher un repère.**

    :param p:     coordonnées cartériennes du repère en m et en degré
    :type  p:     liste de flottant
    :param color: couleur du point du repère, blanc par défaut
    :type  color: RGBA tuple (Rouge Vert Bleu Alpha), optionel
    :param label: texte de l'étiquette, None pour ignorer
    :type  label: chaîne de caractères, optionel
    
    :exemple: draw_axis([0.0277, -0.4114, 0.6295, -2.47,-23.18, 32.15], label='1')

    .. caution:: Un repère n'est pas une pose, le repère ajouté n'est donc pas intégré à la trajectoire courante. Pour cela, il faut utiliser la fonction add_pose(...)
    """
    
    scene.objects['Commands']['draw_rep']   = True
    scene.objects['Commands']['draw_p']     = p
    scene.objects['Commands']['draw_color'] = color
    scene.objects['Commands']['draw_text']  = label
    scene.objects['Commands']['draw_data']  = []
    scene.objects['Commands']['draw']       = True # Passage du thread du cycle pour aller au thread de blender
    while scene.objects['Commands']['draw']:
        # print("draw_axis")
        sleep(0.01)


# Recolorisation d'une trajectoire polyligne : dégradé du jaune vers le vert clair
def draw_recolor(i_pt_debut, i_pt_fin):
    # print ("recolor", i_pt_debut, i_pt_fin)
    # color_yellow          = (0.800, 0.619, 0.021, 1) # Jaune
    # color_green_light     = (0.246, 0.687, 0.078, 1) # Vert clair
    # color_green_lesslight = (0.149, 0.406, 0.049, 1) # Vert moins clair
    # color_green           = (0.083, 0.223, 0.028, 1) # Vert 
    nb_pt = i_pt_fin-i_pt_debut+1
    for i in range(nb_pt):
        delta_r = (abs(0.800-0.149)/nb_pt)*i
        delta_g = (abs(0.619-0.406)/nb_pt)*i
        delta_b = (abs(0.021-0.049)/nb_pt)*i
        scene.objects['p'+str(i_pt_debut+i)].color = (0.800-delta_r, 0.619-delta_g, 0.021+delta_b, 1)

###############################################################################
# Fichier
###############################################################################

# Sauvegarder la trajectoire dans un fichier csv
def save_traj(traj, filename):
    """
    **Sauvegarder la trajectoire dans un fichier csv.**

    :param traj:     liste des points : [['label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'],[...],...].
    :type  traj:     liste
    :param filename: nom du fichier
    :type  filename: chaîne de caractères

    :Exemple:    
    .. code:: python
            :number-lines:

            traj=[]
            traj.append(['1', [-141.56, 29.1,  84.13, 0.32, 67.14, -141.69], 33, [0.1971,  -0.24665, 0.12143, -179.25,  0.13, -0.13], '', ''])
            traj.append(['2', [-141.41, 38.07, 89.09, 0.37, 53.22, -141.63], 33, [0.19845, -0.24694, 0.05458, -179.25,  0.13, -0.13], '', ''])
            traj.append(['3', [-141.41, 38.07, 88.36, 0.37, 53.22, -141.63], 17, [0.20111, -0.25034, 0.05688, -179.82, -0.32, -0.13], '', ''])
            save_traj(traj, 'traj.csv')

    """
    twin_traj.traj_save_csv(traj, filename=filename)

# Charger la trajectoire à partir d'un fichier csv
def load_traj(filename):
    """
    **Charger une trajectoire à partir d'un fichier csv.**

    :param filename: nom du fichier
    :type  filename: chaîne de caractères

    :return: liste des points : [['label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'],[...],...].
    :rtype:  liste 

    :exemple: traj=load_traj('traj.csv')
    """
    return twin_traj.traj_load_csv(filename=filename)
    
###############################################################################
# Boutons poussoirs
# Pour les entrées le forçage empèche leur activation par la physique du modèle 3D, les intérations utilisateur ou par Arduino
###############################################################################

# Compte-rendu du bouton d'arrêt d'urgence
def aru():
    if scene.objects['ARU']['activated'] or scene.objects['ARU']['activated_real']:
        return True
    return False

###############################################################################
# Jumelage
###############################################################################

# Activer le jumelage
def jumeau(brochage):
    board=twin_open(brochage)
    if board is None:
        return None

    # Brochage -> Panneau IO
    for i in range (1,19,1):
        idx= "IO_var-"+str(i)
        if scene.objects[idx]['visible']: 
            if scene.objects[idx+"-r_activ"]['pluged']:
                scene.objects[idx+"-r_activ-colbox"].restorePhysics()
                scene.objects[idx+"-r_value-colbox"].restorePhysics()
                scene.objects[idx+"-r_activ"].color = color_io_activate
                scene.objects[idx+"-r_activ"]['redraw'] = True # Forcer le redraw de l'objet (Bugfix)
    # bpy.context.view_layer.update() # Bug de mise à jour
    scene.objects['Commands']['redraw'] = True # Forcer le redraw de scène (Bugfix)
    return board

# Fermer le jumelage
def jumeau_stop():
    serial_close(scene.objects['System']['board'])

    # Brochage -> Panneau IO
    for i in range (1,19,1):
        idx= "IO_var-"+str(i)
        if scene.objects[idx]['visible']:
            scene.objects[idx+"-r_activ"]['pluged'] = False
            scene.objects[idx+"-r_activ-colbox"].suspendPhysics()
            scene.objects[idx+"-r_value-colbox"].suspendPhysics()
            scene.objects[idx+"-r_activ"].color = color_io_state_low
            scene.objects[idx+"-r_activ"]['redraw'] = True # Forcer le redraw de l'objet (Bugfix)
    scene.objects['Commands']['redraw'] = True # Forcer le redraw de scène (Bugfix)

###############################################################################
# Cycle
###############################################################################

##
# Départ
##

def start(fct, filename=None):

    # a = np.array([[1, 2, 3],[4, 5, 6]])
    # print (a.shape)

    # FIXME remettre le panneau IO
    # # Fichier CSV par le panneau IO
    # if len(scene.objects['IO-panel']['csv_var'])>0:
    #     csv(scene.objects['IO-panel']['csv_var'])

    # # Plot par le panneau IO
    # if len(scene.objects['IO-panel']['plot_var'])>0:
    #     plot(scene.objects['IO-panel']['plot_var'])

    # Go !
    if filename is None:
        print("Exécution du script : "+scene.objects['Commands']['script'])
    else:
        print("Exécution du script : "+filename)
    twin_threading.start(threads_cmd, "commands", fct)

##
# Variante
#
# La variante 1 appelée 'Pince rigide' : modèle 3D avec l'outil pince rigide
# La variante 2 appelée 'Stylo'        : modèle 3D avec un stylo
# La variante 3 appelée 'Ventouse'     : modèle 3D avec l'outil ventouse
# La variante 4 appelée 'Pince souple' : modèle 3D avec l'outil pince souple
#
# Configuration des variantes du modèle 3D -> variant_dict : 
# 'nom de l'objet 3D' : liste des variantes où l'objet est présent. 
# Les objets hors dictionnaire sont présent quelque soit la variante sélectionnée.
##

def variant(variant_num):
    var_txt = [
        "variante 1 : Pince rigide",
        "variante 2 : Stylo",
        "variante 3 : Ventouse",
        "variante 4 : Pince souple"]
    
    if variant_num != scene.objects['System']['variant']:
        print("Initialisation : Bras 6 axes MKX3D -", var_txt[variant_num-1])
        scene.objects['System']['variant'] = variant_num
        scene.objects['Model-text']['Text'] = "Bras 6 axes MKX3D  -  " + var_txt[variant_num-1]
        # scene.objects['Model-text'].blenderObject.data.body="Bras 6 axes MKX3D  -  " + var_txt[variant_num-1] # Bug de la propriétés 'Text' (UPBGE) -> passage par 'body' de bpy (Blender)

##
# Arrêt
##

def stop():

    # # Arrêt des sorties non forcées
    # q=[]
    # for i in range(6):
    #     obj = scene.objects[motors[i]]
    #     q.append(obj['q'])
    # # print ('stop :', q)
    # for i_txt in motors:
    #     obj = scene.objects[i_txt]
    #     obj['cw']=False
    #     obj['acw']=False
    # bras.move_q(q, scene.objects['Moteur Pince']['value'], wait=False, cont=False)
    # sleep(0.1)

    # Affichage
    scene.objects['Solver-text']['Text']    = "Solveur prêt."
    scene.objects['Commands']['draw']       = False
    scene.objects['Commands']['draw_p']     = False
    scene.objects['Commands']['draw_rep']   = False
    scene.objects['Commands']['draw_traj']  = False
    scene.objects['Commands']['draw_text']  = ""
    scene.objects['Commands']['draw_color'] = color_white
    scene.objects['Commands']['draw_data']  = []
    scene.objects['Commands']['clear_pt']   = False
    scene.objects['Commands']['clear_rep']  = False
    scene.objects['Commands']['clear_lbl']  = False

    # Trajectoire
    if scene.objects['Commands']['draw_traj']:
        draw_traj(scene.objects['Commands']['draw_data'])
        scene.objects['Commands']['draw_traj']   = False
        # scene.objects['Commands']['clear_pt']  = False
        # scene.objects['Commands']['clear_rep'] = False
        # scene.objects['Commands']['clear_lbl'] = False
    
    # Clear
    # if scene.objects['Commands']['draw_traj'] == False:
    if scene.objects['Commands']['clear_pt'] or scene.objects['Commands']['clear_rep'] or scene.objects['Commands']['clear_lbl']:
        draw_clear(scene.objects['Commands']['clear_pt'], scene.objects['Commands']['clear_rep'], scene.objects['Commands']['clear_lbl'])
        scene.objects['Commands']['clear_pt']  = False
        scene.objects['Commands']['clear_rep'] = False
        scene.objects['Commands']['clear_lbl'] = False


    # draw_clear() # Plus de raz

    # if scene.objects['Moteur']['cw_forcing']==False:
    #     scene.objects['Moteur']['cw']=False
    # if scene.objects['Moteur']['cw_forcing_real']==False:
    #     scene.objects['Moteur']['cw_real_forced']=False
    # if scene.objects['Moteur']['acw_forcing']==False:
    #     scene.objects['Moteur']['acw']=False
    # if scene.objects['Moteur']['acw_forcing_real']==False:
    #     scene.objects['Moteur']['acw_real_forced']=False

    # # Panneau IO : bug d'affichage de la led lors d'un stop -> action séparée
    # if obj['activated'] and scene.objects[obj['io']]['activated'] == False:
    #     twin_io.led_high(obj, False, 'io')
    # if obj['activated']==False and scene.objects[obj['io']]['activated']:
    #     twin_io.led_low(obj, False, 'io')
    
    # Jumeau
    if scene.objects['System']['twins']:
        jumeau_stop()
        sleep(1)
        # serial_close(scene.objects['System']['board'])

        # # Brochage -> Panneau IO
        # for i in range (1,19,1):
        #     name= "IO_var-"+str(i)
        #     if scene.objects[name]['visible']:
        #         scene.objects[name+"-r_activ"]['pluged']=False
        #         scene.objects[name+"-r_activ"].color= color_io_state_low
        # sleep(1)

    # Suivi des données
    if scene.objects['System']['daq_csv']:
        csv_generate()
    if scene.objects['System']['daq_plot']:
        csv_plot_generate()
        plot_generate(threads_plot)

    # Thread
    twin_threading.stop(threads_cmd, "commands")

##
# Fin naturelle
##

def end():
    fin()

def fin():
    if scene.objects['System']['debug_thread']:
        print("Thread commands is arrived.")
    sleep(0.125)
    scene.objects['System']['thread_cmd']=False
    sleep(0.125)
