Icons

bras_mkx3d.png :
- Source repository : https://forge.apps.education.fr/bras-mkx3d
- Author : Philippe Roy ( https://forge.apps.education.fr/phroy )
- Licence : Creative Commons BY 4.0

blender-edutech.png :
- Source repository : https://forge.apps.education.fr/bras-mkx3d
- Author : Philippe Roy ( https://forge.apps.education.fr/phroy )
- Licence : Creative Commons BY 4.0

la_forge-komit.svg :
- Remix by Philippe Roy ( https://forge.apps.education.fr/phroy )
- Initial file :  avatar_Komit_face_transparent.png 
- Source repository : https://forge.apps.education.fr/docs/visuel-forge
- Author : Juliette Taka ( https://juliettetaka.com/fr )
- Licence : Creative Commons BY 4.0

upbge.svg :
- TM / ®UPBGE Project
- Website : https://upbge.org

