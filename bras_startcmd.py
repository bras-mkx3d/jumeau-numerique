# -*- coding: utf-8 -*-
import os
from bras_lib import * # MKX3D Arm

###############################################################################
# bras_startcmd.py
# @title: Init script
###############################################################################

###############################################################################
# Functions
###############################################################################

###############################################################################
# Commands
###############################################################################

def commands():

    set_speed(0, 10, 10) # 10 rad/s
    # set_speed(0, 30, 30) # 30 rad/s

###############################################################################
# En: External call << DONT CHANGE THIS SECTION >>
# Fr: Appel externe << NE PAS MODIFIER CETTE SECTION >>
###############################################################################

###
# Variant 1 : Rigid grabber
# Variant 2 : Pen
# Variant 3 : Suction cup
# Variant 4 : Soft grabber
###

def cycle():
    commands()
    end()

if __name__=='start':
    start(cycle, os.path.join(os.getcwd(), 'bras_startcmd.py'))
if __name__=='stop':
    stop()
if __name__=='init':
    variant(1) # Rigid grabber variant

