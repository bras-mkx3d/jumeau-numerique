import os, sys
import importlib
import subprocess # Multiprocessus
import threading  # Multithreading
import runpy      # Exécution de script Python légère (sans import)
import math, mathutils
import xml.etree.ElementTree as ET # Parser de fichier XML
import webbrowser

import bge, bpy # Blender Game Engine (UPBGE)

import twin_traj            # Gestion des trajectoires
from twin_time import sleep # Gestion du temps

###############################################################################
# twin.py
# @title: Bibliothèque générale de l'environnement 3D pour le développement de jumeau numérique
# @project base: Blender-EduTech
# @project specifical: Bras MKX3D (quelques fonctions spécifiques)
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2020-2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# UPBGE scene
scene = bge.logic.getCurrentScene()
eevee = bpy.context.scene.eevee
system = importlib.import_module(scene.objects['System']['system']) # Système
scene.objects['Commands']['script'] = os.path.join(os.getcwd(), scene.objects['System']['system']+"_cmd.py") # Script par défaut
motors=['Moteur S0', 'Moteur S1', 'Moteur S2', 'Moteur S3', 'Moteur S4', 'Moteur S5']

# Threads de base
threads_twin = []

###############################################################################
#
# Propriétés des objets 3D binaires (bouton poussoir, capteur TOR, voyant, ...):
# - activated              : valeur logique numérique
# - activated_real         : valeur logique réelle (à l'instant présent)
# - activated_real_forced  : valeur logique réelle forcée (uniquement les sorties)
# - forcing                : forçage activé de la valeur logique numérique 
# - forcing_real           : forçage activé de la valeur logique réelle
# - mo                     : mouse over : focus souris sur l'objet
# - click                  : clic souris sur l'objet
#
# Propriétés des objets 3D analogiques (capteur analogique):
# - value                  : valeur numérique
# - value_real             : valeur réelle
# - value_click            : valeur numérique générée par le clic
# - value_unclick          : valeur numérique générée après le clic 
# - activated              : valeur logique numérique correspondant au passage du seuil (FIXME : Est-ce qu'on garde cette propriété ?)
# - forcing                : forçage activé de la valeur logique numérique 
# - forcing_real           : forçage activé de la valeur logique réelle
# - mo                     : mouse over : focus souris sur l'objet
# - click                  : clic souris sur l'objet
#
# Propriétés des moteurs :
# - cw                     : valeur logique numérique de l'activation sens horaire
# - acw                    : valeur logique numérique de l'activation sens trigo
# - cw_real                : valeur logique réelle de l'activation sens horaire
# - acw_real               : valeur logique réelle de l'activation sens trigo
# - cw_real_forced         : valeur logique réelle forcée de l'activation sens horaire
# - acw_real_forced        : valeur logique réelle forcée de l'activation sens trigo
# - cw_forcing             : forçage activé de la valeur logique numérique de l'activation sens horaire
# - acw_forcing            : forçage activé de la valeur logique numérique de l'activation sens trigo
# - cw_forcing_real        : forçage activé de la valeur logique réelle de l'activation sens horaire
# - acw_forcing_real       : forçage activé de la valeur logique réelle de l'activation sens trigo
# - alpha                  : angle parcouru (rad)
# - speed                  : vitesse numérique (rad/s)
# - speed_setting          : réglage de la vitesse numérique (rad/s)
# - direction_real         : sens réel (variante Grove)
# - direction_forcing_real : forçage activé de la valeur logique réelle du sens (variante Grove)
# - direction_real_forced  : valeur logique réelle forcée du sens (variante Grove)
# - speed_real             : vitesse réelle (0-255, variante Grove)
# - speed_setting_real     : réglage de la vitesse réelle (0-255, variante Grove)
# - speed_forcing_real     : forçage activé de la valeur de la vitesse réelle (0-255, variante Grove)
# - speed_real_forced      : valeur forcée de la valeur de la vitesse réelle (0-255, variante Grove)
#
###############################################################################

# Memory
sys.setrecursionlimit(10**5) # Limite sur la récursivité (valeur par défaut : 1000) -> segfault de Blender

# Config file
twin_config = ET.parse('twin_config.xml').getroot()

# Couleurs
color_cmd            = (0.8, 0.8, 0.8, 1)       # Blanc
color_cmd_hl         = (0.8, 0.619, 0.021, 1)   # Jaune
# color_passive        = (0.8, 0.005, 0.315,1)    # Élément non activable : magenta
# color_passive        = (0.799, 0.031, 0.038, 1) # Élément non activable : rouge
color_passive        = (0.799, 0.107, 0.087, 1) # Élément non activable : rouge clair
# color_active         = (0.799, 0.130, 0.063,1)  # Bouton activable : orange
color_hl             = (0.8  , 0.8  , 0.8  , 1) # Élément focus : blanc
color_activated      = (0.8, 0.619, 0.021, 1)   # Élément activé numériquement uniquement : jaune
color_activated_real = (0.799, 0.031, 0.038, 1) # Élément activé physiquement uniquement : rouge (hors clic)
color_activated_dbl  = (0.246, 0.687, 0.078, 1) # Élément activé physiquement et numériquement : vert clair

# Moteur
color_mot_activated = (0.800, 0.619, 0.021, 1) # Moteur activé : jaune

# ARU
color_active         = (0.799, 0.031, 0.038, 1)  # Bouton activable : rouge
color_activated      = (0.799, 0.130, 0.063, 1)  # Bouton activé numériquement uniquement : orange

# Générales
color_white          = (0.8  , 0.8  , 0.8  , 1) # Blanc
color_blue           = (0.007, 0.111, 0.638, 1) # Couleur bleu (actionneur type moteur)
color_blue_dark      = (0.004, 0.054, 0.296, 1) # Couleur bleu noir
color_grey           = (0.285, 0.285, 0.285, 1) # Couleur gris
color_turquoise      = (0.051, 0.270, 0.279, 1) # Couleur turquoise
color_turquoise_dark = (0.030, 0.148, 0.152, 1) # Couleur turquoise foncée
color_yellow         = (0.800, 0.619, 0.021, 1) # Jaune
color_led_yellow     = (0.799, 0.617, 0.021, 1) # Couleur Led jaune (led)
color_red            = (0.694, 0.098, 0.098, 1) # Rouge
color_aru            = (0.693, 0.097, 0.097, 1) # Couleur de l'arrêt d'urgence
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_blue_light     = (0.015, 0.150, 0.687, 1) # Bleu clair
color_purple         = (0.799, 0.005, 0.314, 1) # Violet foncé
color_purple_light   = (0.687, 0.125, 0.578, 1) # Violet clair

# Repère
color_rep_red        = (0.799, 0.107, 0.087, 1) # Axe x
color_rep_green      = (0.083, 0.223, 0.028, 1) # Axe y
color_rep_blue       = (0.015, 0.150, 0.687, 1) # Axe z
color_rep_target     = (0.051, 0.270, 0.279, 1) # Origine Rc (cible)
color_rep_grey       = (0.283, 0.283, 0.283, 1) # Désactivation des axes

# Constantes
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED  = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE       = bge.logic.KX_INPUT_ACTIVE
# JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

###############################################################################
# Print formaté (debug)
###############################################################################

# Vecteur rotation absolu/relatif en degrée
def print_T(text_begin, T, Tparent=None):
    if Tparent==None:
        print(text_begin+" : "+'{:>6.2f}'.format(T.to_euler().x*(180/math.pi))+", "+
              '{:>6.2f}'.format(T.to_euler().y*(180/math.pi))+", "+
              '{:>6.2f}'.format(T.to_euler().z*(180/math.pi)))
    else:
        print(text_begin+" : "+'{:>6.2f}'.format((T.to_euler().x-Tparent.to_euler().x)*(180/math.pi))+", "+
              '{:>6.2f}'.format((T.to_euler().y-Tparent.to_euler().y)*(180/math.pi))+", "+
              '{:>6.2f}'.format((T.to_euler().z-Tparent.to_euler().z)*(180/math.pi)))

# Vecteur rotation degrée
def print_r(text_begin, rot):
    print(text_begin+" : "+'{:>6.2f}'.format(rot.x*(180/math.pi))+", "+
          '{:>6.2f}'.format(rot.y*(180/math.pi))+", "+
          '{:>6.2f}'.format(rot.z*(180/math.pi)))

###############################################################################
# Gestion du clavier + Joystick
###############################################################################

# Mode : Orbit(0) par défaut, Pan(1) avec Shift, Zoom (2) avec Ctrl, Eclaté (1) avec Shift
def keyboard(cont):
    obj = scene.objects['System']

    # Test

    # Clavier
    keyboard = bge.logic.keyboard

    # Manette positionnement de Rc (cible pour le déplacement manuel)
    joy = bge.logic.joysticks[0] # de 0 à 6
    if joy is not None :
        if 6 in joy.activeButtons: # Activer/désactiver (touche start)
            system.move_manual_rc(joy)
        else:
            if obj['manual_rc']:
                system.move_manual_rc(joy)

    # Touche ESC
    if JUST_ACTIVATED in keyboard.inputs[bge.events.ESCKEY].queue:

        # Maj du fichier de config (screen size : data/config/screen/width-> [0][0].text)
        screen_width, screen_height = bge.render.getWindowWidth(), bge.render.getWindowHeight()
        twin_config[0][0].text = str(screen_width)
        twin_config[0][1].text = str(screen_height)
        twin_config[0][2].text = str(scene.objects['Commands']['quality'])
        twin_config[0][3].text = str(scene.objects['Commands']['theme'])
        buffer_xml = ET.tostring(twin_config)
        with open("twin_config.xml", "wb") as f:
            f.write(buffer_xml)

        # Sortir
        bge.logic.endGame()

    ##
    # Mode de manipulation de la scène 3D
    ##

    # Shift -> mode 1 : Pan (clic milieu) ou Eclaté (clic gauche)
    if JUST_ACTIVATED in keyboard.inputs[bge.events.LEFTSHIFTKEY].queue:
        obj['manip_mode'] = 1
    if JUST_ACTIVATED in keyboard.inputs[bge.events.RIGHTSHIFTKEY].queue:
        obj['manip_mode'] = 1

    # Ctrl -> mode 2 : Zoom (clic milieu)
    if JUST_ACTIVATED in keyboard.inputs[bge.events.LEFTCTRLKEY].queue:
        obj['manip_mode'] = 2
    if JUST_ACTIVATED in keyboard.inputs[bge.events.RIGHTCTRLKEY].queue:
        obj['manip_mode'] = 2

    # Pas de modificateur -> mode 0 : Orbit (clic milieu)
    if JUST_RELEASED in keyboard.inputs[bge.events.LEFTSHIFTKEY].queue:
        obj['manip_mode'] = 0
    if JUST_RELEASED in keyboard.inputs[bge.events.RIGHTSHIFTKEY].queue:
        obj['manip_mode'] = 0
    if JUST_RELEASED in keyboard.inputs[bge.events.LEFTCTRLKEY].queue:
        obj['manip_mode'] = 0
    if JUST_RELEASED in keyboard.inputs[bge.events.RIGHTCTRLKEY].queue:
        obj['manip_mode'] = 0
    
    # Touche Home -> Reset de la vue
    if JUST_ACTIVATED in keyboard.inputs[bge.events.HOMEKEY].queue:
        manip_reset()

    ##
    # Vues
    ##

    # Touche 0 pavé numérique -> Vue en perspective
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD0].queue:
        system.view(keyboard)

    # Touche 1 pavé numérique -> Vue de face
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD1].queue:
        system.view(keyboard)

    # Touche 3 pavé numérique -> Vue de coté
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD3].queue:
        system.view(keyboard)

    # Touche 7 pavé numérique -> Vue de haut
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD7].queue:
        system.view(keyboard)

    # Touche G -> Afficher/cacher grille
    if JUST_ACTIVATED in keyboard.inputs[bge.events.GKEY].queue:
        system.view(keyboard)

    # Touche F9 -> Modèle géométrique
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F9KEY].queue:
        geoview()

    ##
    # Cycle
    ##

    # Touche F5 -> Run
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F5KEY].queue:
        cycle_run()

    # Touche F6 -> Stop / Init
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F6KEY].queue:
        cycle_stop()

    ##
    # Aide
    ##

    # Touche F1 -> Documentation
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F1KEY].queue:
        webbrowser.open(scene.objects['System']['doc'])

    ##
    # Trajectoires
    ##

    # Touche F8 -> Panneau trajectoires
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F8KEY].queue:
        twin_traj.open_panel()

    # Touche Retour arrière -> Supprime les points (collision et trajectoires) et les repères
    if JUST_ACTIVATED in keyboard.inputs[bge.events.BACKSPACEKEY].queue and obj['manip_mode']!=1: # Supprime les points (collision et trajectoires)
        system.draw_clear(True, False, False)
    if JUST_ACTIVATED in keyboard.inputs[bge.events.BACKSPACEKEY].queue and obj['manip_mode']==1: # Supprime tout (points et repères) 
        system.draw_clear(True, True, True) 

    # Touche F7 -> Autopositionnement de la pince
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F7KEY].queue:
        twin_traj.object_autograp()

    # Saisie point avec Entrée
    if JUST_ACTIVATED in keyboard.inputs[bge.events.ENTERKEY].queue:
        twin_traj.pose_add()

    # Touche 5 pavé numérique -> Bascule en repère global/local
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD5].queue:
        if obj['manual_rc']:
            if scene.objects['Rc']['move_global']:
                scene.objects['Rc']['move_global']=False
            else:
                scene.objects['Rc']['move_global']=True

    ##
    # Bloquage des moteurs pour IK
    ##

    if obj['manual_rc']:
        if JUST_ACTIVATED in keyboard.inputs[bge.events.ONEKEY].queue:
            system.move_manual_rc_mot(1)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.TWOKEY].queue:
            system.move_manual_rc_mot(2)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.THREEKEY].queue:
            system.move_manual_rc_mot(3)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.FOURKEY].queue:
            system.move_manual_rc_mot(4)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.FIVEKEY].queue:
            system.move_manual_rc_mot(5)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.SIXKEY].queue:
            system.move_manual_rc_mot(6)

    ##
    # Commande manuelle des moteurs
    ##

    if obj['manual_rc']==False:

        # Touche 1 à 6 -> q1 à q6 et 7 -> valeur outil
        if JUST_ACTIVATED in keyboard.inputs[bge.events.ONEKEY].queue:
            system.move_manual_q(1, None)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.TWOKEY].queue:
            system.move_manual_q(2, None)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.THREEKEY].queue:
            system.move_manual_q(3, None)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.FOURKEY].queue:
            system.move_manual_q(4, None)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.FIVEKEY].queue:
            system.move_manual_q(5, None)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.SIXKEY].queue:
            system.move_manual_q(6, None)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.SEVENKEY].queue:
            system.move_manual_ot()

        # Rotation sens horaire (1), rotation sens anti-horaire (-1), stop (0), raz (2), raz de tout les moteurs (3),
        # pas- (4), pas+ (5), pas à pas sens horaire (6), pas à pas sens horaire (7)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.ENDKEY].queue:
            system.move_manual_q(None, 0)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.PAGEDOWNKEY].queue:
            system.move_manual_q(None, -1)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.PAGEUPKEY].queue:
            system.move_manual_q(None, 1)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.DELKEY].queue and obj['manip_mode']!=1:
            system.move_manual_q(None, 2)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.DELKEY].queue and obj['manip_mode']==1:
            system.move_manual_q(None, 3)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.DOWNARROWKEY].queue:
            system.move_manual_q(None, 4)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.UPARROWKEY].queue:
            system.move_manual_q(None, 5)
        # if JUST_ACTIVATED in keyboard.inputs[bge.events.PADMINUS].queue:
        #     system.move_manual_q(None, 4)
        # if JUST_ACTIVATED in keyboard.inputs[bge.events.PADPLUSKEY].queue:
        #     system.move_manual_q(None, 5)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.LEFTARROWKEY].queue:
            system.move_manual_q(None, 6)
        if JUST_ACTIVATED in keyboard.inputs[bge.events.RIGHTARROWKEY].queue:
            system.move_manual_q(None, 7)

###############################################################################
# Commandes
###############################################################################

##
# Redessiner un objet figé (bugfix)
##

def cmd_redraw(cont):
    scene.objects['Commands']['redraw']=False
    for obj in scene.objects:
        if "redraw" in obj.getPropertyNames():
            if obj['redraw']:
                obj.blenderObject.data.update_tag()
                if scene.objects['System']['debug_render']:
                    print ("EEVEE : redraw de l'objet:", obj)
                bpy.context.view_layer.update() # Mise à jour
                obj['redraw']=False
    bpy.context.view_layer.update() # Mise à jour

##
# Initialisation des commandes
##

def cmd_init():

    # UI : Commands
    scene.objects['Run-Hl'] .setVisible(False,True)
    scene.objects['Stop']   .setVisible(True,True)
    scene.objects['Stop-Hl'].setVisible(False,True)
    scene.objects['IO-Hl']  .setVisible(False,True)
    scene.objects['Traj-Hl'].setVisible(False,True)
    scene.objects['Doc-Hl'] .setVisible(False,True)
    
    scene.objects['Geoview-Hl']  .setVisible(False,True)
    scene.objects['ResetView-Hl'].setVisible(False,True)
    scene.objects['File-Hl']     .setVisible(False,True)
    scene.objects['Config-Hl']   .setVisible(False,True)
    scene.objects['About-Hl']    .setVisible(False,True)

    # UI : Text, ...
    scene.objects['Cmd-text']   ['Text']=""
    scene.objects['Script-text']['Text']=scene.objects['Commands']['script']
    scene.objects['Twins-text'] ['Text']="Connection fermée."
    scene.objects['Solver-text']['Text']="Solveur prêt."
    scene.objects['Input-text'] ['Text']=""
    scene.objects['Input-text2']['Text']=""
    scene.objects['Model-text'].blenderObject.data.body="Variante de la maquette non définie." # Bug de la propriétés 'Text' (UPBGE) -> passage par 'body' de bpy (Blender)
    scene.objects['Cmd-text']      .setVisible(True,False)
    scene.objects['Joystick-icon'] .setVisible(False,True)
    scene.objects['Keyboard-icon'] .setVisible(False,True)
    scene.objects['Keyboard-icon2'].setVisible(False,True)
    system.p_message()

    # Objets flottants (points, repères et étiquettes)
    scene.objects['Commands']['draw']       = False
    scene.objects['Commands']['draw_pt']    = False
    scene.objects['Commands']['draw_rep']   = False
    scene.objects['Commands']['draw_lbl']   = False
    scene.objects['Commands']['draw_p']     = None
    scene.objects['Commands']['draw_color'] = None
    scene.objects['Commands']['draw_text']  = None
    scene.objects['Commands']['draw_traj']  = False
    scene.objects['Commands']['draw_list']  = None
    scene.objects['Commands']['clear_pt']   = False
    scene.objects['Commands']['clear_rep']  = False
    scene.objects['Commands']['clear_lbl']  = False

    # Paramètres de trajectoire
    scene.objects['Commands']['move_draw_color']     = color_yellow
    scene.objects['Commands']['move_draw_thickness'] = 1.0
    scene.objects['Commands']['move_draw_tickskip']  = 3

    # IK Blender
    # scene.objects['Commands']['ikb_fkine'] = False
    # scene.objects['Commands']['ikb_fkine_q'] = None
    # scene.objects['Commands']['ikb_fkine_p'] = None
    # scene.objects['Commands']['ikb_fkine_traj'] = None

    # Gizmo
    scene.objects['Ro'].setVisible(False,True)
    scene.objects['Ro'].suspendPhysics()
    for obj_i in scene.objects['Ro'].children:
        obj_i.suspendPhysics()
        for obj_j in obj_i.children:
            obj_j.suspendPhysics()
    scene.objects['Ro-axe_x'].color = color_rep_red
    scene.objects['Ro-axe_y'].color = color_rep_green
    scene.objects['Ro-axe_z'].color = color_rep_blue
    for obj_i in scene.objects['Ro-axe_rx'].children:
        obj_i.color = color_rep_red
    for obj_i in scene.objects['Ro-axe_ry'].children:
        obj_i.color = color_rep_green
    for obj_i in scene.objects['Ro-axe_rz'].children:
        obj_i.color = color_rep_blue

    # Jumeau
    scene.objects['System']['twins'] = False

    # Panneau des trajectoire
    twin_traj.init()

##
# Highlight des commandes
##

def cmd_hl(cont):
    obj = cont.owner

    # Activation
    if cont.sensors['MO'].status == JUST_ACTIVATED and scene.objects['System']['manip_mode']==0:

        # Pas de colbox
        if "-colbox" not in obj.name:
            obj.setVisible(False,True)
            scene.objects[obj.name+'-Hl'].setVisible(True,True)

        # Avec un colbox
        else:

            # Run (exception)
            if obj.name=="Run-colbox":
                if scene.objects['System']['run'] == True:
                    pass
                else:
                    scene.objects['Run'].setVisible(False,True)
                    scene.objects['Run-Hl'].setVisible(True,True)

            # Autres
            else:
                scene.objects[obj.name[:-7]].setVisible(False,True)
                scene.objects[obj.name[:-7]+'-Hl'].setVisible(True,True)

        # Texte
        text_hl ={"Run-colbox"         : "Exécuter (F5)",
                  "Stop-colbox"        : "Stop et initialisation (F6)",
                  "IO-colbox"          : "Panneau des entrées/sorties (pas implémenté)", # FIXME : implémentation du panneau IO à faire
                  "Traj-colbox"        : "Gestion des trajectoires (F8)",
                  "Geoview-colbox"     : " Modèle géométrique (F9)",
                  "Doc-colbox"         : "Documentation (F1)",
                  "ResetView-colbox"   : "Reset de la vue (Touche Début)",
                  "File-colbox"        : "Changer le fichier de commandes",
                  "Config-colbox"      : "Configuration",
                  "About-colbox"       : "À propos"}
        scene.objects['Cmd-text']['modal']= False
        scene.objects['Cmd-text']['Text']= text_hl[obj.name]
        scene.objects['Cmd-text'].setVisible(True,False)

    # Désactivation
    if cont.sensors['MO'].status == JUST_RELEASED and (scene.objects['System']['manip_mode']==0 or scene.objects['System']['manip_mode']==9):

        # Texte
        if scene.objects['Cmd-text']['modal']:
            # scene.objects['Cmd-text']['Text']= "Erreur dans le script ...  modal"
            scene.objects['Cmd-text'].setVisible(True,False)
        else:
            scene.objects['Cmd-text']['Text']= ""
            scene.objects['Cmd-text'].setVisible(False,False)

        # Pas de colbox
        if "-colbox" not in obj.name:
            scene.objects[obj.name+'-Hl'].setVisible(False,True)
            obj.setVisible(True,True)

        # Avec un colbox
        else:

            # Run (exception)
            if obj.name=="Run-colbox":
                if scene.objects['System']['run'] == True:
                    pass
                else:
                    scene.objects['Run-Hl'].setVisible(False,True)
                    scene.objects['Run'].setVisible(True,True)

            # Autres
            else:
                scene.objects[obj.name[:-7]+'-Hl'].setVisible(False,True)
                scene.objects[obj.name[:-7]].setVisible(True,True)

##
# Click sur les commandes
##

def cmd_click(cont):
    obj = cont.owner
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0:

        # Play
        if obj.name == "Run-colbox":
            cycle_run()

        # Stop
        if obj.name == "Stop-colbox":
            cycle_stop()

        # Entrées / Sorties
        # FIXME : implémentation du panneau IO à faire
        # if obj.name=="IO-colbox":
        #     twin_io.open_panel()

        # Trajet
        if obj.name == "Traj-colbox":
            twin_traj.open_panel()

        # Modèle géométrique
        if obj.name == "Geoview-colbox":
            geoview()

        # Aide
        if obj.name == "Doc-colbox":
            webbrowser.open(scene.objects['System']['doc'])

        # Reset-view
        if obj.name == "ResetView-colbox":
            manip_reset()

        # Fichier de commande
        if obj.name == "File-colbox":
            cmd_file()
            system.system_reset()

        # Configuration
        if obj.name == "Config-colbox":
            cmd_config()

        # About
        if obj.name == "About-colbox":
            cmd_about()

##
# Commandes ouvrant d'autres fenêtres
##

# Changer le fichier de commande
def cmd_file():

    # Terminer le processus 'file' précédent
    if ('file_proc' in scene.objects['Commands']):
        if scene.objects['Commands']['file_proc'].poll()==None:
            scene.objects['Commands']['file_proc'].terminate()

    # Démarrer le processus 'file'
    theme = str(scene.objects['Commands']['theme'])
    if scene.objects['Commands']['debug_windows']: # Dev wxPython sous GNU/linux
        print ("Développement wxPython sous GNU/linux : Sélectionneur de fichier")
        scene.objects['Commands']['file_proc'] = subprocess.Popen(["python", os.path.join(os.getcwd(),"wx","file.py"), theme, 'cmd'],
                                                                  stdout=subprocess.PIPE, encoding = 'utf8')
    elif sys.platform=="linux": # GTK+3 pour GNU/linux
        scene.objects['Commands']['file_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"gtk","file.py"), theme, 'cmd'],
                                                                  stdout=subprocess.PIPE, encoding = 'utf8')
    else: # wxPython pour Windows
        scene.objects['Commands']['file_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"wx","file.py"), theme, 'cmd'],
                                                                  stdout=subprocess.PIPE, encoding = 'utf8')

    # Récupérer le nom du fichier
    stout = scene.objects['Commands']['file_proc'].communicate()
    if stout[0][:-1] != 'None' and len(stout[0][:-1])>0 :
        scene.objects['Commands']['script'] = stout[0][:-1]
        scene.objects['Script-text']['Text']=scene.objects['Commands']['script']

# Configuration
def cmd_config():

    # Terminer le processus 'config' précédent
    if ('config_proc' in scene.objects['Commands']):
        if scene.objects['Commands']['config_proc'].poll()==None:
            scene.objects['Commands']['config_proc'].terminate()

    # Démarrer le processus 'config'
    data_list=[]
    data_list.append(twin_config[0][0].text) # Largeur écran
    data_list.append(twin_config[0][1].text) # Hauteur écran
    data_list.append(twin_config[0][2].text) # Qualité
    data_list.append(twin_config[0][3].text) # Thème
    if scene.objects['Commands']['debug_windows']: # Dev wxPython sous GNU/linux
        print ("Développement wxPython sous GNU/linux : Configurateur")
        scene.objects['Commands']['config_proc'] = subprocess.Popen(["python", os.path.join(os.getcwd(),"wx","config.py"), ','.join(data_list)],
                                                                    stdout=subprocess.PIPE, encoding = 'utf8')
    elif sys.platform=="linux": # GTK+3 pour GNU/linux
        scene.objects['Commands']['config_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"gtk","config.py"), ','.join(data_list)],
                                                                    stdout=subprocess.PIPE, encoding = 'utf8')
    else: # wxPython pour Windows
        scene.objects['Commands']['config_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"wx","config.py"), ','.join(data_list)],
                                                                    stdout=subprocess.PIPE, encoding = 'utf8')

    # Récupérer les données de configuration
    stout = scene.objects['Commands']['config_proc'].communicate() # ('1589x893,Basse,1\n', None)
    if stout[0][:-1] != 'None' and len(stout[0][:-1])>0 :
        data= stout[0][:-1].split(',')

        # Appliquer la nouvelle configuration et enregistrer le fichier 'twin_config.xml'
        bge.render.setWindowSize(int(data[0]),int(data[1]))
        # view_set_quality(int(data[2])) # Provoque le plantage -> redémarage 
        scene.objects['Commands']['quality'] = int(data[2])
        scene.objects['Commands']['theme'] = int(data[3])

        # Enregistrer le fichier 'twin_config.xml'
        twin_config[0][0].text = str(data[0])
        twin_config[0][1].text = str(data[1])
        twin_config[0][2].text = str(scene.objects['Commands']['quality'])
        twin_config[0][3].text = str(scene.objects['Commands']['theme'])
        buffer_xml = ET.tostring(twin_config)
        with open("twin_config.xml", "wb") as f:
            f.write(buffer_xml)

# About
# FIXME : avec wxPython la processus ne se ferme pas, se met au repos.
def cmd_about_thread():
    theme = str(scene.objects['Commands']['theme'])
    if scene.objects['Commands']['debug_windows']: # Dev wxPython sous GNU/linux
        print ("Développement wxPython sous GNU/linux : About")
        command = "python" + " " + os.path.join(sys.executable, os.path.join(os.getcwd(),"wx","about.py"))
    elif sys.platform=="linux": # GTK+3 pour GNU/linux
        command = sys.executable + " " + os.path.join(sys.executable, os.path.join(os.getcwd(),"gtk","about.py")) + " " + theme
    else: # wxPython pour Windows
        command = sys.executable + " " + os.path.join(sys.executable, os.path.join(os.getcwd(),"wx","about.py"))
    os.system(command)

def cmd_about():
    threads_twin.append(threading.Thread(target=cmd_about_thread))
    threads_twin[len(threads_twin)-1].start()

###############################################################################
# Vue
###############################################################################

##
# Dessiner une cercle (bas niveau)
##

def circle(center, radius, color):
    ang = 0
    ang_step = 0.1
    radius_x=radius*0.95 # Au jugé
    radius_y=radius*0.15 # Au jugé
    while ang < 2*math.pi:
        x0 = center[0]+float(radius_x*math.cos(ang))
        y0 = center[1]+float(radius_y*math.sin(ang))
        z0 = center[2]+float(radius*math.sin(ang))
        x1 = center[0]+float(radius_x*math.cos(ang+ang_step))
        y1 = center[1]+float(radius_y*math.sin(ang+ang_step))
        z1 = center[2]+float(radius*math.sin(ang+ang_step))
        bge.render.drawLine([x0,y0,z0],[x1,y1,z1],color)
        ang += ang_step

##
# Initialisation de la vue 3D
##

def view_init():

    # Configuration de l'écran (taille, qualité et thème)
    bge.render.setWindowSize(int(twin_config[0][0].text),int(twin_config[0][1].text))
    scene.objects['Commands']['quality'] = int(twin_config[0][2].text)
    scene.objects['Commands']['theme'] = int(twin_config[0][3].text)
    view_set_quality(scene.objects['Commands']['quality'])

    # Windows
    scene.objects['Traj-panel'].setVisible(False,True)
    scene.objects['System']['io'] = False

    # Ajout du Hud
    scene.active_camera = scene.objects["Camera"]
    scene.objects['Sun'].setVisible(True,True)
    scene.addOverlayCollection(scene.cameras['Camera-Hud'], bpy.data.collections['Hud'])
    # scene.objects['Sun-Hud'].setVisible(True,True)

    # Mémorisation de la position de départ
    scene.objects['System']['init']           = scene.objects['System'].worldTransform.copy()
    scene.objects['Camera-pt']['init']        = scene.objects['Camera-pt'].worldTransform.copy()
    scene.objects['Camera face-pt']['init']   = scene.objects['Camera face-pt'].worldTransform.copy()
    scene.objects['Camera arr-pt']['init']    = scene.objects['Camera arr-pt'].worldTransform.copy()
    scene.objects['Camera gauche-pt']['init'] = scene.objects['Camera gauche-pt'].worldTransform.copy()
    scene.objects['Camera droit-pt']['init' ] = scene.objects['Camera droit-pt'].worldTransform.copy()
    scene.objects['Camera haut-pt']['init' ]  = scene.objects['Camera haut-pt'].worldTransform.copy()
    scene.objects['Camera bas-pt']['init' ]   = scene.objects['Camera bas-pt'].worldTransform.copy()
    scene.objects['Camera']['init']           = scene.objects['Camera'].localTransform.copy()
    scene.objects['Camera']['init_z']         = scene.objects['Camera'].worldPosition.z
    scene.objects['Camera face']['init']      = scene.objects['Camera face'].localTransform.copy()
    scene.objects['Camera arr']['init']       = scene.objects['Camera arr'].localTransform.copy()
    scene.objects['Camera gauche']['init']    = scene.objects['Camera gauche'].localTransform.copy()
    scene.objects['Camera droit']['init']     = scene.objects['Camera droit'].localTransform.copy()
    scene.objects['Camera haut']['init']      = scene.objects['Camera haut'].localTransform.copy()
    scene.objects['Camera bas']['init']       = scene.objects['Camera bas'].localTransform.copy()

    # Repère et grille
    scene.objects['View-text'].setVisible(False,True)
    scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
    scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
    scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
    scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
    scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
    scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
    if 'Grid' in scene.objects:
        scene.objects['Commands']['grid'] = True
        scene.objects['Grid-u-face'].setVisible(False,True)
        scene.objects['Grid-v-face'].setVisible(False,True)
        scene.objects['Grid-u-face']['gv_visible'] = False
        scene.objects['Grid-v-face']['gv_visible'] = False
        scene.objects['Grid-u-cote'].setVisible(False,True)
        scene.objects['Grid-v-cote'].setVisible(False,True)
        scene.objects['Grid-u-cote']['gv_visible'] = False
        scene.objects['Grid-v-cote']['gv_visible'] = False
        scene.objects['Grid-u-haut'].setVisible(False,True)
        scene.objects['Grid-v-haut'].setVisible(False,True)
        scene.objects['Grid-u-haut']['gv_visible'] = False
        scene.objects['Grid-v-haut']['gv_visible'] = False
    else:
        scene.objects['Commands']['grid'] = False

    # Geoview
    for obj in scene.objects:
        if 'gv' in obj.getPropertyNames():
            if obj['gv']:
                obj.setVisible(False,True)

# Qualité du rendu EEVEE de 0 à 4
def view_set_quality(quality):
    scene.objects['Commands']['quality'] = quality
    
    # Inconvenant
    if quality == 0:
        eevee.use_taa_reprojection = False
        eevee.use_ssr = False  # Screen space reflection
        eevee.use_gtao = False # Ambient occlusion
        eevee.taa_render_samples = 1
        eevee.taa_samples = 1
        eevee.use_volumetric_lights = False
        eevee.use_volumetric_shadows = False
        eevee.shadow_cascade_size='64'
        eevee.shadow_cube_size='64'

    # Basse
    if quality == 1:
        eevee.use_taa_reprojection = True
        eevee.use_ssr = True             # Screen space reflection
        eevee.use_ssr_refraction = False # Screen space refractions
        eevee.use_ssr_halfres = True
        eevee.use_gtao = False
        eevee.taa_render_samples = 32
        eevee.taa_samples = 8
        eevee.use_volumetric_lights = False
        eevee.use_volumetric_shadows = False
        eevee.shadow_cascade_size='64'
        eevee.shadow_cube_size='64'

    # Moyenne
    if quality == 2:
        eevee.use_taa_reprojection = True
        eevee.use_ssr = True            # Screen space reflection
        eevee.use_ssr_refraction = True # Screen space refractions
        eevee.use_ssr_halfres = True
        eevee.use_gtao = False
        eevee.taa_render_samples = 64
        eevee.taa_samples = 16
        eevee.use_volumetric_lights = True
        eevee.use_volumetric_shadows = False
        eevee.shadow_cascade_size='1024'
        eevee.shadow_cube_size='512'

    # Haute
    if quality == 3:
        eevee.use_taa_reprojection = True
        eevee.use_ssr = True    
        eevee.use_ssr_refraction = True
        eevee.use_ssr_halfres = False
        eevee.use_gtao = False
        eevee.taa_render_samples = 64
        eevee.taa_samples = 16
        eevee.use_volumetric_lights = True
        eevee.use_volumetric_shadows = False
        eevee.shadow_cascade_size='1024'
        eevee.shadow_cube_size='512'

    # Épique
    if quality == 4:
        eevee.use_taa_reprojection = True
        eevee.use_ssr = True  
        eevee.use_ssr_refraction = True
        eevee.use_ssr_halfres = False
        eevee.use_gtao = True
        eevee.taa_render_samples = 64
        eevee.taa_samples = 16
        eevee.use_volumetric_lights = True
        eevee.use_volumetric_shadows = True
        eevee.shadow_cascade_size='4096'
        eevee.shadow_cube_size='4096'

##
# Vue du modèle géométique
#
# Si obj['gv']         = True  -> uniquement visible avec la vue geoview
# Si obj['gv']         = False -> visible avec la vue geoview et la vue système (toujours visible)
# Si obj['gv_visible'] = True  -> visible dans la vue geoview aussi
# Si obj['gv_scale']   = Agrandissement pour la vue geoview
##

def geoview():

    ##
    # Fermer
    ##

    if scene.objects['System']['geoview']:
        scene.objects['System']['geoview'] = False
        scene.objects['System'].setVisible(True,True)
        for obj in scene.objects:
            if 'gv' in obj.getPropertyNames() and obj['gv']:
                obj.setVisible(False,True)
            if 'gv_scale' in obj.getPropertyNames():
                obj.worldScale=obj.worldScale/obj['gv_scale']
            if 'gv_visible' in obj.getPropertyNames() and obj['gv_visible'] == False:
                obj.setVisible(False,True)

        # Repères Rc
        scene.objects['Rc'].setVisible(scene.objects['System']['manual_rc'], True)
        scene.objects['Rc-global'].setVisible(scene.objects['System']['manual_rc']*scene.objects['Rc']['move_global'], True)

        # Gizmo
        scene.objects['Ro'].setVisible(scene.objects['System']['gizmo'], True)

        # Repère de la vue
        scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
        scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
        scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
        scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
        scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
        scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
        return

    ##
    # Ouvrir
    ##

    # Affichage
    for obj in scene.objects:
        if 'gv_visible' in obj.getPropertyNames():
            if obj.visible:
                obj['gv_visible']=True
            else:
                obj['gv_visible']=False
    scene.objects['System']['geoview'] = True
    scene.objects['System'].setVisible(False,True)
    for obj in scene.objects:
        if 'gv' in obj.getPropertyNames():
            obj.setVisible(True,True)
        if 'gv_visible' in obj.getPropertyNames() and obj['gv_visible']:
            obj.setVisible(True,True)
        if 'gv_scale' in obj.getPropertyNames():
            obj.worldScale=obj.worldScale*obj['gv_scale']

    # Repères Rc
    scene.objects['Rc'].setVisible(scene.objects['System']['manual_rc'], True)
    scene.objects['Rc-global'].setVisible(scene.objects['System']['manual_rc']*scene.objects['Rc']['move_global'], True)

    # Gizmo
    scene.objects['Ro'].setVisible(scene.objects['System']['gizmo'], True)

###############################################################################
# Manipulation du mécanisme
###############################################################################

##
# Reset de la manipulation de la vue
##

def manip_reset():
    scene.objects['System'].worldTransform=scene.objects['System']['init']
    scene.objects['Camera-pt'].worldTransform=scene.objects['Camera-pt']['init']
    scene.objects['Camera face-pt'].worldTransform=scene.objects['Camera face-pt']['init']
    scene.objects['Camera arr-pt'].worldTransform=scene.objects['Camera arr-pt']['init']
    scene.objects['Camera gauche-pt'].worldTransform=scene.objects['Camera gauche-pt']['init']
    scene.objects['Camera droit-pt'].worldTransform=scene.objects['Camera droit-pt']['init']
    scene.objects['Camera haut-pt'].worldTransform=scene.objects['Camera haut-pt']['init']
    scene.objects['Camera bas-pt'].worldTransform=scene.objects['Camera bas-pt']['init']
    scene.objects['Camera'].localTransform=scene.objects['Camera']['init']
    scene.objects['Camera face'].localTransform=scene.objects['Camera face']['init']
    scene.objects['Camera arr'].localTransform=scene.objects['Camera arr']['init']
    scene.objects['Camera gauche'].localTransform=scene.objects['Camera gauche']['init']
    scene.objects['Camera droit'].localTransform=scene.objects['Camera droit']['init']
    scene.objects['Camera haut'].localTransform=scene.objects['Camera haut']['init']
    scene.objects['Camera bas'].localTransform=scene.objects['Camera bas']['init']
    scene.objects['Camera face'].ortho_scale = 2
    scene.objects['Camera arr'].ortho_scale = 2
    scene.objects['Camera gauche'].ortho_scale = 2
    scene.objects['Camera droit'].ortho_scale = 2
    scene.objects['Camera haut'].ortho_scale = 2
    scene.objects['Camera bas'].ortho_scale = 2
    # scene.active_camera.shift_x = 0
    # scene.active_camera.shift_y = 0

    # Geoview
    for obj in scene.objects:
        if 'gv_zoom' in obj.getPropertyNames():
            # obj.worldScale=obj['init_scale']
            # print (obj.name, obj['init_scale'])
            obj.worldScale=(0.0450, 0.0450, 0.0450) # FIXME : codé en dur

##
# Position de départ pour la manipulation de la vue
##

def manip_start(cont):
    obj = cont.owner

    # Start
    if cont.sensors['ClickM'].status == JUST_ACTIVATED:
        obj['manip_click_x'] = cont.sensors['ClickM'].position[0]
        obj['manip_click_y'] = cont.sensors['ClickM'].position[1]

        # Détection du cadran de la caméra
        if scene.active_camera == scene.objects["Camera"]:
            obj['manip_rot_x'] = scene.objects['Camera-pt'].worldOrientation.to_euler().x
            obj['manip_rot_y'] = scene.objects['Camera-pt'].worldOrientation.to_euler().y
            obj['manip_rot_z'] = scene.objects['Camera-pt'].worldOrientation.to_euler().z

    # # Stop
    # if cont.sensors['ClickM'].status == JUST_RELEASED:
    #     scene.objects['Camera-Hud'].worldTransform=scene.objects['Camera'].worldTransform

##
# Manipulation du modèle ou de la caméra
##

def manip(cont):
    obj = cont.owner
    sensibilite_orbit = 0.0005
    sensibilite_pan = 0.005/100 # Plus petit objet, facteur d'échelle faible
    sensibilite_zoom = 0.01/100 # Plus petit objet, facteur d'échelle faible
    sensibilite_scale_ecriture = 0.005 # Ecriture en Geoview : 0.005 pour un zoom de 0.008 ((delta_x+delta_y)*sensibilite_zoom)
    delta_x = cont.sensors['DownM'].position[0]-obj['manip_click_x']
    delta_y = cont.sensors['DownM'].position[1]-obj['manip_click_y']

    # Orbit
    if obj['manip_mode'] == 0:
        x0 = scene.objects['Orbit-Hud'].worldPosition.x
        y0 = scene.objects['Orbit-Hud'].worldPosition.y
        z0 = scene.objects['Orbit-Hud'].worldPosition.z
        width = bge.render.getWindowWidth()
        height = bge.render.getWindowHeight()
        dist_orbit_y_base = 200 # Pour 1280 x 720
        # dist_orbit_y_base = 200 # Pour 1280 x 720
        # radius_orbit_y_base = 5
        radius_orbit_y_base = 0.875
        # radius_orbit_y_base = 0.0875
        diag_base = math.sqrt(1280**2+720**2)
        diag = math.sqrt(width**2+height**2)
        dist_orbit_y = dist_orbit_y_base*(diag/diag_base)
        radius_orbit_y = radius_orbit_y_base*(diag/diag_base)
        dist_orbit = math.sqrt(((width/2)-obj['manip_click_x'])**2+((height/2)-obj['manip_click_y'])**2)
        if obj['manip_click_y']> height/2 :
            dist_orbit = dist_orbit+((math.sqrt(((height/2)-obj['manip_click_y'])**2)/175)*25) # Correction des Y sous le centre ?

        # Orbit sur le plan (xz pour la vue de face)
        if dist_orbit < dist_orbit_y :

            # Dessin de l'orbite
            circle ([x0,y0,z0], radius_orbit_y_base, color_cmd)
            n=10
            # pas_x=(delta_x*40*sensibilite_orbit)/n
            pas_x=(delta_x*4*sensibilite_orbit)/n
            # pas_y=(((width/2)-cont.sensors['DownM'].position[0])+((height/2)-cont.sensors['DownM'].position[1]))*0.005
            pas_y=((((width/2)-cont.sensors['DownM'].position[0])+((height/2)-cont.sensors['DownM'].position[1]))*0.005)/10
            # pas_z=(delta_y*40*sensibilite_orbit)/n
            pas_z=(delta_y*4*sensibilite_orbit)/n
            for i in range (n):
                bge.render.drawLine([x0+pas_x*i, y0+abs(pas_y*math.sin((3.14*i)/n)), z0-pas_z*i],
                                    [x0+pas_x*(i+1), y0+abs(pas_y*math.sin((3.14*(i+1))/n)), z0-pas_z*(i+1)],
                                    [0.8, 0.619, 0.021])

            # Une seule commande (simplification)
            if abs(delta_x) >= abs(delta_y):
                delta_y = 0
            else:
                delta_x = 0

            # Caméra perpective
            if scene.active_camera == scene.objects["Camera"]:
                # scene.objects['Camera-pt'].applyRotation((-delta_y*sensibilite_orbit, 0, -delta_x*sensibilite_orbit), False)
                # scene.objects['Camera'].worldOrientation.to_euler().x
                # print ("Camera-pt x,y,z", math.degrees(obj['manip_rot_x']), math.degrees(obj['manip_rot_y']), math.degrees(obj['manip_rot_z']))

                # En face
                if obj['manip_rot_z']>=math.radians(-45) and obj['manip_rot_z']<=math.radians(45):
                    if obj['manip_rot_x']>=math.radians(-45) and obj['manip_rot_x']<=math.radians(90):
                        # print ("En face - en face")
                        scene.objects['Camera-pt'].applyRotation((-delta_y*sensibilite_orbit, 0, -delta_x*sensibilite_orbit), False)
                    if obj['manip_rot_x']>math.radians(-100) and obj['manip_rot_x']<math.radians(-45):
                        # print ("En face - dessus")
                        scene.objects['Camera-pt'].applyRotation((-delta_y*sensibilite_orbit, -delta_x*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']>math.radians(90) and obj['manip_rot_x']<math.radians(180):
                        # print ("En face - dessous")
                        scene.objects['Camera-pt'].applyRotation((-delta_y*sensibilite_orbit, delta_x*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']<=math.radians(-100) or obj['manip_rot_x']>=math.radians(180):
                        # print ("En face - retournée")
                        scene.objects['Camera-pt'].applyRotation((-delta_y*sensibilite_orbit, 0, delta_x*sensibilite_orbit), False)

                # A gauche
                if obj['manip_rot_z']<math.radians(-45) and obj['manip_rot_z']>math.radians(-135):
                    if obj['manip_rot_x']>=math.radians(-45) and obj['manip_rot_x']<=math.radians(90):
                        # print ("A gauche - en face")
                        scene.objects['Camera-pt'].applyRotation((0, delta_y*sensibilite_orbit,-delta_x*sensibilite_orbit), False)
                    if obj['manip_rot_x']>math.radians(-100) and obj['manip_rot_x']<math.radians(-45):
                        # print ("A gauche - dessus")
                        scene.objects['Camera-pt'].applyRotation((-delta_x*sensibilite_orbit, delta_y*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']>math.radians(90) and obj['manip_rot_x']<math.radians(180):
                        # print ("A gauche - dessous")
                        scene.objects['Camera-pt'].applyRotation((delta_x*sensibilite_orbit, delta_y*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']<=math.radians(-100) or obj['manip_rot_x']>=math.radians(180):
                        # print ("A gauche - retournée")
                        scene.objects['Camera-pt'].applyRotation((0, delta_y*sensibilite_orbit, delta_x*sensibilite_orbit), False)

                # A droite
                if obj['manip_rot_z']>math.radians(45) and obj['manip_rot_z']<math.radians(135):
                    if obj['manip_rot_x']>=math.radians(-45) and obj['manip_rot_x']<=math.radians(90):
                        # print ("A droite - en face")
                        scene.objects['Camera-pt'].applyRotation((0, -delta_y*sensibilite_orbit,-delta_x*sensibilite_orbit), False)
                    if obj['manip_rot_x']>math.radians(-100) and obj['manip_rot_x']<math.radians(-45):
                        # print ("A droite - dessus")
                        scene.objects['Camera-pt'].applyRotation((delta_x*sensibilite_orbit, -delta_y*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']>math.radians(90) and obj['manip_rot_x']<math.radians(180):
                        # print ("A droite - dessous")
                        scene.objects['Camera-pt'].applyRotation((-delta_x*sensibilite_orbit, -delta_y*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']<=math.radians(-100) or obj['manip_rot_x']>=math.radians(180):
                        # print ("A droite - retournée")
                        scene.objects['Camera-pt'].applyRotation((0, -delta_y*sensibilite_orbit, delta_x*sensibilite_orbit), False)

                # Derrière
                if obj['manip_rot_z']<=math.radians(-135) or obj['manip_rot_z']>=math.radians(135):
                    if obj['manip_rot_x']>=math.radians(-45) and obj['manip_rot_x']<=math.radians(90):
                        # print ("Derrière - en face")
                        scene.objects['Camera-pt'].applyRotation((delta_y*sensibilite_orbit, 0, -delta_x*sensibilite_orbit), False)
                    if obj['manip_rot_x']>math.radians(-100) and obj['manip_rot_x']<math.radians(-45):
                        # print ("Derrière - dessus")
                        scene.objects['Camera-pt'].applyRotation((delta_y*sensibilite_orbit, delta_x*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']>math.radians(90) and obj['manip_rot_x']<math.radians(180):
                        # print ("Derrière - dessous")
                        scene.objects['Camera-pt'].applyRotation((delta_y*sensibilite_orbit, delta_x*sensibilite_orbit, 0), False)
                    if obj['manip_rot_x']<=math.radians(-100) or obj['manip_rot_x']>=math.radians(180):
                        # print ("Derrière - retournée")
                        scene.objects['Camera-pt'].applyRotation((delta_y*sensibilite_orbit, 0, -delta_x*sensibilite_orbit), False)

            # Caméra vue de face/arr
            if scene.active_camera == scene.objects["Camera face"]:
                scene.objects['Camera face-pt'].applyRotation((-delta_y*sensibilite_orbit, 0, -delta_x*sensibilite_orbit), False)
            if scene.active_camera == scene.objects["Camera arr"]:
                scene.objects['Camera arr-pt'].applyRotation((delta_y*sensibilite_orbit, 0, -delta_x*sensibilite_orbit), False)

            # Caméra vue de coté
            if scene.active_camera == scene.objects["Camera gauche"]:
                scene.objects['Camera gauche-pt'].applyRotation((0, delta_y*sensibilite_orbit, -delta_x*sensibilite_orbit), False)
            if scene.active_camera == scene.objects["Camera droit"]:
                scene.objects['Camera droit-pt'].applyRotation((0, -delta_y*sensibilite_orbit, -delta_x*sensibilite_orbit), False)

            # Caméra vue de haut/bas
            if scene.active_camera == scene.objects["Camera haut"]:
                scene.objects['Camera haut-pt'].applyRotation((-delta_y*sensibilite_orbit, -delta_x*sensibilite_orbit, 0), False)
            if scene.active_camera == scene.objects["Camera bas"]:
                scene.objects['Camera bas-pt'].applyRotation((-delta_y*sensibilite_orbit, delta_x*sensibilite_orbit, 0), False)

        # Orbit sur la normale
        else:
            circle ([x0,y0,z0], radius_orbit_y_base, color_cmd_hl)
            if abs(delta_x) >= abs(delta_y):
                if obj['manip_click_x'] < bge.render.getWindowWidth()/2 and obj['manip_click_y'] < bge.render.getWindowHeight()/2:
                    scene.active_camera.applyRotation((0, 0, delta_x*sensibilite_orbit), True)
                elif obj['manip_click_x'] > bge.render.getWindowWidth()/2 and obj['manip_click_y'] > bge.render.getWindowHeight()/2:
                    scene.active_camera.applyRotation((0, 0, -delta_x*sensibilite_orbit), True)
                elif obj['manip_click_x'] > bge.render.getWindowWidth()/2 and obj['manip_click_y'] < bge.render.getWindowHeight()/2:
                    scene.active_camera.applyRotation((0, 0, delta_x*sensibilite_orbit), True)
                else:
                    scene.active_camera.applyRotation((0, 0, -delta_x*sensibilite_orbit), True)
            else:
                if obj['manip_click_x'] < bge.render.getWindowWidth()/2 and obj['manip_click_y'] < bge.render.getWindowHeight()/2:
                    scene.active_camera.applyRotation((0, 0, -delta_y*sensibilite_orbit), True)
                elif obj['manip_click_x'] > bge.render.getWindowWidth()/2 and obj['manip_click_y'] > bge.render.getWindowHeight()/2:
                    scene.active_camera.applyRotation((0, 0, delta_y*sensibilite_orbit), True)
                elif obj['manip_click_x'] > bge.render.getWindowWidth()/2 and obj['manip_click_y'] < bge.render.getWindowHeight()/2:
                    scene.active_camera.applyRotation((0, 0, delta_y*sensibilite_orbit), True)
                else:
                    scene.active_camera.applyRotation((0, 0, -delta_y*sensibilite_orbit), True)

    # Pan
    if obj['manip_mode'] == 1: # Shift
        scene.active_camera.applyMovement((delta_x*-sensibilite_pan, delta_y*sensibilite_pan, 0), True)
        # scene.cameras['Camera-Hud'].applyMovement((delta_x*-sensibilite_pan, delta_y*sensibilite_pan, 0), True)
        
        # scene.active_camera.shift_x += delta_x*-sensibilite_pan # Conflit entre les colboxs de la caméra et l'overlay, c'est dommage car beaucoup plus élégant
        # scene.active_camera.shift_y += -delta_y*-sensibilite_pan # Conflit entre les colboxs de la caméra et l'overlay, c'est dommage car beaucoup plus élégant

    # Zoom
    if obj['manip_mode'] == 2: # Ctrl
        if scene.active_camera == scene.objects["Camera"]:
            # scene.objects['Camera'].lens += ((delta_x+delta_y)*sensibilite_zoom)
            scene.active_camera.applyMovement((0, 0, (delta_x+delta_y)*sensibilite_zoom), True)

            # Mise à l'échelle des écritures dans Géoview
            scale_ecriture= 1+((((delta_x+delta_y)*sensibilite_zoom)/0.008)*0.005)
            if scene.active_camera.worldPosition.z < scene.objects['Camera']['init_z']: # Pas de grossissement
                for obj in scene.objects:
                    if 'gv_zoom' in obj.getPropertyNames():
                        obj.worldScale=obj.worldScale*scale_ecriture
        else:
            scene.active_camera.ortho_scale += ((delta_x+delta_y)*sensibilite_zoom)

##
# Molette
##

def manip_wheel(cont):
    obj = cont.owner
    sensibilite_wheel = 10/100 # Plus petit objet, facteur d'échelle faible
    # scale_ecriture= 1+((-sensibilite_wheel/0.008)*0.005)
    scale_ecriture= 1+((-sensibilite_wheel/0.009)*0.005)

    # Dézoom
    if cont.sensors['WheelUp'].positive:
        if scene.active_camera == scene.objects["Camera"]:
            scene.objects['Camera'].applyMovement((0, 0, -sensibilite_wheel), True)
            scene.objects['Camera-Hud'].applyMovement((0, 0, -sensibilite_wheel), True)
            # scene.objects['Camera'].lens -= sensibilite_wheel

            # Mise à l'échelle des écritures dans Géoview
            if scene.objects['Camera'].worldPosition.z < scene.objects['Camera']['init_z']: # Pas de grossissement
                for obj in scene.objects:
                    if 'gv_zoom' in obj.getPropertyNames():
                        obj.worldScale=obj.worldScale*scale_ecriture
        else:
            scene.active_camera.ortho_scale -= sensibilite_wheel

    # Zoom
    if cont.sensors['WheelDown'].positive:
        if scene.active_camera == scene.objects["Camera"]:
            scene.objects['Camera'].applyMovement((0, 0, sensibilite_wheel), True)
            scene.objects['Camera-Hud'].applyMovement((0, 0, sensibilite_wheel), True)
            # scene.objects['Camera'].lens += sensibilite_wheel

            # Mise à l'échelle des écritures dans Géoview
            if scene.objects['Camera'].worldPosition.z < scene.objects['Camera']['init_z']: # Pas de grossissement
                scale_ecriture= 1+((sensibilite_wheel/0.008)*0.005)
                for obj in scene.objects:
                    if 'gv_zoom' in obj.getPropertyNames():
                        obj.worldScale=obj.worldScale*scale_ecriture
        else:
            scene.active_camera.ortho_scale += sensibilite_wheel


###############################################################################
# Fichier
###############################################################################

def file_open():

    # Terminer le processus file précédent
    if ('file_proc' in scene.objects['Commands']):
        if scene.objects['Commands']['file_proc'].poll()==None:
            scene.objects['Commands']['file_proc'].terminate()

    # Démarrer le processus file
    scene.objects['Commands']['file_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"gtk","file.py"), 'cmd'], stdout=subprocess.PIPE, encoding = 'utf8')

    # Récupérer le nom du fichier
    stout = scene.objects['Commands']['file_proc'].communicate()
    if stout[0][:-1] != 'None' and len(stout[0][:-1])>0 :
        scene.objects['Commands']['script'] = stout[0][:-1]
        scene.objects['Script-text']['Text']=scene.objects['Commands']['script']

###############################################################################
# Cycle
###############################################################################


##
# Validation du code Python
##

def python_validation(file):

    # Terminer le processus fils précédent
    if ('pylint_proc' in scene.objects['Commands']):
        if scene.objects['Commands']['pylint_proc'].poll()==None:
            scene.objects['Commands']['pylint_proc'].terminate()

    # Lancer le processus fils
    scene.objects['Commands']['pylint_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(), "twin_pylint.py"), file], stdout=subprocess.PIPE, encoding = 'utf8')
    stout = scene.objects['Commands']['pylint_proc'].communicate()

    # Présence d'erreur
    if stout[0][:-1] != 'None' and len(stout[0][:-1])>0 :

        # UI : information
        scene.objects['Cmd-text']['modal']= True
        scene.objects['Cmd-text']['Text']= "Erreur dans le script ... "
        scene.objects['Cmd-text'].setVisible(True,False)

        # Affichage console
        stout_lines = stout[0].split("\n")
        for stout_line in stout_lines:
            print ("Pylint :", stout_line) # Affichage console
        return False

    # Absence d'erreur
    scene.objects['Cmd-text']['modal']= False
    scene.objects['Cmd-text']['Text']= ""
    scene.objects['Cmd-text'].setVisible(False,False)
    return True

##
# Executer le script de démarrage (bras_startcmd.py)
##

def cycle_startcmd():
    module_name= os.path.join(os.getcwd(), 'bras_startcmd.py')
    print (module_name)
    if os.path.exists(module_name):
        if python_validation(module_name):
            runpy.run_path(module_name, run_name='start') # Execution du script de démarrage

##
# Mise en route du cycle
##

def cycle_run():

    # Commandes
    scene.objects['System']['run']=True
    scene.objects['Run'].setVisible(False,True)
    scene.objects['Run-Hl'].setVisible(False,True)
    scene.objects['Run-colbox'].suspendPhysics()

    # Démarrage du cycle
    if scene.objects['System']['thread_cmd']==False:
        sleep(0.125)
        scene.objects['System']['thread_cmd']=True
        system.system_reset()
        sleep(0.125)
        scene.objects['System']['time']=0
        scene.objects['System']['daq']=False
        scene.objects['System']['daq_csv']=False
        scene.objects['System']['daq_plot']=False
        # twin_io.init_objects() # Init des objets par le panneau IO
        # twin_io.init_daq() # Init de l'acquisition des données par le panneau IO
        scene.objects['Twins-text']['Text'] = "Connection fermée."
        module_name=os.path.split((scene.objects['Commands']['script']))[1][:-3]
        if python_validation(scene.objects['Commands']['script']):
            runpy.run_path(scene.objects['Commands']['script'], run_name='start') # Exécution du script utilisateur

##
# Arrêt et réinitialisation du cycle (forçage)
##

def cycle_stop():
    scene.objects['System']['thread_cmd']=False

    # Arrêt immédiat des moteurs
    q=[]
    for i in range(6):
        obj = scene.objects[motors[i]]
        q.append(obj['q'])
    # print ('cycle stop :', q)
    for i_txt in motors:
        obj = scene.objects[i_txt]
        obj['cw']=False
        obj['acw']=False
    system.move_q(q, scene.objects['Moteur Pince']['value'], wait=False, cont=False)
    # sleep(0.01)

##
# Fin du cycle
##

def cycle_end(cont):
    if cont.sensors['End cycle'].positive:
        scene.objects['System']['run']=False
        module_name = os.path.split((scene.objects['Commands']['script']))[1][:-3]
        if python_validation(scene.objects['Commands']['script']):
            runpy.run_path(scene.objects['Commands']['script'], run_name='stop') # Fin du script utilisateur
        system.system_reset() # Réinitialisation du système

        # Commandes
        scene.objects['Run'].setVisible(True,True)
        scene.objects['Run-colbox'].restorePhysics()

##
# Highlight sur les éléments cliquable du systèmes
##

def cycle_hl(cont):

    # Présence d'une boite de colision
    if "-colbox" in cont.owner.name:
        obj = scene.objects[cont.owner.name[:-7]]
    else:
        obj = cont.owner

    # Activation
    if cont.sensors['MO'].status == JUST_ACTIVATED:
        obj['mo'] = True

        # Composant avec un groupe de couleur
        if "color_group" in obj.getPropertyNames():
            for obj_name in obj['color_group']:
                scene.objects[obj_name].color = color_hl
        else:
            obj.color = color_hl

        # Description
        if obj['description'] is not None:
            scene.objects['Cmd-text']['modal']= False
            scene.objects['Cmd-text']['Text']=obj['description']
            scene.objects['Cmd-text'].setVisible(True,True)

    # Désactivation
    if cont.sensors['MO'].status == JUST_RELEASED :
        obj['mo'] = False
        scene.objects['Cmd-text']['Text']=""

        # Composant passif
        if 'active' in obj.getPropertyNames():
            if obj['active'] == False:
                if 'color_group' in obj.getPropertyNames():
                    for obj_name in obj['color_group']:
                        scene.objects[obj_name].color = color_passive
                    else:
                        obj.color = color_passive
                return

        # Moteur activé/désactivé via le clavier
        if 'q_manual' in obj.getPropertyNames():
            if obj['q_manual']:
                for obj_name in obj['color_group']:
                    scene.objects[obj_name].color = color_mot_activated
            else:
                for obj_name in obj['color_group']:
                    cycle_actuator_color(obj_name)
            return

        # Moteur de pince
        if 'value_manual' in obj.getPropertyNames():
            if obj['value_manual']:
                for obj_name in obj['color_group']:
                    scene.objects[obj_name].color = color_mot_activated
            else:
                for obj_name in obj['color_group']:
                    cycle_actuator_color(obj_name)
            return

        # Pince
        if 'grab' in obj.getPropertyNames():
            if "color_group" in obj.getPropertyNames():
                for obj_name in obj['color_group']:
                    if scene.objects[obj_name]['grab']:
                        scene.objects[obj_name].color = color_yellow
                    else:
                        cycle_actuator_color(obj_name)
            return

        # Composant avec un groupe de couleur
        if "color_group" in obj.getPropertyNames():
            for obj_name in obj['color_group']:
                cycle_actuator_color(obj_name)
            return

        # Composant simple
        obj.color = color_active

##
# Click sur les éléments cliquables normalement ouverts (NO) du système (activation numérique)
##

def cycle_click(cont):
    obj = cont.owner

    # Activation
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0 and obj['forcing']==False:
        if "active" in obj.getPropertyNames():
            if obj['active']:
                 obj.color = color_activated
                 obj['activated'] = True
                 obj['click'] = True
        else:
            obj.color = color_activated
            obj['activated'] = True
            obj['click'] = True

    # Désactivation
    if cont.sensors['Click'].status == JUST_RELEASED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0 and obj['forcing']==False:
        obj['activated'] = False
        obj['click'] = False
        if cont.sensors['MO'].positive:
            obj.color = color_hl
        else:
            if "active" in obj.getPropertyNames():
                if obj['active']==False:
                    obj.color = color_passive
                else:
                    obj.color = color_active
            else:
                obj.color = color_active

##
# Click sur les éléments cliquables normalement fermés (NC)
# Lors de l'activation la valeur est False
##

def cycle_click_nc(cont):
    obj = cont.owner

    # Activation
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0 and obj['forcing']==False:
        if "active" in obj.getPropertyNames():
            if obj['active']:
                 obj.color = color_activated
                 obj['activated'] = False
                 obj['click'] = True
        else:
            obj.color = color_activated
            obj['activated'] = False
            obj['click'] = True

    # Désactivation
    if cont.sensors['Click'].status == JUST_RELEASED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0 and obj['forcing']==False:
        obj['activated'] = True
        obj['click'] = False
        if cont.sensors['MO'].positive:
            obj.color = color_hl
        else:
            if "active" in obj.getPropertyNames():
                if obj['active']==False:
                    obj.color = color_passive
                else:
                    obj.color = color_active
            else:
                obj.color = color_active

##
# Click sur les éléments rémanents (activation numérique)
# Lors de la désactivation, activated reprend la valeur d'avant le click
# FIXME : toujours nécessaire  ?
##

def cycle_click_persistent(cont):
    obj = cont.owner
 
    # Passif
    if "active" in obj.getPropertyNames():
        if obj['active'] == False:
            obj.color = color_passive
            return

    # Activation
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0 and obj['forcing']==False:
        obj.color = color_activated
        obj['activated_prev'] = obj['activated']
        obj['activated'] = True
        obj['click'] = True

    # Désactivation
    if cont.sensors['Click'].status == JUST_RELEASED and cont.sensors['MO'].positive and obj['forcing']==False:
        obj['activated'] = obj['activated_prev']
        obj['click'] = False
        if cont.sensors['MO'].positive:
            obj.color = color_hl
        else:
            if  obj['activated_prev'] :
                obj.color = color_activated
            else:
                obj.color = color_active
        obj['activated_prev'] = False

##
# Click sur les éléments commutables du système (activation numérique)
##

def cycle_switch(cont):
    obj = cont.owner
    obj_on=scene.objects[obj.name+"-on"]

    # Passif
    if "active" in obj.getPropertyNames():
        if obj['active'] == False:
            obj.color = color_passive
            return

    # Activation
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0 and obj['forcing']==False:

        if obj['activated']:  # On -> Off
            obj.color = color_hl
            obj['activated'] = False
            obj['click'] = True
            obj.setVisible(True,False)
            obj_on.setVisible(False,False)
        else: # Off -> On
            obj_on.color = color_activated
            obj['activated'] = True
            obj['click'] = True
            obj_on.setVisible(True,False)
            obj.setVisible(False,False)

    # Désactivation
    if cont.sensors['Click'].status == JUST_RELEASED and obj['forcing']==False:
        obj['click'] = False
        if cont.sensors['MO'].positive:
            obj.color = color_hl
        else:
            if obj['activated']:
                obj_on.color = color_activated
            else:
                obj.color = color_active

##
# Couleurs sur les éléments sensibles du système
# obj : objet numérique
# obj_on : objet numérique à l'état activé (si différent)
##

def cycle_sensitive_color(obj, obj_on=None):
    
    # Mouse over
    if obj['mo'] == True and obj['click'] == False and obj.color !=color_hl:
        obj.color = color_hl

    # Jumeau
    elif scene.objects['System']['twins']:
        if obj['activated_real']==False and obj['activated']==False and obj.color !=color_active:
            obj.color = color_active
        elif obj['activated_real'] and obj['activated'] and obj.color !=color_activated_dbl:
            obj.color = color_activated_dbl
            if obj_on is not None:
                obj_on.color =color_activated_dbl
        elif obj['activated_real'] and obj['activated']==False and obj.color !=color_activated_real:
            obj.color = color_activated_real
        elif obj['activated_real']==False and obj['activated'] and obj.color !=color_activated:
            obj.color = color_activated
            if obj_on is not None:
                obj_on.color =color_activated
        elif obj['activated_real']==False and obj['activated']==False and obj.color !=color_activated:
            obj.color = color_active

    # Uniquement maquette 3D
    else:
        if obj['activated'] == True and obj.color !=color_activated:
            obj.color = color_activated
        elif obj['activated'] == False and obj.color !=color_active:
            obj.color = color_active

##
# Couleurs des actionneurs
##

def cycle_actuator_color(name):
    obj = scene.objects[name]
    if obj['color'] == "white":
        obj.color = color_white
    if obj['color'] == "blue":
        obj.color = color_blue
    elif obj['color'] == "blue-dark":
        obj.color = color_blue_dark
    elif obj['color'] == "grey":
        obj.color = color_grey
    elif obj['color'] == "led_yellow":
        obj.color = color_led_yellow
    elif obj['color'] == "aru":
        obj.color = color_aru
    elif obj['color'] == "turquoise":
        obj.color = color_turquoise
    elif obj['color'] == "turquoise-dark":
        obj.color = color_turquoise_dark

##
# Comportement standard des boutons poussoirs
##

def cycle_bp(cont):
    obj = cont.owner

    # Modèle 3D -> Panneau IO
    # if obj['activated']:
    #     twin_io.led_high(obj, False, 'io')
    # if obj['activated'] == False:
    #     twin_io.led_low(obj, False, 'io')

    # Arduino -> Modèle 3D
    if scene.objects['System']['twins'] and obj['forcing_real']==False:
        if obj['pin'] is not None :
            if obj['pin'].read()==True and obj['activated_real'] == False :
                obj['activated_real'] = True
                # twin_io.led_high(obj, True, 'io_real')
            if obj['pin'].read()==False and obj['activated_real'] == True :
                obj['activated_real'] = False
                # twin_io.led_low(obj, True, 'io_real')

    # Couleurs
    cycle_sensitive_color(obj)

##
# Comportement standard des boutons à glissière
##

def cycle_bg(cont):
    obj = cont.owner
    obj_on=scene.objects[obj.name+"-on"]

    # Modèle 3D -> Panneau IO
    # if obj['activated']:
        # twin_io.led_high(obj, False, 'io')
    # if obj['activated'] == False:
        # twin_io.led_low(obj, False, 'io')

    # Arduino -> Modèle 3D
    if scene.objects['System']['twins'] and obj['forcing_real']==False:
        if  obj['pin'] is not None :
            if obj['pin'].read()==True and obj['activated_real'] == False :
                obj['activated_real'] = True
                # twin_io.led_high(obj, True, 'io_real')
            if obj['pin'].read()==False and obj['activated_real'] == True :
                obj['activated_real'] = False
                # twin_io.led_low(obj, True, 'io_real')

    # Couleurs
    cycle_sensitive_color(obj, obj_on)
            
##
# Comportement standard des capteurs (analogique)
##

def cycle_sensor(cont):
    obj = cont.owner

    # Physique du modèle 3D
    # if obj['activated'] and obj['value']!=obj['value_click']:
    #     obj['value']=obj['value_click']
    #     twin_io.led_high(obj, False, 'io')
    # if obj['activated']==False and obj['value']!=obj['value_unclick']:
    #     obj['value']=obj['value_unclick']
    #     twin_io.led_low(obj, False, 'io')

    # Arduino -> Modèle 3D
    if scene.objects['System']['twins'] and obj['forcing_real']==False:
        if  obj['pin'] is not None :
            obj['value_real'] = obj['pin'].read()
        # FIXME : état vers le panneau IO

    # Couleurs
    cycle_sensitive_color(obj)

##
# Comportement standard des voyants
##

def cycle_voy(cont):
    obj = cont.owner
    obj_on=scene.objects[obj.name+"-on"]
   
    # Activation du modèle numérique
    if obj['activated'] and obj_on.visible == False:
        obj_on.setVisible(True,False)
        obj.setVisible(False,False)
        # twin_io.led_high(obj, False, 'io')

    # Désactivation du modèle numérique
    if obj['activated']==False and obj_on.visible:
        obj.setVisible(True,False)
        obj_on.setVisible(False,False)
        # twin_io.led_low(obj, False, 'io')

    # # Panneau IO : bug d'affichage de la led lors d'un stop -> action séparée
    # if obj['activated'] and scene.objects[obj['io']]['activated'] == False:
    #     twin_io.led_high(obj, False, 'io')
    # if obj['activated']==False and scene.objects[obj['io']]['activated']:
    #     twin_io.led_low(obj, False, 'io')

    # Modèle réel
    if scene.objects['System']['twins']:

        # Activation
        if obj['activated_real_forced'] and obj['activated_real']==False:
            if  obj['pin'] is not None:
                obj['pin'].write(1)
                obj['activated_real']=True
                # twin_io.led_high(obj, True, 'io_real')

        # Désactivation
        if obj['activated_real_forced'] ==False and obj['activated_real']:
            if  obj['pin'] is not None:
                obj['pin'].write(0)
                obj['activated_real']=False
                # twin_io.led_low(obj, True, 'io_real')

    # Mouse over
    if obj['mo'] == True and obj.color !=color_hl:
        obj.color = color_hl

##
# Mise en couleur des actionneurs
##

def cycle_def_focusgroup(focusgroup, description):
    group=[]
    for obj in focusgroup:
        group.append(obj[0])
    for obj in focusgroup:
        scene.objects[obj[0]]['color_group'] = group
        scene.objects[obj[0]]['color'] = obj[1]
        scene.objects[obj[0]]['description'] = description
        cycle_actuator_color(obj[0])

