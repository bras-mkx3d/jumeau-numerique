import sys

from PyQt6 import QtCore, QtGui, QtWidgets # GUI Qt6

import matplotlib # Grapheur Matplotlib
import matplotlib.pyplot as plts
matplotlib.use('QtAgg')
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar

from twin_plot import plotconfig_get_dict, plotconfig_get, plotconfig_get_title, plotconfig_get_enable, plot_nb, csv_read, plots_static # Gestion communes des graphiques (Qt, wx)

###############################################################################
# twin_plot_qt.py
# @title: Visualisation des données (pyQt6+Matplotlib)
# @project: Blender-EduTech
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# FIXME : à basculer sur GTK

plotconfig={}

###############################################################################
# Zone de dessin
###############################################################################

class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        if plot_nb(plotconfig) ==1: # plot_nb() : nombre de graphiques
            fig, self.plt = plts.subplots() 
        else:
            fig, self.plt = plts.subplots(plot_nb(plotconfig), 1, sharex=True) # plot_nb() : nombre de graphiques
        super(MplCanvas, self).__init__(fig)

###############################################################################
# Graphique dynamique
###############################################################################

class DynamicPlot(QtWidgets.QMainWindow):

    # Création du graphique
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        self.setCentralWidget(self.canvas)
        n_data = 50
        # self.xdata = list(range(n_data))
        # self.ydata = [random.randint(0, 10) for i in range(n_data)]
        self.xdata = [0]
        self.ydata = [0]
        self.update_plot()
        self.show()

        # Timer pour le update_plot
        fps = 120  # Blender est cadencé à 60 fps
        self.timer = QtCore.QTimer()
        # self.timer.setInterval(int(1000/fps))
        self.timer.setInterval(1)
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()
        print("Qt : Canvas ready\n")

    # Lecture des données et mise à jour du graphique
    def update_plot(self):
        plt = self.canvas.subplot

        # Données
        msg=""
        for line in sys.stdin:
            msg = line.rstrip()
            break
        print("Qt : ", msg)

        # Essai
        new_x=self.xdata[len(self.xdata)-1]+1
        self.xdata.append(new_x)
        self.ydata.append(random.randint(0, 10))

        # Lecture du Pipe simple
        # msg=""
        # print (sys.stdin)
        # contents = sys.stdin.read(1)
        # # contents = sys.stdin.buffer
        # print ("contents :", contents)

        # for line in sys.stdin:
        #     msg = line.rstrip()
        #     break
        # print(msg)

        # X et Y
        # FIXME : temps et une valeur
        # msg_list=msg.split(',')
        # print(msg_list)
        # if msg_list[0] != "":
        #     self.xdata = self.xdata + [float(msg_list[0])]
        #     self.ydata = self.ydata + [float(msg_list[1])]

        # # Lecture du Pipe
        # # i=0
        # # lines = sys.stdin.readlines()
        # # print (lines)
        # # print ("b")
        
        # msg_line=""
        # # msg_lines=[]
        # for line in sys.stdin:
        #     # i+=1
        #     # print (i)
        #     msg_line = line.rstrip()
        #     # msg_lines.append(msg_line)
        #     # print("Qt msg_lines :", msg)
        #     # if i==2:
        #     #     break
        #     break
        # print("Qt :", msg_line)
        # # print("Qt msg_lines :", msg_lines)

        # # X et Y
        # # FIXME : temps msg_list[0] et une seule valeur msg_list[1]
        # msg_list=msg_line.split(',')
        # # print(msg_list)
        # if msg_list[0] != "":
        #     self.xdata = self.xdata + [float(msg_list[0])]
        #     self.ydata = self.ydata + [float(msg_list[1])]
               

        # for line in msg_lines:
        #     msg_list=line.split(',')
        #     # print(msg_list)
        #     if msg_list[0] != "":
        #         self.xdata = self.xdata + [float(msg_list[0])]
        #         self.ydata = self.ydata + [float(msg_list[1])]

        # for line in sys.stdin:
        # msg_list=msg.split(',')
        # print(msg_list)
        # if msg_list[0] != "":
        #     self.xdata = self.xdata + [float(msg_list[0])]
        #     self.ydata = self.ydata + [float(msg_list[1])]

        # self.ydata = self.ydata + [random.randint(0, 10)]
        # Drop off the first y element, append a new one.
        # self.ydata = self.ydata[1:] + [random.randint(0, 10)]

        # Redraw
        plt.cla()  
        plt.plot(self.xdata, self.ydata, 'r')
        self.canvas.draw()

###############################################################################
# Graphique statique
###############################################################################

class MainWindow(QtWidgets.QMainWindow):

    # Création du graphique
    def __init__(self, *args, **kwargs):

        # Zone graphique (barre d'outil et graphique)
        # FIXME : perd de la place quand on agrandi
        super(MainWindow, self).__init__(*args, **kwargs)
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        # print (self.canvas.getContentsMargins())
        toolbar = NavigationToolbar(self.canvas, self)
        # print (toolbar.getContentsMargins())
     
        # Implantation de la fenêtre
        layout = QtWidgets.QVBoxLayout()
        # print (layout.getContentsMargins())
        # layout.setContentsMargins(0,0,0,0)
        layout.addWidget(toolbar)
        layout.addWidget(self.canvas)
        widget = QtWidgets.QWidget()
        # print (widget.getContentsMargins())
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        # print (self.getContentsMargins())

        # Remplissage des graphiques à partir du fichier CSV
        fields, xdata, ydata = csv_read(sys.argv[1])
        plots_static(self.canvas.plt, fields, xdata, ydata, plotconfig, plot_title)
        self.show()

###############################################################################
# Application
###############################################################################

if __name__ == "__main__":

    # Configuration des plots
    plotconfig=plotconfig_get_dict()
    plot_title=plotconfig_get_title(plotconfig)

    # Fenêtre
    app = QtWidgets.QApplication(sys.argv)
    # w = DynamicPlot()
    w = MainWindow() # StaticPlot()
    w.setWindowTitle(plot_title)
    app.exec()
    sys.exit(0)
