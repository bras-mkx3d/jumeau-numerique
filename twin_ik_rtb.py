import sys
import math

import bge # Blender Game Engine (UPBGE)

import roboticstoolbox as rtb # Robotics Toolbox for Python
from spatialmath import SE3
import spatialmath.base as sm
import numpy as np

# # NumPy
# if 'numpy' not in sys.modules and hasattr(sys, '__numpy__'):
#     # restore module cache with backup ref
#     numpy = sys.modules['numpy'] = sys.__numpy__
# else:
#     import numpy
#     # store a backup ref
#     sys.__numpy__ = numpy


###############################################################################
# twin_ik_rtb.py
# @title: Solveur de cinématique inverse Robotics Toolbox for Python (RTB)
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Maths
PRECISION_DIST   = 5 # Nombre de décimal pour les distances en m
PRECISION_ANGLE  = 2 # Nombre de décimal pour les angles en degré

# UPBGE scene
scene = bge.logic.getCurrentScene()

###############################################################################
# Modélisation géométrique du robot 
###############################################################################

# Définition du robot Denavit-Hartenberg modifié
robot = rtb.DHRobot(
    [
        rtb.RevoluteMDH(alpha=0.0,        a=0.0,     offset=-math.pi/2, d=0.22075, qlim=[  -math.pi,     math.pi]),   # Segment 1
        rtb.RevoluteMDH(alpha=math.pi/2,  a=0.0,     offset=math.pi/2,  d=0.0,     qlim=[  -math.pi/2,   math.pi/2]), # Segment 2
        rtb.RevoluteMDH(alpha=0.0      ,  a=0.22152, offset=math.pi/2,  d=0.0,     qlim=[  -math.pi/2,   math.pi/2]), # Segment 3
        rtb.RevoluteMDH(alpha=math.pi/2,  a=0.0,     offset=0.0,        d=0.228,   qlim=[  -math.pi,     math.pi]),   # Segment 4
        rtb.RevoluteMDH(alpha=-math.pi/2, a=0.0,     offset=0.0,        d=0.0,     qlim=[  -math.pi/2,   math.pi/2]), # Segment 5
        rtb.RevoluteMDH(alpha=math.pi/2,  a=0.0,     offset=-math.pi/2,    d=0.203,   qlim=[-2*math.pi,   2*math.pi]),   # Segment 6
    ], name="MKX3D")

def rtb_get_robot():
    return (robot)

###############################################################################
# Maths
###############################################################################

def p2T(p):
    return (SE3(p[0],p[1],p[2])*SE3.RPY(p[3],p[4],p[5], unit="deg"))

def T2list(T):
    array_var = np.array(T)
    return array_var.tolist()

###############################################################################
# Modèle géométrique
###############################################################################

##
# Modèle géométrique direct (MGD)
#
# q          : coordonnées articulaires [q1,q2,q3,q4,q5,q6] (float)
# unit       : unité des angles ('rad', 'deg')
# Retourne p : coordonnées opérationnelles [x,y,z,rx,ry,rz] (float, en m et degrés)
# # Retourne T : transformée (matrice 4x4)
##

def rtb_fkine(q, unit="rad"):
    if unit=="deg":
        T = robot.fkine(np.radians(np.array(q)))
    else:
        T = robot.fkine(np.array(q))
    p_list=[]
    position=T.t
    for i in range (len(position)):
        p_list.append(round(position[i], PRECISION_DIST))
    orientation=sm.tr2rpy(np.array(T), unit='deg')
    for i in range (len(orientation)):
        p_list.append(round(orientation[i], PRECISION_ANGLE))
    # return p_list, T
    return p_list

##
# Modèle géométrique indirecte (MGI - solveur Levemberg-Marquadt)
#
# p                : coordonnées opérationnelles [x,y,z,rx,ry,rz] (float, en m et degrés)
# Retourne q       : coordonnées articulaires [q1,q2,q3,q4,q5,q6] (float, en degrés)
# Retourne success : solution géométrique trouvée (binaire)
# Retourne error   : distance entre la cible et la solution trouvée (réel, en m)
##


def rtb_ikine(p):
    T = SE3(p[0],p[1],p[2])*SE3.RPY(p[3],p[4],p[5], unit="deg")
    sol = robot.ikine_LM(T)
    T2 = robot.fkine(np.array(sol.q))
    p2=T.t
    error = round(math.sqrt((p2[0]-p[0])**2+(p2[1]-p[1])**2+(p2[2]-p[2])**2), 5) # Fonction distance
    return ([round(math.degrees(sol.q[0]), PRECISION_ANGLE),
             round(math.degrees(sol.q[1]), PRECISION_ANGLE),
             round(math.degrees(sol.q[2]), PRECISION_ANGLE),
             round(math.degrees(sol.q[3]), PRECISION_ANGLE),
             round(math.degrees(sol.q[4]), PRECISION_ANGLE),
             round(math.degrees(sol.q[5]), PRECISION_ANGLE)], sol.success, error)
