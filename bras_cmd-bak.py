# -*- coding: utf-8 -*-
from bras_lib import * # MKX3D Arm

###############################################################################
# bras_cmd.py
# @title: Commandes du bras MKX3D
###############################################################################

# Couleurs
color_white          = (0.8  , 0.8  , 0.8  , 1) # Blanc
color_blue           = (0.007, 0.111, 0.638, 1) # Couleur bleu (actionneur type moteur)
color_blue_dark      = (0.004, 0.054, 0.296, 1) # Couleur bleu noir
color_grey           = (0.285, 0.285, 0.285, 1) # Couleur gris
color_turquoise      = (0.051, 0.270, 0.279, 1) # Couleur turquoise
color_turquoise_dark = (0.030, 0.148, 0.152, 1) # Couleur turquoise foncée
color_yellow         = (0.800, 0.619, 0.021, 1) # Jaune
color_led_yellow     = (0.799, 0.617, 0.021, 1) # Couleur Led jaune (led)
color_red            = (0.694, 0.098, 0.098, 1) # Rouge
color_aru            = (0.693, 0.097, 0.097, 1) # Couleur de l'arrêt d'urgence
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_blue_light     = (0.015, 0.150, 0.687, 1) # Bleu clair
color_purple         = (0.799, 0.005, 0.314, 1) # Violet foncé
color_purple_light   = (0.687, 0.125, 0.578, 1) # Violet clair

# Brochage
brochage={}

###############################################################################
# Functions
###############################################################################

###############################################################################
# Commands
###############################################################################

def commands():

    # Write your code here ...
    move_q([-10,-70,20, 30, 40, 20])
    move_q([-90, 50,30,-100, 90,-20], draw=True)

    # while True:
    #     pass


###############################################################################
# En: External call << DONT CHANGE THIS SECTION >>
# Fr: Appel externe << NE PAS MODIFIER CETTE SECTION >>
###############################################################################

###
# Variant 1 : Rigid grabber
# Variant 2 : Pen
# Variant 3 : Suction cup
# Variant 4 : Soft grabber
###

def cycle():
    commands()
    end()

if __name__=='start':
    start(cycle)
if __name__=='stop':
    stop()
if __name__=='init':
    variant(1) # Rigid grabber variant
