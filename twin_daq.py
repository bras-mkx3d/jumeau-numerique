import os, sys
import importlib
import threading # Multithreading
import subprocess # Multiprocessus

import bge # Blender Game Engine (UPBGE)

from twin_time import twin_tempo # Gestion du temps
import time
import csv as csvlib # Parser de fichier CSV
import xml.etree.ElementTree as ET # Parser de fichier XML

###############################################################################
# twin_daq.py
# @title: Enregistement et visualisation des données
# @project: Blender-EduTech
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# UPBGE scene
scene = bge.logic.getCurrentScene()

# Colors
color_io            =  (0.198, 0.109, 0.8, 1)   # Violet
color_io_hl         =  (0.8, 0.005, 0.315, 1)   # Magenta
color_io_activate   =  (0.051, 0.270, 0.279,1)  # Turquoise
color_io_state_high =  (0.799, 0.031, 0.038, 1) # Rouge : état haut
color_io_state_low  =  (0.300, 0.300, 0.300, 1) # Gris clair : état bas
color_io_state_hl   =  (0.10, 0.10, 0.10, 1)    # Gris moyen

# Récupérer les données publiques
system=importlib.import_module(scene.objects['System']['system']) # Système
daq_config = system.get_public_vars()

# Données de l'acquisition
csv_data=[]
plot_data=[]

###############################################################################
# Accès aux variables publiques du système
###############################################################################

##
# Retourne la valeur d'une entrée/sortie
##

def get(name):
    if name not in daq_config:
        print ("Accès aux données : variable '"+name+"' absente du système -> fonction 'get('"+name+"')' annulée.")
        return None
    return(scene.objects[daq_config[name][0][0]][daq_config[name][0][1]]*daq_config[name][0][4]) # Echelle de la valeur : daq_config[name][0][4]

##
# Force la valeur d'une entrée/sortie
##

def set(name, forcing=False, value=None):
    if name not in daq_config:
        print ("Accès aux données : variable '"+name+"' absente du système -> fonction 'set('"+name+"',...)' annulée.")
        return False

    # Forcage
    if forcing==False :
        scene.objects[daq_config[name][0][0]][daq_config[name][3][3]]=False
        # print ("Forcage :", daq_config[name][0][0], daq_config[name][3][3], scene.objects[daq_config[name][0][0]][daq_config[name][3][3]])
        return True

    # Activation du forçage
    scene.objects[daq_config[name][0][0]][daq_config[name][3][3]]=True
    # print ("Forcage :", daq_config[name][0][0], daq_config[name][3][3], scene.objects[daq_config[name][0][0]][daq_config[name][3][3]])

    # IO réelle
    if "_real" in daq_config[name][0][1]:
        scene.objects[daq_config[name][0][0]][daq_config[name][3][4]]=value
        # print ("IO réelle :", daq_config[name][0][0], daq_config[name][3][4], scene.objects[daq_config[name][0][0]][daq_config[name][3][4]])
 
    # IO numérique
    else:
        scene.objects[daq_config[name][0][0]][daq_config[name][3][2]]=value
        # print ("IO num :", daq_config[name][0][0], daq_config[name][3][2], scene.objects[daq_config[name][0][0]][daq_config[name][3][2]])
    
    return True

###############################################################################
# Enregistrement des données
###############################################################################

##
# Activation de l'acquisition pour le fichier CSV
##

def csv(data):

    # Mise en place des variables du CSV
    csv_data.clear()
    scene.objects['System']['csv_var'] =[]
    row = ['t']
    for var in data:
        scene.objects['System']['csv_var'].append([daq_config[var][0][0], daq_config[var][0][1]])
        row.append(var)
    csv_data.append(row)
    scene.objects['System']['daq']=True
    scene.objects['System']['daq_csv']=True

##
# Activation de l'acquisition pour le graphique
##

def plot(data_groups):

    # Lecture des variables
    data=[]
    for data_group in data_groups:
        if isinstance(data_group, list):
            for data_subgroup in data_group: # Scan du sous-groupe
                data.append(data_subgroup)
        else:
            data.append(data_group) # Pas de groupe

    # Mise en place des variables du plot
    plot_data.clear()
    scene.objects['System']['plot_var'] =[]
    row = ['t']
    for var in data:
        scene.objects['System']['plot_var'].append([daq_config[var][0][0], daq_config[var][0][1]])
        row.append(var)
    plot_data.append(row)
    plotconfig_generate(data_groups)
    scene.objects['System']['daq']=True
    scene.objects['System']['daq_plot']=True

##
# Ajout des données (lecture/s = fps = 60)
##

def daq_add(cont):

    # CSV
    if scene.objects['System']['daq_csv']:
        row_daq = [round(scene.objects['System']['time'], 3)]
        for var in scene.objects['System']['csv_var']:
            row_daq.append(round(scene.objects[var[0]][var[1]], 3))
        csv_data.append(row_daq)

    # Plot
    if scene.objects['System']['daq_plot']:
        row_plot = [round(scene.objects['System']['time'], 3)]
        for var in scene.objects['System']['plot_var']:
            row_plot.append(round(scene.objects[var[0]][var[1]], 3))
        plot_data.append(row_plot)

###############################################################################
# Tableau CSV
###############################################################################

# Format français des décimaux
def localize_floats(row):
    return [
        str(el).replace('.', ',') if isinstance(el, float) else el
        for el in row
    ]

##
# Génération du fichier CSV
##

def csv_generate():

    # Ouvrir le flux
    scene.objects['System']['daq_csv']=False
    scene.objects['System']['daq']=False
    fichier_csv = scene.objects['System']['system']+'.csv'
    filename=os.path.join(os.path.split((scene.objects['Commands']['script']))[0], fichier_csv)
    scene.objects['Cmd-text']['modal']= True
    scene.objects['Cmd-text']['Text']= "Génération du fichier : "+fichier_csv
    scene.objects['Cmd-text'].setVisible(True,False)
    print ("Accès aux données : génération du fichier de données '"+ fichier_csv+"'")
    with open(filename, 'w') as file:
        writer = csvlib.writer(file, delimiter = ';', lineterminator = '\n')

        # Ecriture des données
        for t_data in csv_data:
            writer.writerow(localize_floats(t_data))

##
# Génération du fichier de données pour le graphique (format CSV)
##

def csv_plot_generate():
    scene.objects['System']['daq_plot']=False
    scene.objects['System']['daq']=False
    fichier_csv = 'plot_data.csv'
    filename=os.path.join(os.path.split((scene.objects['Commands']['script']))[0], fichier_csv)
    print ("Accès aux données : génération du fichier de données '"+ fichier_csv+"'")
    with open(filename, 'w') as file:
        writer = csvlib.writer(file, delimiter = ';', lineterminator = '\n')
        for t_data in plot_data:
            writer.writerow(localize_floats(t_data))

###############################################################################
# Graphique statique
###############################################################################

##
# Création du fichier twin_plot.xml (configuration du graphique)
##

def plotconfig_generate(data_groups):

    # Création du XML
    daq_config_list=list(daq_config)
    xml_data = ET.Element('data')
    xml_figure = ET.SubElement(xml_data, 'figure')
    xml_plot = ET.SubElement(xml_figure, 'plot')
    xml_title = ET.SubElement(xml_plot, 'title')
    xml_title.text="Chronogrammes - "+scene.objects['System']['name']
    for var in daq_config_list:
        xml_var = ET.SubElement(xml_plot, 'var')
        xml_var.text=var

        # Configuration graphique de la série de données
        if len(daq_config[var][2]) >=4:
            xml_mark = ET.SubElement(xml_var, 'marker')
            xml_mark.text=daq_config[var][2][0]
            xml_line = ET.SubElement(xml_var, 'linestyle')
            xml_line.text=daq_config[var][2][1]
            xml_color = ET.SubElement(xml_var, 'color')
            xml_color.text=daq_config[var][2][2]
            xml_size = ET.SubElement(xml_var, 'linewidth')
            xml_size.text=str(daq_config[var][2][3])
        xml_type = ET.SubElement(xml_var, 'type') # Binaire, analogique ou numérique
        xml_type.text=str(daq_config[var][0][2])

        # Détection de groupe de graphique
        data_group_i=0
        xml_group = ET.SubElement(xml_var, 'group')
        xml_group.text="-1" # Pas de groupe par défaut
        for data_group in data_groups:
            if isinstance(data_group, list):
                data_group_i+=1
                for data_subgroup in data_group: # Scan du sous-groupe
                    if data_subgroup == var: 
                        xml_group.text=str(data_group_i)
            else:
                if data_group == var: # Pas de groupe
                    xml_group.text="0"

    # Ecriture fichier
    plotconfig = ET.ElementTree(xml_data)
    with open("plot_config.xml", "wb") as f: # XML généré plat (not pretty print)
        plotconfig.write(f)

##
# Génération du graphique
# Threading -> reste actif après le Stop
##

def plot_generate_thread(repfichier_csv):
    if sys.platform=="linux": # wxPython ne s'installe pas bien sur GNU/linux -> Qt6
        command = sys.executable + " " + os.path.join(sys.executable, os.getcwd(), "twin_plot_qt.py") + " "+ repfichier_csv
    else: # Qt6 ne s'installe pas bien sur Windows -> wxPython
        command = sys.executable + " " + os.path.join(sys.executable, os.getcwd(), "twin_plot_wx.py") + " "+ repfichier_csv
    os.system(command)
 
def plot_generate(threads):
    fichier_csv = 'plot_data.csv'
    repfichier_csv='"'+os.path.join(os.path.split((scene.objects['Commands']['script']))[0], fichier_csv)+'"'
    print ("Accès aux données : génération du graphique à partir du fichier '"+ fichier_csv+"'")
    threads.append(threading.Thread(target=plot_generate_thread, args=(repfichier_csv,)))
    threads[len(threads)-1].start()

###############################################################################
# Graphique interactif
# FIXME : ne marche pas
###############################################################################

##
# Mise en forme des décimaux
##

def truncate(n, decimals=0):
    multiplier = 10**decimals
    return int(n* multiplier)/multiplier

##
# Création du graphique interactif
##

def plot_start(data):
    # subprocess.run([sys.executable, os.path.join(os.getcwd(), "twin_plot.py")], , stdin=subprocess.PIPE) # Process bloquant

    # Terminer le processus plot précédent
    if ('plot_proc' in scene.objects['System']):
        if scene.objects['System']['plot_proc'].poll()==None:
            scene.objects['System']['plot_proc'].terminate()
    scene.objects['System']['plot_draw'] = True
    scene.objects['System']['plot_time'] = 0

    # Configuration des données
    scene.objects['System']['plot_data'] =[]
    for obj in data:
        scene.objects['System']['plot_data'].append(daq_config[obj][0][0])

    # Démarrer le processus plot
    scene.objects['System']['plot_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(), "twin_plot_qt.py")], stdin=subprocess.PIPE, encoding = 'utf8', universal_newlines=True)
    # # scene.objects['System']['plot_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(), "twin_plot_qt.py")], stdin=subprocess.PIPE, stdout=subprocess.PIPE, encoding = 'utf8')
    # # stout = scene.objects['System']['plot_proc'].communicate() # FIXME : attente du message retour pour lancer l'acquisition
    # # print("Blender stout : ", stout)
    scene.objects['System']['plot']=True

##
# Ajout des données (lecture/s = fps = 60)
##

def plot_add(cont):
    if cont.sensors['Plot'].positive :

        # Affichage du graphique
        if scene.objects['System']['plot_draw']:
            if scene.objects['System']['plot_time'] != truncate(scene.objects['System']['time'], 0) or True:

                # Préparation du message
                # FIXME : ajouter les valeurs réelles et valeurs numériques ('activated_real')
                scene.objects['System']['plot_time'] = truncate(scene.objects['System']['time'], 0)
                # msg=str(round(scene.objects['System']['plot_time'], 1))
                # msg=format(scene.objects['System']['plot_time'],".2f")
                msg=format(scene.objects['System']['plot_time'],".2f")
                for obj in scene.objects['System']['plot_data']:
                    if scene.objects[obj]['activated']:
                        msg = msg+",1"
                    else:
                        msg = msg+",0"
                msg = msg+"\n"

                # Envoi (Pipe)
                if scene.objects['System']['plot_proc'].poll()==None:
                    # # scene.objects['System']['plot_proc'].communicate(input=time_send.encode())[0] # Communication bloquante
                    # scene.objects['System']['plot_proc'].communicate(input=msg.encode())[0] # Communication bloquante
                    scene.objects['System']['plot_proc'].communicate(input=msg.encode()) # Communication bloquante
                    # scene.objects['System']['plot_proc'].stdin.write(msg)
                    print ("twin : ", msg)
                else:
                    # Arrêt
                    print ("Stop")
                    scene.objects['System']['plot']=False
                    scene.objects['System']['plot_draw'] =False
                    scene.objects['System']['plot_proc'].terminate()

            # Arrêt de l'affichage du graphique
            if scene.objects['System']['plot_proc'].poll()==None:
                pass
            else:
                print ("Stop")
                scene.objects['System']['plot']=False
                scene.objects['System']['plot_draw'] =False
                scene.objects['System']['plot_proc'].terminate()
