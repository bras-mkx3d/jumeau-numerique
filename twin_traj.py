import os, sys
import importlib
import subprocess     # Multiprocessus
import twin_threading # Multithreading (multitâches)
import math, mathutils
import csv as csvlib  # Parser de fichier CSV

import bge, bpy # Bibliothèque Blender Game Engine (UPBGE)

import bras                        # Gestion du bras robotisé
from twin_time import sleep, tempo # Gestion du temps

###############################################################################
# twin_traj.py
# @title: Gestion des trajectoires
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>g
# @copyright: Copyright (C) 2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Maths
PRECISION_DIST   = 5 # Nombre de décimal pour les distances en m
PRECISION_ANGLE  = 2 # Nombre de décimal pour les angles en degré

# UPBGE scene
scene = bge.logic.getCurrentScene()
system = importlib.import_module(scene.objects['System']['system']) # Système
motors=['Moteur S0', 'Moteur S1', 'Moteur S2', 'Moteur S3', 'Moteur S4', 'Moteur S5']

# Couleurs
color_hl              =  (0.800, 0.800, 0.800, 1)   # Bouton focus : blanc
color_traj            =  (0.198, 0.109, 0.8  , 1)   # Violet
color_traj_hl         =  (0.8  , 0.005, 0.315, 1)   # Magenta
color_traj_unactive   =  (0.300, 0.300, 0.300, 1)   # Gris clair
# color_traj_activate   =  (0.051, 0.270, 0.279,1)  # Turquoise
# color_traj_state_high =  (0.799, 0.031, 0.038, 1) # Rouge : état haut
# color_traj_state_low  =  (0.300, 0.300, 0.300, 1) # Gris clair : état bas
# color_traj_state_hl   =  (0.10, 0.10, 0.10, 1)    # Gris moyen

# Générales
color_white          = (0.8  , 0.8  , 0.8  , 1) # Blanc
color_blue           = (0.007, 0.111, 0.638, 1) # Couleur bleu (actionneur type moteur)
color_blue_dark      = (0.004, 0.054, 0.296, 1) # Couleur bleu noir
color_blue_light     = (0.015, 0.150, 0.687, 1) # Bleu clair
color_grey           = (0.285, 0.285, 0.285, 1) # Couleur gris
color_turquoise      = (0.051, 0.270, 0.279, 1) # Couleur turquoise
color_turquoise_dark = (0.030, 0.148, 0.152, 1) # Couleur turquoise foncée
color_yellow         = (0.800, 0.619, 0.021, 1) # Jaune
color_led_yellow     = (0.799, 0.617, 0.021, 1) # Couleur Led jaune (led)
color_red            = (0.694, 0.098, 0.098, 1) # Rouge
color_aru            = (0.693, 0.097, 0.097, 1) # Couleur de l'arrêt d'urgence
color_green          = (0.083, 0.223, 0.028, 1) # Vert clair
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_purple         = (0.799, 0.005, 0.314, 1) # Violet foncé
color_purple_light   = (0.687, 0.125, 0.578, 1) # Violet clair

# Repère
color_rep_red        = (0.799, 0.107, 0.087, 1) # Axe x
color_rep_green      = (0.083, 0.223, 0.028, 1) # Axe y
color_rep_blue       = (0.015, 0.150, 0.687, 1) # Axe z
color_rep_target     = (0.051, 0.270, 0.279, 1) # Origine Rc (cible)
color_rep_grey       = (0.283, 0.283, 0.283, 1) # Désactivation des axes

# UPBGE constants
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED  = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE       = bge.logic.KX_INPUT_ACTIVE
# SENSOR_JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED
# SENSOR_JUST_ACTIVATED   = bge.logic.KX_SENSOR_JUST_ACTIVATED

# Threads pour la trajectoire
threads_traj=[]
threads_obj=[]

###############################################################################
#
# Fichier trajectoire au format CSV :
# Ligne CSV : 'label','q1','q2','q3','q4','q5','q6','ot','x','y','z','rx','ry','rz','prop','mode' (une ligne par point)
# Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode' (une ligne par point)
#
# - label             : étiquette du point (texte)
# - q1,q2,q3,q4,q5,q6 : coordonnées articulaires du point (réels en degrée)
# - ot                : valeur de l'outil au point (entier de 0 à 100)
# - x,y,z,rx,r,rz     : coordonnées cartésiennes du point (réels en m et degrée)
# - prop              : propriétés du trajet (liste sous forme de texte)
# - mode              : mode de saisie (texte)
#
# Propriétés du trajet (pas d'ordre d'écriture):
# - color             : couleur des points (RGBA tuple (Rouge Vert Bleu Alpha), color_yellow par défaut)
# - speed             : vitesse du trajet (réel en m/s)
# - thickness         : épaisseur, taille des points (réel, facteur d'échelle, 1 par défaut)
# - tickskip          : densité des points de la trajectoire, tous les x ticks (60 fps) (entier, 3 par défaut)
# - cont              : mode trajectoire continue (tous les mouvements arrivent en même temps) (binaire, uniquement en mode bloquant, par défaut)
# - force             : permet de dépasser les limites angulaires des moteurs (binaire)
#
###############################################################################

###############################################################################
# Panneau
###############################################################################

##
# Initialisation du panneau
##

def init():
    bt=['Traj_load', 'Traj_object', 'Traj_play', 'Traj_stop', 'Traj_save_csv', 'Traj_save_python', 'Traj_clear']
    for i_bt in bt:
        scene.objects[i_bt].color= color_traj
        scene.objects[i_bt+"-colbox"].suspendPhysics()
   
##
# Activer/Désactiver le panneau des entrées/sorties
# Position ouverte (-1.56854 , -156.456 , 90.7125 ), position fermée (-1.43062, -156.456 , 90.7125 ), 
##

def open_panel():

    # Fermer
    if scene.objects['Commands']['traj']:
        bt=['Traj_load', 'Traj_object', 'Traj_play', 'Traj_stop', 'Traj_save_csv', 'Traj_save_python', 'Traj_clear']
        scene.objects['Traj-panel'].setVisible(False,True)
        scene.objects['Commands']['traj']=False
        for i_bt in bt:
            scene.objects[i_bt+"-colbox"].suspendPhysics()
        return

    # Ouvrir
    bt=['Traj_load', 'Traj_object', 'Traj_play', 'Traj_save_csv', 'Traj_save_python', 'Traj_clear']
    scene.objects['Traj-panel'].setVisible(True, True)
    scene.objects['Commands']['traj']=True
    for i_bt in bt:
        scene.objects[i_bt+"-colbox"].restorePhysics()
    scene.objects['Traj_stop'].color= color_traj_unactive
    scene.objects['Traj_stop'+"-colbox"].suspendPhysics()

##
# Highlight
##

def hl(cont):
    obj = cont.owner
    name=obj.name[:-7]

    # Focus souris
    if cont.sensors['MO'].status == JUST_ACTIVATED:
        scene.objects[name].color = color_traj_hl

        # Text
        text_hl ={'Traj_load-colbox':"Ouvrir un fichier trajectoire (CSV)",
                  'Traj_object-colbox':"Afficher les objets",
                  'Traj_play-colbox':"Exécuter la trajectoire",
                  'Traj_stop-colbox':"Arrêter l\"exécution de la trajectoire",
                  'Traj_save_csv-colbox':"Enregister la trajectoire dans le fichier \"traj.csv\"",
                  'Traj_save_python-colbox':"Enregister la trajectoire dans le script Python \"traj.py\"",
                  'Traj_clear-colbox':"Effacer la trajectoire (Ctrl + Back)"}
        scene.objects['Cmd-text']['modal']= False
        scene.objects['Cmd-text']['Text']= text_hl[obj.name]
        scene.objects['Cmd-text'].setVisible(True,False)

    # Perte du focus souris
    if cont.sensors['MO'].status == JUST_RELEASED:
        scene.objects['Cmd-text']['Text'] = ""
        scene.objects['Cmd-text'].setVisible(False,False)
        if name == "Traj_stop" and scene.objects['System']['traj'] == False:
            scene.objects[name].color = color_traj_unactive
        elif name == "Traj_play" and scene.objects['System']['traj']:
            scene.objects[name].color = color_traj_unactive
        else:
            scene.objects[name].color = color_traj

##
# Click
##

def click(cont):
    obj = cont.owner
    name=obj.name[:-7]
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0:

        # Ouvrir fichier CSV
        if obj.name == 'Traj_load-colbox':
            data = traj_load_csv()
            if data is not None:
                bras.draw_traj(data)

        # Ajouter une objet 3D à la scène
        if obj.name == 'Traj_object-colbox':
            object_show()

        # Jouer la trajectoire
        if obj.name == 'Traj_play-colbox':
            traj_play()

        # Stopper la trajectoire
        if obj.name == 'Traj_stop-colbox':
            traj_stop()

        # Sauvegarder fichier CSV
        if obj.name == 'Traj_save_csv-colbox':
            traj_save_csv()

        # Générer script Python
        if obj.name == 'Traj_save_python-colbox':
            traj_save_python()

        # Effacer les trajectoires (points et repères)
        if obj.name== 'Traj_clear-colbox':
            bras.draw_clear(True, True, True)
            scene.objects['Solver-text']['Text']="Trajectoire effacée"

###############################################################################
# Poses
###############################################################################

##
# Ajouter une pose
##

def pose_add():
    color=color_white
    obj = scene.objects['System']
    obj_rp = scene.objects['Rp']

    # # Double saisie
    # if obj['move_draw_rep']!=0 :
    #     if obj_rp.worldTransform == scene.objects['rep'+str(obj['move_draw_rep']-1)].worldTransform:
    #         scene.objects['Solver-text']['Text']="Ajout d'une pose à la trajectoire : double saisie."
    #         return

    # Trop de poses
    if obj['move_draw_rep'] == obj['move_draw_reps']:
        scene.objects['Solver-text']['Text']="Ajout d'une pose à la trajectoire : nombre de poses maximum atteint."
        return

    # Coordonnées
    q=[]
    for i_txt in motors:
        if obj['manual_rc']:
            q.append(round(scene.objects[i_txt]['q_bone'], PRECISION_ANGLE))
        else:
            q.append(round(scene.objects[i_txt]['q'], PRECISION_ANGLE))
    p=[round(obj_rp.worldPosition.x, PRECISION_DIST),
       round(obj_rp.worldPosition.y, PRECISION_DIST),
       round(obj_rp.worldPosition.z, PRECISION_DIST),
       round(math.degrees(obj_rp.worldOrientation.to_euler().x), PRECISION_ANGLE),
       round(math.degrees(obj_rp.worldOrientation.to_euler().y), PRECISION_ANGLE),
       round(math.degrees(obj_rp.worldOrientation.to_euler().z), PRECISION_ANGLE)]
    if obj['manual_q']:
        mode='manual_q'
    elif obj['manual_rc']:
        mode='manual_rc'
    else:
        mode='unspecified'

    # Données
    data=[]
    data.append(q)    # q
    data.append(scene.objects['Moteur Pince']['value']) # ot
    data.append('')   # prop
    data.append(mode) # mode
    bras.draw_axis(p, color_white, str(obj['move_draw_rep']), data)


###############################################################################
# Trajectoires
###############################################################################

# Format français des décimaux sur toute une ligne
def localize_floats(row):
    return [
        str(el).replace('.', ',') if isinstance(el, float) else el
        for el in row
    ]

# Format anglais des décimaux sur un champs
def localize_floats_el(field):
    start=0
    replace_pos=[]
    field=field.replace('=,', '=0,')
    while True:
        pos=field.find(',', start)
        if pos == -1:
            break
        else:
            el = field[pos-1]+"."+field[pos+1]
            txt=None
            try:
                txt=float(el)
            except ValueError:
                pass
            if field[pos+1]==" ":
                txt=None
            if txt is not None:
                replace_pos.append(pos)
            start=pos+1
    for i in replace_pos:
        field=field[:i-1]+"."+field[i+1:]
    return field

##
# Ouvrir la trajectoire (fichier CSV)
# Ligne CSV : 'label','q1','q2','q3','q4','q5','q6','ot','x','y','z','rx','ry','rz','prop','mode'
# Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'
##

def traj_load_csv(filename=None):
    obj = scene.objects['System']

    # Sélection du fichier par défaut
    if filename is None:

        # Terminer le processus file précédent
        if ('file_proc' in scene.objects['Commands']):
            if scene.objects['Commands']['file_proc'].poll()==None:
                scene.objects['Commands']['file_proc'].terminate()

        # Démarrer le processus 'file'
        theme = str(scene.objects['Commands']['theme'])
        if scene.objects['Commands']['debug_windows']: # Dev wxPython sous GNU/linux
            print ("Développement wxPython sous GNU/linux : Sélection de fichier")
            scene.objects['Commands']['file_proc'] = subprocess.Popen(["python", os.path.join(os.getcwd(),"wx","file.py"), theme, 'traj'],
                                                                      stdout=subprocess.PIPE, encoding = 'utf8')
        elif sys.platform=="linux": # GTK+3 pour GNU/linux
            scene.objects['Commands']['file_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"gtk","file.py"), theme, 'traj'],
                                                                      stdout=subprocess.PIPE, encoding = 'utf8')
        else: # wxPython pour Windows
            scene.objects['Commands']['file_proc'] = subprocess.Popen([sys.executable, os.path.join(os.getcwd(),"wx","file.py"), theme, 'traj'],
                                                                      stdout=subprocess.PIPE, encoding = 'utf8')
        
        # Récupérer le nom du fichier
        stout = scene.objects['Commands']['file_proc'].communicate()
        if stout[0][:-1] != 'None' and len(stout[0][:-1])>0 :
            filename = stout[0][:-1]
        else:
            return None

    # Lecture du fichier
    print("Trajectoires : lecture du fichier de données '"+ filename+"'")
    scene.objects['Solver-text']['Text']="Chargement de la trajectoire à partir du fichier CSV : "+filename
    buff=[]
    with open(filename, newline='', encoding='utf8') as file:
        reader = csvlib.reader(file, delimiter=';', lineterminator='\n')
        for row in reader:
            buff_row=[]
            for value in row:
                buff_row.append(value.replace(',', '.'))
            buff.append(buff_row)

    # Formatage (int, float, string)
    data=[]
    for i in range (1, len(buff), 1):
        data_row=[]
        # if scene.objects['System']['debug_traj']:
        #     print("Chargement d'un fichier CSV : ligne brute :", buff[i])
        data_row.append(buff[i][0]) # label
        data_row.append([float(buff[i][1]), float(buff[i][2]), float(buff[i][3]), float(buff[i][4]), float(buff[i][5]), float(buff[i][6])]) # q
        if len(buff[i][7])>0:
            data_row.append(float(buff[i][7])) # ot
        else:
            data_row.append(None)
        if len(buff[i][8])>0 and len(buff[i][9])>0 and len(buff[i][10])>0 and len(buff[i][11])>0 and len(buff[i][12])>0 and len(buff[i][13])>0:
            data_row.append([float(buff[i][8]), float(buff[i][9]), float(buff[i][10]), float(buff[i][11]), float(buff[i][12]), float(buff[i][13])]) # p
        else:
            data_row.append(None)
        # print (buff[i][14])
        # print (buff[i][14].replace('.', ','))
        # print (localize_floats_el(buff[i][14].replace('.', ',')))
        data_row.append(localize_floats_el(buff[i][14].replace('.', ','))) # liste des propriétés de la trajectoire (sous forme de texte)
        data_row.append(buff[i][15]) # mode de saisie
        if scene.objects['System']['debug_traj']:
            print("Chargement d'un fichier CSV : données :", data_row)
        data.append(data_row)
    return data

##
# Sauvegarder la trajectoire en CSV
# Ligne CSV : 'label','q1','q2','q3','q4','q5','q6','ot','x','y','z','rx','ry','rz','prop','mode'
# Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'
##

def traj_save_csv(traj=None, filename=None):
    obj = scene.objects['System']
    filename_default ='traj.csv' # Fichier de sauvegarde

    # Fichier par défaut
    if filename is None:
        filename=os.path.join(os.path.split((scene.objects['Commands']['script']))[0], filename_default)

    # Ouvrir le flux
    print("Trajectoires : génération du fichier de données '"+ filename+"'")
    scene.objects['Solver-text']['Text']="Sauvegarde de la trajectoire dans le fichier CSV : "+filename
    with open(filename, 'w', encoding='utf-8') as file:
        writer = csvlib.writer(file, delimiter = ';', lineterminator = '\n')

        # Ecriture des données
        title=['label','q1','q2','q3','q4','q5','q6','ot','x','y','z','rx','ry','rz','prop','mode']
        writer.writerow(title)

        # Poses à l'écran
        data=[]
        if traj is None:
            for i in range (obj['move_draw_rep']):
                rep_obj= scene.objects['rep'+str(i)]
                if 'label' in rep_obj.getPropertyNames() and rep_obj['label'] is not None:
                    data.clear()
                    data.append(rep_obj['label'])
                    data += localize_floats([round(x, PRECISION_ANGLE) for x in rep_obj['q']])
                    data.append(int(rep_obj['ot']))
                    data += localize_floats([round(x, PRECISION_DIST)  for x in rep_obj['p'][:3]])
                    data += localize_floats([round(x, PRECISION_ANGLE) for x in rep_obj['p'][3:]])
                    data.append(rep_obj['prop'])
                    data.append(rep_obj['mode'])
                    writer.writerow(data)

        # Liste de points
        else:
            for i in range (len(traj)):
                data.clear()
                data.append(traj[i][0])
                data += localize_floats([round(x, PRECISION_ANGLE) for x in traj[i][1]])
                data.append(int(traj[i][2]))
                data += localize_floats([round(x, PRECISION_DIST)  for x in traj[i][3][:3]])
                data += localize_floats([round(x, PRECISION_ANGLE) for x in traj[i][3][3:]])
                data.append(traj[i][4])
                data.append(traj[i][5])
                writer.writerow(data)

##
# Générer un script Python
# Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'
##

def traj_save_python(traj=None, filename=None):
    obj = scene.objects['System']
    filename_default ='traj.py' # Fichier de sauvegarde

    # Fichier par défaut
    if filename is None:
        filename=os.path.join(os.path.split((scene.objects['Commands']['script']))[0], filename_default)

    # Ouvrir le flux
    print("Trajectoires : génération du script Python '"+ filename+"'")
    scene.objects['Solver-text']['Text']="Générer la trajectoire dans script Python : "+filename
    with open(filename, 'w', encoding='utf-8') as file:

        # En-tête
        file.write("# -*- coding: utf-8 -*- \n")
        file.write("from bras_lib import * # Bibliothèque utilisateur du bras MKX3D\n")
        file.write("\n")
        file.write("# traj.py : Trajectoire générée\n")
        file.write("\n")
        file.write("\n")
        file.write("def commandes():\n")
        file.write("    draw_clear()\n")
        file.write("    tempo(1)\n")
        file.write("\n")

        # Poses à l'écran
        file.write("    # Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'\n")
        file.write("\n")
        file.write("    traj=[]\n")
        file.write("\n")
        data=[]
        if traj is None:
            for i in range (obj['move_draw_rep']):
                rep_obj= scene.objects['rep'+str(i)]
                if 'label' in rep_obj.getPropertyNames() and rep_obj['label'] is not None:
                    data.clear()
                    data.append(rep_obj['label'])
                    data.append(rep_obj['q'])
                    data.append(int(rep_obj['ot']))
                    data.append(rep_obj['p'])
                    data.append(rep_obj['prop'])
                    data.append(rep_obj['mode'])
                    file.write("    traj.append("+str(data)+")\n") # ['8', [90.0, -30.0, 0.0, 0.0, -26.5, 0.0], 32, [0.39393, 0.0, 0.72191, 56.77, -0.18, 89.96], '', 'manual_q'])
                # if i == 0:
                #     file.write("    add_pose('"+str(data[0])+"', "+str(data[1])+", "+str(data[2])+")\n")
                #     file.write("    move_q("+str(data[1])+", ot="+str(data[2])+")\n")
                #     file.write("\n")
                # else:
                    # file.write("    traj.append("+str(data)+")\n") # ['8', [90.0, -30.0, 0.0, 0.0, -26.5, 0.0], 32, [0.39393, 0.0, 0.72191, 56.77, -0.18, 89.96], '', 'manual_q'])
        else:
            data=traj # Liste de points
        file.write("\n")

        # Affichage
        file.write("    add_pose(traj[0][0], traj[0][1], traj[0][2])\n")
        file.write("    add_traj(traj[1:])\n")
        
        # Déplacement
        file.write("    move_q(traj[0][1], ot=traj[0][2])\n")
        file.write("    move_traj(draw=True)\n")

        # Pied        
        file.write("\n")
        file.write("\n")
        file.write("def cycle():\n")
        file.write("    commandes()\n")
        file.write("    fin()\n")
        file.write("\n")
        file.write("if __name__=='start':\n")
        file.write("    start(cycle)\n")
        file.write("if __name__=='stop':\n")
        file.write("    stop()\n")
        file.write("if __name__=='init':\n")
        file.write("    variant(1) # Variante Pince rigide\n")
    file.close()

##
# Exécuter la trajectoire
##

# Déplacement point à point
# Thread pour l'attente de la fin du mouvement
def traj_stop_thread():
    q=[]
    for i in range(6):
        obj = scene.objects[motors[i]]
        q.append(obj['q'])
    system.move_q(q, scene.objects['Moteur Pince']['value'])

# Stopper l'exécution de la trajectoire
def traj_stop():
    scene.objects['System']['traj'] = False

    # Commandes trajectoires
    bt=['Traj_load', 'Traj_object', 'Traj_play', 'Traj_save_csv', 'Traj_save_python', 'Traj_clear']
    for i_bt in bt:
        scene.objects[i_bt].color = color_traj
        scene.objects[i_bt+"-colbox"].restorePhysics()
    scene.objects['Traj_stop'].color = color_traj_unactive
    scene.objects['Traj_stop'+"-colbox"].suspendPhysics()
    
    # Remettre les vitesses initiales
    for i in range(6):
        obj = scene.objects[motors[i]]
        obj['speed_setting'] = obj['speed_prev'] # Vitesse avant la trajectoire
    
    # Stop de la tâche
    if len(threads_traj)>0:
        twin_threading.stop(threads_traj, "traj")
        sleep(0.01)
        twin_threading.start(threads_traj, "traj", traj_stop_thread)

    # Init des paramètres de la trajectoire
    scene.objects['System']['move_draw'] = False
    scene.objects['Commands']['move_draw_color'] = color_yellow
    scene.objects['Commands']['move_draw_thickness'] = 1
    scene.objects['System'].sensors['Draw_move'].skippedTicks = 3

# Jouer la trajectoire
def traj_play():
    obj = scene.objects['System']
    obj['traj'] = True
    
    # Commandes trajectoires
    bt=['Traj_load', 'Traj_object', 'Traj_play', 'Traj_save_csv', 'Traj_save_python', 'Traj_clear']
    for i_bt in bt:
        scene.objects[i_bt].color = color_traj_unactive
        scene.objects[i_bt+"-colbox"].suspendPhysics()
    scene.objects['Traj_stop'].color = color_traj
    scene.objects['Traj_stop'+"-colbox"].restorePhysics()

    # Désactivation de la commande manuelle des moteurs
    if obj['manual_q']:
        obj['manual_q'] = False
        scene.objects['Keyboard-icon'].setVisible(False,True)
        scene.objects['Input-text']['Text'] = ""
        scene.objects['Solver-text']['Text'] = "Solveur prêt."

        # Désactivation de la pince
        if scene.objects['Moteur Pince']['value_manual']:
            obj_ot=scene.objects['Moteur Pince']
            obj_ot['value_manual'] = False
            obj_ot['value_manual_raz'] = False
            obj_ot['activated'] = False
            obj_ot['value_target'] = obj_ot['value'] # Fixe la position
            for obj_group in obj_ot['color_group']:
                scene.objects[obj_group]['value_manual'] = False
                actuator_color(obj_group)

        # Désactivation du moteur en cours
        else:
            for motor_name in motors:
                obj_motor=scene.objects[motor_name]
                if obj_motor['q_manual']:
                    obj_motor['q_manual'] = False
                    obj['q_manual_raz'] = False
                    obj_motor['cw'] = False
                    obj_motor['acw'] = False
                    obj_motor['q_target'] = obj_motor['q'] # Fixe la position
                    for obj_group in scene.objects[motor_name]['color_group']:
                        scene.objects[obj_group]['q_manual'] = False
                        actuator_color(obj_group)
                    break

    # Désactivation de la commande manuelle par point cible
    if obj['manual_rc']:
        obj_rc = scene.objects['Rc']
        obj_rc_global = scene.objects['Rc-global']
        obj['manual_rc'] = False
        obj_rc.setVisible(False, True)
        obj_rc_global.setVisible(False, True)
        scene.objects['Joystick-icon'].setVisible(False,True)
        scene.objects['Input-text']['Text']  = ""
        scene.objects['Solver-text']['Text'] = "Solveur prêt."
        bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=False
        bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=False

    # Effacer les points
    bras.draw_clear(True, False, False)

    # Go !
    print("Trajectoires : exécution de la trajectoire")
    scene.objects['Solver-text']['Text']="Exécution de la trajectoire"
    twin_threading.start(threads_traj, "traj", traj_play_thread)

# Déplacement point à point pour la trajectoire
# Thread pour l'attente de la fin du mouvement
def traj_play_thread():
    obj = scene.objects['System']
    for i in range (obj['move_draw_rep']):
        obj_rep = scene.objects['rep'+str(i)]
        eval("system.move_q(obj_rep['q'], obj_rep['ot'], draw=True, "+str(obj_rep['prop'])+")")
    obj['traj'] = False

    # Commandes trajectoires
    bt=['Traj_load', 'Traj_object', 'Traj_play', 'Traj_save_csv', 'Traj_save_python', 'Traj_clear']
    for i_bt in bt:
        scene.objects[i_bt].color = color_traj
        scene.objects[i_bt+"-colbox"].restorePhysics()
        scene.objects[i_bt]['redraw']=True
    scene.objects['Traj_stop'].color = color_traj_unactive
    scene.objects['Traj_stop'+"-colbox"].suspendPhysics()
    scene.objects['Traj_stop']['redraw']=True
    scene.objects['Commands']['redraw'] = True

###############################################################################
# Objets 3D
###############################################################################

##
# Ajouter des objets 3D
##

def object_init():

    # Import des fichiers comme bibliothèque
    set1 = set(bpy.data.objects)
    files = os.listdir(os.path.join(os.getcwd(),"object"))
    for file in files:
        file_name, file_extension = os.path.splitext(file)
        if file_extension == '.blend':
            bpy.ops.wm.append(directory=os.path.join(os.getcwd(),"object", file, "Collection"), filename='Object')
    set2 = set(bpy.data.objects)

    # Import des fichiers comme objet
    # set1 = set(bpy.data.objects)
    # files=os.listdir(os.path.join(os.getcwd(),"object"))
    # for file in files:
    #     file_name, file_extension = os.path.splitext(file)
    #     if file_extension == '.blend':
    #         bpy.ops.wm.append(directory=os.path.join(os.getcwd(),"object", file, "Object"), filename=file_name)
    #         bpy.ops.wm.append(directory=os.path.join(os.getcwd(),"object", file, "Object"), filename=file_name+"-solid")
    #         bpy.ops.wm.append(directory=os.path.join(os.getcwd(),"object", file, "Object"), filename=file_name+"-wf")
    # set2 = set(bpy.data.objects)

    # Conversion en BGE
    for obj in set2 :
        if not(obj in set1):
            scene.convertBlenderObject(bpy.data.objects[obj.name])
            if scene.objects['System']['debug_traj']:
                print("Initialisation : import de l'objet '"+obj.name+"'" )
            scene.objects[obj.name].setVisible(False,True)
            scene.objects[obj.name].suspendPhysics()

    # Parentage
    # Les objets importés sont des repères (empty) comportant deux enfants :
    # - '-solid' : objet solide : représentation normale
    # - '-wf'    : objet filaire : représentation lors du déplacement de l'objet avec le Gizmo
    # - '-face-*' : objet face : partie sensible pour la préhension
    
    scene.objects['System']['traj_objects'] = []
    for obj in set2 :
        if not(obj in set1):
            if '-solid' in obj.name:
                scene.objects[obj.name].setParent(scene.objects[obj.name[:-6]])
            elif '-wf' in obj.name:
                scene.objects[obj.name].setParent(scene.objects[obj.name[:-3]])
            elif '-face-' in obj.name:
                scene.objects[obj.name].setParent(scene.objects[obj.name[:obj.name.find('-face-')]])
            elif '.' not in obj.name: # Ne pas prendre en compte des objets dupliqués 
                scene.objects['System']['traj_objects'].append(obj.name)
                scene.objects[obj.name]['init'] = scene.objects[obj.name].worldTransform.copy()
                scene.objects[obj.name]['contact_list'] = []
               
    print("Initialisation : liste des objets disponibles :", scene.objects['System']['traj_objects'])
    if scene.objects['System']['debug_traj']:
        for i in range(len(scene.objects['System']['traj_objects'])):
            print("Initialisation : objets disponibles + enfants :",scene.objects[scene.objects['System']['traj_objects'][i]].name, ":" , scene.objects[scene.objects['System']['traj_objects'][i]].children)


##
# Objets 3D
##

# Afficher/replacer les objets 3D
def object_clear(name=None):
    if name is None:
        for obj in scene.objects['System']['traj_objects']:
            if scene.objects[obj].visible:
                scene.objects[obj].setVisible(False,True)
                scene.objects[obj].suspendPhysics()
    else:
        scene.objects[name].setVisible(False,True)
        scene.objects[name].suspendPhysics()

# Afficher/replacer les objets 3D
def object_show(name=None):

    # Liste des objets
    lst_obj=[]
    for obj_i in scene.objects:
        lst_obj.append(str(obj_i.name))
    
    for obj in scene.objects['System']['traj_objects']:

        # Replacer
        if scene.objects[obj].visible:

            # Désactiver le Gizmo
            scene.objects['System']['gizmo']=False
            scene.objects['Ro'].setVisible(False, True)
            scene.objects['Ro']['manip_obj']=""
            for obj_i in scene.objects['Ro'].children:
                obj_i.suspendPhysics()
                for obj_j in obj_i.children:
                    obj_j.suspendPhysics()
            scene.objects[obj+"-wf"].setVisible(False,True)
            scene.objects[obj+"-wf"].suspendPhysics()

            # Replacer le solide
            scene.objects[obj+"-solid"].setVisible(True,True)
            scene.objects[obj].worldTransform = scene.objects[obj]['init']
            scene.objects[obj].restorePhysics()
            scene.objects[obj].restoreDynamics()
            print("Trajectoires : replace l'objet :", obj)
            scene.objects[obj]['contact_list'] = []

        # Afficher
        else:

            # Objet avancé (solid, wireframe et faces)
            if obj+"-solid" in lst_obj:
                scene.objects[obj].setVisible(True,True)
                scene.objects[obj+"-solid"].color = color_grey
                scene.objects[obj+"-solid"].setVisible(True,True)
                scene.objects[obj+"-solid"].restorePhysics()
                scene.objects[obj+"-wf"].color = color_grey
                scene.objects[obj+"-wf"].setVisible(False,True)
                scene.objects[obj+"-wf"].suspendPhysics()
                scene.objects[obj].restorePhysics()
                scene.objects[obj].restoreDynamics()
                print("Trajectoires : affiche l'objet avancé :", obj)

            # Objet simple
            else:
                if "-solid" not in obj and "-wf" not in obj and "-face-" not in obj:
                    scene.objects[obj].setVisible(True,True)
                    # bpy.data.objects["Table"].game.friction=100.0
                    # bpy.data.objects["Table"].game.rolling_friction=100.0
                    # print("friction Table:", bpy.data.objects["Table"].game.friction, bpy.data.objects["Table"].game.rolling_friction)
                    # print("friction Table BGE :", scene.objects['Table'].friction)
                    # # print("friction :", scene.objects[obj].friction)
                    # print("friction :", obj, bpy.data.objects[obj].game.friction, bpy.data.objects[obj].game.rolling_friction)
                    # print("friction BGE :", scene.objects[obj].friction)
                    scene.objects[obj].restorePhysics()
                    scene.objects[obj].restoreDynamics()
                    print("Trajectoires : affiche l'objet :", obj)

# Highlight de l'objet
def object_hl(cont):
    obj = cont.owner

    # Focus souris
    if cont.sensors['MO'].status == JUST_ACTIVATED:
        if len(obj.children)>0:
            for obj_child in obj.children:
                if "-solid" in obj_child.name :
                    obj_child.color = color_hl
                    break
        else:
            obj.color = color_hl
        scene.objects['Cmd-text']['modal'] = False
        scene.objects['Cmd-text'].setVisible(True,True)
        if 'label' in obj.getPropertyNames():
            scene.objects['Cmd-text']['Text'] = obj['label']
        if obj.parent is not None and 'label' in obj.parent.getPropertyNames():
            scene.objects['Cmd-text']['Text'] = obj.parent['label']
    
    # Perte du focus souris
    if cont.sensors['MO'].status == JUST_RELEASED:
        if len(obj.children)>0:
            for obj_child in obj.children:
                if "-solid" in obj_child.name :
                    obj_child.color = color_grey
                    break
        else:
            obj.color = color_grey
        if 'label' in obj.getPropertyNames() and scene.objects['Cmd-text']['Text']==obj['label']:
            scene.objects['Cmd-text']['Text'] = ""
        if obj.parent is not None and 'label' in obj.parent.getPropertyNames() and scene.objects['Cmd-text']['Text'] == obj.parent['label']:
            scene.objects['Cmd-text']['Text'] = ""

##
# Gizmo (manipulateur, Ro pour objet)
##

def object_gizmo(cont):
    obj = cont.owner

    if cont.sensors['ClickR'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0:

        # Activer gizmo
        if scene.objects['System']['gizmo']==False:
            scene.objects['System']['gizmo']=True
            scene.objects['Ro'].worldTransform = obj.worldTransform
            scene.objects['Ro'].suspendPhysics()
            scene.objects['Ro']['manip_obj']=obj.name
            scene.objects['Ro']['manip_axe']=""
            scene.objects['Ro'].setVisible(True, True)
            for obj_i in scene.objects['Ro'].children:
                obj_i.restorePhysics()
                for obj_j in obj_i.children:
                    obj_j.restorePhysics()
            obj.suspendPhysics()
            scene.objects[obj.name+"-solid"].setVisible(False,True)
            scene.objects[obj.name+"-wf"].restorePhysics()
            scene.objects[obj.name+"-wf"].setVisible(True,True)

        # Désactiver gizmo
        else:
            scene.objects['System']['gizmo']=False
            scene.objects['Ro'].setVisible(False, True)
            scene.objects['Ro']['manip_obj']=""
            for obj_i in scene.objects['Ro'].children:
                obj_i.suspendPhysics()
                for obj_j in obj_i.children:
                    obj_j.suspendPhysics()
            obj.suspendPhysics()
            obj.setVisible(False,True)
            scene.objects[obj.name[:-3]+"-solid"].setVisible(True,True)
            obj.parent.restorePhysics()
            obj.parent.restoreDynamics()


# Highlight du gizmo
def object_man_hl(cont):
    obj = cont.owner

    # Focus souris
    if cont.sensors['MO'].status == JUST_ACTIVATED and scene.objects['Ro']['manip_axe']=="":
        if 'Ro-axe_rx' in obj.name:
            for obj_i in scene.objects['Ro-axe_rx'].children:
                obj_i.color = color_hl
        elif 'Ro-axe_ry' in obj.name:
            for obj_i in scene.objects['Ro-axe_ry'].children:
                obj_i.color = color_hl
        elif 'Ro-axe_rz' in obj.name:
            for obj_i in scene.objects['Ro-axe_rz'].children:
                obj_i.color = color_hl
        else:
            obj.color  = color_hl

    # Perte du focus souris
    if cont.sensors['MO'].status == JUST_RELEASED and scene.objects['Ro']['manip_axe']=="":
        if obj.name != scene.objects['Ro']['manip_axe']:
            if obj.name == 'Ro-axe_x':
                obj.color  = color_rep_red
            elif obj.name == 'Ro-axe_y':
                obj.color  = color_rep_green
            elif obj.name == 'Ro-axe_z':
                obj.color  = color_rep_blue
            elif 'Ro-axe_rx' in obj.name:
                for obj_i in scene.objects['Ro-axe_rx'].children:
                    obj_i.color = color_rep_red
            elif 'Ro-axe_ry' in obj.name:
                for obj_i in scene.objects['Ro-axe_ry'].children:
                    obj_i.color = color_rep_green
            elif 'Ro-axe_rz' in obj.name:
                for obj_i in scene.objects['Ro-axe_rz'].children:
                    obj_i.color = color_rep_blue

# Manipulation
def object_man_click(cont):
    obj = cont.owner
    obj_ro = scene.objects['Ro']
    
    # Démarrer la manipulation
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0:
        obj_ro.restorePhysics()
        obj_ro['manip_axe']=obj.name
        obj_ro['manip_click_x']=cont.sensors['Click'].position[0]
        obj_ro['manip_click_y']=cont.sensors['Click'].position[1]

        # Sens des axes du repère Ro à l'écran
        orig_screen = scene.active_camera.getScreenPosition(obj_ro.worldPosition)
        x_screen    = scene.active_camera.getScreenPosition(obj_ro.worldPosition+0.00001*obj_ro.getAxisVect((1,0,0)))
        y_screen    = scene.active_camera.getScreenPosition(obj_ro.worldPosition+0.00001*obj_ro.getAxisVect((0,1,0)))
        z_screen    = scene.active_camera.getScreenPosition(obj_ro.worldPosition+0.00001*obj_ro.getAxisVect((0,0,1)))

        sign = lambda x: (x > 0) - (x < 0) # Fonction signe
        scene.objects['Ro']['sign_x_x'] = sign(x_screen[0]-orig_screen[0]) # Sens de l'axe x projeté sur x à l'écran
        scene.objects['Ro']['sign_x_y'] = sign(x_screen[1]-orig_screen[1]) # Sens de l'axe x projeté sur y à l'écran
        scene.objects['Ro']['sign_y_x'] = sign(y_screen[0]-orig_screen[0]) # Sens de l'axe y projeté sur x à l'écran
        scene.objects['Ro']['sign_y_y'] = sign(y_screen[1]-orig_screen[1]) # Sens de l'axe y projeté sur y à l'écran
        scene.objects['Ro']['sign_z_x'] = sign(z_screen[0]-orig_screen[0]) # Sens de l'axe z projeté sur x à l'écran
        scene.objects['Ro']['sign_z_y'] = sign(z_screen[1]-orig_screen[1]) # Sens de l'axe z projeté sur y à l'écran

        # Highlight
        list_axe=['Ro-axe_x', 'Ro-axe_rx-cadran1', 'Ro-axe_rx-cadran2', 'Ro-axe_rx-cadran3', 'Ro-axe_rx-cadran4',
                  'Ro-axe_y', 'Ro-axe_ry-cadran1', 'Ro-axe_ry-cadran2', 'Ro-axe_ry-cadran3', 'Ro-axe_ry-cadran4',
                  'Ro-axe_z', 'Ro-axe_rz-cadran1', 'Ro-axe_rz-cadran2', 'Ro-axe_rz-cadran3', 'Ro-axe_rz-cadran4']
        for axe in list_axe:
            if axe != obj.name:
                scene.objects[axe].suspendPhysics()
        scene.objects[scene.objects['Ro']['manip_obj']+"-wf"].suspendPhysics()

    # Arrêt de la manipulation
    if cont.sensors['Click'].status == JUST_RELEASED:
        axe = scene.objects['Ro']['manip_axe']
        if axe == 'Ro-axe_x' or 'Ro-axe_rx' in axe:
            scene.objects[axe].color  = color_rep_red
        if axe == 'Ro-axe_y' or 'Ro-axe_ry' in axe:
            scene.objects[axe].color  = color_rep_green
        if axe == 'Ro-axe_z' or 'Ro-axe_rz' in axe:
            scene.objects[axe].color  = color_rep_blue
        scene.objects['Ro'].suspendPhysics()
        scene.objects['Ro']['manip_axe']=""
        for obj_i in scene.objects['Ro'].children:
            obj_i.restorePhysics()
            for obj_j in obj_i.children:
                obj_j.restorePhysics()
        if scene.objects['Ro']['manip_obj'] != "":
            scene.objects[scene.objects['Ro']['manip_obj']+"-wf"].restorePhysics()

        # Coloriage
        if obj.name == 'Ro-axe_x':
            obj.color  = color_rep_red
        elif obj.name == 'Ro-axe_y':
            obj.color  = color_rep_green
        elif obj.name == 'Ro-axe_z':
            obj.color  = color_rep_blue
        elif 'Ro-axe_rx' in obj.name:
            for obj_i in scene.objects['Ro-axe_rx'].children:
                obj_i.color = color_rep_red
        elif 'Ro-axe_ry' in obj.name:
            for obj_i in scene.objects['Ro-axe_ry'].children:
                obj_i.color = color_rep_green
        elif 'Ro-axe_rz' in obj.name:
            for obj_i in scene.objects['Ro-axe_rz'].children:
                obj_i.color = color_rep_blue

# Remise à 0
def object_man_raz(cont):
    obj = cont.owner

    if cont.sensors['ClickR'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0:
        obj_manip = scene.objects[scene.objects['Ro']['manip_obj']]
        
        if obj.name == 'Ro-axe_x':
            obj_manip.worldPosition.x = obj_manip['init'].to_translation()[0]
        if obj.name == 'Ro-axe_y':
            obj_manip.worldPosition.y = obj_manip['init'].to_translation()[1]
        if obj.name == 'Ro-axe_z':
            obj_manip.worldPosition.z = obj_manip['init'].to_translation()[2]
        if 'Ro-axe_r' in obj.name:
        # if obj.name == 'Ro-axe_rx' or obj.name == 'Ro-axe_ry' or obj.name == 'Ro-axe_rz':
            obj_manip.worldOrientation = obj_manip['init'].to_quaternion()
        scene.objects['Ro'].worldTransform = obj_manip.worldTransform

        # Message serveur
        position_txt=str("["+'{:>5.4f}'.format(obj_manip.worldPosition.x)+", "+
                         '{:>5.4f}'.format(obj_manip.worldPosition.y)+", "+
                         '{:>5.4f}'.format(obj_manip.worldPosition.z)+", "+
                         '{:>6.2f}'.format(math.degrees(obj_manip.worldOrientation.to_euler().x))+", "+
                         '{:>6.2f}'.format(math.degrees(obj_manip.worldOrientation.to_euler().y))+", "+
                         '{:>6.2f}'.format(math.degrees(obj_manip.worldOrientation.to_euler().z))+"]")
        scene.objects['Solver-text']['Text']="Déplacement de l'objet \""+obj_manip['label']+"\" : "+position_txt

# Déplacement
def object_manip(cont):
    obj = cont.owner
    if obj['manip_axe']=="":
        return
    sensibilite_pan=0.00002
    sensibilite_orbit=0.0002

    # Click
    delta_x=cont.sensors['Down'].position[0]-obj['manip_click_x']
    delta_y=cont.sensors['Down'].position[1]-obj['manip_click_y']
    sign_x_x=obj['sign_x_x']
    sign_x_y=obj['sign_x_y']
    sign_y_x=obj['sign_y_x']
    sign_y_y=obj['sign_y_y']
    sign_z_x=obj['sign_z_x']
    sign_z_y=obj['sign_z_y']

    # Trainée linéaire
    if obj['manip_axe']=='Ro-axe_x':
        obj.applyMovement(((sign_x_x*delta_x+sign_x_y*delta_y)*sensibilite_pan, 0, 0), True)
    if obj['manip_axe']=='Ro-axe_y':
        obj.applyMovement((0, (sign_y_x*delta_x+sign_y_y*delta_y)*sensibilite_pan, 0), True)
    if obj['manip_axe']=='Ro-axe_z':
        obj.applyMovement((0, 0, (sign_z_x*delta_x+sign_z_y*delta_y)*sensibilite_pan), True)

    # Trainée angulaire
    # Une seule commande (simplification) pour l'orbit
    if abs(delta_x)>=abs(delta_y):
        delta_y=0
    else:
        delta_x=0
    
    # print (obj['manip_axe'])
    # print ("axe x", sign_x_x, sign_x_y)
    # print ("axe y", sign_y_x, sign_y_y)
    # print ("axe z", sign_z_x, sign_z_y)
    # print ("delta", delta_x, delta_y)

    # Inversion du sens par rapport au repère de référence (rotation sur x)
    if 'Ro-axe_rx' in obj['manip_axe'] : # Pour x : y vers le fond et z vers le haut -> sign_y_y =-1 et sign_z_y =-1 )
        delta_x = -sign_z_y*delta_x # que le z delta_x = sign_y_y*sign_z_y*delta_x
        delta_y = -sign_z_y*delta_y # que le z delta_y = sign_y_y*sign_z_y*delta_y
    if 'Ro-axe_ry' in obj['manip_axe'] : # Pour y : x vers le devant et z vers le haut -> sign_x_y =1 et sign_z_y =-1 )
        delta_x = -sign_z_y*delta_x # que le z delta_x = sign_x_y*sign_z_y*delta_x
        delta_y = sign_z_y*delta_y # que le z delta_y = sign_x_y*sign_z_y*delta_y
    if 'Ro-axe_rz' in obj['manip_axe'] : # Pour z : x vers le fond et y vers le haut -> sign_x_y =-1 et sign_y_y =-1 )
        delta_x = sign_y_y*delta_x # que le z delta_x = sign_x_y*sign_z_y*delta_x
        delta_y = -sign_y_y*delta_y # que le z delta_y = sign_x_y*sign_z_y*delta_y

    ##
    # Axe x
    ##
    
    # Axe x : x+ et y+
    if sign_x_x == 1 and sign_x_y == 1 :
        if obj['manip_axe']=='Ro-axe_rx-cadran1':
            obj.applyRotation(((-delta_x + delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran2':
            obj.applyRotation(((-delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran3':
            obj.applyRotation(((delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran4':
            obj.applyRotation(((delta_x + delta_y)*sensibilite_orbit, 0, 0), True)

    # Axe x : x+ et y-
    if sign_x_x == 1 and sign_x_y == -1 :
        if obj['manip_axe']=='Ro-axe_rx-cadran1':
            obj.applyRotation(((delta_x + delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran2':
            obj.applyRotation(((delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran3':
            obj.applyRotation(((-delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran4':
            obj.applyRotation(((-delta_x + delta_y)*sensibilite_orbit, 0, 0), True)

    # Axe x : x- et y
    if sign_x_x == -1 and sign_x_y == 1 :
        if obj['manip_axe']=='Ro-axe_rx-cadran1':
            obj.applyRotation(((-delta_x + delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran2':
            obj.applyRotation(((-delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran3':
            obj.applyRotation(((delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran4':
            obj.applyRotation(((delta_x + delta_y)*sensibilite_orbit, 0, 0), True)

    # Axe x : x- et y-
    if sign_x_x == -1 and sign_x_y == -1 :
        if obj['manip_axe']=='Ro-axe_rx-cadran1':
            obj.applyRotation(((delta_x + delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran2':
            obj.applyRotation(((delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran3':
            obj.applyRotation(((-delta_x - delta_y)*sensibilite_orbit, 0, 0), True)
        if obj['manip_axe']=='Ro-axe_rx-cadran4':
            obj.applyRotation(((-delta_x + delta_y)*sensibilite_orbit, 0, 0), True)

    ##
    # Axe y
    ##

    # Axe y : x+ et y+
    if sign_y_x == 1 and sign_y_y == 1 :
        if obj['manip_axe']=='Ro-axe_ry-cadran1':
            obj.applyRotation((0, (-delta_x + delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran2':
            obj.applyRotation((0, (-delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran3':
            obj.applyRotation((0, (delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran4':
            obj.applyRotation((0, (delta_x + delta_y)*sensibilite_orbit, 0), True)

    # Axe y : x+ et y-
    if sign_y_x == 1 and sign_y_y == -1 :
        if obj['manip_axe']=='Ro-axe_ry-cadran1':
            obj.applyRotation((0, (delta_x + delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran2':
            obj.applyRotation((0, (delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran3':
            obj.applyRotation((0, (-delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran4':
            obj.applyRotation((0, (-delta_x + delta_y)*sensibilite_orbit, 0), True)

    # Axe y : x- et y
    if sign_y_x == -1 and sign_y_y == 1 :
        if obj['manip_axe']=='Ro-axe_ry-cadran1':
            obj.applyRotation((0, (-delta_x + delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran2':
            obj.applyRotation((0, (-delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran3':
            obj.applyRotation((0, (delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran4':
            obj.applyRotation((0, (delta_x + delta_y)*sensibilite_orbit, 0), True)

    # Axe y : x- et y-
    if sign_y_x == -1 and sign_y_y == -1 :
        if obj['manip_axe']=='Ro-axe_ry-cadran1':
            obj.applyRotation((0, (delta_x + delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran2':
            obj.applyRotation((0, (delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran3':
            obj.applyRotation((0, (-delta_x - delta_y)*sensibilite_orbit, 0), True)
        if obj['manip_axe']=='Ro-axe_ry-cadran4':
            obj.applyRotation((0, (-delta_x + delta_y)*sensibilite_orbit, 0), True)

    ##
    # Axe z
    ##

    # Axe z : x+ et y+
    if sign_z_x == 1 and sign_z_y == 1 :
        if obj['manip_axe']=='Ro-axe_rz-cadran1':
            obj.applyRotation((0, 0, (-delta_x + delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran2':
            obj.applyRotation((0, 0, (-delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran3':
            obj.applyRotation((0, 0, (delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran4':
            obj.applyRotation((0, 0, (delta_x + delta_y)*sensibilite_orbit), True)

    # Axe z : x+ et y-
    if sign_z_x == 1 and sign_z_y == -1 :
        if obj['manip_axe']=='Ro-axe_rz-cadran1':
            obj.applyRotation((0, 0, (delta_x + delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran2':
            obj.applyRotation((0, 0, (delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran3':
            obj.applyRotation((0, 0, (-delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran4':
            obj.applyRotation((0, 0, (-delta_x + delta_y)*sensibilite_orbit), True)

    # Axe z : x- et y
    if sign_z_x == -1 and sign_z_y == 1 :
        if obj['manip_axe']=='Ro-axe_rz-cadran1':
            obj.applyRotation((0, 0, (-delta_x + delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran2':
            obj.applyRotation((0, 0, (-delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran3':
            obj.applyRotation((0, 0, (delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran4':
            obj.applyRotation((0, 0, (delta_x + delta_y)*sensibilite_orbit), True)

    # Axe z : x- et y-
    if sign_z_x == -1 and sign_z_y == -1 :
        if obj['manip_axe']=='Ro-axe_rz-cadran1':
            obj.applyRotation((0, 0, (delta_x + delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran2':
            obj.applyRotation((0, 0, (delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran3':
            obj.applyRotation((0, 0, (-delta_x - delta_y)*sensibilite_orbit), True)
        if obj['manip_axe']=='Ro-axe_rz-cadran4':
            obj.applyRotation((0, 0, (-delta_x + delta_y)*sensibilite_orbit), True)

    # Placement de l'objet
    scene.objects[obj['manip_obj']].worldTransform = obj.worldTransform

    # Message serveur
    position_txt=str("["+'{:>5.4f}'.format(obj.worldPosition.x)+", "+
               '{:>5.4f}'.format(obj.worldPosition.y)+", "+
               '{:>5.4f}'.format(obj.worldPosition.z)+", "+
               '{:>6.2f}'.format(math.degrees(obj.worldOrientation.to_euler().x))+", "+
               '{:>6.2f}'.format(math.degrees(obj.worldOrientation.to_euler().y))+", "+
               '{:>6.2f}'.format(math.degrees(obj.worldOrientation.to_euler().z))+"]")
    scene.objects['Solver-text']['Text']="Déplacement de l'objet \""+scene.objects[obj['manip_obj']]['label']+"\" : "+position_txt

###############################################################################
# Déplacement des objets par le robot
###############################################################################

# Placer le bras sur l'objet 3D
def object_target(cont):
    obj = cont.owner
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['System']['manip_mode']==0:
        if scene.objects['System']['manual_rc']:
            scene.objects['Rc'].worldTransform = obj.worldTransform

# Autofermeture de la pince pour l'objet
def object_autograp():
    print ("Autofermeture de la pince pas implémentée")

# Arrêter la chute des objets
def object_fall(cont):
    obj = cont.owner
    sensor = obj.sensors['Fall']
    if sensor.positive :
        for obj_i in sensor.hitObjectList:
            print("Trajectoires : objet sorti de la table :", obj_i.name)
            obj_i.worldLinearVelocity=(0,0,0)
            obj_i.worldAngularVelocity=(0,0,0)
            obj_i.suspendDynamics()

# Outil proche de la table
def object_near(cont):
    obj = cont.owner
    sensor = obj.sensors['Near']
    return
    if sensor.positive :
        for obj_i in sensor.hitObjectList:
            print ("Proche : "+obj.name+" -> "+obj_i.name+" ; Distance : "+str(obj.getDistanceTo(obj_i)))
            if obj.getDistanceTo(obj_i)<0.001 :
                obj_i.color = color_red
            else:
                obj_i.color = color_white

# def oncollision(object, point, normal, points):
#     print("Hit by", object)
    # for point in points:
    #     print(point.localPointA)
    #     print(point.localPointB)
    #     print(point.worldPoint)
    #     print(point.normal)
    #     # bge.render.drawLine(point.worldPoint,point.normal,color_yellow)
    #     print(point.combinedFriction)
    #     print(point.combinedRestitution)
    #     print(point.appliedImpulse)

# def object_collision_callback(cont):
#     # cont = bge.logic.getCurrentController()
#     own = cont.owner
#     own.collisionCallbacks = [oncollision]

# Dessiner un cercle
def circle(center, radius, color):
    ang = 0
    ang_step = 0.1
    radius_x=radius*0.95 # Au jugé
    radius_y=radius*0.15 # Au jugé
    if scene.active_camera == scene.objects['Camera']:
        center2 = scene.cameras['Camera-Hud'].worldTransform @ scene.active_camera.worldTransform.inverted() @ center
        while ang < 2*math.pi:
            x0 = center2[0]+float(radius_x*math.cos(ang))
            y0 = center2[1]+float(radius_y*math.sin(ang))
            z0 = center2[2]+float(radius*math.sin(ang))
            x1 = center2[0]+float(radius_x*math.cos(ang+ang_step))
            y1 = center2[1]+float(radius_y*math.sin(ang+ang_step))
            z1 = center2[2]+float(radius*math.sin(ang+ang_step))
            bge.render.drawLine([x0,y0,z0],[x1,y1,z1],color)
            ang += ang_step

# Saisie de l'objet par la pince
def object_grab(cont):
    obj = cont.owner # scene.objects['pavé']
    obj_dgt1 = scene.objects['Doigt 1']
    obj_dgt2 = scene.objects['Doigt 2']
    sensor = obj.sensors['Grab']
    # print ("sensor :",sensor.hitObjectList, "contact_list :", obj['contact_list'])

    # Activation du contact
    for obj_i in sensor.hitObjectList:
        if obj_i.name not in obj['contact_list']:

            # Point de contact
            print ("Contact : "+obj.name+" -> "+obj_i.name+" ; Distance : "+str(obj.getDistanceTo(obj_i)))
            collision, points = obj.collide(obj_i)
            if collision:
                for point in points:
                    if scene.objects['System']['debug_traj']:
                        circle(point.worldPoint, 0.005, color_yellow)
                    bras.draw_point(point.worldPoint, color_red, None)

            # Contact pièce/doigt
            if obj_i['grab']==False:
                obj_i.color = color_yellow
                obj_i['grab']=True

    # Prendre pièce
    if obj_dgt1['grab'] and obj_dgt2['grab']:
        if len(obj.children)>0:
            for obj_child in obj.children:
                if "-solid" in obj_child.name :
                    obj_child.color = color_yellow
                    break
        else:
            obj.color = color_yellow
        obj['taken']=True
        scene.objects['Moteur Pince']['grabbed']=obj.name
        obj.suspendDynamics()
        obj.setParent(scene.objects['Pince'])

    # Désactivation du contact : doigt 1
    if obj_dgt1['grab'] and obj['taken']==False and 'Doigt 1' not in sensor.hitObjectList:
        print ("Contact détachement : "+obj.name+" -> "+obj_dgt1.name+" ; Distance : "+str(obj.getDistanceTo(obj_dgt1)))
        obj_dgt1['grab']=False
        obj_dgt1.color = color_turquoise

    # Désactivation du contact : doigt 2
    if obj_dgt2['grab'] and obj['taken']==False and 'Doigt 2' not in sensor.hitObjectList:
        print ("Contact détachement : "+obj.name+" -> "+obj_dgt2.name+" ; Distance : "+str(obj.getDistanceTo(obj_dgt2)))
        obj_dgt2['grab']=False
        obj_dgt2.color = color_turquoise

# Friction objets/table
def object_friction(cont):
    obj = cont.owner
    sensor = obj.sensors['Friction']
    seuil_friction_lin = 0.001
    seuil_friction_ang = 0.01
    # coef_friction_lin  = 0.1
    coef_friction_lin  = 0.3
    coef_friction_ang  = 0.01

    if sensor.positive:
        friction = lambda x, seuil, coef: round(x*coef, 3) if abs(x) > seuil else 0 # Fonction friction
        for obj_i in sensor.hitObjectList:
            # print ("Friction : "+obj.name+" -> "+obj_i.name+" ; Distance : "+str(obj.getDistanceTo(obj_i)))
            obj_i.worldLinearVelocity.x  = friction(obj_i.worldLinearVelocity.x,  seuil_friction_lin, coef_friction_lin)
            obj_i.worldLinearVelocity.y  = friction(obj_i.worldLinearVelocity.y,  seuil_friction_lin, coef_friction_lin)
            obj_i.worldLinearVelocity.z  = friction(obj_i.worldLinearVelocity.z,  seuil_friction_lin, coef_friction_lin)
            obj_i.worldAngularVelocity.x = friction(obj_i.worldAngularVelocity.x, seuil_friction_ang, coef_friction_ang)
            obj_i.worldAngularVelocity.y = friction(obj_i.worldAngularVelocity.y, seuil_friction_ang, coef_friction_ang)
            obj_i.worldAngularVelocity.z = friction(obj_i.worldAngularVelocity.z, seuil_friction_ang, coef_friction_ang)

            # Désactivation du contact (physique) si tout est 0
            if obj_i.worldLinearVelocity.x == 0 and obj_i.worldLinearVelocity.y == 0 and obj_i.worldLinearVelocity.z == 0:
                if obj_i.worldAngularVelocity.x == 0 and obj_i.worldAngularVelocity.y == 0 and obj_i.worldAngularVelocity.z == 0:
                    # obj_i.suspendDynamics() # FIXME à voir ! 
                    if scene.objects['System']['debug_traj']:
                        pass
                        # print ("Friction objets/table : Vitesses linéaires et angulaires :", obj_i.worldLinearVelocity, obj_i.worldAngularVelocity, "-> suspendDynamics()")
                else:
                    if scene.objects['System']['debug_traj']:
                        # pass
                        print ("Friction objets/table : Vitesses linéaires et angulaires :", obj_i.worldLinearVelocity, obj_i.worldAngularVelocity)
            else:
                if scene.objects['System']['debug_traj']:
                    # pass
                    print ("Friction objets/table : Vitesses linéaires et angulaires :", obj_i.worldLinearVelocity, obj_i.worldAngularVelocity)
  
###############################################################################
# Affichage
###############################################################################

##
# Couleurs des actionneurs
##

def actuator_color(name):
    obj = scene.objects[name]
    if obj['color'] == "white":
        obj.color = color_white
    if obj['color'] == "blue":
        obj.color = color_blue
    elif obj['color'] == "blue-dark":
        obj.color = color_blue_dark
    elif obj['color'] == "grey":
        obj.color = color_grey
    elif obj['color'] == "led_yellow":
        obj.color = color_led_yellow
    elif obj['color'] == "aru":
        obj.color = color_aru
    elif obj['color'] == "turquoise":
        obj.color = color_turquoise
    elif obj['color'] == "turquoise-dark":
        obj.color = color_turquoise_dark
