# -*- coding: utf-8 -*-
from bras_lib import * # MKX3D Arm

###############################################################################
# object.py
# @title: exemple : take an object
###############################################################################

# Couleurs
color_white          = (0.8  , 0.8  , 0.8  , 1) # Blanc
color_blue           = (0.007, 0.111, 0.638, 1) # Couleur bleu (actionneur type moteur)
color_blue_dark      = (0.004, 0.054, 0.296, 1) # Couleur bleu noir
color_grey           = (0.285, 0.285, 0.285, 1) # Couleur gris
color_turquoise      = (0.051, 0.270, 0.279, 1) # Couleur turquoise
color_turquoise_dark = (0.030, 0.148, 0.152, 1) # Couleur turquoise foncée
color_yellow         = (0.800, 0.619, 0.021, 1) # Jaune
color_led_yellow     = (0.799, 0.617, 0.021, 1) # Couleur Led jaune (led)
color_red            = (0.694, 0.098, 0.098, 1) # Rouge
color_aru            = (0.693, 0.097, 0.097, 1) # Couleur de l'arrêt d'urgence
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_blue_light     = (0.015, 0.150, 0.687, 1) # Bleu clair
color_purple         = (0.799, 0.005, 0.314, 1) # Violet foncé
color_purple_light   = (0.687, 0.125, 0.578, 1) # Violet clair

# Brochage
brochage={}

###############################################################################
# Functions
###############################################################################

###############################################################################
# Commands
###############################################################################

def commands():

    ##
    # Initialisation
    ##

    draw_clear()
    object_clear()
    tempo(1)
    set_speed(0,10,10) # Rapide
    # set_speed(0, 1, 1)
    # set_speed(0, 3, 3) # 3 rad/s
    move_q([0,0,0,0,0,0], ot=33)


    ##
    # Take an object
    ##

    traj=[]

    # Approach 
    traj.append(['1',  [-141.56, 29.1,  84.13, 0.32, 67.14, -141.69], 33, [0.1971,  -0.24665, 0.12143, -179.25,  0.13, -0.13], '', 'manual_rc'])
    traj.append(['2',  [-141.41, 38.07, 89.09, 0.37, 53.22, -141.63], 33, [0.19845, -0.24694, 0.05458, -179.25,  0.13, -0.13], '', 'manual_rc'])
    traj.append(['3',  [-141.41, 38.07, 88.36, 0.37, 53.22, -141.63], 17, [0.20111, -0.25034, 0.05688, -179.82, -0.32, -0.13], '', 'manual_q'])

    # Grab
    traj.append(['4',  [-141.41, 38.07, 88.36, 0.37, 53.22, -141.63], 11,  [0.20111, -0.25034, 0.05688, -179.82, -0.32, -0.13], '', 'manual_q'])
    traj.append(['5',  [-141.47, 33.47, 87.16, 0.41, 59.25, -141.55], 11,  [0.19941, -0.24837, 0.08645, -179.58, -0.21, -0.25], '', 'manual_rc'])

    # Move with the object
    traj.append(['6',  [ -84.20, 33.75, 86.47, 0.17, 59.38,  -84.16], 11,  [0.31961,  0.03293, 0.08723, -179.54, -0.24, -0.25], '', 'manual_rc'])
    traj.append(['7',  [ -84.40, 36.99, 86.05, 0.18, 56.54,  -84.37], 11,  [0.32412,  0.03232, 0.07049, -179.54, -0.27, -0.26], '', 'manual_rc'])

    # Drop
    traj.append(['8',  [ -84.40, 36.99, 86.05, 0.18, 56.54,  -84.37], 20,  [0.32412,  0.03232, 0.07049, -179.54, -0.27, -0.26], '', 'manual_q'])

    # Retract
    traj.append(['9',  [ -84.40, 36.99, 86.05, 0.18, 56.54,  -84.37], 100, [0.32412,  0.03232, 0.07049, -179.54, -0.27, -0.26], '', 'manual_q'])
    traj.append(['10', [ -84.16, 19.61, 74.76, 0.15, 85.01,  -84.04], 100, [0.302,    0.03145, 0.20902, -179.51, -0.45, -0.26], '', 'manual_rc'])

    add_object('pavé')
    set_object('pavé') # Default position
        
    add_traj(traj)
    move_traj(draw=True)
   
    # while True:
    #     pass

###############################################################################
# En: External call << DONT CHANGE THIS SECTION >>
# Fr: Appel externe << NE PAS MODIFIER CETTE SECTION >>
###############################################################################

###
# Variant 1 : Rigid grabber
# Variant 2 : Pen
# Variant 3 : Suction cup
# Variant 4 : Soft grabber
###

def cycle():
    commands()
    end()

if __name__=='start':
    start(cycle)
if __name__=='stop':
    stop()
if __name__=='init':
    variant(1) # Rigid grabber variant
