# Bras MKX3D - Jumeau numérique

Un jumeau numérique d'un système technique permet de visualiser son comportement à travers une maquette numérique. Le principal intérêt est de valider un
modèle comportemental d'abords par la simulation (avant mise en oeuvre du jumeau réel) puis expérimentalement (mesure des écarts entre les deux jumeaux).

Ici le jumeau numérique du bras va nous servir à programmer les trajectoires de la pince hors ligne. Nous pourrons alors mettre en lien les deux jumeaux et
appliquer les règles programmées sur le jumeau réel.

[![MKX3D présentation](img/lien_video.png)](https://tube-sciences-technologies.apps.education.fr/w/68DoiGYuMfRK8JcFKVSors)

![Photo](img/bras_mkx3d-photo.png)

## Téléchargement

Les binaires (Game Engine Runtime) sont hébergés sur [**phroy.org**](https://www.phroy.org/mkx3d.html).

La mise en paquet est réalisée par [Pixi](https://prefix.dev/).

## Documentation

La documentation est ici : **https://bras-mkx3d.forge.apps.education.fr/jumeau-numerique/**

La documentation est générée par [Sphinx](https://www.sphinx-doc.org/en/master/).

## Modèle 3D (CAO)

Le modèle CAO du bras est dans le [dépôt du modèle 3D du Bras MKX3D](https://forge.apps.education.fr/bras-mkx3d/modele-3d/).

Le bras est libre :	
 - le modèle CAO est diffusé sous licence Creative Commons BY-NC-SA,
 - il est réalisable par des techniques de fabrication usuelles des fablabs (impression 3D),
 - il est composé d'éléments standards : carte Arduino Mega, moteur Nema, ...,
 - sa platforme de développement logiciel est libre : GNU/Linux, Blender/UPBGE et Python.
 
Il a été développé par :
- Pascal Dalmeida [linkedin](https://www.linkedin.com/in/pascal-dalmeida-0a1025153)
- Enric Gómez [linkedin](https://www.linkedin.com/in/mezarch)
- Philippe de Poumayrac de Masredon [linkedin](https://www.linkedin.com/in/philippe-de-poumayrac)

## Jumelage numérique

Le jumelage numérique est basé sur la liaison série entre l'ordinateur (port USB) et un micro-contrôleur Arduino Mega.

## Solveur de cinématique inverse

Les calculs des trajectoires du robot sont effectués de deux manières possibles : 
 - interne, avec le solveur iTaSC IK de Blender via une armature (assemblage d'os) soumise à une contrainte IK (Inverse Kinematic),
- ou externe avec la bibliothèque [**Robotics Toolbox for Python (RTB)**](https://github.com/petercorke/robotics-toolbox-python).

Le solveur interne à Blender est bien plus rapide que RTB mais les calculs de positions et d'orientations dépendents des objets 3D de la scène. Ils peuvent
alors comporter des erreurs liées à l'emplacement ou/et la géométrie des objets 3D.

Le solveur RTB permet d'avoir des calculs indépendants de la scène de Blender, Blender ne sert ici qu'à la visualisation des résultats.

La bibliothèque Robotics Toolbox for Python (RTB) est développée intialement par [Peter Corke](https://petercorke.com/), distribuée sous la Licence MIT ; elle
permet :
- le paramétrage géométrique du robot : Denavit-Hartenberg et URDF,
- le calcul de trajectoires (MGD, MGI),
- la visualisation spatiale du robot.

L'idée est d'utiliser le solveur IK de Blender pour définir la trajectoire à travers les points cibles saisis manuellement (à la manette). Puis d'utiliser le
solveur RTB pour piloter le robot de manière fiable. ll est prévu de pouvoir visualiser les écarts entre les deux solveurs en terme de précision positionnelle
et de proposition du modèle géométrique inverse (IK).

## Développement

<table>
  <tr>
    <td> <div> <a href="https://forge.apps.education.fr"> <img src="img/la_forge-brigit_et_komit-blanc.svg" alt="Brigit et Komit"> </a> </div> </td>
   <td> <div> Les fichiers sources sont dans le projet <b> Bras MKX3D </b> de La Forge des Communs Numériques Éducatifs :
            <a href="https://forge.apps.education.fr/bras-mkx3d"> https://forge.apps.education.fr/bras-mkx3d</a>.  </div> <div> &nbsp; </div> <div> L'environnement de développement est basé sur : la plateforme de
      modélisation et d'animation 3D [Blender](https://blender.org), le moteur de jeu 3D [UPBGE](https://upbge.org), le langage [Python](https://python.org) et 
	  les micro-contrôleurs [Arduino](https://www.arduino.cc).	  </div> </td>
  </tr>
</table>

### Découvrir Blender/UPBGE+Python+Arduino

Des tutoriels sont disponibles pour découvrir le développement sur cette plateforme : https://forge.apps.education.fr/blender-edutech/tutoriels .

D'autres jumeaux numériques ont été développés sous cet environnement : [portail coulissant](https://forge.apps.education.fr/blender-edutech/portail-coulissant/jumeau-numerique),
[monte-charge](https://forge.apps.education.fr/blender-edutech/monte-charge/jumeau-numerique), [volet roulant](https://forge.apps.education.fr/blender-edutech/volet-roulant/jumeau-numerique).

### Environnement de développement

La version de Blender/UPBGE utilisée pour le développement est la version 0.36.1 (20 août 2023).

Les bibliothèques suivantes ne sont pas incluses par défaut dans l'environnement UPBGE : 
- [**Pylint**](https://pylint.pycqa.org) : vérificateur de code Python
- [**pySerial**](https://pyserial.readthedocs.io) : communication sur le port série
- [**Matplotlib**](https://matplotlib.org) : grapheur (visualisation de données)
- [**SciPy**](https://scipy.org/) : calcul scientifique (algèbre linéaire, statistiques, traitement du signal, traitement d'image, ...)
- [**Robotics Toolbox for Python**](https://github.com/petercorke/robotics-toolbox-python) : solveur de cinématique inverse
- [**PyGObject**](https://pygobject.gnome.org/) : interface graphique utilisateur (GUI toolkit) GTK+3 utilisé pour GNU/Linux
- [**wxPython**](https://www.wxpython.org/) : interface graphique utilisateur (GUI toolkit) wxWidgets utilisé pour Windows

Certaines bibliothèques ne peuvent pas être installées avec leur version la plus récente :
- **SciPy** : provoque un conflit de version de la bibliothèque NumPy utilisée par Blender -> **version 1.11.1**
- **Robotics Toolbox for Python**  : provoque un conflit de version de la bibliothèque NumPy utilisée par Blender -> **version 1.1.0**

Il faut donc les installer localement (dans UPBGE), les étapes sont :
- **GNU/Linux** : La configuration ici présente est UPBGE installé sur ~ avec Python 3.10 :
	- Aller dans le répertoire local de Python de UPBGE: $ cd ~/upbge-0.36.1-linux-x86_64/3.6/python/bin
	- Télécharger le script ['get-pip.py'](https://bootstrap.pypa.io/get-pip.py) pour installer le gestionnaire de paquet [**Pip**](https://pip.pypa.io)
	- Installer le gestionnaire de paquet Pip : $ ./python3.10 get-pip.py
	-  Installer Pylint : $ ./pip install pylint
	- Installer pySerial : $ ./pip install pyserial
	- Installer SciPy version 1.11.1 : $ ./pip install scipy==1.11.1
	- Installer Robotics Toolbox for Python version 1.1.0 : $ ./pip install roboticstoolbox-python==1.1.0
    - Installer PyGObject : $ ./pip install PyGObject
	
- **Windows** : La configuration ici présente est UPBGE installé sur le bureau utilisateur (prenom.nom) :
  - Ouvrir un terminal Powershell (éventuellement en passant par Anaconda Navigator)
   - Aller dans le répertoire local de Python de UPBGE: cd C:\Users\prenom.nom\Desktop\upbge-0.36.1-windows-x86_64\3.6\python\bin
	- Télécharger le script ['get-pip.py'](https://bootstrap.pypa.io/get-pip.py) pour installer le gestionnaire de paquet [**Pip**](https://pip.pypa.io)
	- Installer le gestionnaire de paquet Pip : python.exe get-pip.py
	- Aller dans le répertoire 'Scripts' : cd C:\Users\prenom.nom\Desktop\upbge-0.36.1-windows-x86_64\3.6\python\Scripts
	- Installer Pylint : pip.exe install pylint
	- Installer pySerial : pip.exe install pyserial
	- Installer SciPy version 1.11.1 : pip.exe install scipy==1.11.1
	- Installer Robotics Toolbox for Python version 1.1.0 : pip.exe install roboticstoolbox-python==1.1.0
	- Installer wxPython : pip.exe install wxpython
	
- Si l'installation des paquets Python ne se fait pas dans le bon répertoire ('site-packages' des bibliothèques du Python embarqué par UPBGE), il faut allors spécifier le répertoire cible avec
  l'option '-t target' lors de la commande 'pip install'. Par exemple pour Pylint :
  - **GNU/Linux** : $ ./pip install pylint -t ~/upbge-0.36.1-linux-x86_64/3.6/python/lib/python3.10/site-packages 
  - **Windows** : $ pip.exe install pylint -t C:\Users\prenom.nom\Desktop\upbge-0.36.1-windows-x86_64\3.6\python\lib\site-packages
  
- Si, lors de l'installation des différents paquets, le processus n'arrive pas à aboutir (problème de compilation, ...), vous pouvez télécharger un
  environnement UPBGE clé en main (avec toutes la bibliothèques incluses). Ils sont hébergés sur [phroy.org](https://www.phroy.org/mkx3d.html) :
  - **GNU/Linux** : upbge-dev-0.36.1-linux-x86_64.zip
