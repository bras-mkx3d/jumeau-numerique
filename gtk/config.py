import sys

import gi # GTK+3
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

###############################################################################
# config.py
# @title: Configurateur en GTK+3
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024-2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

###############################################################################
# Fenêtre
###############################################################################

##
# Quitter
##

# Choisir une configuration
def ok_click(button, size_combo, qual_combo, theme_combo):
    size= size_combo.get_active_text().split('x')
    print(size[0]+","+size[1]+","+str(qual_combo.get_active())+","+str(theme_combo.get_active())) # stdout = "1280,720,1,1"
    Gtk.main_quit()

# Fermer la fenêtre ou annuler
def end(win, data=None):
    Gtk.main_quit()

# Sortir si ESC
def keypressed(button, event, data=None):
    if event.keyval == Gdk.KEY_Escape:
        Gtk.main_quit()

##
# Implantation
##

def layout(theme):

    # Thème
    if theme == 1:
        Gtk.Settings.get_default().set_property("gtk-theme-name", "Adwaita-dark") # Thème dark
    else:
        Gtk.Settings.get_default().set_property("gtk-theme-name", "Adwaita") # Thème clair

    # Fenêtre
    win = Gtk.Window()
    win.set_default_size(-1, -1)
    win.set_border_width(0)
    win.connect("destroy", end)
    win.connect("key-release-event", keypressed)

    # Barre de la fenêtre
    hb = Gtk.HeaderBar()
    hb.set_show_close_button(False)
    hb.props.title = "Configuration du jumeau numérique du bras MKX3D"
    win.set_titlebar(hb)
    cancel_button = Gtk.Button(label="Annuler") # Bouton annulation
    cancel_button.connect("clicked", end)
    hb.pack_start(cancel_button)
    ok_button = Gtk.Button(label="Sélectionner la configuration") # Bouton ok
    hb.pack_end(ok_button)

    # List box 
    win_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
    win.add(win_box)
    win_listbox = Gtk.ListBox()
    # win_listbox.set_border_width(10)
    win_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
    win_box.pack_start(win_listbox, True, True, 0)

    # Taille de l'écran
    size_row = Gtk.ListBoxRow()
    win_listbox.add(size_row)
    size_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
    size_hbox.set_border_width(5)
    size_row.add(size_hbox)
    size_label = Gtk.Label(label="Taille de l'écran", xalign=0)
    size_combo = Gtk.ComboBoxText()
    size_combo.append("0", "640x360")
    size_combo.append("1", "960x540")
    size_combo.append("2", "1280x720")
    size_combo.append("3", "1920x1080")
    size_hbox.pack_start(size_label, True, True, 5)
    size_hbox.pack_start(size_combo, False, True, 5)

    # Qualité du rendu
    qual_row = Gtk.ListBoxRow()
    win_listbox.add(qual_row)
    qual_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
    qual_hbox.set_border_width(5)
    qual_row.add(qual_hbox)
    qual_label = Gtk.Label(label="Qualité (redémarrage nécessaire)", xalign=0)
    qual_combo = Gtk.ComboBoxText()
    qual_combo.append("0", "Inconvenant")
    qual_combo.append("1", "Basse")
    qual_combo.append("2", "Moyenne")
    qual_combo.append("3", "Haute")
    qual_combo.append("4", "Épique")
    qual_hbox.pack_start(qual_label, True, True, 5)
    qual_hbox.pack_start(qual_combo, False, True, 5)

    # Thème
    theme_row = Gtk.ListBoxRow()
    win_listbox.add(theme_row)
    theme_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
    theme_hbox.set_border_width(5)
    theme_row.add(theme_hbox)
    theme_label = Gtk.Label(label="Thème", xalign=0)
    theme_combo = Gtk.ComboBoxText()
    theme_combo.append("0", "Clair")
    theme_combo.append("1", "Foncé")
    theme_hbox.pack_start(theme_label, True, True, 5)
    theme_hbox.pack_start(theme_combo, False, True, 5)
    
    return win, size_combo, qual_combo, theme_combo, ok_button


# Boucle principale
if __name__ == "__main__":

    # Implantation de la fenêtre
    config = [int(i) for i in sys.argv[1].split(',')] # stdin = 1280,720,1,1
    theme = config[3]
    win, size_combo, qual_combo, theme_combo, ok_button = layout(theme)

    # Configuration courante
    screen_width_mode  = [640, 960, 1280, 1920]
    screen_height_mode = [360, 540,  720, 1080]
    screen_mode_txt=["640x360", "960x540", "1280x720", "1920x1080"]
    if config[0] in screen_width_mode and config[1] in screen_height_mode:
        size_combo.props.active = screen_width_mode.index(config[0])
    else:
        for i in range(len(screen_width_mode)):
            # print (i, int(config[0][0].text), screen_width_mode[i])
            if config[0] < screen_width_mode[i]:
                size_combo.insert(i, "10", str(config[0])+"x"+str(config[1]))
                size_combo.props.active=i
                break
    qual_combo.props.active  = config[2]
    theme_combo.props.active = theme

    # Configuration sélectionnée
    ok_button.connect("clicked", ok_click, size_combo, qual_combo, theme_combo)
    
    # Go !
    win.show_all()
    Gtk.main()
