import os, sys

import gi # GTK+3
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

###############################################################################
# file.py
# @title: Sélecteur de fichier en GTK+3
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024-2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##


# Sélection du script Python
def dialog_cmd():
    dialog = Gtk.FileChooserDialog()
    dialog.set_title("Sélection du fichier de commande")
    filter_py = Gtk.FileFilter()
    filter_py.set_name("Script Python")
    filter_py.add_mime_type("text/x-python")
    dialog.add_filter(filter_py)
    dialog.set_current_folder(os.getcwd())
    return dialog

# Sélection de la trajectoire
def dialog_traj():
    dialog = Gtk.FileChooserDialog()
    dialog.set_title("Sélection du fichier trajectoire")
    filter_py = Gtk.FileFilter()
    filter_py.set_name("Tableau CSV")
    filter_py.add_mime_type("text/csv")
    dialog.add_filter(filter_py)
    dialog.set_current_folder(os.getcwd())
    return dialog

# Boucle principale
if __name__ == "__main__":

    # Thème
    theme = int(sys.argv[1])
    if theme == 1:
        Gtk.Settings.get_default().set_property("gtk-theme-name", "Adwaita-dark") # Thème dark
    else:
        Gtk.Settings.get_default().set_property("gtk-theme-name", "Adwaita") # Thème clair

    # Fenêtre
    if sys.argv[2]=='cmd':
        dialog=dialog_cmd()
    if sys.argv[2]=='traj':
        dialog=dialog_traj()

    # Boutons
    dialog.add_button("_Open",   Gtk.ResponseType.OK)
    dialog.add_button("_Cancel", Gtk.ResponseType.CANCEL)
    dialog.set_default_response(Gtk.ResponseType.OK)

    # Affichage
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
        print(dialog.get_filename())
    dialog.destroy()

