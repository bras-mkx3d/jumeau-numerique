import os, sys

import gi # GTK+3
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import GdkPixbuf

###############################################################################
# about.py
# @title: Fenêtre A propos en GTK+3
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024-2025 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Boucle principale
if __name__ == "__main__":

    # Thème
    theme = int(sys.argv[1])
    if theme == 1:
        Gtk.Settings.get_default().set_property("gtk-theme-name", "Adwaita-dark") # Thème dark
    else:
        Gtk.Settings.get_default().set_property("gtk-theme-name", "Adwaita") # Thème clair

    # Fenêtre
    dialog = Gtk.AboutDialog()
    dialog.set_program_name("Bras MKX3D")
    dialog.set_version("1.2")
    dialog.set_title("Jumeau numérique du Bras MKX3D")
    dialog.set_name("Jumeau numérique du Bras MKX3D")
    dialog.set_comments("")
    dialog.set_logo(GdkPixbuf.Pixbuf.new_from_file_at_size(os.path.join(os.getcwd(),"asset","icons","app","bras_mkx3d.png"), 128, 128))

    # Website
    dialog.set_website_label("Dépôt Git - La Forge")
    dialog.set_website("https://forge.apps.education.fr/bras-mkx3d/jumeau-numerique")

    # Copyright
    dialog.set_copyright("© 2024-2025 Philippe Roy - Licence GNU GPL 3.0")

    # Licence
    dialog.set_wrap_license(True)
    dialog.set_license_type(Gtk.License.GPL_3_0_ONLY)
    dialog.set_license("""Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier sous les termes de la licence publique générale GNU telle qu'elle est publiée par la Free Software Foundation ; soit la version 3 de la licence, ou (comme vous voulez) toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; même sans la garantie de COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la licence publique générale GNU pour plus de détails.

Vous devriez avoir reçu une copie de la licence publique générale GNU avec ce programme. Si ce n'est pas le cas, voir http://www.gnu.org/licenses/ .""")
#     # This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#     # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#     # You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.""")
#     # dialog.connect('response', lambda dialog, data: dialog.destroy())

    # Crédits
    dialog.set_authors(["Philippe Roy https://forge.apps.education.fr/phroy"])
    dialog.add_credit_section(" ", [" "])
    
    credits_description = {
    'Modèle CAO du Bras'          : ["Modèle CAO du Bras","Dépôt du modèle 3D","https://forge.apps.education.fr/bras-mkx3d/modele-3d/", "CC BY-NC-SA 4.0"],
    'Blender'                     : ["Blender","Plateforme de modélisation et d\'animation 3D","https://blender.org", "GNU GPL"],
    'UPBGE'                       : ["UPBGE","Moteur de jeu 3D","https://upbge.org", "GNU GPL"],
    'Python'                      : ["Python","Langage de programmation","https://python.org", "PSFL"],
    'Pylint'                      : ["Pylint","Bibliothèque de vérification d\'un code Python","https://pylint.pycqa.org", "GNU GPL"],
    'pySerial'                    : ["pySerial","Bibliothèque de communication série","https://pyserial.readthedocs.io", "BSD-3-Clause"],
    'Robotics Toolbox for Python' : ["Robotics Toolbox for Python","Solveur de cinématique inverse","https://github.com/petercorke/robotics-toolbox-python", "MIT Licence"],
    'Matplotlib'                  : ["Matplotlib","Bibliothèque de visualisation graphique de données","https://matplotlib.org", "BSD"],
    'GTK+3'                       : ["GTK+3","Bibliothèque GUI","https://www.gtk.org/", "GNU LGPL 2.1"],
    'PyGObject'                   : ["PyGObject","API Python de GTK","https://pygobject.gnome.org/", "GNU LGPL 2.1"],
    'Game-icons.net'              : ["Game-icons.net","Icônes","https://game-icons.net/","CC BY 3.0"],
    'Kenney'                      : ["Kenney","Icônes","https://www.kenney.nl/","CC0 1.0"],
    'EspressoDolce'               : ["Espresso Dolce","Police de caractères","https://www.dafont.com/fr/espresso-dolce.font", "OFL"],
    'Sphinx'                      : ["Sphinx","Générateur de documentation","https://www.sphinx-doc.org/", "BSD"],
    'Pixi'                        : ["Pixi","Gestionnaire de paquets","https://prefix.dev/", "BSD"]}

    # dialog.add_credit_section("Modèle CAO du Bras", ["Dépôt du modèle 3D https://forge.apps.education.fr/bras-mkx3d/modele-3d/ - CC BY-NC-SA 4.0"])
    # dialog.add_credit_section("Platforme 3D",
    #                           ["Blender https://blender.org - GNU GPL",
    #                            "UPBGE https://upbge.org - GNU GPL"])
    # dialog.add_credit_section("Langage de programmation",
    #                           ["Python https://python.org - PSFL",
    #                            "Pylint https://pylint.pycqa.org - GNU GPL"])
    # dialog.add_credit_section("Communication série",
    #                           ["pySerial https://pyserial.readthedocs.io - BSD-3-Clause"])
    
    credits_description_list=list(credits_description)
    for i in range(len(credits_description_list)):
        dialog.add_credit_section(credits_description[credits_description_list[i]][0],
                                  [f"{credits_description[credits_description_list[i]][1]}"+
                                  f"{credits_description[credits_description_list[i]][2]} - "
                                  f"{credits_description[credits_description_list[i]][3]}"])

    # Affichage
    dialog.show_all()
    dialog.connect("destroy", Gtk.main_quit)
    Gtk.main()
