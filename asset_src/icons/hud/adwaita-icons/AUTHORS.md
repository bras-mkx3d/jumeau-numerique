Icons by Adwaita Icon Theme

Authors :
-  Jakub Steiner : http://jimmac.eu

Licence : Creative Commons BY SA 3.0

Source repository : www.github.com/GNOME/adwaita-icon-theme
