# Simulateur de programmation du bras motorisé 6 axes MKX3D

Programmation Blender/UPBGE: 
- Licence : GNU GPL V3
- Dépôt : https://forge.apps.education.fr/bras-mkx3d/jumeau-numerique
- Auteur : Philippe Roy <philippe.roy1@ac-nice.fr>

Maquette numérique : Fichiers SolidWorks
- Licence : Creative Commons BY-NC-SA V4
- Dépôt : https://forge.apps.education.fr/bras-mkx3d/modele-3d
- Auteurs : 
	- Pascal Dalmeida [linkedin](https://www.linkedin.com/in/pascal-dalmeida-0a1025153)
	- Enric Gómez [linkedin](https://www.linkedin.com/in/mezarch)
	- Philippe de Poumayrac de Masredon [linkedin](https://www.linkedin.com/in/philippe-de-poumayrac)
 
