import math, mathutils

import bge, bpy # Blender Game Engine (UPBGE)

from twin_time import sleep # Gestion du temps

###############################################################################
# twin_ik_blender.py
# @title: Solveur de cinématique inverse de Blender
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Maths
PRECISION_DIST  = 5   # Nombre de décimal pour les distances en m
PRECISION_ANGLE = 2   # Nombre de décimal pour les angles en degré

# Armature
TEMPO_RENDER    = 0.1 # Temporisation en s pour pouvoir bien lire l'armature (calcul IK par rendu 3D)
ITERATIONS_IK   = 5   # Nombre d'itérations pour le calcul IK par rendu 3D de l'armature

# UPBGE scene
scene = bge.logic.getCurrentScene()

###############################################################################
# Modèle géométrique
###############################################################################

##
# Modèle géométrique direct (MGD)
#
# q          : coordonnées articulaires [q1,q2,q3,q4,q5,q6] (réel)
# unit       : unité des angles ('rad', 'deg')
# Retourne p : coordonnées opérationnelles [x,y,z,rx,ry,rz] (réel, en m et degrés)
##

# Fonction passe-plat
def ikb_fkine(q, unit="rad"):
    obj_ik = scene.objects['IK ikb']
    obj_ik['q']         = q
    obj_ik['unit']      = unit
    obj_ik['ikb_fkine'] = True
    while obj_ik['ikb_fkine']: # Attente de la fin du calcul
        sleep(0.01)
    return obj_ik['p']

# Fonction de calcul
def ikb_fkine_slv(q, unit="rad"):
    arm = scene.objects['Armature ikb']
    arm_bpy = bpy.context.scene.objects['Armature ikb']
    obj = scene.objects['Rp ikb']

    # Affichage
    arm.setVisible(True, True)
    scene.objects['Rc ikb'].setVisible(False, True)

    # Armature
    # arm_bpy.pose.bones["Pince ikb"].constraints["IK ikb"].enabled = False
    # arm_bpy.pose.bones["Pince ikb"].constraints["IK-rot ikb"].enabled = False
    bpy.data.objects["Armature ikb"].pose.bones["Pince ikb"].constraints["IK ikb"].enabled = False
    bpy.data.objects["Armature ikb"].pose.bones["Pince ikb"].constraints["IK-rot ikb"].enabled = False
    sleep(TEMPO_RENDER)
    if unit=="deg":
        arm.channels['Segment 1 ikb'].rotation_euler = mathutils.Vector([ 0, 1, 0])*math.radians(q[0])
        arm.channels['Segment 2 ikb'].rotation_euler = mathutils.Vector([-1, 0, 0])*math.radians(q[1])
        arm.channels['Segment 3 ikb'].rotation_euler = mathutils.Vector([-1, 0, 0])*math.radians(q[2])
        arm.channels['Segment 4 ikb'].rotation_euler = mathutils.Vector([ 0, 1, 0])*math.radians(q[3])
        arm.channels['Segment 5 ikb'].rotation_euler = mathutils.Vector([-1, 0, 0])*math.radians(q[4])
        arm.channels['Segment 6 ikb'].rotation_euler = mathutils.Vector([ 0, 1, 0])*math.radians(q[5])
    else:
        arm.channels['Segment 1 ikb'].rotation_euler = mathutils.Vector([ 0, 1, 0])*q[0]
        arm.channels['Segment 2 ikb'].rotation_euler = mathutils.Vector([-1, 0, 0])*q[1]
        arm.channels['Segment 3 ikb'].rotation_euler = mathutils.Vector([-1, 0, 0])*q[2]
        arm.channels['Segment 4 ikb'].rotation_euler = mathutils.Vector([ 0, 1, 0])*q[3]
        arm.channels['Segment 5 ikb'].rotation_euler = mathutils.Vector([-1, 0, 0])*q[4]
        arm.channels['Segment 6 ikb'].rotation_euler = mathutils.Vector([ 0, 1, 0])*q[5]
    arm.update()
    # bpy.context.view_layer.update() # Bug de mise à jour 
    # sleep(TEMPO_RENDER)

    # Résulats
    p_list=[]
    for i in range(len(obj.worldPosition)):
        p_list.append(round(obj.worldPosition[i], PRECISION_DIST))
    for i in range(len(obj.worldOrientation.to_euler())):
        p_list.append(round(math.degrees(obj.worldOrientation.to_euler()[i]), PRECISION_ANGLE))
    return p_list

# Déclanchement par le thread de Blender
def cmd_ikb_fkine(cont):
    obj_ik = cont.owner
    obj_ik['p'] = ikb_fkine_slv(obj_ik['q'], obj_ik['unit'])
    obj_ik['ikb_fkine'] = False

##
# Modèle géométrique indirecte (MGI)
#
# p                : coordonnées opérationnelles [x,y,z,rx,ry,rz] (réel, en m et degrés)
# orientation      : contrainte aussi sur l'orientation (binaire, par défaut)
# error_max        : Marge d'erreur sur la validation du solveur IK Blender (par défaut : 0.001 m)
# Retourne q       : coordonnées articulaires [q1,q2,q3,q4,q5,q6] (réel, en degrés)
# Retourne success : solution géométrique trouvée (binaire)
# Retourne error   : distance entre la cible et la solution trouvée (réel, en m)
##

# Fonction passe-plat
def ikb_ikine(p, orientation=True, error_max=0.001):
    obj_ik = scene.objects['IK ikb']
    obj_ik['p']           = p
    obj_ik['orientation'] = orientation
    obj_ik['error_max']   = error_max
    obj_ik['ikb_ikine']   = True
    while obj_ik['ikb_ikine']: # Attente de la fin du calcul
        sleep(TEMPO_RENDER)
    return obj_ik['q'], obj_ik['success'], obj_ik['error']

# Fonction de calcul
def ikb_ikine_slv(p, orientation=True, error_max=0.001):
    arm = scene.objects['Armature ikb']
    arm_bpy = bpy.context.scene.objects['Armature ikb']
    # arm_bpy = bpy.data.objects["Armature ikb"]
    obj_rc_ik = scene.objects['Rc ikb']
    obj_ik = scene.objects['IK ikb']

    # Affichage
    sleep(TEMPO_RENDER) # TEMPO : crash ici ... c'est déjà arrivé
    arm.setVisible(True, True)
    obj_rc_ik.setVisible(True, True)
    obj_ik.setVisible(True, True)

    # Positionnement de la cible 'Rc ikb'
    vec_loc = mathutils.Vector((p[0],p[1],p[2]))
    vec_rot = mathutils.Euler((math.radians(p[3]), math.radians(p[4]), math.radians(p[5])))
    vec_sca = mathutils.Vector((1,1,1))
    mat = mathutils.Matrix.LocRotScale(vec_loc, vec_rot, vec_sca)
    obj_rc_ik.worldPosition = vec_loc
    obj_rc_ik.worldOrientation = vec_rot
    # sleep(TEMPO_RENDER)
    obj_ik.worldPosition = obj_rc_ik.worldPosition
    vec_z_rc = obj_rc_ik.getAxisVect([0,0,1])
    obj_ik.alignAxisToVect(vec_z_rc,1,1.0)
    vec_x_rc = obj_rc_ik.getAxisVect([1,0,0])
    obj_ik.alignAxisToVect(vec_x_rc,0,1.0)
    sleep(TEMPO_RENDER)

    # Armature
    i = 0
    success = False
    while success == False:
        if orientation:
            bpy.data.objects["Armature ikb"].pose.bones["Pince ikb"].constraints["IK ikb"].enabled = False
            bpy.data.objects["Armature ikb"].pose.bones["Pince ikb"].constraints["IK-rot ikb"].enabled = True
            # arm_bpy.pose.bones["Pince ikb"].constraints["IK ikb"].enabled = False
            # arm_bpy.pose.bones["Pince ikb"].constraints["IK-rot ikb"].enabled = True
        else:
            bpy.data.objects["Armature ikb"].pose.bones["Pince ikb"].constraints["IK-rot ikb"].enabled = False
            bpy.data.objects["Armature ikb"].pose.bones["Pince ikb"].constraints["IK ikb"].enabled = True
            # arm_bpy.pose.bones["Pince ikb"].constraints["IK-rot ikb"].enabled = False
            # arm_bpy.pose.bones["Pince ikb"].constraints["IK ikb"].enabled = True
        arm.update()
        sleep(TEMPO_RENDER)

        # Succès
        if scene.objects['System']['debug_traj']:
            print ("\033[0;45mSolveur IK Blender : ikb_ikine(...) : itération "+str(i+1)+"/"+str(ITERATIONS_IK)+"\033[0m")
        error = obj_rc_ik.getDistanceTo(scene.objects['Rp ikb'])
        if error < error_max:
            success=True
        else:
            success=False

        # Trop d'itérations
        i += 1
        # print (i)
        if i == ITERATIONS_IK:
            break
   
    # q bones
    q=[]
    bone1 = arm_bpy.pose.bones['Segment 1 ikb']
    mat1 = bone1.matrix
    q.append(round(math.degrees(mat1.to_euler().z), PRECISION_ANGLE))

    bone2 = arm_bpy.pose.bones['Segment 2 ikb']
    mat2 = bone1.matrix.inverted() @ bone2.matrix
    q.append(round(math.degrees(-mat2.to_euler().x), PRECISION_ANGLE))

    bone3 = arm_bpy.pose.bones['Segment 3 ikb']
    mat3 = bone2.matrix.inverted() @ bone3.matrix
    q.append(round(math.degrees(-mat3.to_euler().x), PRECISION_ANGLE))

    bone4 = arm_bpy.pose.bones['Segment 4 ikb']
    mat4 = bone3.matrix.inverted() @ bone4.matrix
    q.append(round(math.degrees(mat4.to_euler().y), PRECISION_ANGLE))

    bone5 = arm_bpy.pose.bones['Segment 5 ikb']
    mat5 = bone4.matrix.inverted() @ bone5.matrix
    q.append(round(math.degrees(-mat5.to_euler().x), PRECISION_ANGLE))

    bone6 = arm_bpy.pose.bones['Segment 6 ikb']
    mat6 = bone5.matrix.inverted() @ bone6.matrix
    q.append(round(math.degrees(mat6.to_euler().y), PRECISION_ANGLE))

    # Résulats
    return q, success, error

# Déclanchement par le thread de Blender
def cmd_ikb_ikine(cont):
    obj_ik = cont.owner
    obj_ik['q'], obj_ik['success'], obj_ik['error'] = ikb_ikine_slv(obj_ik['p'], orientation=obj_ik['orientation'], error_max=obj_ik['error_max'])
    obj_ik['ikb_ikine'] = False
