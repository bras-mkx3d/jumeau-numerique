# Bras MKX3D - Objets pour la scène

Ce répertoire contient les objets qui doivent être importés dans la scène. Tous les fichiers du répertoire sont automatiquement chargés.

Les objets sont au format Blender (fichier *.blend).

La pose (position et orientation) de l'objet sera la même que celle du fichier Blender. Le fichier présent 'pave.blend' peut servir d'exemple car il comporte
aussi la table nue, ce qui permet aisément de positionner l'objet de manière relative au robot.

La structure de l'objet à importer doit avoir :
- l'objet parent qui est la boite de colision (le cube englobant)
- l'objet solide, enfant de la boite colision, avec l'extension '-solid'
- l'objet filaire (servant au déplacement avec le Gizmo), enfant de la boite colision, avec l'extension '-wf' (wireframe)

Les briques logiques : 
- objet parent : MO, Click, ClickR et Grab
- objet filaire : MO, Click et ClickR

Les propriétés : 
- objet parent : objet, label et taken
- objet filaire : objet et label

La structure du fichier présent 'pave.blend' est un bon modèle pour créer d'autres fichiers objet.
