import time
import bge # Bibliothèque Blender Game Engine (UPBGE)

###############################################################################
# twin_time.py
# @title: Gestion du temps
# @project: Blender-EduTech
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# UPBGE scene
scene = bge.logic.getCurrentScene()

##
# Temporisation
##

# Temporisation basée sur l'OS
def sleep(duree):
    time.sleep(duree)

# Temporisation basée par l'horloge de UPBGE
def tempo(duration):
    scene.objects['Commands']['time']=0
    while scene.objects['Commands']['time']<duration:
        # print (scene.objects['Commands']['time'])
        time.sleep(0.001)

def twin_tempo(duration): # Fonction dépréciée
    tempo(duration)

##
# t (temps)
##

def get_t():
    # return truncate(scene.objects['System']['time'], 3)
    return round(scene.objects['System']['time'], 3)

def set_t(date):
    scene.objects['System']['time']=date

def reset_t():
    scene.objects['System']['time']=0
