.. _presentation-index:

============
Présentation
============

.. toctree::
   :maxdepth: 2

	      
Le bras MKX3D
+++++++++++++

.. figure:: /img/presentation/bras_mkx3d-photo.png
   :width: 70%
   :align: center
	   
|

Objectif
--------

Le bras robotisé MKX3D est un **bras manipulateur 6 axes libre**. L’objectif du bras est d’avoir un système libre pour la formation des étudiants en robotique, de
ce fait il est fortement inspiré des robots industriels.

Le nom MKX vient de **MéKatroniX** en référence aux élèves ingénieurs par apprentissage de l'IMT Mines Alès.

Matériel libre
--------------

En effet ce bras est un `matériel libre <https://fr.wikipedia.org/wiki/Matériel_libre>`_:

- le **modèle CAO** est diffusé sous la licence `Creative Commons BY-NC-SA <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_,
- il est auto-réalisable car toutes les **pièces sont imprimables**,
- il est uniquement composé **d'éléments standards** : carte Arduino Mega, moteur Nema, ...

Le modèle CAO du bras est hébergé sur `La Forge <https://forge.apps.education.fr/bras-mkx3d/modele-3d>`_ logiciel de l'Éducation Nationale (France).

Le design de ce bras toujours été libre et il remonte à plus de 10 ans :
  
  - **2013** : version initiale développé par `Andreas Hölldorfer <https://github.com/4ndreas>`_ (`dépôt Git
    <https://github.com/4ndreas/BetaBots-Robot-Arm-Project/tree/master>`_)
  - **2016**: version améliorée par `Enric Gómez <https://www.linkedin.com/in/mezarch/>`_ et `Marc kitusmark <https://github.com/kitusmark>`_ (`dépôt Git
    <https://github.com/BCN3D/BCN3D-Moveo>`_)
  - **2019** : ajout du 6\ :sup:`ème` axe et version actuelle développée par : `Pascal Dalmeida <https://www.linkedin.com/in/pascal-dalmeida-0a1025153/>`_ et `Philippe de
    Poumayrac de Masredon <https://www.linkedin.com/in/philippe-de-poumayrac/>`_


Le jumeau numérique
+++++++++++++++++++

.. figure:: /img/presentation/bras_mkx3d.png
   :width: 100%
   :align: center
	   
|

Un jumeau numérique d'un système technique permet de **visualiser son comportement** à travers une **maquette numérique**. Le principal intérêt est de **valider
un modèle comportemental** par la simulation et de **quantifier les écarts** expérimentalement.
	   
Le jumeau numérique est un `logiciel libre <https://fr.wikipedia.org/wiki/Logiciel_libre>`_ (diffusé sous la `licence GNU GPL <https://www.gnu.org/licenses/gpl-3.0.html>`_) ; il est aussi hébergé sur `La Forge
<https://forge.apps.education.fr/bras-mkx3d/jumeau-numerique>`_ logiciel de l'Éducation Nationale (France).

Environnement de développement
------------------------------

L'environnement de développement, lui aussi libre, est basé sur la plateforme de modélisation et d'animation `3D Blender <https://www.blender.org/>`_, le
moteur de jeu `UPBGE <https://upbge.org/>`_ et le langage `Python <https://www.python.org/>`_.

.. figure:: /img/presentation/environnement_prog-logos.png
   :figwidth: 30%
   :align: center

Cinématique inverse
-------------------

.. figure:: /img/presentation/rtb.png
   :figwidth: 10%
   :align: right

La cinématique inverse peut être calculée avec deux solveurs : en interne avec le solveur **iTaSC IK intégré à Blender** ou en externe avec la bibliothèque
`Robotics Toolbox for Python (RTB) <https://github.com/petercorke/robotics-toolbox-python>`_.

Jumelage
--------

.. figure:: /img/presentation/arduino.png
   :figwidth: 15%
   :align: right

Le jumeau réel est piloté par une carte `Arduino <https://www.arduino.cc/>`_ relié au jumeau numérique par une liaison série.

