.. _logiciel-commande:

Commande manuelle
+++++++++++++++++

Commande directe des moteurs (mode q)
-------------------------------------

Les moteur sont commandés **individuellement** par le **clavier**. Le moteur sélectionné est mis en évidence en **jaune**.

.. figure:: /img/logiciel/modeq-1.png
   :width: 100%
   :align: center
	   
|

Commande par positionnement d'un point cible (mode Rc)
------------------------------------------------------

Les moteur sont commandés **simultanément** en plaçant un **point cible** à la **manette**.

.. figure:: /img/logiciel/moderc-1.png
   :width: 100%
   :align: center
	   
|

.. figure:: /img/logiciel/moderc-2.png
   :width: 100%
   :align: center
	   
|
