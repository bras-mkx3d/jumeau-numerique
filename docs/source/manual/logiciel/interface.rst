.. _logiciel-interface:

Interface
+++++++++

Environnement de programmation
------------------------------

Le bras se commande directement sur **l'interface (clavier, souris et manette)** ou par des **scripts en langage Python**.

L'environnement se décompose en 3 fenêtres : le **simulateur**, un **éditeur de texte** et la **console**.

.. figure:: /img/logiciel/environnement-1.png
   :width: 100%
   :align: center
	   
|

Décompresser l'archive **bras_mkx3d.zip**. Le simulateur et la console se lancent en même temps avec **bras_mkx3d.bat** (Windows) ou **bras_mkx3d.sh** (GNU/Linux).

.. figure:: /img/logiciel/environnement-2.png
   :width: 100%
   :align: center
	   
|


Manipulation de la maquette numérique
-------------------------------------

.. figure:: /img/logiciel/manip-1.png
   :width: 100%
   :align: center
	   
|

.. figure:: /img/logiciel/manip-2.png
   :width: 100%
   :align: center
	   
|
