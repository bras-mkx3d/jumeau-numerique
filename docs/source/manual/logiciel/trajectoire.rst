.. _logiciel-trajectoire:

Trajectoire
+++++++++++

Charger et sauvegarder des trajectoires pré-calculées
-----------------------------------------------------

Les trajectoires peuvent être sauvegardées dans des **fichiers de données** au `format CSV <https://fr.wikipedia.org/wiki/Comma-separated_values>`_.

.. figure:: /img/logiciel/trajectoire-1.png
   :width: 100%
   :align: center
	   
|

Les fichiers **traj.csv** et **traj.py** sont réservés, il n'y a pas le choix dans le nom des fichiers lors de la sauvegarde.

Fichier trajectoire
-------------------

Le format du fichier CSV est d'avoir **une pose par ligne** : 

.. figure:: /img/logiciel/trajectoire-2.png
   :width: 100%
   :align: center
	   
|

Les **propriétés** sont des options :

- **color**: couleur des points (RGBA tuple : (Rouge, Vert, Bleu, Alpha))
- **speed** : vitesse du trajet en m/s (réel)
- **thickness** : épaisseur, taille des points (réel, facteur d'échelle, 1 par défaut)
- **tickskip** : densité des points, tous les x ticks (60 fps) (entier, 3 par défaut)
- **cont** : tous les mouvements arrivent en même temps (binaire, par défaut)
- **force** : permet de dépasser les limites angulaires des moteurs (binaire)

.. figure:: /img/logiciel/trajectoire-3.png
   :width: 100%
   :align: center
	   
|

Manipuler un objet
------------------

Le bras peut **manipuler des objets**. 

.. figure:: /img/logiciel/objet-1.png
   :width: 100%
   :align: center
	   
|

Créer des nouveaux objets
-------------------------

Les objets sont les **fichiers Blender** présents dans le répertoire **objects**. Ils sont automatiquement chargés au démarrage.

La pose (position et orientation) de l'objet sera la même que celle du fichier Blender. Le fichier présent 'pave.blend' peut servir d'exemple car il comporte
aussi la table nue, ce qui permet aisément de positionner l'objet de manière relative au robot.

La **structure** de l'objet à importer doit respecter certaines règles :

- un **objet principal** : la boite de colision (le cube englobant) de l'ensemble,
- l'objet principal est dans la **collection 'Object'**,
- un **objet solide** : situation par défaut ; enfant de l'objet principal avec l'extension '-solid',
- un **objet filaire** : déplacement avec le Gizmo ; enfant de l'objet principal avec l'extension '-wf' (wireframe).

  .. figure:: /img/logiciel/objet-2.png
   :width: 100%
   :align: center
	   
|

  .. figure:: /img/logiciel/objet-3.png
   :width: 100%
   :align: center

Les **briques logiques** :

- objet principal : MO, Click, ClickR et Grab
- objet filaire : MO, Click et ClickR

Les **propriétés** :

- objet principal : objet, label et taken
- objet filaire : objet et label

La structure du **fichier présent 'pave.blend'** est un bon modèle pour créer d'autres fichiers objet.
