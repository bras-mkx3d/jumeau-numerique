.. _api-index:

============================
Programmation du robot (API)
============================

.. toctree::
   :maxdepth: 2


Le pilotage du jumeau numérique du bras MKX3D se fait par un **script en Python**.

Cette section vous présente les différents commandes possibles.

	      	      
Coordonnées articulaires
++++++++++++++++++++++++

Déplacer le robot avec les coordonnées articulaires
---------------------------------------------------
.. autofunction:: bras_lib.move_q

Récupérer les coordonnées articulaires à partir de coordonnées cartésiennes
---------------------------------------------------------------------------
.. autofunction:: bras_lib.get_q


Coordonnées cartésiennes
+++++++++++++++++++++++++++

Déplacer le robot avec les coordonnées cartésiennes
------------------------------------------------------
.. autofunction:: bras_lib.move_p

Récupérer les coordonnées cartésiennes à partir de coordonnées articulaires
---------------------------------------------------------------------------
.. autofunction:: bras_lib.get_p

Récupérer les coordonnées cartésiennes de la position actuelle en utilisant le moteur géométrique de Blender
------------------------------------------------------------------------------------------------------------
.. autofunction:: bras_lib.get_p_blender

		  
Pince
+++++

Définir l\'ouverture la pince
-----------------------------
.. autofunction:: bras_lib.set_ot

Récupérer la valeur courante de l\'ouverture la pince
-----------------------------------------------------
.. autofunction:: bras_lib.get_ot


Trajectoire
+++++++++++

Ajouter une pose
----------------
.. autofunction:: bras_lib.add_pose

Ajouter une trajectoire
-----------------------
.. autofunction:: bras_lib.add_traj

Déplacer le robot suivant la trajectoire
----------------------------------------
.. autofunction:: bras_lib.move_traj

Sauvegarder la trajectoire dans un fichier csv
----------------------------------------------
.. autofunction:: bras_lib.save_traj

Charger la trajectoire à partir d\'un fichier csv
-------------------------------------------------
.. autofunction:: bras_lib.load_traj


Objet
+++++

Ajouter un objet
----------------
.. autofunction:: bras_lib.add_object

Liste des objets disponibles
----------------------------
.. autofunction:: bras_lib.list_object

Positionne un objet
-------------------
.. autofunction:: bras_lib.set_object

Récupérer l\'état d\'un objet
-----------------------------
.. autofunction:: bras_lib.get_object

Cacher un objet
---------------
.. autofunction:: bras_lib.object_clear


Affichage
+++++++++

Afficher un point
-----------------
.. autofunction:: bras_lib.draw_point

Afficher un repère
------------------
.. autofunction:: bras_lib.draw_axis

Effacer les points/repères
--------------------------
.. autofunction:: bras_lib.draw_clear


Vitesses des moteurs
++++++++++++++++++++

Définir les réglages de vitesse d\'un ou des moteurs
----------------------------------------------------
.. autofunction:: bras_lib.set_speed

Réinitialiser les vitesses d\'un ou des moteurs
-----------------------------------------------
.. autofunction:: bras_lib.reset_speed

Récupérer les vitesses d\'un ou des moteurs
-------------------------------------------
.. autofunction:: bras_lib.get_speed


Commande directe des moteurs
++++++++++++++++++++++++++++

Mettre en rotation d\'un moteur
-------------------------------

.. autofunction:: bras_lib.mot


Modélisation du bras
++++++++++++++++++++

Récupérer la définition du robot
--------------------------------

.. autofunction:: bras_lib.get_robot
   
