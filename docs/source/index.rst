.. Bras MKX3D documentation master file, created by
   sphinx-quickstart on Wed Sep 18 16:23:53 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================================
Documentation pour le jumeau numérique du bras MKX3D
====================================================

Bienvenue dans la documentation du jumeau numérique du **bras MKX3D** !

.. figure:: /img/presentation/jumeaux.png
   :width: 100%
   :align: center

|

Le bras MKX3D est un **bras manipulateur 6 axes libre** : 

- Le modèle CAO du bras est un **matériel libre**, diffusé la licence `Creative Commons BY-NC-SA <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_.
- Le jumeau numérique est un **logiciel libre**, diffusé sous la licence `GNU GPL <https://www.gnu.org/licenses/gpl-3.0.html>`_.

Le site du projet est sur `La Forge <https://forge.apps.education.fr/bras-mkx3d/jumeau-numerique>`_ logiciel de l'Education Nationale.

.. figure:: /img/presentation/libre-logos.png
   :figwidth: 75%
   :align: center

|

La présente documentation est diffusée sous la licence `Creative Commons BY-SA <https://creativecommons.org/licenses/by-sa/4.0/>`_ ; elle a été générée avec
`Sphinx <https://www.sphinx-doc.org>`_. La documentation est aussi sous forme de `manuel utilisateur  <https://forge.apps.education.fr/bras-mkx3d/documentation>`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   manual/presentation/index
   manual/logiciel/index
   manual/api/index

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
