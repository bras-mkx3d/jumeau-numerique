import sys
import csv # Creating/parsing CSV file
import xml.etree.ElementTree as ET # Creating/parsing XML file

import wx # GUI wxPython
import matplotlib # Grapheur Matplotlib
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import (FigureCanvasWxAgg as FigureCanvas, NavigationToolbar2WxAgg as NavigationToolbar)

from twin_plot import plotconfig_get_dict, plotconfig_get, plotconfig_get_title, plotconfig_get_enable, plot_nb, csv_read, plots_static # Gestion communes des graphiques (Qt, wx)

###############################################################################
# twin_plot_wx.py
# @title: Visualisation des données (wxPython + Matplotlib)
# @project: Blender-EduTech
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# FIXME : à basculer sur GTK

plotconfig={}

###############################################################################
# Zone de dessin
###############################################################################

class CanvasFrame(wx.Frame):
    
    def __init__(self):
        super().__init__(None, -1, plot_title)
        # super().__init__(None, -1, plot_title 'Visualisation des données')
        
        # Création des zones graphique (Plots)
        self.figure = Figure()
        if plot_nb(plotconfig) ==1: # plot_nb() : nombre de graphiques
            plt = self.figure.subplots() # plot_nb() : nombre de graphiques
        else:
            plt = self.figure.subplots(plot_nb(plotconfig), 1, sharex=True) # plot_nb() : nombre de graphiques
        self.canvas = FigureCanvas(self, -1, self.figure)

        # Remplissage des graphiques à partir du fichier CSV
        fields, xdata, ydata = csv_read(sys.argv[1])
        plots_static(plt, fields, xdata, ydata, plotconfig, plot_title)
        self.canvas.draw()
        
        # Implantation de la fenêtre (canvas)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.TOP | wx.LEFT | wx.EXPAND)
        self.add_toolbar()
        self.SetSizer(self.sizer)
        self.Fit()

    # Barre d'outils
    def add_toolbar(self):
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Realize()
        self.sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.toolbar.update()
        

###############################################################################
# Application
###############################################################################

class App(wx.App):
    def OnInit(self):
        frame = CanvasFrame()
        frame.Show(True)
        return True

if __name__ == "__main__":

    # Configuration des plots
    plotconfig=plotconfig_get_dict()
    plot_title=plotconfig_get_title(plotconfig)

    # Fenêtre
    app = App()
    app.MainLoop() # StaticPlot()
    
