import csv as csvlib # Parser de fichier CSV
import xml.etree.ElementTree as ET # Parser de fichier XML

###############################################################################
# twin_plot.py
# @title: Visualisation des données (Matplotlib)
# @project: Blender-EduTech
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

###############################################################################
# Configuration des plots
###############################################################################

def plotconfig_get_enable():
    twin_config = ET.parse('twin_config.xml').getroot()
    if twin_config[1][0].text == "True":
        return True
    else:
        return False

##
# Génère le dictionnaire plotconfig à partir du fichier XML plotconfig.xml
##

def plotconfig_get_dict():
    plotconfig_tree = ET.parse('plot_config.xml').getroot()
    plotconfig_tmp={}

    # Titre du graphique
    plotconfig_tmp.update({'title': {'text' : plotconfig_tree[0][0][0].text}})

    # Série de données
    for var in plotconfig_tree[0][0]:
        if len(var)>0:
            var_dict={}
            for i in range (len(var)):
                if var[i] is not None:
                    var_dict.update({var[i].tag : var[i].text})
            plotconfig_tmp.update({var.text: var_dict})
    return  plotconfig_tmp

##
# Returne une valeur du dictionnaire plotconfig
##

def plotconfig_get(plotconfig, var, key):
    if key in  plotconfig[var]:
        return plotconfig[var][key]
    else:
        return None

##
# Returne une valeur du dictionnaire plotconfig
##

def plotconfig_get_title(plotconfig):
    return plotconfig['title']['text']

##
# Nombre de groupe de courbe(s)
##

def plot_nb(plotconfig):
    plotconfig_list=list(plotconfig)
    nbgroup = 0

    # Regroupement ('group' > 0)
    for var in plotconfig_list:
        if ('group' in plotconfig[var]):
            if nbgroup < int(plotconfig[var]['group']):
                nbgroup = int(plotconfig[var]['group'])

    # Plot solitaire ('group' = 0)
    # 'group' = -1 -> variable non affichée
    for var in plotconfig_list:
        if ('group' in plotconfig[var]):
            if int(plotconfig[var]['group']) == 0:
                nbgroup +=1

    return nbgroup

###############################################################################
# Fichier CSV
###############################################################################

##
# Fichier CSV -> Tableau X,Y
##

def csv_read(file):

    # Lecture fichier CSV
    # print ("Debug :", file_complet)
    # file = file_complet[1:-1] # On enlève les ' autour du chemin et nom de fichier
    # file=file_complet
    # print ("Debug :", file)
    fields = []
    rows = []
    with open(file, newline='') as csv_buff:
        csv_reader = csvlib.reader(csv_buff, delimiter=';')
        fields = next(csv_reader)
        for row in csv_reader:
            rows.append(row)

    # Mise en tableau à deux colonnes (xdata,ydata)
    xdata=[]
    ydata=[]
    i=0
    for field in fields:
        xdata_row=[]
        ydata_row=[]
        for row in rows:
            xdata_row.append(float(row[0].replace(',', '.'))) # Revenir au format US des décimaux
            ydata_row.append(float(row[i].replace(',', '.'))) # Revenir au format US des décimaux
        xdata.append(xdata_row)
        ydata.append(ydata_row)
        i+=1

    return fields, xdata, ydata


###############################################################################
# Création des graphiques
###############################################################################

# Création d'une courbe
def plot_draw(plt, var, xdata, ydata, plotconfig):

    # Courbe d'une variable analogique (continue)
    if plotconfig_get(plotconfig, var, 'type') =='a':
        plt.plot(xdata, ydata, label=var, color=plotconfig_get(plotconfig, var, 'color'), linewidth=plotconfig_get(plotconfig, var, 'linewidth'),
                 linestyle=plotconfig_get(plotconfig, var, 'linestyle'), marker=plotconfig_get(plotconfig, var, 'marker'))

    # Courbe d'une variable binaire (discret)
    if plotconfig_get(plotconfig, var, 'type') =='d':
        plt.plot(xdata, ydata, label=var, drawstyle='steps-post', color=plotconfig_get(plotconfig, var, 'color'), linewidth=plotconfig_get(plotconfig, var, 'linewidth'),
                 linestyle=plotconfig_get(plotconfig, var, 'linestyle'), marker=plotconfig_get(plotconfig, var, 'marker'))
        plt.set_ylim(0,1.1) # niveau 0 et niveau 1

    # Courbe d'une variable numérique (discret)
    if plotconfig_get(plotconfig, var, 'type') =='n':
        plt.plot(xdata, ydata, label=var, drawstyle='steps-post', color=plotconfig_get(plotconfig, var, 'color'), linewidth=plotconfig_get(plotconfig, var, 'linewidth'),
                 linestyle=plotconfig_get(plotconfig, var, 'linestyle'), marker=plotconfig_get(plotconfig, var, 'marker'))

# Création des graphiques statiques
def plots_static(plt, fields, xdata, ydata, plotconfig, title):

    # Configuration des plots activée
    plotconfig_enable=plotconfig_get_enable()

    # Plots
    # Groupe de plots : si group = 0 -> nouveau groupe, plot solitaire
    #                                        si group = -1 -> pas de plot
    #                                        si group > 0 -> numéro du groupe
    plt_i=0 # Compteur de plot
    plt_grp=[] # Groupe de plot [[numéro du plot, groupe du fichier CSV]]
    for i in range(len(fields)):
        var = fields[i]
        plt_current=-1 # Numéro du plot à créer

        if ('group' in plotconfig[var]): # Pas de Plot
            if int(plotconfig[var]['group']) !=-1: # Pas de Plot

                # Plot solitaire
                if int(plotconfig[var]['group']) ==0:
                    plt_current = plt_i
                    plt_grp.append([plt_i, 0])
                    plt_i +=1

                # Plot groupé
                else: 
                    plt_new = True # Flag d'un nouveau groupe
                    for j in range(len(plt_grp)):
                        if plt_grp[j][1] == int(plotconfig[var]['group']): # Groupe déjà existant
                            plt_current = plt_grp[j][0]
                            plt_new = False
                            break

                    # Nouveau groupe
                    if plt_new:
                        plt_current = plt_i
                        plt_grp.append([plt_i, int(plotconfig[var]['group'])])
                        plt_i +=1

                # Création du plot unique
                if plot_nb(plotconfig) ==1: 
                    if plotconfig_enable: # Configuration des plots activée
                        plot_draw(plt, var, xdata[i], ydata[i], plotconfig)
                    else:
                        plt.plot(xdata[i], ydata[i], '.-', label=var) # Configuration matplotlib par défaut

                    # Légende ou titre d'axe y
                    if plt_grp[plt_current][1]==0:
                        plt.set_ylabel(var)
                    else:
                        plt.legend()

                # Création des subplots
                if plot_nb(plotconfig) >1: 
                    if plotconfig_enable: # Configuration des plots activée
                        plot_draw(plt[plt_current], var, xdata[i], ydata[i], plotconfig)
                    else:
                        plt[plt_current].plot(xdata[i], ydata[i], '.-', label=var) # Configuration matplotlib par défaut

                    # Légende ou titre d'axe y
                    if plt_grp[plt_current][1]==0:
                        plt[plt_current].set_ylabel(var)
                    else:
                        plt[plt_current].legend()

    # Décoration
    if plot_nb(plotconfig) ==1: # 1 plot
        plt.set_xlabel("Temps (s)")
        if plotconfig_enable:
            plt.set_title(title)
        else:
            plt.set_title(title)
        plt.axhline(linewidth=1, color='k')
        plt.grid(True, linestyle='--')
        # canvas.plt.legend()
    else: # Plusieurs plots
        plt[plt_i-1].set_xlabel("Temps (s)")
        plt[0].set_title(title)
        for i in range (plt_i):
            plt[i].axhline(linewidth=1, color='k')
            plt[i].grid(True, linestyle='--')
            # plt[i].legend()
