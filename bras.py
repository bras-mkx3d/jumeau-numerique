import math, mathutils
import runpy # Exécution de script Python légère (sans import)

import bge, bpy # Blender Game Engine (UPBGE)

import twin                       # Bibliothèque de l'environnement 3D des jumeaux numériques
import twin_threading             # Multithreading (multitâches)
from twin_time   import sleep     # Gestion du temps
from twin_ik_rtb import rtb_fkine # Solveur IK Robotics Toolbox for Python

###############################################################################
# bras.py
# @title: Commandes pour le bras MKX3D
# @project: Bras MKX3D
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2024 Philippe Roy
# @license: GNU GPL
###############################################################################

##
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier
# sous les termes de la licence publique générale GNU telle qu'elle est publiée par
# la Free Software Foundation ; soit la version 3 de la licence, ou
# (comme vous voulez) toute version ultérieure.
#
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; même sans la garantie de
# COMMERCIALITÉ ou d'ADÉQUATION A UN BUT PARTICULIER. Voir la
# licence publique générale GNU pour plus de détails.
#
# Vous devriez avoir reçu une copie de la licence publique générale GNU
# avec ce programme. Si ce n'est pas le cas, voir <http://www.gnu.org/licenses/>.
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Maths
PRECISION_DIST  = 5     # Nombre de décimal pour les distances en m
PRECISION_ANGLE = 2     # Nombre de décimal pour les angles en degré
STABILITY_IK    = 0.005 # Distance mini pour stabiliser le solveur IK de Blender
TEMPO_RENDER    = 0.1   # Temporisation en s pour avoir un rendu 3D à jour

# UPBGE scene
scene = bge.logic.getCurrentScene()
motors = ['Moteur S0', 'Moteur S1', 'Moteur S2',
          'Moteur S3', 'Moteur S4', 'Moteur S5']

###############################################################################
# Configuration des variables publiques
# 'nom_variable' :
# - Objet 3D : [nom de l'objet 3D, propriété associée à la valeur, type de la valeur ('d' (digital, binary), 'a', (analog) ou 'n' (numeric)),
#               variantes (0 = toute variante), échelle]
# - Configuration de la broche : [nom de la propriété stockant l'object broche (pyfirmata),
#                                 type de broche par défaut : 'd' (digital), 'a' (analog) ou 'p' (pwm)), mode de la broche par défaut : 'i' (input) ou 'o' (output)]
# - Configuration du graphique : ['marque', 'type de ligne', 'couleur', linewidth]] (Codification de Matplotlib)
# - Configuration du panneau des entrées/sorties : [Numéro de la ligne, propriété stockant l'object voyant,
#                                                   propriété numérique/réelle, flag de forçage, propriété à forcer (uniquement réelle)
#
#  'nom_variable_r' est la valeur réelle de la variable (valeur numérique) 'nom_variable' issue du jumelage numérique.
#  Dans ce cas, il n'y a pas configuration de broche car elle est présente sur la variable 'nom_variable'.
#  Ce distinguo ne concerne que les entrées, car les sorties sont pilotées par le numérique.
#
###############################################################################

public_vars = {
    't':               [['System',                 'time',          'a', 0, 1], [],                        []],
    'aru':             [['ARU',                    'activated',     'd', 0, 1], [],                        ['.', '-', 'red', 1],           [2, 'io',               'activated',     'forcing']],
    'aru_r':           [['ARU',                    'activated_real', 'd', 0, 1], [],                        ['.', '-', 'darkred', 1],       [2, 'io_real',          'activated_real', 'forcing_real',          'activated_real']],
    'q1':              [['Moteur S0',              'q',             'a', 0, 1], [],                        ['.', '-', 'blue', 1],          [1, 'io_q',             'q',             'q1_forcing']],
    'q1_r':            [['Moteur S0',              'q_real',        'a', 0, 1], [],                        ['.', '-', 'darkblue', 1],      [1, 'io_q_real',        'q_real',        'q1_forcing_real',       'q1_real_forced']]}

# 'mot_f' :           [['Moteur',                 'acw',           'd',0,1], [],                        ['.','-','violet',1],        [9, 'io_acw',           'acw',           'acw_forcing']],
# 'mot_f_r' :         [['Moteur',                 'acw_real',      'd',2,1], ['pin_acw','d','o'],       ['.','-','darkviolet',1],    [9, 'io_acw_real',      'acw_real',      'acw_forcing_real',      'acw_real_forced']],
# 'mot_s' :           [['Moteur',                 'direction_real','d',1,1], ['pin_direction','d','o'], ['.','-','blue',1],          [16,'io_direction_real','direction_real','direction_forcing_real','direction_real_forced']],
# 'mot_v' :           [['Moteur',                 'speed_real',    'a',1,1], ['pin_speed','p','o'],     ['.','-','darkblue',1],      [17,'io_speed_real',    'speed_real',    'speed_forcing_real',    'speed_real_forced']],
# 'mot_angle' :       [['Moteur',                 'alpha',         'a',0,1], [],                        ['.','-','blue',1],          [11,'io_alpha']],
# 'mot_vitesse' :     [['Moteur',                 'speed',         'a',0,1], [],                        ['.','-','darkblue',1],      [12,'io_speed']],
# 'mot_pas' :         [['Moteur',                 'step',          'a',0,1], [],                        []],
# 'portail_x' :       [['Portail',                'x',             'a',0,1], [],                        ['.','-','turquoise',1],     [13,'io_x']],
# 'portail_vitesse' : [['Portail',                'speed',         'a',0,1], [],                        ['.','-','darkturquoise',1], [14,'io_speed']],
# 'portail_pas' :     [['Portail',                'step',          'a',0,1], [],                        []],
# 'gyr' :             [['Led',                    'activated',     'd',0,1], [],                        ['.','-','gold',1],          [1,'io',                'activated',     'forcing']],
# 'gyr_r' :           [['Led',                    'activated_real','d',0,1], ['pin','d','o'],           ['.','-','goldenrod',1],     [1,'io_real',           'activated_real','forcing_real',      'activated_real_forced']],
# 'ir_emet' :         [['Emetteur IR',            'activated',     'd',0,1], [],                        ['.','-','red',1],           [6,'io',                'activated',     'forcing']],
# 'ir_emet_r' :       [['Emetteur IR',            'activated_real','d',0,1], ['pin','d','o'],           ['.','-','darkred',1],       [6,'io_real',           'activated_real','forcing_real',      'activated_real_forced']],
# 'ir_recep' :        [['Recepteur IR',           'activated',     'd',0,1], [],                        ['.','-','red',1],           [7,'io',                'activated',     'forcing']],
# 'ir_recep_r' :      [['Recepteur IR',           'activated_real','d',0,1], ['pin','d','i'],           ['.','-','darkred',1],       [7,'io_real',           'activated_real','forcing_real',      'activated_real']]}

# Couleurs bouton
# color_passive        = (0.800, 0.005, 0.315, 1) # Élément non activable : magenta
# color_passive        = (0.799, 0.031, 0.038, 1) # Elément non activable : rouge
color_passive        = (0.799, 0.107, 0.087, 1) # Élément non activable : rouge clair
color_active         = (0.799, 0.130, 0.063, 1) # Élément activable : orange
color_hl             = (0.800, 0.800, 0.800, 1) # Élément focus : blanc
color_activated      = (0.800, 0.800, 0.800, 1) # Élément activé numériquement uniquement : blanc
color_activated_real = (0.799, 0.031, 0.038, 1) # Élément activé physiquement uniquement : rouge (hors clic)
color_activated_dbl  = (0.246, 0.687, 0.078, 1) # Élément activé physiquement et numériquement : vert clair

# Couleurs Moteur
color_mot_activated = (0.800, 0.619, 0.021, 1) # Moteur activé : jaune
color_mot_passive   = (0.799, 0.107, 0.087, 1) # Moteur bloqué (IK): rouge

# Générales
color_white          = (0.8  , 0.8  , 0.8  , 1) # Blanc
color_blue           = (0.007, 0.111, 0.638, 1) # Couleur bleu (actionneur type moteur)
color_blue_dark      = (0.004, 0.054, 0.296, 1) # Couleur bleu noir
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_grey           = (0.285, 0.285, 0.285, 1) # Couleur gris
color_turquoise      = (0.051, 0.270, 0.279, 1) # Couleur turquoise
color_turquoise_dark = (0.030, 0.148, 0.152, 1) # Couleur turquoise foncée
color_yellow         = (0.800, 0.619, 0.021, 1) # Jaune
color_led_yellow     = (0.799, 0.617, 0.021, 1) # Couleur Led jaune (led)
color_red            = (0.694, 0.098, 0.098, 1) # Rouge
color_aru            = (0.693, 0.097, 0.097, 1) # Couleur de l'arrêt d'urgence
color_green          = (0.083, 0.223, 0.028, 1) # Vert
color_green_light    = (0.246, 0.687, 0.078, 1) # Vert clair
color_purple         = (0.799, 0.005, 0.314, 1) # Violet foncé
color_purple_light   = (0.687, 0.125, 0.578, 1) # Violet clair

# Couleurs Repère
color_rep_red = (0.799, 0.107, 0.087, 1)  # Axe x
color_rep_green = (0.083, 0.223, 0.028, 1)  # Axe y
color_rep_blue = (0.015, 0.150, 0.687, 1)  # Axe z
color_rep_target = (0.051, 0.270, 0.279, 1)  # Origine Rc (cible)
color_rep_grey = (0.283, 0.283, 0.283, 1)  # Désactivation des axes

# Constantes UPBGE
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED  = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE       = bge.logic.KX_INPUT_ACTIVE
# JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

# Threads pour les déplacements
threads_move = []

###############################################################################
# Maths
###############################################################################

# Angle (Roll Pitch Yaw) vers Quaternion (vectorisé)
def rpy2quat(axis, angle):
    euler = mathutils.Euler((axis[0]*angle, axis[1]*angle, axis[2]*angle), 'XYZ')
    quat=euler.to_quaternion()
    vec = mathutils.Vector((quat.w, quat.x, quat.y, quat.z))
    return vec

###############################################################################
# Print formaté (debug)
###############################################################################

# Vecteur rotation absolu/relatif en degrée
def print_T(text_begin, T, Tparent=None):
    if Tparent is None:
        print(text_begin+" : "+'{:>6.2f}'.format(math.degrees(T.to_euler().x))+", "+
              '{:>6.2f}'.format(math.degrees(T.to_euler().y))+", "+
              '{:>6.2f}'.format(math.degrees(T.to_euler().z)))
    else:
        print(text_begin+" : "+'{:>6.2f}'.format(math.degrees(T.to_euler().x-Tparent.to_euler().x))+", "+
              '{:>6.2f}'.format(math.degrees(T.to_euler().y-Tparent.to_euler().y))+", "+
              '{:>6.2f}'.format(math.degrees(T.to_euler().z-Tparent.to_euler().z)))

# Vecteur rotation degrée
def print_r(text_begin, rot):
    print(text_begin+" : "+'{:>6.2f}'.format(math.degrees(rot.x))+", "+
          '{:>6.2f}'.format(math.degrees(rot.y))+", "+
          '{:>6.2f}'.format(math.degrees(rot.z)))

###############################################################################
# Initialisation de la scène
###############################################################################

##
# Initialisation du système
##

def init(cont):
    if cont.sensors['Init'].positive == False: # 1 seule fois 
        return False

    scene.objects['System']['name'] = "Bras MKX3D"
    scene.objects['System']['doc']  = "https://bras-mkx3d.forge.apps.education.fr/jumeau-numerique/index.html"
    twin.view_init() # Manipulation du modèle 3D 
    twin.cmd_init() # Commandes
    scene.objects['System']['draw_points'] = [] # Points des trajectoires
    twin.twin_traj.object_init() # Objets pour les trajectoires

    ##
    # Brochage
    ##
    
    for pin in public_vars:
        if public_vars[pin][1] != []:
            scene.objects[public_vars[pin][0][0]][public_vars[pin][1][0]] = None

    ##
    # Configuration des moteurs des axes
    #
    # 200 pas/tour -> 1,8°/pas
    # Réglage des cartes moteur : pas complet (à confirmer)
    # Vitesse de q1 à q6 : 100 pas/s (config sur l'Arduino)
    # Accélération : q1,q6 : 8000 pas/s² ; q2,q3,q4,q5 : 2000 pas/s² (config sur l'Arduino)
    ##

    motor_step = 1.8 # 1,8 deg/pas
    speed_init = [100,         100,         100,         100,         100,         100]
    speed_max  = [100,         100,         100,         100,         100,         100]
    accel_init = [8000,        2000,        2000,        2000,        2000,        8000]
    r          = [8.333,       5.5,         22.2,        5.18,        18.99,       1] # Rapport de réduction
    mot_pulley = [['Poulie S0'],
                  ['Poulie S1','Poulie 2 S1'],
                  ['Poulie S2'],
                  ['Butée aiguilles S3'],
                  ['Poulie S4'],
                  ['Butée aiguilles S5','Raccord S5']] # Objets tourants
    mot_bone   = ['Segment 1', 'Segment 2', 'Segment 3', 'Segment 4', 'Segment 5', 'Segment 6'] # Os récepteur
    mot_axis   = [[0, 1, 0]  , [-1, 0, 0]  , [-1, 0, 0]  , [0, 1, 0]  , [-1, 0, 0]  , [0, 1, 0]  ] # Axe moteur (axe de l'armature, repère local)
    mot_limit  = [[-math.pi,math.pi],[-math.pi/2,math.pi/2],[-math.pi/2,math.pi/2],[-math.pi,math.pi],[-math.pi/2,math.pi/2],[-2*math.pi,2*math.pi]] # Limite des angles moteur

    for i in range (len(motors)):
        obj = scene.objects[motors[i]]
        obj['r']              = r[i]
        obj['pulley']         = mot_pulley[i]
        obj['bone']           = mot_bone[i]
        obj['axis']           = mot_axis[i]
        obj['limit']          = [math.degrees(mot_limit[i][0]),math.degrees(mot_limit[i][1])] # Limite de q en deg
        obj['alpha']          = 0 # Angle moteur en rad
        obj['alpha_step']     = 0
        obj['q']              = 0 # Angle segment en deg
        obj['q_target']       = 0
        obj['q_manual']       = False # q en mode manuel (mode q)
        obj['q_manual_sbs']   = False # q en mode manuel pas à pas (mode q sbs)
        # obj['q_manual_speed'] = '0.5' # Réglage de la vitesse du segment, en texte car buggé avec des float, génial !
        obj['q_manual_step']  = 2.0 # Réglage du pas du segment en mode q sbs sert aussi pour définir la vitesse en mode q
        obj['q_force']        = False # Autorisation du dépassement des valeurs limites
        obj['speed']              = 0 # Vitesse courante du moteur en rad/s (dérivée numérique d'alpha)
        obj['speed_setting']      = math.radians(speed_init[i] * motor_step) # Réglage de la vitesse du moteur numérique en rad/s (ici 3,14 rad /s (1 tr/s)
        obj['speed_prev']         = obj['speed_setting']
        obj['speed_max']          = math.radians(speed_max[i]  * motor_step)  # Réglage de la vitesse max du moteur numérique en rad/s (ici 3,14 rad /s (1 tr/s)
        obj['speed_real_setting'] = speed_init[i] # Réglage de la vitesse du moteur réel en pas/s avec 1,8°/pas
        # scene.objects['Armature'].channels[mot_bone[i]].rotation_mode = bge.logic.ROT_MODE_QUAT # Rotation par quaternion
        scene.objects['Armature'].channels[mot_bone[i]].rotation_mode = bge.logic.ROT_MODE_XYZ # Rotation par angle (pitch, yaw and roll)
        scene.objects['Armature ikb'].channels[mot_bone[i]+" ikb"].rotation_mode = bge.logic.ROT_MODE_XYZ # Rotation par angle (pitch, yaw and roll)
        bpy.data.objects["Armature"].pose.bones[mot_bone[i]].use_ik_rotation_control=False # Pas de contrainte sur la rotation (que IK)
        bpy.data.objects["Armature"].pose.bones[mot_bone[i]].ik_rotation_weight=1.0 # Blocage du segment en IK si contrainte sur la rotation
        bpy.data.objects["Armature ikb"].pose.bones[mot_bone[i]+" ikb"].use_ik_rotation_control=False # Pas de contrainte sur la rotation (que IK)
        bpy.data.objects["Armature ikb"].pose.bones[mot_bone[i]+" ikb"].ik_rotation_weight=1.0 # Blocage du segment en IK si contrainte sur la rotation

    ##
    # Configuration de la pince
    # Position ouverte : plateu moteur : 50.40002052703745
    # Position fermée  : plateu moteur : -24.59998839021686
    ##

    scene.objects['Moteur Pince']['actived']       = False
    scene.objects['Moteur Pince']['stroke']        = math.pi/2 # Course : 1/4 tr
    scene.objects['Moteur Pince']['stroke_step']   = scene.objects['Moteur Pince']['stroke'] / 100 # Quantum de la course en rad/value unit
    scene.objects['Moteur Pince']['speed_setting'] = scene.objects['Moteur Pince']['stroke'] # Course complète en 1s
    scene.objects['Moteur Pince']['value']         = 33 # Ouverture de départ en %
    scene.objects['Moteur Pince']['value_real']    = 33.0 # Ouverture de départ en %
    scene.objects['Moteur Pince']['value_target']  = 33
    scene.objects['Moteur Pince']['value_manual_step'] = 2.0

    ##
    # Création de point et de repère (pooling)
    ##

    # Points
    scene.objects['System']['move_draw_pts']=1000
    for i in range (scene.objects['System']['move_draw_pts']):
        # obj= scene.addObject('Point', scene.objects['System'])
        obj= scene.addObject('Point', None, 0.00, True)
        obj.color = color_white
        obj.worldPosition=[0,0,0]
        obj.worldScale=[1,1,1]
        obj.suspendPhysics()
        obj.setVisible(False, True)
        obj.name="p"+str(i)
        obj.setParent(scene.objects['System'])

    # Repères
    scene.objects['System']['move_draw_reps']=20
    for i in range (scene.objects['System']['move_draw_reps']):
        # obj= scene.addObject('Rep', scene.objects['System'])
        obj= scene.addObject('Rep', None, 0.00, True)
        obj.worldPosition=[0,0,0]
        obj.worldScale=[1,1,1]
        obj.suspendPhysics()
        obj.setVisible(False, True)
        obj.name="rep"+str(i)
        obj.setParent(scene.objects['System'])

    # Etiquettes
    scene.objects['System']['move_draw_lbls']=20
    for i in range (scene.objects['System']['move_draw_lbls']):
        # obj= scene.addObject('Label', scene.objects['System'])
        obj= scene.addObject('Label', None, 0.00, True)
        obj.worldPosition=[0,0,0]
        obj.worldScale=[0.045,0.045,0.045]
        obj.suspendPhysics()
        obj.setVisible(False, True)
        obj.name="lbl"+str(i)
        obj.setParent(scene.objects['System'])
        
    ##
    # Armature et points cibles
    ##

    # Armature
    scene.objects['Armature ikb'].setVisible(False, True)
    scene.objects['Rc ikb'].setVisible(False, True)

    # Repère cible pour IK Blender
    obj= scene.objects['Rc']
    scene.objects['Rc-axe_x'].color=color_rep_red
    scene.objects['Rc-axe_x-txt'].color=color_rep_red
    scene.objects['Rc-axe_y'].color=color_rep_green
    scene.objects['Rc-axe_y-txt'].color=color_rep_green
    scene.objects['Rc-axe_z'].color=color_rep_blue
    scene.objects['Rc-axe_z-txt'].color=color_rep_blue
    scene.objects['Rc-origine'].color=color_rep_target
    scene.objects['Rc-origine-txt'].color=color_rep_target
    obj.setVisible(False, True)
    obj['init'] = obj.worldTransform.copy()
    scene.objects['Rc-axe_x0'].color=color_rep_red
    scene.objects['Rc-axe_y0'].color=color_rep_green
    scene.objects['Rc-axe_z0'].color=color_rep_blue
    scene.objects['Rc-global'].setVisible(False, True)

    ##
    # Configuration du point outil
    #
    # Rw : Repère monde
    # Rm : Repère machine
    # Rp : Repère outil / point outil
    ##

    # Position initiale absolu du point outil (Rp/Rw) en m et en deg
    scene.objects['Rp']['x_init']  = 0
    scene.objects['Rp']['y_init']  = 0
    scene.objects['Rp']['z_init']  = 0.873
    scene.objects['Rp']['rx_init'] = 0
    scene.objects['Rp']['ry_init'] = 0
    scene.objects['Rp']['rz_init'] = 165 # Marque : angle dans blender à -15° -> mise à 0°

    # Décallage Rm/Rw en m et en deg
    # Callage sur les marques : angle dans blender à -15° pour Rm, Segment 1 -> à 0°
    scene.objects['Rm']['x_offset']  = 0
    scene.objects['Rm']['y_offset']  = 0
    scene.objects['Rm']['z_offset']  = 0
    scene.objects['Rm']['rx_offset'] = 0
    scene.objects['Rm']['ry_offset'] = 0
    scene.objects['Rm']['rz_offset'] = 165 # Marque : angle dans blender à -15° -> mise à 0°

    ##
    # Groupe de focus pour les actionneurs et focus bouton
    ##
    
    twin.cycle_def_focusgroup([['Moteur S0','grey'],
                               ['Poulie S0','grey'],
                               ['Courroie S0','grey']], "Moteur axe 1 : q1")

    twin.cycle_def_focusgroup([['Moteur S1','grey'],
                               ['Moteur 2 S1','grey'],
                               ['Poulie S1','grey'],
                               ['Poulie 2 S1','grey'],
                               ['Courroie S1','grey'],
                               ['Courroie 2 S1','grey']], "Moteur axe 2 : q2")

    twin.cycle_def_focusgroup([['Moteur S2','grey'],
                               ['Poulie S2','grey'],
                               ['Courroie S2','grey']], "Moteur axe 3 : q3")

    twin.cycle_def_focusgroup([['Moteur S3','grey'],
                               ['Butée aiguilles S3','grey']], "Moteur axe 4 : q4")

    twin.cycle_def_focusgroup([['Moteur S4','grey'],
                               ['Poulie S4','grey'],
                               ['Courroie S4','grey']], "Moteur axe 5 : q5")

    twin.cycle_def_focusgroup([['Moteur S5','grey'],
                               ['Butée aiguilles S5','grey'],
                               ['Raccord S5','grey']], "Moteur axe 6 : q6")

    twin.cycle_def_focusgroup([['Moteur Pince','grey'],
                               ['Plateau moteur Pince','grey']], "Ouverture outil (pince) : ot")

    twin.cycle_def_focusgroup([['Doigt 1','turquoise'],
                               ['Doigt 2','turquoise']], "Position et orientation : p")
    # twin.cycle_def_focusgroup([['Doigt 1','turquoise'],
    #                            ['Doigt 2','turquoise'],
    #                            ['Doigt 1-face','turquoise'],
    #                            ['Doigt 2-face','turquoise']], "Position et orientation : p")

    scene.objects['ARU']['description']="Arrêt d'urgence"

    # Script de démarrage
    twin.cycle_startcmd()

    # Rénitialisation du système
    system_reset()

def get_public_vars():
    return public_vars

###############################################################################
# Actionneurs
###############################################################################

##
# Moteurs
##

def mot(cont):
    obj = cont.owner
    fps = 60 # Frame Per Second
    obj['alpha_step']= obj['speed_setting'] / fps # Pas du du moteur en rad/frame , obj['speed_setting'] en rad/s (par défaut 1,57 rad/s (1/2 tr/s))
    if obj['r']==0: # Rapport de réduction, division par 0 -> 1
        obj['r']=1
    obj['q_step']=math.degrees(obj['alpha_step']/obj['r']) # Pas du segment en deg/frame
    
    ##
    # Fonction par coordonnée articulaire
    ##

    # Position atteinte -> Stop
    if obj['q'] == obj['q_target'] and (obj['q_manual']==False or obj['q_manual_sbs']==True) :
        obj['cw']=False
        obj['acw']=False
        obj['speed'] = 0
        obj['last_time'] = scene.objects['System']['time']
        return

    # Position pas atteinte
    if obj['q'] != obj['q_target'] and (obj['q_manual']==False or obj['q_manual_sbs']==True):
        
        # Stop
        if abs(obj['q_target']-obj['q']) <= obj['q_step']: # Ne peut pas faire mieux en vitesse rapide ou mode rapide
            obj['cw']=False
            obj['acw']=False

            # Termine l'action pour avoir la position exacte
            obj['alpha_step'] = math.radians((obj['q_target']-obj['q'])*obj['r'])
            obj['q_step']     = math.degrees(obj['alpha_step']/obj['r'])
            if len(obj['pulley'])==1:
                scene.objects[obj['pulley'][0]].applyRotation((0, 0, obj['alpha_step']), True)
            elif len(obj['pulley'])==2:
                scene.objects[obj['pulley'][0]].applyRotation((0, 0, obj['alpha_step']), True)
                scene.objects[obj['pulley'][1]].applyRotation((0, 0, obj['alpha_step']), True)
            # scene.objects[obj['bone']].applyRotation((0, 0, obj['alpha_step'] / obj['r']), True) # Mouvement sur l'armature par les objets
            # scene.objects['Armature'].channels[obj['bone']].rotation_mode = bge.logic.ROT_MODE_XYZ # Mouvement bone BGE : rotation par angle roll, pitch, yaw
            scene.objects['Armature'].channels[obj['bone']].rotation_euler+=mathutils.Vector(obj['axis'])*(obj['alpha_step']/obj['r']) # Mouvement bone BGE par angles d'euler
            # scene.objects['Armature'].channels[obj['bone']].rotation_quaternion+=rpy2quat(obj['axis'], (obj['alpha_step']/obj['r'])) # Mouvement bone BGE par quaternion
            scene.objects['Armature'].update()
            obj['alpha'] = obj['alpha']+obj['alpha_step']
            obj['speed'] = 0
            obj['last_time'] = scene.objects['System']['time']
            if obj['q_target']-obj['q']<0.00001: # Erreur de calcul
                obj['q']     = obj['q_target']
                obj['alpha'] = math.radians(obj['q']*obj['r'])
            else:
                obj['q'] = math.degrees(obj['alpha']/obj['r'])

        else:

            # Sens anti-horaire
            if obj['q'] < obj['q_target']:
                obj['cw']=False
                obj['acw']=True

            # Sens horaire
            if obj['q'] > obj['q_target']:
                obj['cw']=True
                obj['acw']=False

    ##
    # Fonctions bas niveau : uniquement sur le modèle numérique, commande manuelle
    ##

    # Rotation sens horaire (clockwise)
    if obj['cw'] and obj['acw'] == False:

        # Contrôle sur la limite basse
        if obj['q']-obj['q_step'] < obj['limit'][0]:
            print ("Limite basse atteinte pour q"+str(motors.index(obj.name)+1)+" : q : "+str(round(obj['q'],PRECISION_ANGLE))+" ; q_limit : "+str(obj['limit'][0])+" ; q_target : "+str(round(obj['q_target'],PRECISION_ANGLE)))
            if obj['q_force']==False:
                obj['q_target'] = obj['q']
                obj['cw']=False
                obj['acw']=False
                obj['speed'] = 0
                obj['last_time'] = scene.objects['System']['time']
                return
        
        if len(obj['pulley'])==1:
            scene.objects[obj['pulley'][0]].applyRotation((0, 0, -obj['alpha_step']), True)
        elif len(obj['pulley'])==2:
            scene.objects[obj['pulley'][0]].applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects[obj['pulley'][1]].applyRotation((0, 0, -obj['alpha_step']), True)
        # # scene.objects[obj['bone']].applyRotation((0, 0, -obj['alpha_step'] / obj['r']), True) # Mouvement sur l'armature par les objets
        # scene.objects['Armature'].channels[obj['bone']].rotation_mode = bge.logic.ROT_MODE_XYZ # Mouvement bone BGE : rotation par angle roll, pitch, yaw
        scene.objects['Armature'].channels[obj['bone']].rotation_euler+=mathutils.Vector(obj['axis'])*(-obj['alpha_step'] / obj['r']) # Mouvement bone BGE par angles d'euler
        # scene.objects['Armature'].channels[obj['bone']].rotation_quaternion+=rpy2quat(obj['axis'], (-obj['alpha_step']/obj['r'])) # Mouvement bone BGE par quaternion
        scene.objects['Armature'].update()

        # Mouvement bone BPY
        # bone = bpy.context.scene.objects['Armature'].pose.bones[obj['bone']]
        # mat = bone.parent.matrix.inverted() @ bone.matrix
        # bone.rotation_euler = mathutils.Euler((mat.to_euler().x+obj['axis'][0]*(-obj['alpha_step']/obj['r']),
        #                                        mat.to_euler().y+obj['axis'][1]*(-obj['alpha_step']/obj['r']),
        #                                        mat.to_euler().z+obj['axis'][2]*(-obj['alpha_step']/obj['r'])), 'XYZ')
        
        obj['alpha'] = obj['alpha']-obj['alpha_step']
        obj['speed'] = (-obj['alpha_step'])/(scene.objects['System']['time']-obj['last_time'])
        obj['last_time'] = scene.objects['System']['time']
        obj['q']= math.degrees(obj['alpha']/obj['r'])
        order=1

    # Rotation sens anti-horaire (anticlockwise)
    if obj['acw'] and obj['cw'] == False:
        
        # Contrôle sur la limite haute
        if obj['q']+obj['q_step'] > obj['limit'][1]:
            print ("Limite basse atteinte pour q"+str(motors.index(obj.name)+1)+" : q : "+str(round(obj['q'],PRECISION_ANGLE))+" ; q_limit : "+str(obj['limit'][1])+" ; q_target : "+str(round(obj['q_target'],PRECISION_ANGLE)))
            if obj['q_force']==False:
                obj['q_target'] = obj['q']
                obj['cw']=False
                obj['acw']=False
                obj['speed'] = 0
                obj['last_time'] = scene.objects['System']['time']
                return

        if len(obj['pulley'])==1:
            scene.objects[obj['pulley'][0]].applyRotation((0, 0, obj['alpha_step']), True)
        elif len(obj['pulley'])==2:
            scene.objects[obj['pulley'][0]].applyRotation((0, 0, obj['alpha_step']), True)
            scene.objects[obj['pulley'][1]].applyRotation((0, 0, obj['alpha_step']), True)
        # scene.objects[obj['bone']].applyRotation((0, 0, obj['alpha_step'] / obj['r']), True) # Mouvement sur l'armature par les objets
        # scene.objects['Armature'].channels[obj['bone']].rotation_mode = bge.logic.ROT_MODE_XYZ # Mouvement bone BGE : rotation par angle roll, pitch, yaw
        scene.objects['Armature'].channels[obj['bone']].rotation_euler+=mathutils.Vector(obj['axis'])*(obj['alpha_step']/obj['r']) # Mouvement bone BGE par angles d'euler
        # scene.objects['Armature'].channels[obj['bone']].rotation_quaternion+=rpy2quat(obj['axis'], (obj['alpha_step']/obj['r'])) # Mouvement bone BGE par quaternion
        scene.objects['Armature'].update()
        # scene.objects['IK'].localTransform = scene.objects['Rp'].localTransform # Lien entre q manuelle et Rc manuelle
        # scene.objects['Rc'].localTransform = scene.objects['Rp'].localTransform # Lien entre q manuelle et Rc manuelle
        obj['alpha']= obj['alpha']+obj['alpha_step']
        obj['speed']= (obj['alpha_step'])/(scene.objects['System']['time']-obj['last_time'])
        obj['last_time'] = scene.objects['System']['time']
        obj['q']= math.degrees(obj['alpha']/obj['r'])
        order=-1

    # Arrêrer
    if (obj['cw']== False and obj['acw'] == False) :
        obj['speed']= 0
        obj['last_time'] = scene.objects['System']['time']
        order=0

    # Message solveur pour la commande manuelle du moteur
    if obj['q_manual'] :
        move_manual_q_message(motors.index(obj.name)+1, order)
    p_message()

##
# Pince
#
# Position ouverte : value = 100 -> plateau moteur x =  50.40002052703745
# Position fermée  : value = 0   -> plateau moteur x = -24.59998839021686
# x = value * (24.59998839021686+50.40002052703745)/100 - 24.59998839021686
# value = (x + 24.59998839021686) / ((24.59998839021686+50.40002052703745)/100)
##

def gripper(cont):
    obj = cont.owner
    fps = 60 # frame per second
    obj['alpha_step'] = obj['speed_setting'] / fps # Pas du moteur théorique en rad/frame
    # Fonction : valeur du moteur (de 0 à 100) à partir de l'angle du plateau (en rad)
    plat2value = lambda alpha: (math.degrees(alpha)+24.59998839021686)/((24.59998839021686+50.40002052703745)/100)
    # Fonction : angle du plateau (en rad) à partir de la valeur du moteur (de 0 à 100)
    value2plat = lambda value: math.radians(value * (24.59998839021686+50.40002052703745)/100 - 24.59998839021686)
    obj['value_step_plateau'] = plat2value(obj['alpha_step'])-plat2value(0) # Pas du moteur en valeur/frame
    order = 0
  
    if obj['activated']:

        # Contrôle des limites
        if obj['value_target'] < 0:
            obj['value_target'] = 0
        if obj['value_target'] > 100:
            obj['value_target'] = 100

        # Ouverture (sens +)
        if obj['value'] < obj['value_target'] and abs(obj['value_target']-obj['value']) > obj['value_step_plateau']:
            scene.objects['Plateau moteur Pince'].applyRotation((0, 0, -obj['alpha_step']), True) # FIXME : normalement -obj['alpha_step']
            scene.objects['Bielle 1']            .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Biellette 1']         .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Bielle 2']            .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Biellette 2']         .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Doigt 1']             .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Doigt 2']             .applyRotation((0, 0,  obj['alpha_step']), True)
            obj['value'] = plat2value(scene.objects['Plateau moteur Pince'].localOrientation.to_euler().y)
            order = 1

        # Lacher la pièce si ouverture
        if obj['value'] < obj['value_target'] :
            if obj['grabbed'] != "" and scene.objects[obj['grabbed']]['taken']:
                obj_grab = scene.objects[obj['grabbed']]
                if len(obj_grab.children)>0:
                    for obj_child in obj_grab.children:
                        if "-solid" in obj_child.name :
                            obj_child.color = color_grey
                            break
                else:
                    obj_grab.color = color_grey
                scene.objects['Doigt 1'].color = color_turquoise
                scene.objects['Doigt 2'].color = color_turquoise
                scene.objects['Doigt 1']['grab'] = False
                scene.objects['Doigt 2']['grab'] = False
                obj_grab.restoreDynamics()
                obj_grab.removeParent()
                scene.objects[obj['grabbed']]['taken'] = False
                obj['grabbed'] = ""

        # Fermeture (sens -)
        if obj['value'] > obj['value_target'] and abs(obj['value_target']-obj['value']) > obj['value_step_plateau']:
            scene.objects['Plateau moteur Pince'].applyRotation((0, 0,  obj['alpha_step']), True) # FIXME : normalement +obj['alpha_step']
            scene.objects['Bielle 1']            .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Biellette 1']         .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Bielle 2']            .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Biellette 2']         .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Doigt 1']             .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Doigt 2']             .applyRotation((0, 0, -obj['alpha_step']), True)
            obj['value'] = plat2value(scene.objects['Plateau moteur Pince'].localOrientation.to_euler().y)
            order = -1

        # Stop, termine l'action pour avoir la position exacte
        if abs(obj['value_target']-obj['value']) <= obj['value_step_plateau']: # Ne peut pas faire mieux en vitesse rapide
            obj['activated'] = False
            obj['alpha_step'] = value2plat(obj['value_target']) - value2plat(obj['value']) 
            scene.objects['Plateau moteur Pince'].applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Bielle 1']            .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Biellette 1']         .applyRotation((0, 0,  obj['alpha_step']), True)
            scene.objects['Bielle 2']            .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Biellette 2']         .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Doigt 1']             .applyRotation((0, 0, -obj['alpha_step']), True)
            scene.objects['Doigt 2']             .applyRotation((0, 0,  obj['alpha_step']), True)
            obj['value'] = plat2value(scene.objects['Plateau moteur Pince'].localOrientation.to_euler().y)
            order = 0
            
    if obj['value_manual']:
        move_manual_q_message(7, order) # 7 pour l'outil
    p_message()


# Mettre à jour l'affichage du point p 
def p_message():
    p_txt = str("["+'{:>5.4f}'.format(scene.objects['Rp']         .worldPosition.x)+", "+
                '{:>5.4f}'.format(scene.objects['Rp']             .worldPosition.y)+", "+
                '{:>5.4f}'.format(scene.objects['Rp']             .worldPosition.z)+", "+
                '{:>6.2f}'.format(math.degrees(scene.objects['Rp'].worldOrientation.to_euler().x))+", "+
                '{:>6.2f}'.format(math.degrees(scene.objects['Rp'].worldOrientation.to_euler().y))+", "+
                '{:>6.2f}'.format(math.degrees(scene.objects['Rp'].worldOrientation.to_euler().z))+"]")
    q_txt = "["
    for i in range (len(motors)):
        if i < len(motors)-1:
            q_txt = q_txt+str('{:>6.2f}'.format(scene.objects[motors[i]]['q']))+", "
        else:
            q_txt = q_txt+str('{:>6.2f}'.format(scene.objects[motors[i]]['q']))+"]"
    scene.objects['ot-text']['Text'] = "ot : "+str(round(scene.objects['Moteur Pince']['value']))
    scene.objects['q-text']['Text']  = "q  : "+q_txt
    scene.objects['p-text']['Text']  = "p  : "+p_txt

##
# Déplacer le robot en allant aux coordonnées articulaires
#
# Propriétés du trajet:
# - color             : couleur des points (RGBA tuple (Rouge Vert Bleu Alpha), color_yellow par défaut)
# - speed             : vitesse du trajet (réel en m/s)
# - thickness         : épaisseur, taille des points (réel, facteur d'échelle)
# - tickskip          : fréquence d'apparition des points de la trajectoire, tous les x ticks (60 fps) (entier, 3 par défaut)
# - cont              : mode trajectoire continue (tous les mouvements arrivent en même temps) (binaire, uniquement en mode bloquant, par défaut)
# - force             : permet de dépasser les limites angulaires des moteurs (binaire)
##

def move_q(q, ot, wait=True, cont=True, draw=False, color=None, thickness=1, tickskip=3, force=False):

    # Coordonnées articulaires
    for i in range(6):
        if force:
            scene.objects[motors[i]]['q_force']=True
        scene.objects[motors[i]]['q_target']=q[i]

    # Outil
    if ot is not None: 
        scene.objects['Moteur Pince']['activated']=True
        scene.objects['Moteur Pince']['value_target']=ot

    # En mode bloquant
    if wait:

        # Affichage des points du trajet
        if draw:
            i_pt_debut = scene.objects['System']['move_draw_pt']
            if color is None:
                scene.objects['Commands']['move_draw_color'] = color_yellow
            else:
                scene.objects['Commands']['move_draw_color'] = color
            scene.objects['Commands']['move_draw_thickness'] = thickness
            scene.objects['System'].sensors['Draw_move'].skippedTicks = tickskip
            scene.objects['System']['move_draw']=True

        # Trajectoire continue : adaptation des vitesses
        if cont:

            # Sauvegarde des vitesses
            for i in range(6):
                scene.objects[motors[i]]['speed_prev'] = scene.objects[motors[i]]['speed_setting']

            # Trajectoire continue : adaptation des vitesses
            # Détermination des courses d'angle de chaque moteur et du temps le plus long
            q_course     = [0,0,0,0,0,0,0]
            alpha_course = [0,0,0,0,0,0,0]
            t_course     = [0,0,0,0,0,0,0]
            t_max        = 0.0
            for i in range(6):
                obj = scene.objects[motors[i]]
                if obj['speed_setting'] == 0:
                    print("Mouvement : erreur sur move_q(...) : réglage vitesse = 0 rad/s")
                    scene.objects['System']['move_draw'] = False
                    return False
                q_course[i] = abs(obj['q_target']-obj['q'])
                alpha_course[i] = math.radians(q_course[i]*obj['r'])
                t_course[i] = alpha_course[i]/obj['speed_setting']
                if t_max < t_course[i]:
                    t_max = t_course[i]
                scene.objects['System']['move_draw_t_max'] = t_max

            # Définition de la vitesse de chaque moteur
            if t_max != 0:
                for i in range(6):
                    scene.objects[motors[i]]['speed_setting'] = alpha_course[i]/t_max

        # Attente de la fin du mouvement -> Threading pour ne pas bloquer UPBGE
        arrived = False
        while arrived == False:
            arrived = True
            for i in range (len(motors)):
                if scene.objects[motors[i]]['q'] != scene.objects[motors[i]]['q_target']:
                    arrived = False
                    break
            # print ("move_q")
            sleep(0.01)

        # Réinit du draw_move
        if draw:

            # Points intermédiaires : dégradé si couleur=None
            scene.objects['System']['move_draw'] = False
            if color is None:
                i_pt_fin = scene.objects['System']['move_draw_pt']
                draw_recolor(i_pt_debut, i_pt_fin)
                
            # Affichage des points du trajet
            scene.objects['Commands']['move_draw_color'] = color_yellow
            scene.objects['Commands']['move_draw_thickness'] = 1
            scene.objects['System'].sensors['Draw_move'].skippedTicks = 3

        # Reprendre les vitesses définis précédement
        if cont:
            for i in range(6):
                scene.objects[motors[i]]['speed_setting'] = scene.objects[motors[i]]['speed_prev']

    # Réinit
    if force:
        for i in range(6):
            scene.objects[motors[i]]['q_force']=False

###############################################################################
# Commande manuelle par point cible
###############################################################################

##
# Message sur le positionnement de la cible (Rc)
##

def move_manual_rc_message():
    obj_ik = scene.objects['IK']
    obj_rc = scene.objects['Rc']
    obj_rp = scene.objects['Rp']
    
    # Vitesse
    if obj_rc['move_speed']:
        txt="Rapide"
    else:
        txt="Lent"

    # Repère
    if obj_rc['move_global']:
        txt=txt+" - Repère global"
    else:
        txt=txt+" - Repère local"

    # Type de mouvement
    if obj_rc['move_orientation']:
        txt=txt+" - Orientation"
    else:
        txt=txt+" - Position"

    # Ancrage IK
    if obj_ik['move']:
        if obj_ik['move_orientation']:
            txt=txt+" - Ancrage position + orientation"
        else:
            txt=txt+" - Ancrage position"
    else:
        txt=txt+" - Ancrage inactif"

    # Ordre
    scene.objects['Input-text']['Text']=txt

    # Position et orientation de Rc
    position_txt=str("["+'{:>5.4f}'.format(obj_rc.worldPosition.x)+", "+
               '{:>5.4f}'.format(obj_rc.worldPosition.y)+", "+
               '{:>5.4f}'.format(obj_rc.worldPosition.z)+", "+
               '{:>6.2f}'.format(math.degrees(obj_rc.worldOrientation.to_euler().x))+", "+
               '{:>6.2f}'.format(math.degrees(obj_rc.worldOrientation.to_euler().y))+", "+
               '{:>6.2f}'.format(math.degrees(obj_rc.worldOrientation.to_euler().z))+"]")
    # ecart = math.sqrt((obj.worldPosition.x-obj_rp.worldPosition.x)**2+
    #                   (obj.worldPosition.y-obj_rp.worldPosition.y)**2+
    #                   (obj.worldPosition.z-obj_rp.worldPosition.z)**2)
    ecart_txt=str('{:>5.4f}'.format(obj_rc.getDistanceTo(obj_rp)))
    ecart_angle_txt=str("["+'{:>6.2f}'.format(math.degrees(obj_rc.worldOrientation.to_euler().x-obj_rp.worldOrientation.to_euler().x))+", "+
                        '{:>6.2f}'.format(math.degrees(obj_rc.worldOrientation.to_euler().y-obj_rp.worldOrientation.to_euler().y))+", "+
                        '{:>6.2f}'.format(math.degrees(obj_rc.worldOrientation.to_euler().z-obj_rp.worldOrientation.to_euler().z))+"]")
    scene.objects['Solver-text']['Text']="Solveur Blender : positionnement manuel par point cible C : "+position_txt+" ; écart : "+ecart_txt+" ; écart angle : "+ecart_angle_txt

##
# Aller à la cible Rc (IK de Blender)
##

def move_manual_rc_ik(cont):
    obj_ik = cont.owner
    obj_rc = scene.objects['Rc']
    obj_rp = scene.objects['Rp']

    # Position de Rc
    obj_ik.worldPosition = obj_ik.worldPosition + obj_ik.getVectTo(obj_rc)[1]*0.01

    # Orientation de Rc
    vec_z_rc = obj_rc.getAxisVect([0,0,1])
    obj_ik.alignAxisToVect(vec_z_rc,1,1.0)
    vec_x_rc = obj_rc.getAxisVect([1,0,0])
    # obj_ik.alignAxisToVect(vec_x_rc,2,1.0)
    obj_ik.alignAxisToVect(vec_x_rc,0,1.0)

    # Stabilisation -> Gel et dégel de l'ancrage
    if obj_rc.getDistanceTo(obj_rp) < STABILITY_IK:
        bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=False
        bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=False
    else:
        if scene.objects['IK']['move_orientation']:
            if bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled == False:
                bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=True
        else:
            if bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled == False:
                bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=True

    # q_bone
    # FIXME : à controler
    arm = bpy.context.scene.objects['Armature']

    # q bones
    # q=[]
    # bone1 = arm_bpy.pose.bones['Segment 1 ikb']
    # mat1 = bone1.matrix
    # q.append(round(math.degrees(mat1.to_euler().z), precision_angle))

    # bone2 = arm_bpy.pose.bones['Segment 2 ikb']
    # mat2 = bone1.matrix.inverted() @ bone2.matrix
    # q.append(round(math.degrees(-mat2.to_euler().x), precision_angle))

    # bone3 = arm_bpy.pose.bones['Segment 3 ikb']
    # mat3 = bone2.matrix.inverted() @ bone3.matrix
    # q.append(round(math.degrees(-mat3.to_euler().x), precision_angle))

    # bone4 = arm_bpy.pose.bones['Segment 4 ikb']
    # mat4 = bone3.matrix.inverted() @ bone4.matrix
    # q.append(round(math.degrees(mat4.to_euler().y), precision_angle))

    # bone5 = arm_bpy.pose.bones['Segment 5 ikb']
    # mat5 = bone4.matrix.inverted() @ bone5.matrix
    # q.append(round(math.degrees(-mat5.to_euler().x), precision_angle))

    # bone6 = arm_bpy.pose.bones['Segment 6 ikb']
    # mat6 = bone5.matrix.inverted() @ bone6.matrix
    # q.append(round(math.degrees(mat6.to_euler().y), precision_angle))


    bone1 = arm.pose.bones['Segment 1']
    mat1 = bone1.matrix
    # print ("1", round(math.degrees(mat1.to_euler().z),2), round(math.degrees(mat1.to_euler().y),2), round(math.degrees(mat1.to_euler().z),2))
    scene.objects['Moteur S0']['q_bone']= round(math.degrees(mat1.to_euler().z), PRECISION_ANGLE)
    scene.objects['Moteur S0']['q_target']= scene.objects['Moteur S0']['q_bone']

    bone2 = arm.pose.bones['Segment 2']
    mat2 = bone1.matrix.inverted() @ bone2.matrix
    # print ("2", round(math.degrees(mat2.to_euler().x),2), round(math.degrees(mat2.to_euler().y),2), round(math.degrees(mat2.to_euler().z),2))
    scene.objects['Moteur S1']['q_bone']= round(math.degrees(-mat2.to_euler().x), PRECISION_ANGLE)
    scene.objects['Moteur S1']['q_target']= scene.objects['Moteur S1']['q_bone'] # Signe - -> Test
    # scene.objects['Moteur S1']['q_bone']= round(math.degrees(mat2.to_euler().x), precision_angle)
    # scene.objects['Moteur S1']['q_target']= -scene.objects['Moteur S1']['q_bone'] # Signe - -> Test

    bone3 = arm.pose.bones['Segment 3']
    mat3 = bone2.matrix.inverted() @ bone3.matrix
    # print ("3", round(math.degrees(mat3.to_euler().x),2), round(math.degrees(mat3.to_euler().y),2), round(math.degrees(mat3.to_euler().z),2))
    scene.objects['Moteur S2']['q_bone']= round(math.degrees(-mat3.to_euler().x), PRECISION_ANGLE)
    scene.objects['Moteur S2']['q_target']= scene.objects['Moteur S2']['q_bone'] # Signe - -> Test
    # scene.objects['Moteur S2']['q_bone']= round(math.degrees(mat3.to_euler().x), PRECISION_ANGLE)
    # scene.objects['Moteur S2']['q_target']= -scene.objects['Moteur S2']['q_bone'] # Signe - -> Test

    bone4 = arm.pose.bones['Segment 4']
    mat4 = bone3.matrix.inverted() @ bone4.matrix
    # print ("4", round(math.degrees(mat4.to_euler().y),2), round(math.degrees(mat4.to_euler().y),2), round(math.degrees(mat4.to_euler().z),2))
    scene.objects['Moteur S3']['q_bone']= round(math.degrees(mat4.to_euler().y), PRECISION_ANGLE)
    scene.objects['Moteur S3']['q_target']=scene.objects['Moteur S3']['q_bone']

    bone5 = arm.pose.bones['Segment 5']
    mat5 = bone4.matrix.inverted() @ bone5.matrix
    # print ("5", round(math.degrees(mat5.to_euler().x),2), round(math.degrees(mat5.to_euler().y),2), round(math.degrees(mat5.to_euler().z),2))
    scene.objects['Moteur S4']['q_bone']= round(math.degrees(-mat5.to_euler().x), PRECISION_ANGLE)
    scene.objects['Moteur S4']['q_target']= scene.objects['Moteur S4']['q_bone'] # Signe - -> Test
    # scene.objects['Moteur S4']['q_bone']= round(math.degrees(mat5.to_euler().x), PRECISION_ANGLE)
    # scene.objects['Moteur S4']['q_target']= -scene.objects['Moteur S4']['q_bone'] # Signe - -> Test

    bone6 = arm.pose.bones['Segment 6']
    mat6 = bone5.matrix.inverted() @ bone6.matrix
    # print ("6", round(math.degrees(mat6.to_euler().y),2), round(math.degrees(mat6.to_euler().y),2), round(math.degrees(mat6.to_euler().z),2))
    scene.objects['Moteur S5']['q_bone']= round(math.degrees(mat6.to_euler().y), PRECISION_ANGLE)
    scene.objects['Moteur S5']['q_target']=scene.objects['Moteur S5']['q_bone']


##
# Déplacement manuel de la cible Rc
##

def move_manual_rc(joy):
    obj = scene.objects['System']
    joy_events = joy.activeButtons
    joy_axis = joy.axisValues[0:4]
    joy_leftstick_x = joy_axis[0]
    joy_leftstick_y = joy_axis[1]
    joy_rightstick_x = joy_axis[2]
    joy_rightstick_y = joy_axis[3]
    # print(joy_events)
    # print ("leftstick : ", joy_leftstick_x , joy_leftstick_y)
    # print ("rightstick : ", joy_rightstick_x , joy_rightstick_y)
    obj_rc = scene.objects['Rc']
    obj_rp = scene.objects['Rp']
    obj_ik = scene.objects['IK']
    obj_rc_global = scene.objects['Rc-global']

    # Activer/désactiver mode point cible Rc (touche start)
    if 6 in joy_events:

        # Désactiver mode point cible Rc
        if obj['manual_rc']:
            obj_ik['move']=False
            obj['manual_rc']=False

            # Déblocage des moteurs
            for i in range(6):
                obj_mot = scene.objects[motors[i]]

                # Moteurs bloqués
                if 'active' in obj_mot.getPropertyNames():
                    if obj_mot['active'] == False:
                        move_manual_rc_mot(i+1) # Bascule

                # Contrainte de rotation
                bpy.data.objects["Armature"].pose.bones[obj_mot['bone']].use_ik_rotation_control=False # Blocage du segment en IK
                # bpy.data.objects["Armature"].pose.bones[mot_bone[i]].ik_rotation_weight=1.0 # Blocage du segment en IK

            # Affichage commande
            obj_rc.setVisible(False, True)
            obj_rc_global.setVisible(False, True)
            scene.objects['Joystick-icon'].setVisible(False,True)
            scene.objects['Keyboard-icon2'].setVisible(False,True)
            scene.objects['Input-text']['Text']=""
            scene.objects['Input-text2']['Text']=""
            scene.objects['Solver-text']['Text']="Solveur prêt."
            bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=False
            bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=False
            sleep(0.2) # Double saisie
            return

        # Activer mode point cible Rc
        else:
            obj['manual_rc']=True
            obj['manual_q']=False

            # Désactivation du moteur en cours (mode manuel q)
            for motor_name in motors:
                obj_motor=scene.objects[motor_name]
                if obj_motor['q_manual']:
                    obj_motor['q_manual']=False
                    obj['q_manual_sbs']=False
                    obj_motor['cw']=False
                    obj_motor['acw']=False
                    obj_motor['q_target'] = obj_motor['q'] # Fixe la position
                    for obj_group in scene.objects[motor_name]['color_group']:
                        scene.objects[obj_group]['q_manual'] = False
                        twin.cycle_actuator_color(obj_group)
                    break

            # Affichage Rc
            obj_rc.setVisible(True, True)

            # Affichage commande
            scene.objects['Joystick-icon'].setVisible(True,True)
            scene.objects['Keyboard-icon'].setVisible(False,True)
            scene.objects['Keyboard-icon2'].setVisible(True,True)
            scene.objects['Input-text'].setVisible(True,True)
            scene.objects['Input-text2'].setVisible(True,True)
            scene.objects['Input-text']['Text']="Repère local - Translation - Ancrage IK inactif"
            scene.objects['Input-text2']['Text']="Moteurs bloqués : aucun"
            sleep(0.2) # Double saisie

    # Vitesse du curseur (gachette droite)
    if 10 in joy_events:
        obj_rc['move_speed']=True
        step_lin = 0.01
        step_rot = 0.1
    else:
        obj_rc['move_speed']=False
        step_lin = 0.001
        step_rot = 0.01

    # Position / orientation (gachette gauche)
    if 9 in joy_events:
        obj_rc['move_orientation']=True
    else:
        obj_rc['move_orientation']=False

    # Activer ancrage IK (touche A verte)
    if 0 in joy_events:
        obj_ik.worldTransform = obj_rp.worldTransform # Passage du mode objet au mode pose
        if scene.objects['IK']['move_orientation']:
            bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=True
        else:
            bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=True
        obj_ik['move']=True

    # Stopper ancrage IK (touche B rouge)
    if 1 in joy_events: # Stopper (touche B rouge)
        obj_ik['move']=False
        bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=False
        bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=False
        
    # Ancrage sur l'orientation
    if 3 in joy_events:
        if obj_ik['move']: # Ancrage actif
            if scene.objects['IK']['move_orientation']:
                scene.objects['IK']['move_orientation']=False
                if scene.objects['IK']['move']:
                    bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=False
                    bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=True
                # bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].use_rotation=False # Bug -> plantage
            else:
                scene.objects['IK']['move_orientation']=True
                if scene.objects['IK']['move']:
                    bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].enabled=False
                    bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK-rot"].enabled=True
                # bpy.data.objects["Armature"].pose.bones["Pince"].constraints["IK"].use_rotation=True # Bug -> plantage
            sleep(0.2) # Double saisie

    # Init du point (touche back)
    if 4 in joy_events:
        if obj_rc['move_orientation']: # Raz orientation
            obj_rc.alignAxisToVect([0,0,1],2,1.0)
            obj_rc.alignAxisToVect([1,0,0],0,1.0)
        else:
            obj_rc.worldTransform = obj_rc['init'] # Raz position + orientation
            obj_rc_global.worldPosition = obj_rc.worldPosition

    # Placement de la cible sur le point outil (touche X bleu)
    if 2 in joy_events:
        obj_rc.worldTransform = obj_rp.worldTransform

    # Bascule en repère global/local
    if obj_rc['move_global'] and scene.objects['Rc-axe_x0'].visible == False:
        scene.objects['Rc-axe_x0'].color    = color_rep_red
        scene.objects['Rc-axe_x'].color     = color_rep_grey
        scene.objects['Rc-axe_x-txt'].color = color_rep_grey
        scene.objects['Rc-axe_y0'].color    = color_rep_green
        scene.objects['Rc-axe_y'].color     = color_rep_grey
        scene.objects['Rc-axe_y-txt'].color = color_rep_grey
        scene.objects['Rc-axe_z0'].color    = color_rep_blue
        scene.objects['Rc-axe_z'].color     = color_rep_grey
        scene.objects['Rc-axe_z-txt'].color = color_rep_grey
        scene.objects['Rc-global'].setVisible(True, True)
    if obj_rc['move_global'] == False and scene.objects['Rc-axe_x0'].visible:
        scene.objects['Rc-axe_x'].color     = color_rep_red
        scene.objects['Rc-axe_x-txt'].color = color_rep_red
        scene.objects['Rc-axe_y'].color     = color_rep_green
        scene.objects['Rc-axe_y-txt'].color = color_rep_green
        scene.objects['Rc-axe_z'].color     = color_rep_blue
        scene.objects['Rc-axe_z-txt'].color = color_rep_blue
        scene.objects['Rc-global'].setVisible(False, True)

    # En X
    if joy_rightstick_x>0.5:

        # Décoration
        if obj_rc['move_global']:
            scene.objects['Rc-axe_x0'].color=color_white
        else:
            scene.objects['Rc-axe_x'].color=color_white
            scene.objects['Rc-axe_x-txt'].color=color_white

        # Déplacement
        if obj_rc['move_orientation']: # Rotation
            obj_rc.applyRotation((step_rot,0,0), not(obj_rc['move_global']))
        else: # Translation
            if obj_rc['move_global']:
                obj_rc.applyMovement((scene.objects['System'].getAxisVect([1,0,0])*step_lin), False)
            else:
                obj_rc.applyMovement((step_lin,0,0), True)
            obj_rc_global.worldPosition=obj_rc.worldPosition
            
    elif joy_rightstick_x<-0.5:

        # Décoration
        if obj_rc['move_global']:
            scene.objects['Rc-axe_x0'].color=color_white
        else:
            scene.objects['Rc-axe_x'].color=color_white
            scene.objects['Rc-axe_x-txt'].color=color_white

        # Déplacement
        if obj_rc['move_orientation']: # Rotation
            obj_rc.applyRotation((-step_rot,0,0), not(obj_rc['move_global']))
        else: # Translation
            if obj_rc['move_global']:
                obj_rc.applyMovement((scene.objects['System'].getAxisVect([1,0,0])*-step_lin), False)
            else:
                obj_rc.applyMovement((-step_lin,0,0), True)
            obj_rc_global.worldPosition=obj_rc.worldPosition
    else:
        if obj_rc['move_global']:
            scene.objects['Rc-axe_x0'].color=color_rep_red
        else:
            scene.objects['Rc-axe_x'].color=color_rep_red
            scene.objects['Rc-axe_x-txt'].color=color_rep_red

    # En Y
    if joy_rightstick_y<-0.5:

        # Décoration
        if obj_rc['move_global']:
            scene.objects['Rc-axe_y0'].color=color_white
        else:
            scene.objects['Rc-axe_y'].color=color_white
            scene.objects['Rc-axe_y-txt'].color=color_white

        # Déplacement
        if obj_rc['move_orientation']: # Rotation
            obj_rc.applyRotation((0,step_rot,0), not(obj_rc['move_global']))
        else: # Translation
            if obj_rc['move_global']:
                obj_rc.applyMovement((scene.objects['System'].getAxisVect([0,1,0])*step_lin), False)
            else:
                obj_rc.applyMovement((0,step_lin,0), True)
            obj_rc_global.worldPosition=obj_rc.worldPosition

    elif joy_rightstick_y>0.5:

        # Décoration
        if obj_rc['move_global']:
            scene.objects['Rc-axe_y0'].color=color_white
        else:
            scene.objects['Rc-axe_y'].color=color_white
            scene.objects['Rc-axe_y-txt'].color=color_white

        # Déplacement
        if obj_rc['move_orientation']: # Rotation
            obj_rc.applyRotation((0,-step_rot,0), not(obj_rc['move_global']))
        else: # Translation
            if obj_rc['move_global']:
                obj_rc.applyMovement((scene.objects['System'].getAxisVect([0,1,0])*-step_lin), False)
            else:
                obj_rc.applyMovement((0,-step_lin,0), True)
            obj_rc_global.worldPosition=obj_rc.worldPosition
    else:
        if obj_rc['move_global']:
            scene.objects['Rc-axe_y0'].color=color_rep_green
        else:
            scene.objects['Rc-axe_y'].color=color_rep_green
            scene.objects['Rc-axe_y-txt'].color=color_rep_green

    # En Z
    if joy_leftstick_y<-0.5:

        # Décoration
        if obj_rc['move_global']:
            scene.objects['Rc-axe_z0'].color=color_white
        else:
            scene.objects['Rc-axe_z'].color=color_white
            scene.objects['Rc-axe_z-txt'].color=color_white

        # Déplacement
        if obj_rc['move_orientation']: # Rotation
            obj_rc.applyRotation((0,0,step_rot), not(obj_rc['move_global']))
        else: # Translation
            if obj_rc['move_global']:
                obj_rc.applyMovement((scene.objects['System'].getAxisVect([0,0,1])*step_lin), False)
            else:
                obj_rc.applyMovement((0,0,step_lin), True)
            obj_rc_global.worldPosition=obj_rc.worldPosition

    elif joy_leftstick_y>0.5:

        # Décoration
        if obj_rc['move_global']:
            scene.objects['Rc-axe_z0'].color=color_white
        else:
            scene.objects['Rc-axe_z'].color=color_white
            scene.objects['Rc-axe_z-txt'].color=color_white

        # Déplacement
        if obj_rc['move_orientation']: # Rotation
            obj_rc.applyRotation((0,0,-step_rot), not(obj_rc['move_global']))
        else: # Translation
            if obj_rc['move_global']:
                obj_rc.applyMovement((scene.objects['System'].getAxisVect([0,0,1])*-step_lin), False)
            else:
                obj_rc.applyMovement((0,0,-step_lin), True)
            obj_rc_global.worldPosition=obj_rc.worldPosition
    else:
        if obj_rc['move_global']:
            scene.objects['Rc-axe_z0'].color=color_rep_blue
        else:
            scene.objects['Rc-axe_z'].color=color_rep_blue
            scene.objects['Rc-axe_z-txt'].color=color_rep_blue

    # Messages entrée + solveur
    move_manual_rc_message()

##
# Bloquage/débloquage des moteurs
##

def move_manual_rc_mot(motor_i):
    obj_mot  = scene.objects[motors[motor_i-1]]
    obj_bone = bpy.data.objects["Armature"].pose.bones["Segment "+str(motor_i)]

    # Contrainte de la rotation plus forte que la contrainte IK
    if obj_bone.use_ik_rotation_control:
        obj_bone.use_ik_rotation_control = False
        for obj_group in obj_mot['color_group']:
            scene.objects[obj_group]['active'] = True
            twin.cycle_actuator_color(obj_group)
    else:
        obj_bone.use_ik_rotation_control = True
        for obj_group in obj_mot['color_group']:
            scene.objects[obj_group]['active'] = False
            scene.objects[obj_group].color = color_passive

    # Message
    txt=""
    for i in range(6):
        obj = scene.objects[motors[i]]
        if 'active' in obj.getPropertyNames():
            if obj['active'] == False:
                txt = txt + "q"+str(i+1)+" "
    if len(txt)==0:
        scene.objects['Input-text2']['Text']="Moteurs bloqués : aucun"
    else:
        scene.objects['Input-text2']['Text']="Moteurs bloqués : " + txt

##
# Aller à la pose de l'armature
##

def move_q_bone():
    twin_threading.start(threads_move, "move", move_q_bone_thread)

def move_q_bone_thread():
    q=[]
    for i in range(6):
        obj = scene.objects[motors[i]]
        q.append(obj['q_bone'])
    move_q(q, scene.objects['Moteur Pince']['value'])

###############################################################################
# Commande manuelle des moteurs
# Order : rotation horaire (1), anti-horaire (-1), stop (0), raz (2)
###############################################################################

##
# Message sur le positionnement de la cible (Rp)
##

def move_manual_q_message(motor_i, order=None):
    motors2=['Moteur S0', 'Moteur S1', 'Moteur S2', 'Moteur S3', 'Moteur S4', 'Moteur S5', 'Moteur Pince']
    obj=scene.objects[motors2[motor_i-1]]

    # Position et orientation de Rp
    # position_txt=str("["+'{:>5.4f}'.format(scene.objects['Rp'].worldPosition.x)+", "+
    #                  '{:>5.4f}'.format(scene.objects['Rp'].worldPosition.y)+", "+
    #                  '{:>5.4f}'.format(scene.objects['Rp'].worldPosition.z)+", "+
    #                  '{:>6.2f}'.format(math.degrees(scene.objects['Rp'].worldOrientation.to_euler().x))+", "+
    #                  '{:>6.2f}'.format(math.degrees(scene.objects['Rp'].worldOrientation.to_euler().y))+", "+
    #                  '{:>6.2f}'.format(math.degrees(scene.objects['Rp'].worldOrientation.to_euler().z))+"]")
    # scene.objects['Solver-text']['Text']="Solveur Blender : commande manuelle : point outil P : "+position_txt
    scene.objects['Solver-text']['Text']="Solveur Blender : positionnement manuel par commande directe des moteurs."

    # Commande
    if order is not None:
        order_txt="stop"
        if order==1:
            order_txt="rotation horaire"
        if order==-1:
            order_txt="rotation anti-horaire"
        if order==2:
            order_txt="raz"
        if motor_i==7:
            scene.objects['Input-text']['Text']="Commande outil "+order_txt +" : valeur = "+str(round(obj['value']))+" ; pas = "+str(obj['value_manual_step']) # Outil
        else:
            scene.objects['Input-text']['Text']="Commande moteur q"+str(motor_i)+" "+order_txt +" : q"+str(motor_i)+" = "+str(round(obj['q'],2))+" ; pas = "+str(obj['q_manual_step']) # Moteur

##
# Commande manuelle de la pince
##

def move_manual_ot():
    obj=scene.objects['Moteur Pince']

    # Désactivation
    if obj['value_manual']:
        scene.objects['System']['manual_q'] = False
        scene.objects['Keyboard-icon'].setVisible(False,True)
        scene.objects['Input-text']['Text'] = ""
        scene.objects['Solver-text']['Text'] = "Solveur prêt."
        obj['value_manual'] = False
        obj['value_target'] = obj['value'] # Fixe la position
        for obj_group in obj['color_group']:
            scene.objects[obj_group]['value_manual'] = False
            twin.cycle_actuator_color(obj_group)

    # Activation
    else:

        # Désactivation du moteur en cours
        for motor_name in motors:
            obj_motor = scene.objects[motor_name]
            if obj_motor['q_manual']:
                obj_motor['q_manual'] = False
                obj_motor['q_manual_sbs'] = False
                obj_motor['cw'] = False
                obj_motor['acw'] = False
                obj_motor['q_target'] = obj_motor['q'] # Fixe la position
                for obj_group in scene.objects[motor_name]['color_group']:
                    scene.objects[obj_group]['q_manual'] = False
                    twin.cycle_actuator_color(obj_group)
                break

        # Activation
        scene.objects['System']['manual_q'] = True
        scene.objects['Joystick-icon'].setVisible(False,True)
        scene.objects['Keyboard-icon'].setVisible(True,True)
        obj['value_manual'] = True
        for obj_name in obj['color_group']:
            scene.objects[obj_name]['value_manual'] = True
            scene.objects[obj_name].color = color_mot_activated

# Commande manuelle des moteurs
def move_manual_q(motor_i, order):

    # Activation/désactivation du moteur
    if motor_i is not None :
        obj=scene.objects[motors[motor_i-1]]

        # Désactivation
        if obj['q_manual']:
            move_manual_q_message(motor_i, order=None)
            scene.objects['System']['manual_q'] = False
            scene.objects['Keyboard-icon'].setVisible(False,True)
            scene.objects['Input-text']['Text'] = ""
            scene.objects['Solver-text']['Text'] = "Solveur prêt."
            obj['q_manual'] = False
            obj['q_manual_sbs'] = False
            obj['cw'] = False
            obj['acw'] = False
            obj['q_target'] = obj['q'] # Fixe la position
            obj['speed_setting'] = obj['speed_prev'] # Retour à la vitesse programmé
            for obj_group in obj['color_group']:
                scene.objects[obj_group]['q_manual'] = False
                twin.cycle_actuator_color(obj_group)

        # Activation
        else:

            # Désactivation de la pince
            if scene.objects['Moteur Pince']['value_manual']:
                obj_ot=scene.objects['Moteur Pince']
                obj_ot['value_manual'] = False
                obj_ot['activated'] = False
                obj_ot['value_target']  = obj_ot['value'] # Fixe la position
                for obj_group in obj_ot['color_group']:
                    scene.objects[obj_group]['value_manual'] = False
                    twin.cycle_actuator_color(obj_group)

            # Désactivation du moteur en cours
            else:
                obj['q_manual_sbs'] = False
                for motor_name in motors:
                    obj_motor=scene.objects[motor_name]
                    if obj_motor['q_manual']:
                        obj_motor['q_manual'] = False
                        obj_motor['q_manual_sbs'] = False
                        obj_motor['cw'] = False
                        obj_motor['acw'] = False
                        obj_motor['q_target'] = obj_motor['q'] # Fixe la position
                        obj_motor['speed_setting']=obj_motor['speed_prev'] # Retour à la vitesse programmé
                        for obj_group in scene.objects[motor_name]['color_group']:
                            scene.objects[obj_group]['q_manual'] = False
                            twin.cycle_actuator_color(obj_group)
                        break

            # Activation
            scene.objects['System']['manual_q'] = True
            scene.objects['Keyboard-icon'].setVisible(True,True)
            scene.objects['Input-text'].setVisible(True,True)
            obj['q_manual'] = True
            obj['speed_prev'] = obj['speed_setting'] # Passage à la vitesse manuelle
            # FIXME
            # obj['speed_setting'] = math.radians(obj['q_manual_speed']*obj['r']) # Passage à la vitesse manuelle (vitesse du segment, pas celui du moteur)
            for obj_name in obj['color_group']:
                scene.objects[obj_name]['q_manual'] = True
                scene.objects[obj_name].color  = color_mot_activated

    # Ordre pour les moteurs
    if order is not None :

        # Raz de tout
        if order == 3:
            scene.objects['Moteur Pince']['activated'] = True
            scene.objects['Moteur Pince']['value_target'] = 33
            for motor_i in range(len(motors)):
                scene.objects[motors[motor_i]]['q_manual_sbs'] = True # step by step
                scene.objects[motors[motor_i]]['q_target'] = 0
        else: 

            # Pince
            if scene.objects['Moteur Pince']['value_manual']:
                obj=scene.objects['Moteur Pince']

                # Ouvrir
                if order == 1:
                    obj['activated'] = True
                    obj['value_target'] = 100

                # Stop
                if order == 0:
                    obj['activated'] = False
                    obj['value_target'] = obj['value']

                # Fermer
                if order == -1:
                    obj['activated'] = True
                    obj['value_target'] = 0

                # Ouvrir pas à pas 
                if order == 6:
                    obj['activated'] = True
                    obj['value_target'] = round(obj['value'])+obj['value_manual_step']

                # Fermer pas à pas 
                if order == 7:
                    obj['activated'] = True
                    obj['value_target'] = round(obj['value'])-obj['value_manual_step']

                # Raz
                if order==2:
                    obj['activated'] = True
                    obj['value_target'] = 33

                # Pas-
                if order == 4:
                    step_mode = [1, 2, 5, 10, 50] # q_step deg/step
                    i = step_mode.index(obj['value_manual_step'])
                    if i > 0:
                        obj['value_manual_step'] = step_mode[i-1]
                    txt = scene.objects['Input-text']['Text']
                    scene.objects['Input-text']['Text'] = txt[:txt.find(" ; pas =")]+" ; pas ="+str(obj['value_manual_step'])

                # Pas+
                if order == 5:
                    step_mode = [1, 2, 5, 10, 50] # q_step deg/step
                    i = step_mode.index(obj['value_manual_step'])
                    if i < 4:
                        obj['value_manual_step'] = step_mode[i+1]
                    txt = scene.objects['Input-text']['Text']
                    scene.objects['Input-text']['Text'] = txt[:txt.find(" ; pas =")]+" ; pas ="+str(obj['value_manual_step'])

            # Moteur
            else:
                for motor_i in range(len(motors)):
                    obj=scene.objects[motors[motor_i]]
                    if obj['q_manual']:

                        # Sens horaire
                        if order==1:
                            obj['q_manual_sbs'] = False
                            obj['cw']  = True
                            obj['acw'] = False

                        # Stop
                        if order == 0:
                            obj['q_manual_sbs'] = False
                            obj['cw']  = False
                            obj['acw'] = False

                        # Sens anti-horaire
                        if order == -1:
                            obj['q_manual_sbs'] = False
                            obj['cw']  = False
                            obj['acw'] = True

                        # Sens horaire pas à pas 
                        if order == 6:
                            obj['q_manual_sbs'] = True # step by step
                            obj['q_target'] = (round(obj['q']/0.5,0)*0.5)-obj['q_manual_step']

                        # Sens anti-horaire pas à pas 
                        if order == 7:
                            obj['q_manual_sbs'] = True # step by step
                            obj['q_target'] = (round(obj['q']/0.5,0)*0.5)+obj['q_manual_step']

                        # Pas-
                        if order == 4:
                            fps = 60 # Frame Per Second
                            step_mode = [0.5, 1, 2, 5, 10, 45] # q_step deg/step
                            i = step_mode.index(obj['q_manual_step'])
                            if i > 0:
                                obj['q_manual_step'] = step_mode[i-1]
                            move_manual_q_message(motors.index(obj.name)+1, order)
                            txt = scene.objects['Input-text']['Text']
                            scene.objects['Input-text']['Text'] = txt[:txt.find(" ; pas =")]+" ; pas ="+str(obj['q_manual_step'])

                        # Pas+
                        if order == 5:
                            fps = 60 # Frame Per Second
                            step_mode = [0.5, 1, 2, 5, 10, 45] # q_step deg/step
                            i = step_mode.index(obj['q_manual_step'])
                            if i < 5:
                                obj['q_manual_step'] = step_mode[i+1]
                            txt = scene.objects['Input-text']['Text']
                            scene.objects['Input-text']['Text'] = txt[:txt.find(" ; pas =")]+" ; pas ="+str(obj['q_manual_step'])

                        # Raz
                        if order == 2:
                            obj['q_manual_sbs'] = True # step by step
                            obj['q_target'] = 0
                        break

###############################################################################
# Affichage
###############################################################################

##
# Modifier l'état des objets flottants (points, repères et étiquettes, bugfix)
##

# Fonction passe-plat
def draw_cmd(cont):

    # Point
    if scene.objects['Commands']['draw_pt']:
        draw_point(scene.objects['Commands']['draw_p'], scene.objects['Commands']['draw_color'], scene.objects['Commands']['draw_text'])
        scene.objects['Commands']['draw_pt'] = False

    # Repère
    if scene.objects['Commands']['draw_rep']:
        draw_axis(scene.objects['Commands']['draw_p'], scene.objects['Commands']['draw_color'],
                  scene.objects['Commands']['draw_text'], scene.objects['Commands']['draw_data'])
        scene.objects['Commands']['draw_rep'] = False
    
    # Trajectoire
    if scene.objects['Commands']['draw_traj']:
        draw_traj(scene.objects['Commands']['draw_data'])
        scene.objects['Commands']['draw_traj'] = False
        # scene.objects['Commands']['clear_pt']  = False
        # scene.objects['Commands']['clear_rep'] = False
        # scene.objects['Commands']['clear_lbl'] = False
    
    # Clear
    # if scene.objects['Commands']['draw_traj'] == False:
    if scene.objects['Commands']['clear_pt'] or scene.objects['Commands']['clear_rep'] or scene.objects['Commands']['clear_lbl']:
        draw_clear(scene.objects['Commands']['clear_pt'], scene.objects['Commands']['clear_rep'], scene.objects['Commands']['clear_lbl'])
        scene.objects['Commands']['clear_pt']  = False
        scene.objects['Commands']['clear_rep'] = False
        scene.objects['Commands']['clear_lbl'] = False

    bpy.context.view_layer.update() # Mise à jour
    # sleep(0.1)
    scene.objects['Commands']['draw'] = False

##
# Primitives
##

# Afficher un point (sphère 3D, pooling system)
def draw_point(p, color, text, thickness=1):
    obj = scene.objects['System']

    # Point
    p_obj= scene.objects['p'+str(obj['move_draw_pt'])]
    p_obj.worldPosition = [p[0],p[1],p[2]]
    p_obj.worldScale = [thickness,thickness,thickness]
    p_obj.color = color
    p_obj.setVisible(True, False)
    obj['move_draw_pt'] += 1
    if obj['move_draw_pt'] == obj['move_draw_pts']: # Retour à 0
        obj['move_draw_pt'] = 0

    # Etiquette
    if text is not None and len(text)>0:
        lbl_obj = scene.objects['lbl'+str(obj['move_draw_lbl'])]
        mat = mathutils.Matrix.LocRotScale(mathutils.Vector((p[0],p[1],p[2])),
                                           mathutils.Euler((math.radians(p[3]), math.radians(p[4]), math.radians(p[5]))),
                                           mathutils.Vector((0.045,0.045,0.045)))
        lbl_obj.worldTransform = mat
        lbl_obj.color = color
        # lbl_obj.blenderObject.data.body = text # Bug de la propriétés 'Text' (UPBGE) -> passage par 'body' de bpy (Blender)
        lbl_obj['Text'] = text # FIXME : problème sur les étiquettes
        lbl_obj.setVisible(True, False)
        obj['move_draw_lbl'] += 1
        if obj['move_draw_lbl'] == obj['move_draw_lbls']: # Retour à 0
            obj['move_draw_lbl'] = 0

# Afficher un repère/pose (pooling system)
def draw_axis(p, color, text, pose_data):

    # Repère
    obj = scene.objects['System']
    rep_obj= scene.objects['rep'+str(obj['move_draw_rep'])]
    mat = mathutils.Matrix.LocRotScale(mathutils.Vector((p[0],p[1],p[2])),
                                       mathutils.Euler((math.radians(p[3]), math.radians(p[4]), math.radians(p[5]))),
                                       mathutils.Vector((1,1,1)))
    rep_obj.worldTransform = mat
    rep_obj.color = color
    rep_obj.setVisible(True, False)
    obj['move_draw_rep'] += 1
    if obj['move_draw_rep'] == obj['move_draw_reps']: # Retour à 0
        obj['move_draw_rep'] = 0
        obj['move_draw_pose'] = 0

    # Etiquette
    if text is not None and len(text)>0:
        lbl_obj = scene.objects['lbl'+str(obj['move_draw_lbl'])]
        mat_lbl = mathutils.Matrix.LocRotScale(mathutils.Vector((p[0],p[1],p[2])),
                                           mathutils.Euler((math.radians(p[3]), math.radians(p[4]), math.radians(p[5]))),
                                           mathutils.Vector((0.045,0.045,0.045)))
        lbl_obj.worldTransform = mat_lbl
        lbl_obj.color = color_white
        # lbl_obj.blenderObject.data.body = text # Bug de la propriétés 'Text' (UPBGE) -> passage par 'body' de bpy (Blender)
        lbl_obj['Text'] = text # FIXME : problème sur les étiquettes
        lbl_obj.setVisible(True, False)
        obj['move_draw_lbl'] += 1
        if obj['move_draw_lbl'] == obj['move_draw_lbls']: # Retour à 0
            obj['move_draw_lbl'] = 0

    # Données de la pose
    if pose_data is not None and len(pose_data) > 0:
        rep_obj['label'] = text
        rep_obj['q']     = pose_data[0]
        rep_obj['ot']    = pose_data[1]
        rep_obj['p']     = p
        rep_obj['prop']  = pose_data[2]
        rep_obj['mode']  = pose_data[3]
        obj['move_draw_pose'] += 1

# # Afficher une étiquette (pooling system)
# # p : pose sous forme de liste ou de matrice de transformation
# def draw_label(p, color, text):
#     obj = scene.objects['System']
#     lbl_obj= scene.objects['lbl'+str(obj['move_draw_lbl'])]
#     if type(p)==list:
#         mat = mathutils.Matrix.LocRotScale(mathutils.Vector((p[0],p[1],p[2])),
#                                            mathutils.Euler((math.radians(p[3]), math.radians(p[4]), math.radians(p[5]))),
#                                            mathutils.Vector((0.045,0.045,0.045)))
#     else:
#         loc, rot, sca = p.decompose()
#         mat = mathutils.Matrix.LocRotScale(loc, rot, mathutils.Vector((0.045,0.045,0.045)))
#     lbl_obj.worldTransform = mat
#     lbl_obj.color = color
#     # lbl_obj.blenderObject.data.body = text # Bug de la propriétés 'Text' (UPBGE) -> passage par 'body' de bpy (Blender)
#     lbl_obj['Text'] = text # FIXME : problème sur les étiquettes
#     lbl_obj.setVisible(True, False)
#     obj['move_draw_lbl'] +=1
#     if obj['move_draw_lbl']==obj['move_draw_lbls']: # Retour à 0
#         obj['move_draw_lbl']=0
#     # sleep(TEMPO_RENDER)

##
# Affichage des trajectoires
##

# DrawLine -> trop couteux en FPS (10 FPS vs 50 FPS) 
# def draw_raz():
#     scene.objects['System']['draw_points'].clear()
# def draw(cont):
#     obj = cont.owner
#     if len(obj['draw_points'])>2:
#            for i in range (len(obj['draw_points'])):
#                bge.render.drawLine([obj['draw_points'][i][0],obj['draw_points'][i][1],obj['draw_points'][i][2]],
#                                    [obj['draw_points'][i+1][0],obj['draw_points'][i+1][1],obj['draw_points'][i+1][2]],color_white)

# Afficher la trajectoire à la volée
def draw_move(cont):
    draw_point(scene.objects['Rp'].worldPosition,
               scene.objects['Commands']['move_draw_color'], None,
               thickness=scene.objects['Commands']['move_draw_thickness'])

# Recolorisation d'une trajectoire polyligne : dégradé du jaune vers le vert clair
def draw_recolor(i_pt_debut, i_pt_fin):
    # color_yellow          = (0.800, 0.619, 0.021, 1) # Jaune
    # color_green_light     = (0.246, 0.687, 0.078, 1) # Vert clair
    # color_green_lesslight = (0.149, 0.406, 0.049, 1) # Vert moins clair
    # color_green           = (0.083, 0.223, 0.028, 1) # Vert 
    nb_pt = i_pt_fin-i_pt_debut+1
    for i in range(nb_pt):
        delta_r = (abs(0.800-0.149)/nb_pt)*i
        delta_g = (abs(0.619-0.406)/nb_pt)*i
        delta_b = (abs(0.021-0.049)/nb_pt)*i
        scene.objects['p'+str(i_pt_debut+i)].color = (0.800-delta_r, 0.619-delta_g, 0.021+delta_b, 1)
        # print ('p'+str(i_pt_debut+i))
    # sleep(TEMPO_RENDER)

# Afficher la trajectoire pré_calculée
# Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'

def draw_traj(traj):
    obj = scene.objects['System']

    # Effacer les points
    draw_clear(True, False, False)

    # Afficher les repères
    for i in range (len(traj)):
        
        # Trop de poses
        if obj['move_draw_rep'] == obj['move_draw_reps']:
            scene.objects['Solver-text']['Text']="Ajout d'une trajectoire : nombre de poses maximum atteint."
            return

        # Repère
        if traj[i][3] is None:
            p = rtb_fkine(traj[i][1], unit="deg") # RTB est plus fiable
        else:
            p = traj[i][3]
        color = color_white

        # Données
        data = []
        data.append(traj[i][1]) # q
        data.append(traj[i][2]) # ot
        data.append(traj[i][4]) # prop
        data.append(traj[i][5]) # mode
        draw_axis(p, color, str(traj[i][0]), data)

##
# RAZ
##

# Effacer les points, repères et étiquette
def draw_clear(pt, rep, lbl):

    # Armature de calcul
    if scene.objects['Armature ikb'].visible:
        scene.objects['Armature ikb'].setVisible(False, True)
    if scene.objects['Rc ikb'].visible:
        scene.objects['Rc ikb'].setVisible(False, True)

    # Points
    obj = scene.objects['System']
    if pt:
        for i in range(obj['move_draw_pts']):
            p_obj=scene.objects['p'+str(i)]
            if p_obj.visible:
                p_obj.setVisible(False, False)
                p_obj['gv_visible']=False
        obj['move_draw_pt']=0

    # Repère
    if rep:
        for i in range (obj['move_draw_reps']):
            rep_obj=scene.objects['rep'+str(i)]
            if rep_obj.visible:
                rep_obj.setVisible(False, False)
                rep_obj['gv_visible']=False
                rep_obj['label'] = None
                rep_obj['q']     = None
                rep_obj['ot']    = None
                rep_obj['p']     = None
                rep_obj['prop']  = None
                rep_obj['mode']  = None
        obj['move_draw_rep']  = 0
        obj['move_draw_pose'] = 0

    # Etiquette
    if lbl:
        for i in range (obj['move_draw_lbls']):
            lbl_obj=scene.objects['lbl'+str(i)]
            if lbl_obj.visible:
                lbl_obj.setVisible(False, False)
                lbl_obj['gv_visible'] = False
        obj['move_draw_lbl'] = 0

    # Texte solveur
    txt="Trajectoire effacée : "
    if pt:
        if scene.objects['Commands']['clear_rep'] or scene.objects['Commands']['clear_lbl']:
            txt+= "points, "
        else:
            txt+= "points."
    if rep:
        if scene.objects['Commands']['clear_lbl']:
            txt+= "repères, "
        else:
            txt+= "repères."
    if lbl:
        txt+= "étiquettes."
    if scene.objects['System']['debug_render']:
        print (txt)
    scene.objects['Solver-text']['Text'] = txt
    sleep(TEMPO_RENDER)

###############################################################################
# Vues
###############################################################################

# Cacher et désactiver les commandes
def view_hide_cmds():
    scene.objects['Commands'].setVisible(False,True)
    scene.objects['Commands_system'].setVisible(False,True)
    scene.objects['Traj-panel'].setVisible(False,True)
    bt=['Doc', 'IO', 'Run', 'Stop', 'Traj', 'Geoview', 'About', 'File', 'ResetView']
    for i_bt in bt:
        scene.objects[i_bt+"-colbox"].suspendPhysics()

# Afficher et activer les commandes
def view_show_cmds():
    scene.objects['Commands'].setVisible(True,True)
    scene.objects['Commands_system'].setVisible(True,True)
    scene.objects['Run-Hl'].setVisible(False,True)
    scene.objects['Stop-Hl'].setVisible(False,True)
    scene.objects['IO-Hl'].setVisible(False,True)
    scene.objects['Traj-Hl'].setVisible(False,True)
    scene.objects['Geoview-Hl'].setVisible(False,True)
    scene.objects['Doc-Hl'].setVisible(False,True)
    scene.objects['ResetView-Hl'].setVisible(False,False)
    scene.objects['File-Hl'].setVisible(False,False)
    scene.objects['About-Hl'].setVisible(False,False)
    bt=['Doc', 'IO', 'Run', 'Stop', 'Traj', 'Geoview', 'About', 'File', 'ResetView']
    for i_bt in bt:
        scene.objects[i_bt+"-colbox"].restorePhysics()
    if scene.objects['System']['traj']:
        scene.objects['Traj-panel'].setVisible(True,True)

# Vue en perspective et vues 2D
def view(keyboard):
    obj = scene.objects['System']

    # Touche 0 -> Vue en perspective
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD0].queue:
        view_show_cmds()
        scene.active_camera = scene.objects["Camera"]
        scene.objects['View-text'].setVisible(False,True)
        scene.objects['R View-axe_x-face-txt'].setVisible(True,True)
        scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
        scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
        scene.objects['R View-axe_y-face-txt'].setVisible(True,True)
        scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
        scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
        scene.objects['R View-axe_z-face-txt'].setVisible(True,True)
        scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
        scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
        if 'Grid' in scene.objects:
            scene.objects['Grid-u-face'].setVisible(False,True)
            scene.objects['Grid-v-face'].setVisible(False,True)
            scene.objects['Grid-u-cote'].setVisible(False,True)
            scene.objects['Grid-v-cote'].setVisible(False,True)
            scene.objects['Grid-u-haut'].setVisible(False,True)
            scene.objects['Grid-v-haut'].setVisible(False,True)

    # Touche 1 -> Vue de face
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD1].queue:
        if obj['manip_mode']==2:
            if scene.active_camera == scene.objects["Camera arr"]:
                view_show_cmds()
                scene.active_camera = scene.objects["Camera"]
                scene.objects['View-text'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-face'].setVisible(False,True)
                    scene.objects['Grid-v-face'].setVisible(False,True)
            else:
                scene.objects['Camera arr'].applyRotation((0, 0, scene.objects['Camera arr'].localOrientation.to_euler().y), True)
                scene.objects['Camera arr-pt'].worldTransform=scene.objects['Camera arr-pt']['init']
                view_hide_cmds()
                scene.active_camera = scene.objects["Camera arr"]
                scene.objects['View-text'].setVisible(True,True)
                scene.objects['View-text']['Text']="Vue arrière"
                scene.objects['R View-axe_x-face-txt'].setVisible(True,True)
                scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-face-txt'].setVisible(True,True)
                scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-face-txt'].setVisible(True,True)
                scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    if scene.objects['Commands']['grid']:
                        scene.objects['Grid-u-face'].setVisible(True,True)
                        scene.objects['Grid-v-face'].setVisible(True,True)
                    scene.objects['Grid-u-cote'].setVisible(False,True)
                    scene.objects['Grid-v-cote'].setVisible(False,True)
                    scene.objects['Grid-u-haut'].setVisible(False,True)
                    scene.objects['Grid-v-haut'].setVisible(False,True)
        else:
            if scene.active_camera == scene.objects["Camera face"]:
                view_show_cmds()
                scene.active_camera = scene.objects["Camera"]
                scene.objects['View-text'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-face'].setVisible(False,True)
                    scene.objects['Grid-v-face'].setVisible(False,True)
            else:
                scene.objects['Camera face'].applyRotation((0, 0, scene.objects['Camera face'].localOrientation.to_euler().y), True)
                scene.objects['Camera face-pt'].worldTransform=scene.objects['Camera face-pt']['init']
                view_hide_cmds()
                scene.active_camera = scene.objects["Camera face"]
                scene.objects['View-text'].setVisible(True,True)
                scene.objects['View-text']['Text']="Vue de face"
                scene.objects['R View-axe_x-face-txt'].setVisible(True,True)
                scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-face-txt'].setVisible(True,True)
                scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-face-txt'].setVisible(True,True)
                scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    if scene.objects['Commands']['grid']:
                        scene.objects['Grid-u-face'].setVisible(True,True)
                        scene.objects['Grid-v-face'].setVisible(True,True)
                    scene.objects['Grid-u-cote'].setVisible(False,True)
                    scene.objects['Grid-v-cote'].setVisible(False,True)
                    scene.objects['Grid-u-haut'].setVisible(False,True)
                    scene.objects['Grid-v-haut'].setVisible(False,True)

    # Touche 3 -> Vue de coté
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD3].queue:
        if obj['manip_mode']==2:
            if scene.active_camera == scene.objects["Camera gauche"]:
                view_show_cmds()
                scene.active_camera = scene.objects["Camera"]
                scene.objects['View-text'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-cote'].setVisible(False,True)
                    scene.objects['Grid-v-cote'].setVisible(False,True)
            else:
                scene.objects['Camera gauche'].applyRotation((0, 0, scene.objects['Camera gauche'].localOrientation.to_euler().y), True)
                scene.objects['Camera gauche-pt'].worldTransform=scene.objects['Camera gauche-pt']['init']
                view_hide_cmds()
                scene.active_camera = scene.objects["Camera gauche"]
                scene.objects['View-text'].setVisible(True,True)
                scene.objects['View-text']['Text']="Vue de gauche"
                scene.objects['R View-axe_x-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-cote-txt'].setVisible(True,True)
                scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-cote-txt'].setVisible(True,True)
                scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-cote-txt'].setVisible(True,True)
                scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-face'].setVisible(False,True)
                    scene.objects['Grid-v-face'].setVisible(False,True)
                    if scene.objects['Commands']['grid']:
                        scene.objects['Grid-u-cote'].setVisible(True,True)
                        scene.objects['Grid-v-cote'].setVisible(True,True)
                    scene.objects['Grid-u-haut'].setVisible(False,True)
                    scene.objects['Grid-v-haut'].setVisible(False,True)
        else:
            if scene.active_camera == scene.objects["Camera droit"]:
                view_show_cmds()
                scene.active_camera = scene.objects["Camera"]
                scene.objects['View-text'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-cote'].setVisible(False,True)
                    scene.objects['Grid-v-cote'].setVisible(False,True)
            else:
                scene.objects['Camera droit'].applyRotation((0, 0, scene.objects['Camera droit'].localOrientation.to_euler().y), True)
                scene.objects['Camera droit-pt'].worldTransform=scene.objects['Camera droit-pt']['init']
                view_hide_cmds()
                scene.active_camera = scene.objects["Camera droit"]
                scene.objects['View-text'].setVisible(True,True)
                scene.objects['View-text']['Text']="Vue de droite"
                scene.objects['R View-axe_x-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-cote-txt'].setVisible(True,True)
                scene.objects['R View-axe_x-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-cote-txt'].setVisible(True,True)
                scene.objects['R View-axe_y-haut-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-cote-txt'].setVisible(True,True)
                scene.objects['R View-axe_z-haut-txt'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-face'].setVisible(False,True)
                    scene.objects['Grid-v-face'].setVisible(False,True)
                    if scene.objects['Commands']['grid']:
                        scene.objects['Grid-u-cote'].setVisible(True,True)
                        scene.objects['Grid-v-cote'].setVisible(True,True)
                    scene.objects['Grid-u-haut'].setVisible(False,True)
                    scene.objects['Grid-v-haut'].setVisible(False,True)

    # Touche 7 -> Vue de haut/bas
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PAD7].queue:
        if obj['manip_mode']==2:
            if scene.active_camera == scene.objects["Camera bas"]:
                view_show_cmds()
                scene.active_camera = scene.objects["Camera"]
                scene.objects['View-text'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-haut'].setVisible(False,True)
                    scene.objects['Grid-v-haut'].setVisible(False,True)
            else:
                scene.objects['Camera bas'].applyRotation((0, 0, -scene.objects['Camera haut'].localOrientation.to_euler().z), True)
                scene.objects['Camera bas-pt'].worldTransform=scene.objects['Camera haut-pt']['init']
                view_hide_cmds()
                scene.active_camera = scene.objects["Camera bas"]
                scene.objects['View-text'].setVisible(True,True)
                scene.objects['View-text']['Text']="Vue de dessous"
                scene.objects['R View-axe_x-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-haut-txt'].setVisible(True,True)
                scene.objects['R View-axe_y-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-haut-txt'].setVisible(True,True)
                scene.objects['R View-axe_z-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-haut-txt'].setVisible(True,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-face'].setVisible(False,True)
                    scene.objects['Grid-v-face'].setVisible(False,True)
                    scene.objects['Grid-u-cote'].setVisible(False,True)
                    scene.objects['Grid-v-cote'].setVisible(False,True)
                    if scene.objects['Commands']['grid']:
                        scene.objects['Grid-u-haut'].setVisible(True,True)
                        scene.objects['Grid-v-haut'].setVisible(True,True)
        else:            
            if scene.active_camera == scene.objects["Camera haut"]:
                view_show_cmds()
                scene.active_camera = scene.objects["Camera"]
                scene.objects['View-text'].setVisible(False,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-haut'].setVisible(False,True)
                    scene.objects['Grid-v-haut'].setVisible(False,True)
            else:
                scene.objects['Camera haut'].applyRotation((0, 0, -scene.objects['Camera haut'].localOrientation.to_euler().z), True)
                scene.objects['Camera haut-pt'].worldTransform=scene.objects['Camera haut-pt']['init']
                view_hide_cmds()
                scene.active_camera = scene.objects["Camera haut"]
                scene.objects['View-text'].setVisible(True,True)
                scene.objects['View-text']['Text']="Vue de dessus"
                scene.objects['R View-axe_x-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_x-haut-txt'].setVisible(True,True)
                scene.objects['R View-axe_y-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_y-haut-txt'].setVisible(True,True)
                scene.objects['R View-axe_z-face-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-cote-txt'].setVisible(False,True)
                scene.objects['R View-axe_z-haut-txt'].setVisible(True,True)
                if 'Grid' in scene.objects:
                    scene.objects['Grid-u-face'].setVisible(False,True)
                    scene.objects['Grid-v-face'].setVisible(False,True)
                    scene.objects['Grid-u-cote'].setVisible(False,True)
                    scene.objects['Grid-v-cote'].setVisible(False,True)
                    if scene.objects['Commands']['grid']:
                        scene.objects['Grid-u-haut'].setVisible(True,True)
                        scene.objects['Grid-v-haut'].setVisible(True,True)

    # Touche G -> Afficher/cacher grille
    if JUST_ACTIVATED in keyboard.inputs[bge.events.GKEY].queue and 'Grid' in scene.objects:
        if scene.active_camera == scene.objects["Camera face"] or scene.active_camera == scene.objects["Camera arr"]:
            if scene.objects['Commands']['grid']:
                scene.objects['Commands']['grid'] = False
                scene.objects['Grid-u-face'].setVisible(False,True)
                scene.objects['Grid-v-face'].setVisible(False,True)
            else:
                scene.objects['Commands']['grid'] = True
                scene.objects['Grid-u-face'].setVisible(True,True)
                scene.objects['Grid-v-face'].setVisible(True,True)
        if scene.active_camera == scene.objects["Camera gauche"] or scene.active_camera == scene.objects["Camera droit"]:
            if scene.objects['Commands']['grid']:
                scene.objects['Commands']['grid'] = False
                scene.objects['Grid-u-cote'].setVisible(False,True)
                scene.objects['Grid-v-cote'].setVisible(False,True)
            else:
                scene.objects['Commands']['grid'] = True
                scene.objects['Grid-u-cote'].setVisible(True,True)
                scene.objects['Grid-v-cote'].setVisible(True,True)
        if scene.active_camera == scene.objects["Camera bas"] or scene.active_camera == scene.objects["Camera haut"]:
            if scene.objects['Commands']['grid']:
                scene.objects['Commands']['grid'] = False
                scene.objects['Grid-u-haut'].setVisible(False,True)
                scene.objects['Grid-v-haut'].setVisible(False,True)
            else:
                scene.objects['Commands']['grid'] = True
                scene.objects['Grid-u-haut'].setVisible(True,True)
                scene.objects['Grid-v-haut'].setVisible(True,True)

    # Synchro des grilles pour Geoview
    if 'Grid' in scene.objects:
        scene.objects['Grid-u-face']['gv_visible'] = scene.objects['Grid-u-face'].visible
        scene.objects['Grid-v-face']['gv_visible'] = scene.objects['Grid-v-face'].visible
        scene.objects['Grid-u-cote']['gv_visible'] = scene.objects['Grid-u-cote'].visible
        scene.objects['Grid-v-cote']['gv_visible'] = scene.objects['Grid-v-cote'].visible
        scene.objects['Grid-u-haut']['gv_visible'] = scene.objects['Grid-u-haut'].visible
        scene.objects['Grid-v-haut']['gv_visible'] = scene.objects['Grid-v-haut'].visible


###############################################################################
# Système
###############################################################################

##
# Réinitialisation du système
##

def system_reset():
    obj = scene.objects['System']

    # Modes
    obj['move_draw']=False

    # Mise en place de la variante
    variant_prev = obj['variant']
    runpy.run_path(scene.objects['Commands']['script'], run_name='init')
    
    # FIXME : implémentation du panneau IO à faire
    # if variant_prev != obj['variant']:
        # twin.twin_io.init(public_vars)
        # if obj['io']:
        #     twin.twin_io.open_panel()
        #     twin.twin_io.open_panel()

    # # # Moteurs à l'état initial
    # # for i_txt in motors:
    # #     obj = scene.objects[i_txt]
    # #     if len(obj['pulley'])==1:
    # #         scene.objects[obj['pulley'][0]].applyRotation((0, 0, -obj['alpha']), True)
    # #     elif len(obj['pulley'])==2:
    # #         scene.objects[obj['pulley'][0]].applyRotation((0, 0, -obj['alpha']), True)
    # #         scene.objects[obj['pulley'][1]].applyRotation((0, 0, -obj['alpha']), True)
    # #     scene.objects[obj['bone']].applyRotation((0, 0, -obj['alpha'] / obj['r']), True)
    # #     obj['q'] = 0
    # #     obj['q_target'] = 0
    # #     obj['alpha']=0
    # #     obj['speed']=0
    # #     obj['alpha_step']=0

    # # # Pince à l'état initial
    # # obj = scene.objects['Moteur Pince']
    # # obj['value'] = 33 # Ouverture de départ
    # # obj['value_target'] = 33
    # # obj['activated'] = False
    # # alpha = (obj['value_target']-obj['value'])*obj['stroke_step']
    # # scene.objects['Plateau moteur Pince'].applyRotation((0, 0, -alpha), True)
    # # scene.objects['Bielle 1'].applyRotation((0, 0, alpha), True)
    # # scene.objects['Biellette 1'].applyRotation((0, 0, alpha), True)
    # # scene.objects['Bielle 2'].applyRotation((0, 0, -alpha), True)
    # # scene.objects['Biellette 2'].applyRotation((0, 0, -alpha), True)
    # # scene.objects['Doigt 1'].applyRotation((0, 0, -alpha), True)
    # # scene.objects['Doigt 2'].applyRotation((0, 0, alpha), True)
