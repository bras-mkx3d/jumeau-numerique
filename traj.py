# -*- coding: utf-8 -*- 
from bras_lib import * # Bibliothèque utilisateur du bras MKX3D

# traj.py : Trajectoire générée


def commandes():
    draw_clear()
    tempo(1)

    # Trajectoire : 'label',[q1,q2,q3,q4,q5,q6],ot,[x,y,z,rx,ry,rz],'prop','mode'

    traj=[]

    traj.append(['1', [0.0, 21.0, 0.0, 0.0, 0.0, 0.0], 33, [-0.0, 0.23384, 0.82993, -21.0, 0.0, 0.0], '', 'manual_q'])
    traj.append(['2', [45.0, 21.0, 0.0, 0.0, 0.0, 0.0], 33, [-0.16535, 0.16535, 0.82993, -21.0, 0.0, 45.0], '', 'manual_q'])
    traj.append(['3', [95.42, 21.0, 0.0, 0.0, 0.0, 0.0], 33, [-0.2328, -0.02209, 0.82993, -21.0, 0.0, 95.42], '', ''])
    traj.append(['4', [120.0, 21.0, 0.0, 0.0, 0.0, 0.0], 33, [-0.20251, -0.11692, 0.82993, -21.0, 0.0, 120.0], '', 'manual_q'])

    add_pose(str(traj[0][0]), traj[0][1], traj[0][2])
    add_traj(traj[1:])
    move_q(traj[0][1], ot=traj[0][2])
    move_traj(draw=True)


def cycle():
    commandes()
    fin()

if __name__=='start':
    start(cycle)
if __name__=='stop':
    stop()
if __name__=='init':
    variant(1) # Variante Pince rigide
